import java.awt.*;
import java.awt.image.BufferedImage;

final class FunCustomCursor {

	private Component component;
	private Robot robot;


	final void mouseMove(int var1, int var2) {
		robot.mouseMove(var1, var2);
	}

	final void setCursor(Component component, boolean var2) {
		if (!var2) {
			if (null == component) {
				throw new NullPointerException();
			}
		} else {
			component = null;
		}
		if (component != this.component) {
			if (null != this.component) {
				this.component.setCursor(null);
				this.component = null;
			}

			if (component != null) {
				component.setCursor(component.getToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), null));
				this.component = component;
			}

		}
	}

	final void setCustomCursor(Component component, int[] pixels, int width, int height, Point hotSpot) {
		if (null != pixels) {
			BufferedImage image = new BufferedImage(width, height, 2);
			image.setRGB(0, 0, width, height, pixels, 0, width);
			component.setCursor(component.getToolkit().createCustomCursor(image, hotSpot, null));
		} else {
			component.setCursor(null);
		}
	}

	FunCustomCursor() throws Exception {
		robot = new Robot();
	}
}
