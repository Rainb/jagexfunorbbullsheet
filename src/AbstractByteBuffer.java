abstract class AbstractByteBuffer {

	static String a = "ESC - cancel this line";
	static dd b = new dd(7, 0, 1, 1);
	static ph c;
	static ph d;
	static nf e;
	static int[][] f;
	static char[] g = new char[] { '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0',
			'1', '2', '3', '4', '5', '6', '7', '8', '9' };

	static int j = 0;
	static int h = j;
	static boolean i;
	static ph k;


	public static void a(int var0) {
		try {
			d = null;
			b = null;
			if (var0 <= 12) {
				i = true;
			}

			f = null;
			c = null;
			g = null;
			a = null;
			e = null;
			k = null;
		} catch (RuntimeException var2) {
			throw pl.a(var2, "AbstractByteBuffer.C(" + var0 + ')');
		}
	}

	abstract byte[] get(int var1);

	static void a(int var0, boolean var1, int var2, boolean var3, int var4, byte var5, boolean var6, int var7, boolean var8, int var9, int var10, boolean var11) {
		try {
			FunCB.a(var11, 0, var7, var9, var1, var2, var0, var8, 16777215, var6, 16777215, var3, var10, var4);
			if (var5 != 57) {
				g = null;
			}
		} catch (RuntimeException var13) {
			throw pl.a(var13, "AbstractByteBuffer.B(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ',' + var9 + ',' + var10 + ',' + var11 + ')');
		}
	}

	static void a(int var0, int var1, int var2, int var3, int var4) {
		int var13 = client.L;

		try {
			int var5 = var1 + var2;
			int var6 = var4 + var3;
			int var7 = ~cd.l <= ~var2 ? cd.l : var2;
			int var8 = ~var3 >= ~cd.g ? cd.g : var3;
			int var9 = var5 < cd.j ? var5 : cd.j;
			int var10 = cd.e <= var6 ? cd.e : var6;
			int var11;
			int var12;
			if (cd.l <= var2 && ~cd.j < ~var2) {
				label98:
				{
					var11 = var2 + var8 * cd.width;
					var12 = 1 + var10 - var8 >> 1;
					if (var13 == 0) {
						--var12;
						if (var12 < 0) {
							break label98;
						}
					}

					do {
						cd.pixels[var11] = 16777215;
						var11 += cd.width * 2;
						--var12;
					} while (var12 >= 0);
				}
			}

			if (var0 >= -4) {
				h = -11;
			}

			if (var3 >= cd.g && var6 < cd.e) {
				var11 = cd.width * var3 - -var7;
				var12 = var9 + 1 + -var7 >> 1;
				if (var13 != 0) {
					cd.pixels[var11] = 16777215;
					var11 += 2;
				}

				while (true) {
					--var12;
					if (~var12 > -1) {
						break;
					}

					cd.pixels[var11] = 16777215;
					var11 += 2;
				}
			}

			if (~cd.l >= ~var5 && ~var5 > ~cd.j) {
				label74:
				{
					var11 = cd.width * (var8 + (-var2 + var5 & 1)) + var5;
					var12 = -var8 + 1 - -var10 >> 1;
					if (var13 == 0) {
						--var12;
						if (var12 < 0) {
							break label74;
						}
					}

					do {
						cd.pixels[var11] = 16777215;
						var11 += 2 * cd.width;
						--var12;
					} while (var12 >= 0);
				}
			}

			if (~var3 <= ~cd.g && cd.e > var6) {
				var11 = var6 * cd.width + var7 + (1 & var6 + -var3);
				var12 = -var7 + var9 + 1 >> 1;
				if (var13 != 0) {
					cd.pixels[var11] = 16777215;
					var11 += 2;
				}

				while (true) {
					--var12;
					if (~var12 > -1) {
						return;
					}

					cd.pixels[var11] = 16777215;
					var11 += 2;
				}
			}
		} catch (RuntimeException var14) {
			throw pl.a(var14, "AbstractByteBuffer.F(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
		}
	}

	abstract void set(int var1, byte[] var2);

	static String[] a(char c, byte var1, String string) {
		int var9 = client.L;

		try {
			if (var1 != -110) {
				a('$', (byte) -16, null);
			}

			int count = bn.getCharCount(c, string, (byte) -101);
			String[] split = new String[1 + count];
			int i = 0;
			int start = 0;
			int var7 = 0;
			if (var9 == 0 && count <= var7) {
				split[count] = string.substring(start);
				return split;
			} else {
				do {
					int end = start;
					if (var9 != 0) {
						end = start + 1;
					}

					while (~c != ~string.charAt(end)) {
						++end;
					}

					split[i++] = string.substring(start, end);
					start = end - -1;
					++var7;
				} while (count > var7);

				split[count] = string.substring(start);
				return split;
			}
		} catch (RuntimeException var10) {
			throw pl.a(var10, "AbstractByteBuffer.D(" + c + ',' + var1 + ',' + (string != null ? "{...}" : "null") + ')');
		}
	}

}
