import java.awt.*;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

final class RandomAccessWrapper {

	static String b = "Friends";

	static String[] e = new String[3];
	static String f = "Special Item Bonus: ";
	static FunImage[] instructionKeys;
	static String stringQuit = "Quit";
	static int k = -1;
	static int[] g;

	private File file;
	private RandomAccessFile raf;
	private long size;
	private long h;


	final void write(byte[] buf, int offset, int len, int var4) throws IOException {
		try {
			if (~(size + (long) len) < ~h) {
				raf.seek(h);
				raf.write(1);
				throw new EOFException();
			} else {
				if (var4 != -38915644) {
					dispose(39);
				}

				raf.write(buf, offset, len);
				size += (long) len;
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "RandomAccessWrapper.write(" + (buf != null ? "{...}" : "null") + ',' + offset + ',' + len + ',' + var4 + ')');
		}
	}

	final void close(int var1) throws IOException {
		try {
			if (var1 == -1276) {
				if (raf != null) {
					raf.close();
					raf = null;
				}
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "RandomAccessWrapper.G(" + var1 + ')');
		}
	}

	static void a(Component canvas, int var1) {
		try {
			canvas.setFocusTraversalKeysEnabled(false);
			canvas.addKeyListener(rh.keyFocusListener);
			if (var1 != 0) {
				k = 27;
			}
			canvas.addFocusListener(rh.keyFocusListener);
		} catch (RuntimeException var4) {
			throw pl.a(var4, "RandomAccessWrapper.H(" + (canvas != null ? "{...}" : "null") + ',' + var1 + ')');
		}
	}

	final int b(byte[] buf, int off, int len, int var4) throws IOException {
		try {
			int r = raf.read(buf, off, len);
			if (var4 != -21241) {
				k = 55;
			}

			if (~r < -1) {
				size += (long) r;
			}

			return r;
		} catch (RuntimeException var6) {
			throw pl.a(var6, "RandomAccessWrapper.D(" + (buf != null ? "{...}" : "null") + ',' + off + ',' + len + ',' + var4 + ')');
		}
	}

	static void b(int var0) {
		int var2 = client.L;

		try {
			if (var0 == 3) {
				label51:
				{
					if (ig.b < 0) {
						qg.f -= ig.b >> 1;
						if (var2 == 0) {
							break label51;
						}
					}

					qg.f += -ig.b >> 1;
				}

				label46:
				{
					ig.b += qg.f;
					if (~ig.b > -1) {
						ig.b -= ig.b >> 4;
						if (var2 == 0) {
							break label46;
						}
					}

					ig.b += -ig.b >> 4;
				}

				label41:
				{
					if (ki.i >= 0) {
						ok.Tb += -ki.i >> 1;
						if (var2 == 0) {
							break label41;
						}
					}

					ok.Tb -= ki.i >> 1;
				}

				label36:
				{
					ki.i += ok.Tb;
					if (~ki.i > -1) {
						ki.i -= ki.i >> 4;
						if (var2 == 0) {
							break label36;
						}
					}

					ki.i += -ki.i >> 4;
				}

				if (~ki.i < -1) {
					if (~ok.Tb < -1) {
						ok.Tb = -ok.Tb;
					}

					ki.i = 0;
				}
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "RandomAccessWrapper.I(" + var0 + ')');
		}
	}

	final void seek(long pos, byte var3) throws IOException {
		try {
			raf.seek(pos);
			size = pos;
		} catch (RuntimeException var5) {
			throw pl.a(var5, "RandomAccessWrapper.E(" + pos + ',' + var3 + ')');
		}
	}

	final long getLength(byte var1) throws IOException {
		try {
			return raf.length();
		} catch (RuntimeException var3) {
			throw pl.a(var3, "RandomAccessWrapper.getLength(" + var1 + ')');
		}
	}

	public static void dispose(int var0) {
		try {
			e = null;
			stringQuit = null;
			f = null;
			if (var0 != -1) {
				b(71);
			}

			g = null;
			b = null;
			instructionKeys = null;
		} catch (RuntimeException var2) {
			throw pl.a(var2, "RandomAccessWrapper.A(" + var0 + ')');
		}
	}

	protected final void finalize() throws Throwable {
		try {
			if (raf != null) {
				System.out.println("Warning! fileondisk " + file + " not closed correctly using close(). Auto-closing instead. ");
				close(-1276);
			}
		} catch (RuntimeException var2) {
			throw pl.a(var2, "RandomAccessWrapper.finalize(" + ')');
		}
	}

	final File getFile(boolean var1) {
		try {
			if (!var1) {
				b = null;
			}

			return file;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "RandomAccessWrapper.C(" + var1 + ')');
		}
	}

	RandomAccessWrapper(File file, String access, long len) throws IOException {
		try {
			if (~len == 0L) {
				len = Long.MAX_VALUE;
			}

			if (~file.length() < ~len) {
				file.delete();
			}

			raf = new RandomAccessFile(file, access);
			size = 0L;
			h = len;
			this.file = file;
			int b = raf.read();
			if (~b != 0 && !access.equals("r")) {
				raf.seek(0L);
				raf.write(b);
			}

			raf.seek(0L);
		} catch (RuntimeException var6) {
			throw pl.a(var6, "RandomAccessWrapper.<init>(" + (file != null ? "{...}" : "null") + ',' + (access != null ? "{...}" : "null") + ',' + len + ')');
		}
	}

}
