
abstract class gb extends FunNode {

   int f;
   gb g;
   v h;
   volatile boolean i = true;


   int a() {
      return 255;
   }

   abstract void a(int[] var1, int var2, int var3);

   final void b(int[] var1, int var2, int var3) {
      if(i) {
		  a(var1, var2, var3);
      } else {
		  a(var3);
      }
   }

   abstract gb b();

   abstract gb c();

   abstract void a(int var1);

   abstract int d();

}
