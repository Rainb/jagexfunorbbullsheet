import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.Proxy.Type;
import java.nio.charset.Charset;
import java.util.List;

public final class ff extends fk {

   private ProxySelector m;


   final Socket getSocket(byte var1) throws IOException {
      try {
         boolean var2 = Boolean.parseBoolean(System.getProperty("java.net.useSystemProxies"));
         if(!var2) {
            System.setProperty("java.net.useSystemProxies", "true");
         }

         boolean var5 = -444 == ~this.port;

         List var3;
         List var4;
         try {
            var3 = this.m.select(new URI((!var5?"http":"https") + "://" + this.host));
            var4 = this.m.select(new URI((!var5?"https":"http") + "://" + this.host));
         } catch (URISyntaxException var15) {
            return this.createSocket(8192);
         }

         var3.addAll(var4);
         Object[] var6 = var3.toArray();
         FunIOException var7 = null;
         if(var1 > -32) {
            return (Socket)null;
         } else {
            Object[] var8 = var6;

            for(int var9 = 0; var8.length > var9; ++var9) {
               Object var10 = var8[var9];
               Proxy var11 = (Proxy)var10;

               try {
                  Socket var12 = this.a((byte)127, var11);
                  if(var12 != null) {
                     return var12;
                  }
               } catch (FunIOException var13) {
                  var7 = var13;
               } catch (IOException var14) {
                  ;
               }
            }

            if(var7 != null) {
               throw var7;
            } else {
               return this.createSocket(8192);
            }
         }
      } catch (RuntimeException var16) {
         throw var16;
      }
   }

   private final Socket a(byte var1, Proxy var2) throws IOException {
      try {
         int var3 = -8 / ((var1 - 61) / 48);
         if(var2.type() == Type.DIRECT) {
            return this.createSocket(8192);
         } else {
            SocketAddress var4 = var2.address();
            if(var4 instanceof InetSocketAddress) {
               InetSocketAddress var5 = (InetSocketAddress)var4;
               if(var2.type() != Type.HTTP) {
                  if(var2.type() != Type.SOCKS) {
                     return null;
                  } else {
                     Socket var17 = new Socket(var2);
                     var17.connect(new InetSocketAddress(this.host, this.port));
                     return var17;
                  }
               } else {
                  String var6 = null;

                  try {
                     Class var7 = Class.forName("sun.net.www.protocol.http.AuthenticationInfo");
                     Method var8 = var7.getDeclaredMethod("getProxyAuth", new Class[]{String.class, Integer.TYPE});
                     var8.setAccessible(true);
                     Object var9 = var8.invoke((Object)null, new Object[]{var5.getHostName(), new Integer(var5.getPort())});
                     if(var9 != null) {
                        Method var10 = var7.getDeclaredMethod("supportsPreemptiveAuthorization", new Class[0]);
                        var10.setAccessible(true);
                        if(((Boolean)var10.invoke(var9, new Object[0])).booleanValue()) {
                           Method var11 = var7.getDeclaredMethod("getHeaderName", new Class[0]);
                           var11.setAccessible(true);
                           Method var12 = var7.getDeclaredMethod("getHeaderValue", new Class[]{URL.class, String.class});
                           var12.setAccessible(true);
                           String var13 = (String)var11.invoke(var9, new Object[0]);
                           String var14 = (String)var12.invoke(var9, new Object[]{new URL("https://" + this.host + "/"), "https"});
                           var6 = var13 + ": " + var14;
                        }
                     }
                  } catch (Exception var15) {
                     ;
                  }

                  return this.httpProxyConnect(var5.getHostName(), var5.getPort(), var6);
               }
            } else {
               return null;
            }
         }
      } catch (RuntimeException var16) {
         throw var16;
      }
   }

   private final Socket httpProxyConnect(String var1, int var2, String var3) throws IOException {
      try {
         Socket var4 = new Socket(var1, var2);
         var4.setSoTimeout(10000);
         OutputStream var5 = var4.getOutputStream();
         if(null != var3) {
            var5.write(("CONNECT " + this.host + ":" + this.port + " HTTP/1.0\n" + var3 + "\n\n").getBytes(Charset.forName("ISO-8859-1")));
         } else {
            var5.write(("CONNECT " + this.host + ":" + this.port + " HTTP/1.0\n\n").getBytes(Charset.forName("ISO-8859-1")));
         }

         var5.flush();
         BufferedReader var6 = new BufferedReader(new InputStreamReader(var4.getInputStream()));
         String var7 = var6.readLine();
         if(null != var7) {
            if(var7.startsWith("HTTP/1.0 200") || var7.startsWith("HTTP/1.1 200")) {
               return var4;
            }

            if(var7.startsWith("HTTP/1.0 407") || var7.startsWith("HTTP/1.1 407")) {
               int var8 = 0;
               String var9 = "proxy-authenticate: ";

               for(var7 = var6.readLine(); null != var7 && var8 < 50; var7 = var6.readLine()) {
                  if(var7.toLowerCase().startsWith(var9)) {
                     var7 = var7.substring(var9.length()).trim();
                     int var10 = var7.indexOf(32);
                     if(-1 != var10) {
                        var7 = var7.substring(0, var10);
                     }

                     throw new FunIOException(var7);
                  }

                  ++var8;
               }

               throw new FunIOException("");
            }
         }

         var5.close();
         var6.close();
         var4.close();
         return null;
      } catch (RuntimeException var11) {
         throw var11;
      }
   }

   public ff() {
      try {
         this.m = ProxySelector.getDefault();
      } catch (RuntimeException var2) {
         throw var2;
      }
   }
}
