import com.ms.awt.WComponentPeer;
import com.ms.dll.Callback;
import com.ms.dll.Root;
import com.ms.win32.User32;

import java.awt.*;

final class sd extends Callback {

	private boolean a;
	private volatile boolean b = true;
	private volatile int c;
	private int cursor;
	private volatile int topHwnd;


	final void setCursorLocation(int var1, int x, byte y) {
		User32.SetCursorPos(x, var1);
		if (y >= -65) {
			setCursorLocation(35, 90, (byte) 32);
		}
	}

	final synchronized int callback(int var1, int var2, int var3, int var4) {
		int var5;
		if (~var1 != ~topHwnd) {
			var5 = User32.GetWindowLong(var1, -4);
			return User32.CallWindowProc(var5, var1, var2, var3, var4);
		} else {
			if (~var2 == -33) {
				var5 = var4;
				if (~var5 == -2) {
					User32.SetCursor(!b ? 0 : cursor);
					return 0;
				}
			}

			if (var2 == 101024) {
				User32.SetCursor(!b ? 0 : cursor);
				return 0;
			} else {
				if (~var2 == -2) {
					topHwnd = 0;
					b = true;
				}

				return User32.CallWindowProc(c, var1, var2, var3, var4);
			}
		}
	}

	final void a(byte var1, Component component, boolean var3) {
		if (var1 <= 31) {
			a((byte) 52, null, false);
		}
		WComponentPeer peer = (WComponentPeer) component.getPeer();
		int topHwnd = peer.getTopHwnd();
		if (this.topHwnd != topHwnd || b != var3) {
			if (!a) {
				cursor = User32.LoadCursor(0, 32512);
				Root.alloc(this);
				a = true;
			}

			if (this.topHwnd != topHwnd) {
				if (~this.topHwnd != -1) {
					b = true;
					User32.SendMessage(topHwnd, 101024, 0, 0);

					User32.SetWindowLong(this.topHwnd, -4, c);
				}

				this.topHwnd = topHwnd;
				c = User32.SetWindowLong(this.topHwnd, -4, this);
			}

			b = var3;
			User32.SendMessage(topHwnd, 101024, 0, 0);
		}
	}

}
