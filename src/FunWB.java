import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;

class FunWB extends pj {

   static String H = "Auto-respond to <%0>";
   private int I;
   private long J;
   private long K = 0L;
   private int L = -1;
   private boolean M = false;
   static String N;
   private int O;
   static sn P;
   static ph Q;
   private int R;
   private boolean S;


   FunWB(String var1, aa var2, int var3) {
      super(var1, var2);

      try {
         this.R = var3;
         super.renderer = vc.a.e;
         this.a(true, var1, (byte)-114);
         this.S = true;
         this.J = FunAB.a((byte) 104);
      } catch (RuntimeException var5) {
         throw pl.a(var5, "wb.<init>(" + (var1 != null?"{...}":"null") + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

   private final int h(int var1) {
      try {
         int var2 = super.text.length();
         if(var2 == this.O) {
            return this.O;
         } else {
            int var3 = this.O - -1;
            if(var1 >= -67) {
               return 2;
            } else {
               while(~var2 < ~var3 && ~super.text.charAt(var3 + -1) != -33) {
                  ++var3;
               }

               return var3;
            }
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.AB(" + var1 + ')');
      }
   }

   private final void a(boolean var1) {
      try {
         if(!this.S) {
            super.j = 0;
            super.o = 0;
         } else if(super.renderer instanceof ni) {
            ni var2 = (ni)super.renderer;
            ie var3 = var2.a(this, (int)123);
            int var4 = var3.a((byte)110);
            int var5 = var2.a(this, (byte)92);
            int var6 = var2.a(-28305) >> 217180289;
            if(var1) {
               c((byte)-82);
            }

            if(var4 < -var6 + var5) {
               super.o = 0;
               super.j = 0;
            } else {
               label34: {
                  int var7 = super.o + var3.a(this.O, 1);
                  if(~var7 >= ~(-var6 + var5)) {
                     if(~var6 >= ~var7) {
                        break label34;
                     }

                     super.o = super.o + var6 + -var7;
                     if(client.L == 0) {
                        break label34;
                     }
                  }

                  super.o += -var7 + (var5 - var6);
               }

               if(~super.o >= -1) {
                  if(super.o < -var5 - -var6) {
                     super.o = var6 + -var5;
                     return;
                  }
               } else {
                  super.o = 0;
               }

            }
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "wb.UA(" + var1 + ')');
      }
   }

   final boolean a(bn var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      try {
         if(super.a(var1, var2, var3, var4, var5, var6, var7) && super.renderer instanceof ni) {
            int var8 = ((ni)super.renderer).a(this, jn.a, pc.Hb, var5, -8761, var3);
            this.a(~var8 != 0?var8:0, var6 ^ -22433);
            long var9 = FunAB.a((byte) -127);
            this.M = var9 + -this.K < 250L;
            if(this.M) {
               this.I = this.m(0);
               this.O = this.h(-109);
               if(~this.O < -1 && ~super.text.charAt(-1 + this.O) == -33) {
                  --this.O;
               }

               this.L = this.O;
            }

            this.K = var9;
            return true;
         } else {
            return false;
         }
      } catch (RuntimeException var11) {
         throw pl.a(var11, "wb.MA(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
      }
   }

   private final String i(int var1) {
      try {
         int var2 = 41 % ((-4 - var1) / 48);
         int var3 = ~this.O >= ~this.I?this.O:this.I;
         int var4 = this.I < this.O?this.O:this.I;
         return super.text.substring(var3, var4);
      } catch (RuntimeException var5) {
         throw pl.a(var5, "wb.N(" + var1 + ')');
      }
   }

   void a(bn var1, int var2, int var3, int var4) {
      try {
         super.a(var1, var2, -4, var4);
         int var5 = 71 / ((var3 - 40) / 38);
         this.a(false);
         if(~super.r == -2) {
            if(super.renderer instanceof ni) {
               ni var6 = (ni)super.renderer;
               int var7 = var6.a(this, jn.a, pc.Hb, var4, -8761, var2);
               if(~var7 != 0) {
                  if(this.M && ~this.L < ~var7 && ~this.I > ~var7) {
                     var7 = this.L;
                  }

                  this.O = var7;
               }
            }

            this.J = FunAB.a((byte) -128);
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "wb.D(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   final void a(boolean var1, String var2, byte var3) {
      try {
         if(var3 >= -105) {
            N = null;
         }

         if(var2 == null) {
            var2 = "";
         }

         super.text = var2;
         int var4 = var2.length();
         if(this.R != -1 && this.R < var4) {
            super.text = super.text.substring(0, this.R);
         }

         this.O = this.I = super.text.length();
         if(!var1) {
            this.j(8);
         }
      } catch (RuntimeException var5) {
         throw pl.a(var5, "wb.I(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

   void j(int var1) {
      try {
         if(var1 != 8) {
            this.R = 31;
         }

         if(super.listener instanceof ag) {
            ((ag)super.listener).a(this, 83);
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "wb.F(" + var1 + ')');
      }
   }

   private final void a(int var1, String var2) {
      try {
         if(this.R != -1) {
            int var3 = this.R + -super.text.length();
            if(var3 >= 0) {
               return;
            }

            var2 = var2.substring(0, var3);
         }

         label33: {
            if(~this.O != ~super.text.length()) {
               super.text = super.text.substring(0, this.O) + var2 + super.text.substring(this.O, super.text.length());
               if(client.L == 0) {
                  break label33;
               }
            }

            super.text = super.text + var2;
         }

         this.O += var2.length();
         this.I = this.O;
         this.j(var1 ^ 8);
         if(var1 != 0) {
            this.a((bn)null, -26, 107, -117);
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.WA(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   private final void k(int var1) {
      try {
         if(var1 >= 91) {
            if(super.listener instanceof ag) {
               ((ag)super.listener).a(false, this);
            }
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "wb.Q(" + var1 + ')');
      }
   }

   private final void l(int var1) {
      try {
         String var2 = this.i(-104);
         int var3 = -105 % ((-20 - var1) / 46);
         if(var2.length() > 0) {
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(this.i(47)), (ClipboardOwner)null);
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.O(" + var1 + ')');
      }
   }

   final boolean a(int var1, byte var2, bn var3, char var4) {
      try {
         this.J = FunAB.a((byte) 101);
         if(~var4 != -61 && ~var4 != -63) {
            if(var4 >= 32 && ~var4 >= -127) {
               if(this.I != this.O) {
                  this.p(0);
               }

               if(this.R == -1 || ~super.text.length() > ~this.R) {
                  label109: {
                     if(this.O >= super.text.length()) {
                        super.text = super.text + var4;
                        this.I = this.O = super.text.length();
                        if(client.L == 0) {
                           break label109;
                        }
                     }

                     super.text = super.text.substring(0, this.O) + var4 + super.text.substring(this.O, super.text.length());
                     ++this.O;
                     this.I = this.O;
                  }

                  this.j(8);
               }

               return true;
            } else {
               if(var1 == 85) {
                  if(~this.I != ~this.O) {
                     this.p(0);
                     return true;
                  }

                  if(this.O > 0) {
                     this.I = this.O + -1;
                     this.p(0);
                     return true;
                  }
               } else if(var1 != 101) {
                  if(~var1 == -14) {
                     this.o(108);
                     return true;
                  }

                  if(var1 == 96) {
                     if(this.O > 0) {
                        this.a(!ed.e[82]?-1 + this.O:this.m(0), 5988);
                        return true;
                     }
                  } else if(~var1 != -98) {
                     if(var1 == 102) {
                        this.a(0, 5988);
                        return true;
                     }

                     if(var1 == 103) {
                        this.a(super.text.length(), 5988);
                        return true;
                     }

                     if(~var1 == -85) {
                        this.k(105);
                        return true;
                     }

                     if(ed.e[82] && var1 == 65) {
                        this.d((byte)-127);
                        return true;
                     }

                     if(ed.e[82] && ~var1 == -67) {
                        this.l(34);
                        return true;
                     }

                     if(ed.e[82] && var1 == 67) {
                        this.d(false);
                        return true;
                     }
                  } else if(this.O < super.text.length()) {
                     this.a(!ed.e[82]?1 + this.O:this.h(-109), 5988);
                     return true;
                  }
               } else {
                  if(this.O != this.I) {
                     this.p(0);
                     return true;
                  }

                  if(this.O < super.text.length()) {
                     this.I = this.O + 1;
                     this.p(var2 + -61);
                     return true;
                  }
               }

               return var2 != 61?false:false;
            }
         } else {
            return false;
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "wb.E(" + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

   private final void a(int var1, int var2) {
      try {
         this.O = var1;
         if(var2 != 5988) {
            this.L = 72;
         }

         if(!ed.e[81]) {
            this.I = this.O;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.VA(" + var1 + ',' + var2 + ')');
      }
   }

   private final void d(boolean var1) {
      try {
         try {
            String var2 = (String)Toolkit.getDefaultToolkit().getSystemClipboard().getContents((Object)null).getTransferData(DataFlavor.stringFlavor);
            this.p(0);
            this.a(0, var2);
         } catch (Exception var3) {
            ;
         }

         if(var1) {
            this.J = -53L;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.P(" + var1 + ')');
      }
   }

   static final void a(byte var0, long var1, int var3) {
      try {
         if(var0 == 75) {
            FunN var4 = bf.f;
            var4.a(var3, true);
            ++var4.offset;
            int var5 = var4.offset;
            var4.a((int)7, (int)127);
            var4.putLong(var1, -268435456);
            var4.d(var4.offset - var5, (byte)80);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "wb.H(" + var0 + ',' + var1 + ',' + var3 + ')');
      }
   }

   public static void c(byte var0) {
      try {
         P = null;
         N = null;
         if(var0 != 53) {
            c((byte)-104);
         }

         Q = null;
         H = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "wb.BB(" + var0 + ')');
      }
   }

   private final int m(int var1) {
      int var3 = client.L;

      try {
         if(~this.O == -1) {
            return this.O;
         } else {
            if(var1 != 0) {
               Q = null;
            }

            int var2 = -1 + this.O;
            if(var3 != 0) {
               --var2;
            }

            while(var2 > 0 && ~super.text.charAt(-1 + var2) != -33) {
               --var2;
            }

            return var2;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.L(" + var1 + ')');
      }
   }

   private final void d(byte var1) {
      try {
         if(var1 != -127) {
            H = null;
         }

         this.l(78);
         this.p(0);
      } catch (RuntimeException var3) {
         throw pl.a(var3, "wb.R(" + var1 + ')');
      }
   }

   static final boolean n(int var0) {
      try {
         if(var0 != 85) {
            n(-89);
         }

         return bh.f.b(3741);
      } catch (RuntimeException var2) {
         throw pl.a(var2, "wb.G(" + var0 + ')');
      }
   }

   final void a(int var1, int var2, int var3, int var4) {
      try {
         if(super.renderer != null && ~var4 == -1) {
            super.renderer.a(super.w, this, var3, true, var1);
            if(super.renderer instanceof ni) {
               ni var5 = (ni)super.renderer;
               if(~this.O != ~this.I) {
                  var5.a(var1, this.O, true, var3, this, this.I);
               }

               long var6 = FunAB.a((byte) -127);
               if((var6 - this.J) % 1000L < 500L) {
                  var5.a(this.O, this, var3, var1, var2);
               }
            }
         }

         if(var2 != 0) {
            this.L = -20;
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "wb.AA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   final void o(int var1) {
      try {
         this.O = 0;
         super.text = "";
         this.I = 0;
         this.j(8);
         if(var1 < 107) {
            this.S = true;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "wb.M(" + var1 + ')');
      }
   }

   private final void p(int var1) {
      try {
         if(~this.I != ~this.O) {
            int var2 = this.I < this.O?this.I:this.O;
            int var3 = ~this.O < ~this.I?this.O:this.I;
            this.O = var2;
            this.I = var2;
            super.text = super.text.substring(0, var2) + super.text.substring(var3, super.text.length());
            this.j(8);
         }

         if(var1 != 0) {
            this.L = 7;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "wb.K(" + var1 + ')');
      }
   }

}
