import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

final class MainCacheFile {

	static boolean a;
	private long b;
	private byte[] c;
	static BigInteger d = new BigInteger("94985066165976915983695808507103319846654268921925408641703409923983097369857");
	static int e = 480;
	private int f = 0;
	private int g;
	private long h = -1L;
	private long offset;
	private long j;
	private RandomAccessWrapper randomAccess;
	private byte[] l;
	static ph m;
	private long n;
	private long o = -1L;


	private File getFile(int i) {
		try {
			if (i != 5861) {
				dispose(70);
			}

			return randomAccess.getFile(true);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "MainCacheFile.C(" + i + ')');
		}
	}

	final void setOffset(long offset, byte var3) throws IOException {
		try {
			if (~offset > -1L) {
				throw new IOException("Invalid seek to " + offset + " in file " + getFile((int) 5861));
			} else {
				this.offset = offset;
			}
		} catch (RuntimeException var5) {
			throw pl.a(var5, "MainCacheFile.F(" + offset + ',' + var3 + ')');
		}
	}

	public static void dispose(int var0) {
		try {
			d = null;
			m = null;
			if (var0 != 29711) {
				e = 90;
			}
		} catch (RuntimeException var2) {
			throw pl.a(var2, "MainCacheFile.B(" + var0 + ')');
		}
	}

	final long a(byte var1) {
		try {
			return b;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "MainCacheFile.K(" + var1 + ')');
		}
	}

	static void c(int var0) {
		try {
			int var1 = (jm.H + -640) / 2;
			int var2 = qe.j * qe.j;
			int var3 = -(jg.p * jg.p) + var2;
			wc.e.a(-(var3 * 199 / var2) + var1, -55, 90, -90 + cd.height - 124, 199);
			jh.r.a(var3 * 438 / var2 + 202 + var1, var0 ^ 107, var0, cd.height - 120 - 4, 438);
		} catch (RuntimeException var4) {
			throw pl.a(var4, "MainCacheFile.G(" + var0 + ')');
		}
	}

	private void d(int var1) throws IOException {
		int var7 = client.L;

		try {
			if ((long) var1 != h) {
				if (~n != ~h) {
					randomAccess.seek(h, (byte) -122);
					n = h;
				}

				randomAccess.write(l, 0, f, -38915644);
				n += (long) f;
				if (j < n) {
					j = n;
				}

				long var2;
				long var4;
				label71:
				{
					var2 = -1L;
					var4 = -1L;
					if (h < o || (long) g + o <= h) {
						if (~h < ~o || ~o <= ~(h + (long) f)) {
							break label71;
						}

						var2 = o;
						if (var7 == 0) {
							break label71;
						}
					}

					var2 = h;
				}

				label73:
				{
					if (~o <= ~((long) f + h) || ~(o - -((long) g)) > ~(h - -((long) f))) {
						if (~h <= ~((long) g + o) || h - -((long) f) < (long) g + o) {
							break label73;
						}

						var4 = o + (long) g;
						if (var7 == 0) {
							break label73;
						}
					}

					var4 = (long) f + h;
				}

				if (var2 > -1L && var2 < var4) {
					int var6 = (int) (-var2 + var4);
					rd.a(l, (int) (var2 + -h), c, (int) (-o + var2), var6);
				}

				h = -1L;
				f = 0;
			}
		} catch (RuntimeException var8) {
			throw pl.a(var8, "MainCacheFile.I(" + var1 + ')');
		}
	}

	final void close(int var1) throws IOException {
		try {
			d(-1);
			randomAccess.close(var1 ^ -1358);
			if (var1 != 438) {
				a((byte) -95);
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "MainCacheFile.H(" + var1 + ')');
		}
	}

	final void a(int off, int i, boolean var3, byte[] buf) throws IOException {
		int var10 = client.L;

		try {
			try {
				if (~b > ~(offset - -((long) i))) {
					b = offset + (long) i;
				}

				if (!var3) {
					a((byte) -7);
				}

				if (h != -1L && (~h < ~offset || h + (long) f < offset)) {
					d(-1);
				}

				if (h != -1L && (long) l.length + h < (long) i + offset) {
					int size = (int) (-offset + h + (long) l.length);
					rd.a(buf, off, l, (int) (offset + -h), size);
					off += size;
					i -= size;
					offset += (long) size;
					f = l.length;
					d(-1);
				}

				if (i <= l.length) {
					if (~i < -1) {
						if (~h == 0L) {
							h = offset;
						}

						rd.a(buf, off, l, (int) (-h + offset), i);
						offset += (long) i;
						if ((long) f < -h + offset) {
							f = (int) (offset + -h);
						}

					}
				} else {
					if (~offset != ~n) {
						randomAccess.seek(offset, (byte) 16);
						n = offset;
					}

					randomAccess.write(buf, off, i, -38915644);
					n += (long) i;
					if (~j > ~n) {
						j = n;
					}

					long var13;
					label115:
					{
						var13 = -1L;
						if (~o < ~offset || offset >= (long) g + o) {
							if (~offset < ~o || ~((long) i + offset) >= ~o) {
								break label115;
							}

							var13 = o;
							if (var10 == 0) {
								break label115;
							}
						}

						var13 = offset;
					}

					long var7;
					label75:
					{
						var7 = -1L;
						if (~o > ~(offset - -((long) i)) && ~((long) g + o) <= ~((long) i + offset)) {
							var7 = (long) i + offset;
							if (var10 == 0) {
								break label75;
							}
						}

						if ((long) g + o > offset && ~((long) g + o) >= ~((long) i + offset)) {
							var7 = o + (long) g;
						}
					}

					if (var13 > -1L && var7 > var13) {
						int var9 = (int) (var7 - var13);
						rd.a(buf, (int) ((long) off - (-var13 - -offset)), c, (int) (var13 + -o), var9);
					}

					offset += (long) i;
				}
			} catch (IOException var11) {
				n = -1L;
				throw var11;
			}
		} catch (RuntimeException var12) {
			throw pl.a(var12, "MainCacheFile.E(" + off + ',' + i + ',' + var3 + ',' + (buf != null ? "{...}" : "null") + ')');
		}
	}

	final void a(int var1, int var2, int var3, byte[] buf) throws IOException {
		int var14 = client.L;

		try {
			if (var1 == 0) {
				try {
					if (~(var3 - -var2) < ~buf.length) {
						throw new ArrayIndexOutOfBoundsException(var3 + var2 + -buf.length);
					}

					if (~h != 0L && ~h >= ~offset && h + (long) f >= offset - -((long) var2)) {
						rd.a(l, (int) (-h + offset), buf, var3, var2);
						offset += (long) var2;
						return;
					}

					long temp = offset;
					int var8 = var2;
					int var9;
					if (~o >= ~offset && offset < (long) g + o) {
						var9 = (int) ((long) g + o + -offset);
						if (var2 < var9) {
							var9 = var2;
						}

						rd.a(c, (int) (-o + offset), buf, var3, var9);
						var2 -= var9;
						offset += (long) var9;
						var3 += var9;
					}

					label147:
					{
						if (c.length >= var2) {
							if (~var2 >= -1) {
								break label147;
							}

							f(21061);
							var9 = var2;
							if (~var2 < ~g) {
								var9 = g;
							}

							rd.a(c, 0, buf, var3, var9);
							var2 -= var9;
							offset += (long) var9;
							var3 += var9;
							if (var14 == 0) {
								break label147;
							}
						}

						randomAccess.seek(offset, (byte) -127);
						n = offset;
						if (var14 != 0 || ~var2 < -1) {
							do {
								var9 = randomAccess.b(buf, var3, var2, -21241);
								if (var9 == -1) {
									break;
								}

								var3 += var9;
								n += (long) var9;
								offset += (long) var9;
								var2 -= var9;
							} while (~var2 < -1);
						}
					}

					if (~h != 0L) {
						if (h > offset && var2 > 0) {
							var9 = var3 - -((int) (h + -offset));
							if (~(var2 + var3) > ~var9) {
								var9 = var2 + var3;
								if (var14 != 0) {
									buf[var3++] = 0;
									--var2;
									++offset;
								}
							}

							while (~var3 > ~var9) {
								buf[var3++] = 0;
								--var2;
								++offset;
							}
						}

						long var17;
						label158:
						{
							var17 = -1L;
							if (~temp < ~h || h >= temp + (long) var8) {
								if (~temp > ~h || (long) f + h <= temp) {
									break label158;
								}

								var17 = temp;
								if (var14 == 0) {
									break label158;
								}
							}

							var17 = h;
						}

						long var11;
						label85:
						{
							var11 = -1L;
							if (~((long) f + h) < ~temp && (long) var8 + temp >= h - -((long) f)) {
								var11 = (long) f + h;
								if (var14 == 0) {
									break label85;
								}
							}

							if (h < temp + (long) var8 && ~((long) var8 + temp) >= ~(h + (long) f)) {
								var11 = (long) var8 + temp;
							}
						}

						if (~var17 < 0L && ~var11 < ~var17) {
							int var13 = (int) (-var17 + var11);
							rd.a(l, (int) (-h + var17), buf, var3 + (int) (var17 - temp), var13);
							if (~offset > ~var11) {
								var2 = (int) ((long) var2 - (-offset + var11));
								offset = var11;
							}
						}
					}
				} catch (IOException var15) {
					n = -1L;
					throw var15;
				}

				if (~var2 < -1) {
					throw new EOFException();
				}
			}
		} catch (RuntimeException var16) {
			throw pl.a(var16, "MainCacheFile.J(" + var1 + ',' + var2 + ',' + var3 + ',' + (buf != null ? "{...}" : "null") + ')');
		}
	}

	final void a(byte[] var1, boolean var2) throws IOException {
		try {
			a(0, var1.length, 0, var1);
			if (var2) {
				a = false;
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "MainCacheFile.D(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
		}
	}

	private void f(int var1) throws IOException {
		try {
			g = 0;
			if (~offset != ~n) {
				randomAccess.seek(offset, (byte) 83);
				n = offset;
			}

			o = offset;
			if (var1 == 21061) {
				while (~g > ~c.length) {
					int var2 = c.length - g;
					if (var2 > 200000000) {
						var2 = 200000000;
					}

					int var3 = randomAccess.b(c, g, var2, -21241);
					if (~var3 == 0) {
						break;
					}

					n += (long) var3;
					g += var3;
				}

			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "MainCacheFile.A(" + var1 + ')');
		}
	}

	MainCacheFile(RandomAccessWrapper randomAccess, int var2, int var3) throws IOException {
		try {
			this.randomAccess = randomAccess;
			b = j = randomAccess.getLength((byte) 95);
			offset = 0L;
			l = new byte[var3];
			c = new byte[var2];
		} catch (RuntimeException var5) {
			throw pl.a(var5, "MainCacheFile.<init>(" + (randomAccess != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ')');
		}
	}

}
