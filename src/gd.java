final class gd extends gb {

	private pm j = new pm();
	private pm k = new pm();
	private int l = 0;
	private int m = -1;


	final synchronized void a(int var1) {
		do {
			if (m < 0) {
				d(var1);
				return;
			}

			if (l + var1 < m) {
				l += var1;
				d(var1);
				return;
			}

			int var2 = m - l;
			d(var2);
			var1 -= var2;
			l += var2;
			e();
			gi var3 = (gi) k.a(7976409);
			int var6 = var3.a(this);
			if (var6 < 0) {
				var3.f = 0;
				a(var3);
			} else {
				var3.f = var6;
				a(var3.nextNode, var3);
			}
		} while (var1 != 0);

	}

	private void a(gi var1) {
		var1.b(true);
		var1.b();
		var1.g = null;
		FunNode var2 = k.h.nextNode;
		if (var2 == k.h) {
			m = -1;
		} else {
			m = ((gi) var2).f;
		}
	}

	final void a(hg var1, int var2, int var3, int var4) {
		b(pg.b(var1, var2, var3, var4));
	}

	final synchronized void a(gb var1) {
		var1.b(true);
	}

	final synchronized void b(gi var1) {
		if (var1.g != null) {
			if (var1.g != this) {
				throw new RuntimeException();
			} else {
				e();
				a(var1);
			}
		}
	}

	final synchronized void b(gb var1) {
		j.a(var1, true);
	}

	final gb b() {
		return (gb) j.c(true);
	}

	final void a(hg var1, int var2, int var3) {
		b(pg.a(var1, var2, var3));
	}

	final gb c() {
		return (gb) j.a(7976409);
	}

	private void d(int var1) {
		for (gb var2 = (gb) j.a(7976409); var2 != null; var2 = (gb) j.c(true)) {
			var2.a(var1);
		}

	}

	final synchronized void a(int[] var1, int var2, int var3) {
		do {
			if (m < 0) {
				c(var1, var2, var3);
				return;
			}

			if (l + var3 < m) {
				l += var3;
				c(var1, var2, var3);
				return;
			}

			int var4 = m - l;
			c(var1, var2, var4);
			var2 += var4;
			var3 -= var4;
			l += var4;
			e();
			gi var5 = (gi) k.a(7976409);
			int var8 = var5.a(this);
			if (var8 < 0) {
				var5.f = 0;
				a(var5);
			} else {
				var5.f = var8;
				a(var5.nextNode, var5);
			}
		} while (var3 != 0);

	}

	final int d() {
		return 0;
	}

	final synchronized void c(gi var1) {
		if (var1.g != null) {
			throw new RuntimeException();
		} else {
			e();
			var1.g = this;
			var1.a();
			a(k.h.nextNode, var1);
		}
	}

	private void e() {
		if (l > 0) {
			for (gi var1 = (gi) k.a(7976409); var1 != null; var1 = (gi) k.c(true)) {
				var1.f -= l;
			}

			m -= l;
			l = 0;
		}

	}

	private void c(int[] var1, int var2, int var3) {
		for (gb var4 = (gb) j.a(7976409); var4 != null; var4 = (gb) j.c(true)) {
			var4.b(var1, var2, var3);
		}

	}

	private void a(FunNode var1, gi var2) {
		while (var1 != k.h && ((gi) var1).f <= var2.f) {
			var1 = var1.nextNode;
		}

		vm.a((byte) -35, var2, var1);
		m = ((gi) k.h.nextNode).f;
	}

}
