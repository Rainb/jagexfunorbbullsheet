import java.applet.Applet;
import java.util.Hashtable;

final class fd extends FunRB {

   private boolean F;
   static FunImage[] chatFilter;
   private static long[] H = new long[256];
   private boolean I;
   private int J = Integer.MAX_VALUE;
   private int K;
   static ph[] L;
   static String loginNoDisplayName = "You need to choose a name before you can log in. This is the name that will be displayed to other players.";
   static nl N;
   static int O;
   private int P;
   private int Q = Integer.MAX_VALUE;
   static Applet loaderApplet;
   static String S;
   static int T;
   static int U;


   final void g(int var1) {
      try {
         super.g(62);
         super.w.a(0, super.n, (byte)-75, 0, super.t);
		  J = super.p;
         if(var1 <= 2) {
            O = -83;
         }

		  Q = super.i;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "fd.I(" + var1 + ')');
      }
   }

   static void h(int var0) {
      int var10 = client.L;

      try {
         int[] var1;
         int var2;
         int var3;
         FunImage var4;
         int var6;
         int var7;
         if(hc.Qb != null && hc.Qb.b(true) && hc.Qb.a((byte)123, "benefits") && na.p != null && na.p.b(true) && na.p.a((byte)-65, "benefits")) {
            mn.e = fj.a("benefits", "headline", hc.Qb, var0 ^ 2);
            um.c = fj.a("benefits", "blurbpane", hc.Qb, 0);
            ec.s = fj.a("benefits", "button", hc.Qb, var0 + -2);
            pi.B = fj.a("benefits", "signup_text", hc.Qb, 0);
            wg.h = fj.a("benefits", "menu_text", hc.Qb, 0);
            lf.n = fj.a("benefits", "button_frame", hc.Qb, var0 ^ 2);
            an.f = fj.a("benefits", "arrow", hc.Qb, 0);
            dc.h = sj.a((byte)-102, "large_font", na.p, hc.Qb, "benefits");
            dn.f = sj.a((byte)-102, "small_font", na.p, hc.Qb, "benefits");
            var1 = cd.pixels;
            var2 = cd.width;
            var3 = cd.height;
            cd.b(ec.j);
            var4 = new FunImage(an.f.width + 4, an.f.height + 4);
            var4.f();
            an.f.f(2, 2, tl.n);
            ll.a(var4.height, var4.width, 0, (byte)-47, var4, 0, 2, 1);
            an.f = var4;
            ld.b = an.f.a();
            FunImage var5 = new FunImage(195, 221);
            var6 = var5.width / 2;
            var5.f();
            dc.h.c(FunO.a(mh.h, new String[] { ha.t }, (byte) -116), var6, 40, 16777215, -1);
            dn.f.c(FunO.a(ml.k, new String[]{ha.t}, (byte)-116), var6, 60, 16777215, -1);
            dc.h.c(FunO.a(ik.a, new String[]{wf.M}, (byte)-116), var6, 110, 16777215, -1);
            dn.f.c(FunO.a(nc.b, new String[] { wf.M }, (byte) -116), var6, 130, 16777215, -1);
            dc.h.c(hj.a, var6, 180, 16777215, -1);
            dn.f.c(sn.k, var6, 200, 16777215, -1);
            ll.a(var5.height, var5.width, 0, (byte)118, var5, 0, 3, 1);
            um.c.f();
            var5.c(-um.c.r + 18, -um.c.v + 241);
            var7 = 0;
            if(var10 != 0 || qc.g.length > var7) {
               do {
                  ng.H[var7].f();
                  dn.f.a(qc.g[var7], 3, 3, -6 + ng.H[var7].width, ng.H[var7].height - 6, tl.n, -1, 1, 1, dn.f.t + dn.f.s);
                  ll.a(ng.H[var7].height, ng.H[var7].width, 0, (byte)123, ng.H[var7], 0, 3, 1);
                  ++var7;
               } while(qc.g.length > var7);
            }

            qc.g = null;
            hn.i = pi.B.copy();
            hn.i.f();
            qn.a(0, 0, var0 + -111, cd.width, cd.height * 2 / 3, 64);
            cd.a(var1, var2, var3);
            cd.a(ec.j);
            ce.b = (-lf.n.width + ec.s.width) / 2 + 434;
            ln.d = (-lf.n.height + ec.s.height) / 2 + 390;
            hc.Qb = null;
            dc.a = 231 + (-lf.n.width + ec.s.width) / 2;
            fn.i = (-lf.n.height + ec.s.height) / 2 + 390;
         }

         if(sh.G != null) {
            int var8;
            int var15;
            if(tk.j != null && dc.h != null) {
               var1 = cd.pixels;
               var2 = cd.width;
               var3 = cd.height;
               cd.b(ec.j);
               String var14 = FunO.a(tk.f, new String[]{fn.prices[qj.c]}, (byte)-116);
               var15 = dc.h.a(var14, oj.Tb);
               var6 = dc.h.b(var14, oj.Tb, dc.h.s + dc.h.t);
               var7 = (oj.Tb - var15) / 2 + en.k;
               var7 -= 3;
               var15 += 6;
               var8 = hi.c + (wi.a + -var6) / 2;
               var6 += 6;
               int var9;
               if(var7 < 0) {
                  var9 = -var7;
                  tk.j.originalWidth += var9 * 2;
                  var15 += var9 * 2;
                  var7 += var9;
                  en.k += var9;
                  tk.j.r += var9;
                  FunD.c -= var9;
               }

               var8 -= 3;
               int var10000;
               if(~tk.j.originalWidth > ~(var7 + var15)) {
                  var9 = var7 + var15 + -tk.j.originalWidth;
                  var10000 = var15 + var9 * 2;
                  tk.j.originalWidth += 2 * var9;
                  var10000 = var7 + var9;
                  en.k += var9;
                  FunD.c -= var9;
                  tk.j.r += var9;
               }

               if(var8 < 0) {
                  var9 = -var8;
                  hi.c += var9;
                  var6 += var9 * 2;
                  tk.j.v += var9;
                  ul.n -= var9;
                  tk.j.originalHeight += 2 * var9;
                  var8 += var9;
               }

               if(tk.j.originalHeight < var6 + var8) {
                  var9 = var6 + var8 - tk.j.originalHeight;
                  tk.j.originalHeight += var9 * 2;
                  var10000 = var6 + 2 * var9;
                  ul.n -= var9;
                  tk.j.v += var9;
                  hi.c += var9;
                  var10000 = var8 + var9;
               }

               tk.j.d();
               FunImage var18 = new FunImage(tk.j.originalWidth, tk.j.originalHeight);
               var18.f();
               dc.h.a(var14, en.k, hi.c, oj.Tb, wi.a, 16777215, -1, 1, 1, dc.h.s + dc.h.t);
               ll.a(var18.originalHeight, var18.originalWidth, 0, (byte)-94, var18, 0, 3, 1);
               tk.j.f();
               var18.c(0, 0);
               FunAB.a = new FunImage(640, 480);
               FunAB.a.f();
               tk.j.a(FunD.c + (tk.j.originalWidth >> -1277599), ul.n + (tk.j.originalHeight >> -648536735), ph.Bb, 4096);
               tk.j = null;
               FunAB.a.e();
               cd.a(var1, var2, var3);
               cd.a(ec.j);
            }

            if(fi.b != null && dn.f != null) {
               var1 = cd.pixels;
               var2 = cd.width;
               var3 = cd.height;
               cd.b(ec.j);
               var4 = new FunImage(412, 43);
               String var17 = FunO.a(FunBB.e, new String[]{fi.b}, (byte)-116);
               fi.b = null;
               var4.f();
               dn.f.a(var17, 3, 3, var4.width + -6, -6 + var4.height, 16777215, -1, 0, 1, dn.f.s + dn.f.t);
               ll.a(var4.height, var4.width, 0, (byte)124, var4, 0, 3, 1);
               sh.G.f();
               var4.c(199 + -sh.G.r, 83 + -sh.G.v);
               cd.a(var1, var2, var3);
               cd.a(ec.j);
            }

            if(pj.E != null && dn.f != null) {
               var1 = cd.pixels;
               var2 = cd.width;
               var3 = cd.height;
               cd.b(ec.j);
               int var16 = 6 + dn.f.a(mn.b, 640);
               var15 = var16 - -pj.E.originalWidth + 20;
               var6 = -(var15 / 2) + 427;
               var7 = var6 + pj.E.originalWidth + 20;
               sh.G.f();
               dn.f.a(mn.b, var7 - sh.G.r, dn.f.s + dn.f.t + -sh.G.v + 155, 16777215, -1);
               ll.a(50, var16, -4 + var7 - sh.G.r, (byte)27, sh.G, -sh.G.v + 155, 3, 1);
               var8 = 155 + -sh.G.v + -((-dn.f.t + -3 + pj.E.originalHeight - dn.f.s) / 2);
               pj.E.c(-sh.G.r + var6, var8);
               cd.a(var1, var2, var3);
               cd.a(ec.j);
               pj.E = null;
            }

            cd.b(ec.j);
            cd.a(16, 16, 608, 112, 15, oh.b, hn.j);
            if(var0 != 2) {
               U = -28;
            }

            cd.a(231, 144, 393, 232, 15, oh.b, hn.j);
            if(mn.e != null) {
               mn.e.c(0, 0);
            }

            if(um.c != null) {
               um.c.c(0, 0);
            }

            sh.G.c(0, 0);
            if(FunAB.a != null) {
               FunAB.a.c(0, 0);
            }

            if(ec.s != null && lf.n != null) {
               ec.s.c(231, 390);
               FunImage var12 = pi.B;
               if(~ea.e.l == -1) {
                  var12 = hn.i;
               }

               var12.c(0, 0);
               var2 = 40 + (in.a(jg.o << (ea.e.l == 0?4:3), (byte)-71) * 40 >> -304564240);
               if(~var2 < -1) {
                  ec.s.d(230, 389, var2);
                  ec.s.d(232, 389, var2);
                  ec.s.d(232, 391, var2);
                  ec.s.d(230, 391, var2);
                  var12.d(1, 1, var2);
                  var12.d(-1, 1, var2);
                  var12.d(1, -1, var2);
                  var12.d(-1, -1, var2);
               }

               df.a(fn.i, ~ea.e.l == -1, 10, dc.a);
            }

            if(ec.s != null && lf.n != null) {
               ec.s.c(434, 390);
               wg.h.c(0, 0);
               if(ea.e.l == 1) {
                  qn.a(436, 392, var0 + -86, ec.s.width - 4, ec.s.height * 7 / 12, 64);
               }

               df.a(ln.d, ~ea.e.l == -2, 10, ce.b);
            }

            if(an.f != null) {
               int var13 = -(an.f.originalHeight / 2) + 357;
               an.f.c(269 - an.f.originalWidth, var13);
               ld.b.c(586, var13);
               if(var13 < pc.Hb && pc.Hb < an.f.height + var13) {
                  var2 = 40 - -(in.a(jg.o << 967926916, (byte)-45) * 40 >> -1244197200);
                  if(~var2 < -1) {
                     label114: {
                        if(jn.a > 269 - an.f.width && ~jn.a > -270) {
                           an.f.d(-an.f.originalWidth + 268, -1 + var13, var2);
                           an.f.d(-an.f.originalWidth + 269 - -1, var13 - 1, var2);
                           an.f.d(269 - an.f.originalWidth - 1, var13 + 1, var2);
                           an.f.d(-an.f.originalWidth + 270, 1 + var13, var2);
                           if(var10 == 0) {
                              break label114;
                           }
                        }

                        if(~jn.a < -587 && jn.a < an.f.width + 586) {
                           ld.b.d(585, var13 + -1, var2);
                           ld.b.d(587, var13 + -1, var2);
                           ld.b.d(585, 1 + var13, var2);
                           ld.b.d(587, 1 + var13, var2);
                        }
                     }
                  }
               }
            }

            AppletMouseListener.b(29047);
            ng.H[uf.db].c(269, 340);
         }
      } catch (RuntimeException var11) {
         throw pl.a(var11, "fd.L(" + var0 + ')');
      }
   }

   final void a(bn var1, int var2, int var3, int var4) {
      try {
         int var5;
         label73: {
            if((!(super.w instanceof pj) || ((pj)super.w).w) && ~super.r == -2) {
               var5 = -K + jn.a + -var4;
               int var6 = -var2 + -P + pc.Hb;
               if(super.i == var5 && ~super.p == ~var6) {
                  break label73;
               }

               super.p = var6;
               super.i = var5;
               if(!(super.listener instanceof ad)) {
                  break label73;
               }

               ((ad)super.listener).a(var4, this, var2, false);
               if(client.L == 0) {
                  break label73;
               }
            }

            if(I) {
               if(super.i != Q) {
                  var5 = Q + -super.i;
                  super.i += Math.abs(var5) <= 2? var5 > 0?1:-1 :var5 >> -709596735;
               }

               if(super.p != J) {
                  var5 = -super.p + J;
                  super.p += ~Math.abs(var5) >= -3? ~var5 >= -1?-1:1 :var5 >> 1198863585;
               }
            }
         }

         super.a(var1, var2, -4, var4);
         var5 = -125 / ((var3 - 40) / 38);
      } catch (RuntimeException var7) {
         throw pl.a(var7, "fd.D(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   final void a(int var1, int var2, int var3, int var4, int var5, bn var6) {
      try {
         super.a(var1, 105, var3, var4, var5, var6);
         super.r = 0;
         if(var2 <= 70) {
            loaderApplet = null;
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "fd.IA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + (var6 != null?"{...}":"null") + ')');
      }
   }

   final StringBuffer a(int var1, Hashtable table, byte b, StringBuffer buffer) {
      try {
         if(b != 99) {
            O = 115;
         }

         if(a(b + -99, buffer, var1, table)) {
			 b(83, buffer, var1, table);
			 a(table, (byte)97, var1, buffer);
            buffer.append(" revert=").append(I);
            if(~Q != Integer.MIN_VALUE && J != Integer.MAX_VALUE) {
               buffer.append(" to ").append(Q).append(',').append(J);
            }
         }

         return buffer;
      } catch (RuntimeException var6) {
         throw pl.a(var6, "fd.GA(" + var1 + ',' + (table != null?"{...}":"null") + ',' + b + ',' + (buffer != null?"{...}":"null") + ')');
      }
   }

   private fd(int var1, int var2, int var3, int var4, vg var5, aa var6, bn var7, boolean var8, boolean var9) {
      super(var1, var2, var3, var4, var5, var6);

      try {
		  F = var9;
         super.w = var7;
		  I = var8;
      } catch (RuntimeException var11) {
         throw pl.a(var11, "fd.<init>(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + (var5 != null?"{...}":"null") + ',' + (var6 != null?"{...}":"null") + ',' + (var7 != null?"{...}":"null") + ',' + var8 + ',' + var9 + ')');
      }
   }

   public static void i(int var0) {
      try {
         H = null;
         L = null;
         loginNoDisplayName = null;
         int var1 = -25 % ((-49 - var0) / 63);
         chatFilter = null;
         S = null;
         loaderApplet = null;
         N = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "fd.K(" + var0 + ')');
      }
   }

   static dk a(int var0, int var1, el var2, nf cache, int fileHash) {
      try {
         if(var0 != -709596735) {
            return null;
         } else {
            byte[] var5 = cache.c(var1, fileHash, -119);
            return var5 == null?null:new dk(new ByteBuffer(var5), var2);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "fd.A(" + var0 + ',' + var1 + ',' + (var2 != null?"{...}":"null") + ',' + (cache != null?"{...}":"null") + ',' + fileHash + ')');
      }
   }

   static boolean a(nf var0, int var1) {
      try {
         if(var1 != 16777215) {
            loginNoDisplayName = null;
         }

         return var0.b((byte)-118);
      } catch (RuntimeException var3) {
         throw pl.a(var3, "fd.B(" + (var0 != null?"{...}":"null") + ',' + var1 + ')');
      }
   }

   static void j(int var0) {
      try {
         if((~-jd.f == ~FunQB.C || ~FunQB.C == ~(250 - jd.f)) && im.d != null && ma.d != null) {
            im.d.a(ma.d, 256, 96);
         }

         ++FunQB.C;
         if(var0 != 20424) {
            h(86);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "fd.C(" + var0 + ')');
      }
   }

   static void a(nf var0, nf var1, int var2) {
      try {
         if(var2 != 64) {
            j(-116);
         }

         na.p = var1;
         hc.Qb = var0;
      } catch (RuntimeException var4) {
         throw pl.a(var4, "fd.J(" + (var0 != null?"{...}":"null") + ',' + (var1 != null?"{...}":"null") + ',' + var2 + ')');
      }
   }

   final boolean a(bn var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      try {
         boolean var8 = super.a(var1, var2, var3, var4, var5, var6, var7);
         if(var8 && F) {
            return true;
         } else if(!a(1, var5, var4, var3, var7)) {
            return var8;
         } else {
            super.r = var2;
            if(~var2 == -2) {
				P = -super.p + var4 - var3;
				K = -var5 + -super.i + var7;
               ug.h = this;
            }

            return true;
         }
      } catch (RuntimeException var9) {
         throw pl.a(var9, "fd.MA(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
      }
   }

   static {
      for(int var2 = 0; ~var2 > -257; ++var2) {
         long var0 = (long)var2;

         for(int var3 = 0; var3 < 8; ++var3) {
            if((1L & var0) != 1L) {
               var0 >>>= 1;
            } else {
               var0 = -3932672073523589310L ^ var0 >>> -538959871;
            }
         }

         H[var2] = var0;
      }

      T = 4;
      S = "Security";
      U = 64;
   }
}
