final class pk extends bk {

	int[][] L;
	private byte[][] M = new byte[256][];


	private static void a(int var0, int[] var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
		int var11 = 256 - var10;

		for (int var12 = -var7; var12 < 0; ++var12) {
			for (int var13 = -var6; var13 < 0; ++var13) {
				byte var16;
				if ((var16 = var2[var4++]) != 0) {
					int var14 = var1[var5];
					int var15 = var3[var16 & 255];
					var1[var5++] = ((var15 & 16711935) * var10 + (var14 & 16711935) * var11 & -16711936) + ((var15 & '\uff00') * var10 + (var14 & '\uff00') * var11 & 16711680)
							>> 8;
				} else {
					++var5;
				}
			}

			var5 += var8;
			var4 += var9;
		}

	}

	pk(byte[] var1, int[] var2, int[] var3, int[] var4, int[] var5, int[] var6, byte[][] var7) {
		super(var1, var2, var3, var4, var5);
		M = var7;
		L = new int[4][];
		L[0] = var6;
	}

	final void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8) {
		int var9 = var2 + var3 * cd.width;
		int var10 = cd.width - var4;
		int var11 = 0;
		int var12 = 0;
		int var13;
		if (var3 < cd.g) {
			var13 = cd.g - var3;
			var5 -= var13;
			var3 = cd.g;
			var12 += var13 * var4;
			var9 += var13 * cd.width;
		}

		if (var3 + var5 > cd.e) {
			var5 -= var3 + var5 - cd.e;
		}

		if (var2 < cd.l) {
			var13 = cd.l - var2;
			var4 -= var13;
			var2 = cd.l;
			var12 += var13;
			var9 += var13;
			var11 += var13;
			var10 += var13;
		}

		if (var2 + var4 > cd.j) {
			var13 = var2 + var4 - cd.j;
			var4 -= var13;
			var11 += var13;
			var10 += var13;
		}

		if (var4 > 0 && var5 > 0) {
			if (var8) {
				ck.a(cd.pixels, M[var1], var6, var12, var9, var4, var5, var10, var11, var7);
			} else {
				a(0, cd.pixels, M[var1], L[var6], var12, var9, var4, var5, var10, var11, var7);
			}
		}
	}

	private static void a(int var0, int[] var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		int var10 = -(var6 >> 2);
		var6 = -(var6 & 3);

		for (int var11 = -var7; var11 < 0; ++var11) {
			byte var14;
			for (int var12 = var10; var12 < 0; ++var12) {
				if ((var14 = var2[var4++]) != 0) {
					var1[var5++] = var3[var14 & 255];
				} else {
					++var5;
				}

				if ((var14 = var2[var4++]) != 0) {
					var1[var5++] = var3[var14 & 255];
				} else {
					++var5;
				}

				if ((var14 = var2[var4++]) != 0) {
					var1[var5++] = var3[var14 & 255];
				} else {
					++var5;
				}

				if ((var14 = var2[var4++]) != 0) {
					var1[var5++] = var3[var14 & 255];
				} else {
					++var5;
				}
			}

			for (int var13 = var6; var13 < 0; ++var13) {
				if ((var14 = var2[var4++]) != 0) {
					var1[var5++] = var3[var14 & 255];
				} else {
					++var5;
				}
			}

			var5 += var8;
			var4 += var9;
		}

	}

	final void a(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7) {
		int var8 = var2 + var3 * cd.width;
		int var9 = cd.width - var4;
		int var10 = 0;
		int var11 = 0;
		int var12;
		if (var3 < cd.g) {
			var12 = cd.g - var3;
			var5 -= var12;
			var3 = cd.g;
			var11 += var12 * var4;
			var8 += var12 * cd.width;
		}

		if (var3 + var5 > cd.e) {
			var5 -= var3 + var5 - cd.e;
		}

		if (var2 < cd.l) {
			var12 = cd.l - var2;
			var4 -= var12;
			var2 = cd.l;
			var11 += var12;
			var8 += var12;
			var10 += var12;
			var9 += var12;
		}

		if (var2 + var4 > cd.j) {
			var12 = var2 + var4 - cd.j;
			var4 -= var12;
			var10 += var12;
			var9 += var12;
		}

		if (var4 > 0 && var5 > 0) {
			if (var7) {
				ck.a(cd.pixels, M[var1], var6, var11, var8, var4, var5, var9, var10);
			} else {
				a(0, cd.pixels, M[var1], L[var6], var11, var8, var4, var5, var9, var10);
			}
		}
	}
}
