import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

final class FunMouseWheelListener extends AbstractMouseWheelListener implements MouseWheelListener {

	private int wheelRotation = 0;


	final void removeFrom(boolean var1, Component component) {
		if (var1) {
			addTo(null, 9);
		}
		component.removeMouseWheelListener(this);
	}

	public final synchronized void mouseWheelMoved(MouseWheelEvent event) {
		wheelRotation += event.getWheelRotation();
	}

	final synchronized int getMouseWheelRotation(int var1) {
		int temp = wheelRotation;
		wheelRotation = 0;
		if (var1 > -45) {
			wheelRotation = -88;
		}
		return temp;
	}

	final void addTo(Component applet, int var2) {
		applet.addMouseWheelListener(this);
		if (var2 <= 53) {
			mouseWheelMoved(null);
		}
	}

}
