import java.awt.Canvas;
import java.io.IOException;

final class FunTB extends ph {

   static String[] Hb = new String[]{"Master Challenge: prove your prowess", "Unlock 4 more vibrant and varied themes", "Large bucket: grow shapes to the limit", "Use special items in multiplayer...", "...to turn things around spectacularly!", "Loads of extra Achievements", null, null};
   static String createDisplayNameTooltip = "Enter the name you\'d prefer. This is the name displayed to other players.";
   static ph Jb;
   private ph Kb;
   static String Lb = "No";


   static void a(boolean var0, Canvas canvas, int x) {
      try {
         if(x <= ah.hb) {
            if(ri.a((byte)-78)) {
               if(ci.a != 0) {
                  am.a(canvas, -106);
               } else {
                  in.a((byte)-55, false, var0);
                  FunN.draw(117, 0, canvas, 0);
               }
            } else {
               cd.a();
               FunCanvas.a((byte) 68, 240, 320);
               FunN.draw(x + 95, 0, canvas, 0);
            }
         } else {
            boolean var3 = false;
            if(en.m) {
               en.m = false;
               var3 = true;
            }

            me.a(fj.Rb, hn.a(-113), vj.a((byte)-117), var3, 108);
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "FunTB.C(" + var0 + ',' + (canvas != null?"{...}":"null") + ',' + x + ')');
      }
   }

   final boolean a(boolean var1, boolean var2) {
      try {
         if(var2) {
            return false;
         } else {
			 a((byte)126, true);
            return ~mg.p != -1 && ~super.yb == -1 || Kb.yb != 0;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "FunTB.D(" + var1 + ',' + var2 + ')');
      }
   }

   static nf a(int var0, int var1, boolean var2, boolean var3, int var4, boolean var5) {
      try {
         try {
            MainCacheIndex idx = null;
            if(var1 != 127) {
               return null;
            } else {
               MainCacheIndex var7 = null;
               if(be.K.mainCacheDataFile != null) {
                  jh.mainCacheData = new MainCacheFile(be.K.mainCacheDataFile, 5200, 0);
                  be.K.mainCacheDataFile = null;
                  idx = new MainCacheIndex(255, jh.mainCacheData, new MainCacheFile(be.K.mainCacheIdx, 12000, 0), 2097152);
               }

               if(jh.mainCacheData != null) {
                  if(lk.f == null) {
                     lk.f = new MainCacheFile[be.K.indexList.length];
                  }

                  if(lk.f[var0] == null) {
                     lk.f[var0] = new MainCacheFile(be.K.indexList[var0], 12000, 0);
                     be.K.indexList[var0] = null;
                  }

                  var7 = new MainCacheIndex(var0, jh.mainCacheData, lk.f[var0], 2097152);
               }

               tg var8 = bh.f.a(idx, (byte)-124, var2, var0, var7);
               if(var3) {
                  var8.c(42);
               }

               return new nf(var8, var5, var4);
            }
         } catch (IOException var9) {
            throw new RuntimeException(var9.toString());
         }
      } catch (RuntimeException var10) {
         throw pl.a(var10, "FunTB.B(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

   static bi a(boolean var0, int var1) {
      int var7 = client.L;

      try {
         FunN var2;
         label62: {
            var2 = ta.o;
            int var3 = var2.getInt(false);
            ri.e = 127 & var3;
            bj.w = (var3 & 128) != 0;
            FunL.i = var2.getInt(false);
            vl.H = var2.e(16711680);
            if(var1 == ~ri.e) {
               ek.A = var2.j(1808469224);
               el.n = var2.g(-128);
               if(var7 == 0) {
                  break label62;
               }
            }

            el.n = 0;
            ek.A = 0;
         }

         label56: {
            boolean var4 = ~var2.getInt(false) == -2;
            ae.a = var2.getString((byte) -80);
            if(var4) {
               gk.B = var2.getString((byte) -80);
               if(var7 == 0) {
                  break label56;
               }
            }

            gk.B = ae.a;
         }

         label51: {
            if(~ri.e != -2 && ri.e != 4) {
               dn.c = 0;
               va.R = null;
               if(var7 == 0) {
                  break label51;
               }
            }

            dn.c = var2.j(1808469224);
            va.R = var2.getString((byte) -80);
         }

         if(var0) {
            int var5 = var2.j(1808469224);

            try {
               bl var6 = ll.a.a(24358, var5);
               ah.jb = var6.i(-111);
               FunM.y = !gk.B.equals(hc.Lb)?var6.r:null;
               return new bi(var0);
            } catch (Exception var8) {
               dj.a("CC1", 0, var8);
               FunM.y = null;
               ah.jb = null;
               if(var7 == 0) {
                  return new bi(var0);
               }
            }
         }

         ah.jb = ek.a(var2, -1, 80);
         FunM.y = null;
         return new bi(var0);
      } catch (RuntimeException var9) {
         throw pl.a(var9, "FunTB.A(" + var0 + ',' + var1 + ')');
      }
   }

   public static void k(int var0) {
      try {
         Hb = null;
         createDisplayNameTooltip = null;
         Jb = null;
         Lb = null;
         if(var0 != 12000) {
            k(-33);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "FunTB.F(" + var0 + ')');
      }
   }

   FunTB(ph var1, ph var2, ph var3, ph var4, ph var5, ph var6) {
	   super();
      int var16 = client.L;

      try {
         ph var7 = new ph(0L, var2, ta.m.toUpperCase());
         var7.Ab = 1;
		  Kb = new ph(0L, var3);
         ph var8 = new ph(0L, var4);
         ph var9 = new ph(0L, var4, jm.K);
         var9.Ab = 1;
         int var10 = 50;
         int var11 = 0;
         int var12 = 0;
         ph var13;
         ph var14;
         int var15;
         if(var16 != 0) {
            var13 = new ph(0L, var4, pm.a[var12]);
            var14 = new ph(0L, var4, li.i[var12]);
            var15 = var4.I.a(li.i[var12]);
            var13.a(20, 53, var10, 15, 65);
            if(~var15 < ~var11) {
               var11 = var15;
            }

            var14.a(90, -58, var10, 15, 640);
            var8.a(14939, var13);
            var10 += 30;
            var8.a(14939, var14);
            ++var12;
         }

         while(var12 < li.i.length) {
            var13 = new ph(0L, var4, pm.a[var12]);
            var14 = new ph(0L, var4, li.i[var12]);
            var15 = var4.I.a(li.i[var12]);
            var13.a(20, 53, var10, 15, 65);
            if(~var15 < ~var11) {
               var11 = var15;
            }

            var14.a(90, -58, var10, 15, 640);
            var8.a(14939, var13);
            var10 += 30;
            var8.a(14939, var14);
            ++var12;
         }

         var7.a(0, -128, 0, 24, 90 + 20 + var11);
         var10 += 15;
		  a(100, 74, 100, var7.ob + var10, var7.rb);
		  Kb.a(-20 + var7.rb, -105, 5, 15, 15);
         var8.a(0, 104, var7.ob, -var7.ob + super.ob, super.rb);
         var9.a(0, -119, 20, 15, super.rb);
         var8.H = eh.a(3, 8421504, 2105376, -19038, var8.ob, 11579568);
         var7.a(14939, Kb);
         var8.a(14939, var9);
		  a(14939, var7);
		  a(14939, var8);
         super.R = 320 + -(super.rb >> -1491567519);
         var10 = -(super.ob >> -1579021567) + 240;
      } catch (RuntimeException var17) {
         throw pl.a(var17, "FunTB.<init>(" + (var1 != null?"{...}":"null") + ',' + (var2 != null?"{...}":"null") + ',' + (var3 != null?"{...}":"null") + ',' + (var4 != null?"{...}":"null") + ',' + (var5 != null?"{...}":"null") + ',' + (var6 != null?"{...}":"null") + ')');
      }
   }

   static pn c(byte var0) {
      try {
         if(var0 > -90) {
            Jb = null;
         }

         return new pn(di.a((byte)-97), oe.b((byte)-103));
      } catch (RuntimeException var2) {
         throw pl.a(var2, "FunTB.E(" + var0 + ')');
      }
   }

}
