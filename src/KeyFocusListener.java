import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

final class KeyFocusListener implements KeyListener, FocusListener {

   static ph a;
   static String b = "<%0> has left the lobby.";
   static String unableToDeleteIgnore = "Unable to delete name - system busy";
   static int d = 0;
   static boolean[] e = new boolean[64];
   static int f;
   static int g = 128;
   static String h = "Error connecting to server. Please try using a different server.";
   static String i = "\"I think it\'s shaping up quite nicely!\" - Ian T";
   static an j;


   static String a(byte var0) {
      try {
         if(var0 > -27) {
            a = null;
         }

         return FunIC.e;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "KeyFocusListener.B(" + var0 + ')');
      }
   }

   public final synchronized void keyPressed(KeyEvent event) {
      try {
         if(rh.keyFocusListener != null) {
            int key;
            label64: {
               we.m = 0;
               key = event.getKeyCode();
               if(key < 0 || sn.keyCharMap.length <= key) {
                  key = -1;
                  if(client.L == 0) {
                     break label64;
                  }
               }

               key = sn.keyCharMap[key];
               if((128 & key) != 0) {
                  key = -1;
               }
            }

            if(cn.J >= 0 && ~key <= -1) {
               dh.i[cn.J] = key;
               cn.J = 127 & 1 + cn.J;
               if(~cn.J == ~qd.h) {
                  cn.J = -1;
               }
            }

            int modes;
            if(~key <= -1) {
               modes = FunP.d + 1 & 127;
               if(fj.Yb != modes) {
                  ll.t[FunP.d] = key;
                  tj.k[FunP.d] = 0;
                  FunP.d = modes;
               }
            }

            modes = event.getModifiers();
            if((modes & 10) != 0 || key == 85 || ~key == -11) {
               event.consume();
            }

         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "KeyFocusListener.keyPressed(" + (event != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void keyReleased(KeyEvent var1) {
      try {
         if(rh.keyFocusListener != null) {
            int var2;
            label42: {
               we.m = 0;
               var2 = var1.getKeyCode();
               if(var2 < 0 || sn.keyCharMap.length <= var2) {
                  var2 = -1;
                  if(client.L == 0) {
                     break label42;
                  }
               }

               var2 = sn.keyCharMap[var2] & -129;
            }

            if(~cn.J <= -1 && ~var2 <= -1) {
               dh.i[cn.J] = ~var2;
               cn.J = 1 + cn.J & 127;
               if(qd.h == cn.J) {
                  cn.J = -1;
               }
            }
         }

         var1.consume();
      } catch (RuntimeException var3) {
         throw pl.a(var3, "KeyFocusListener.keyReleased(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void focusLost(FocusEvent var1) {
      try {
         if(rh.keyFocusListener != null) {
            cn.J = -1;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "KeyFocusListener.focusLost(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final void keyTyped(KeyEvent var1) {
      try {
         if(rh.keyFocusListener != null) {
            char var2 = var1.getKeyChar();
            if(~var2 != -1 && var2 != '\uffff' && di.a((int)128, var2)) {
               int var3 = 127 & FunP.d + 1;
               if(var3 != fj.Yb) {
                  ll.t[FunP.d] = -1;
                  tj.k[FunP.d] = var2;
                  FunP.d = var3;
               }
            }
         }

         var1.consume();
      } catch (RuntimeException var4) {
         throw pl.a(var4, "KeyFocusListener.keyTyped(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final void focusGained(FocusEvent var1) {}

   public static void b(byte var0) {
      try {
         e = null;
         j = null;
         unableToDeleteIgnore = null;
         a = null;
         b = null;
         int var1 = 67 / ((var0 - -82) / 40);
         h = null;
         i = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "KeyFocusListener.A(" + var0 + ')');
      }
   }

}
