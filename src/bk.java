
abstract class bk extends ud {

   int n = 0;
   private int[] o;
   private int[] p;
   private int[] q;
   private qi[] r;
   int s;
   int t;
   private byte[] u;
   private int[] v;
   private int[] w;
   int x;
   private int[] y;
   private static StringBuffer z = new StringBuffer(100);
   private static int A = 256;
   private static int B = -1;
   private static int C = 0;
   private static String[] D = new String[100];
   private static int E = -1;
   private static int F = 0;
   private static int G = 256;
   private static int H = 0;
   private static int I = -1;
   private static int J = -1;
   private static int K = 0;


   private void a(byte[] var1) {
	   y = new int[256];
      int var2;
      if(var1.length == 257) {
         for(var2 = 0; var2 < y.length; ++var2) {
			 y[var2] = var1[var2] & 255;
         }

		  n = var1[256] & 255;
      } else {
         var2 = 0;

         for(int var3 = 0; var3 < 256; ++var3) {
			 y[var3] = var1[var2++] & 255;
         }

         int[] var4 = new int[256];
         int[] var5 = new int[256];

         for(int var6 = 0; var6 < 256; ++var6) {
            var4[var6] = var1[var2++] & 255;
         }

         for(int var7 = 0; var7 < 256; ++var7) {
            var5[var7] = var1[var2++] & 255;
         }

         byte[][] var8 = new byte[256][];

         int var11;
         for(int var9 = 0; var9 < 256; ++var9) {
            var8[var9] = new byte[var4[var9]];
            byte var10 = 0;

            for(var11 = 0; var11 < var8[var9].length; ++var11) {
               var10 += var1[var2++];
               var8[var9][var11] = var10;
            }
         }

         byte[][] var14 = new byte[256][];

         int var13;
         for(var11 = 0; var11 < 256; ++var11) {
            var14[var11] = new byte[var4[var11]];
            byte var12 = 0;

            for(var13 = 0; var13 < var14[var11].length; ++var13) {
               var12 += var1[var2++];
               var14[var11][var13] = var12;
            }
         }

		  u = new byte[65536];

         for(int var15 = 0; var15 < 256; ++var15) {
            if(var15 != 32 && var15 != 160) {
               for(var13 = 0; var13 < 256; ++var13) {
                  if(var13 != 32 && var13 != 160) {
					  u[(var15 << 8) + var13] = (byte)a(var8, var14, var5, y, var4, var15, var13);
                  }
               }
            }
         }

		  n = var5[32] + var4[32];
      }
   }

   final int a(String var1, int var2) {
      int var3 = a(var1, new int[]{var2}, D);
      int var4 = 0;

      for(int var5 = 0; var5 < var3; ++var5) {
         int var6 = a(D[var5]);
         if(var6 > var4) {
            var4 = var6;
         }
      }

      return var4;
   }

   final int a(String str, int[] var2, String[] strings) {
      if(str == null) {
         return 0;
      } else {
         qf.a((byte)127, 0, ' ', z);
         int var4 = 0;
         int var5 = 0;
         int var6 = -1;
         int var7 = 0;
         byte var8 = 0;
         int var9 = -1;
         char var10 = 0;
         int var11 = 0;
         int len = str.length();

         for(int var13 = 0; var13 < len; ++var13) {
            char var14 = str.charAt(var13);
            if(var14 == 60) {
               var9 = var13;
            } else {
               if(var14 == 62 && var9 != -1) {
                  String tag = str.substring(var9 + 1, var13).toLowerCase();
                  var9 = -1;
                  z.append('<');
                  z.append(tag);
                  z.append('>');
                  if(tag.equals("br")) {
                     strings[var11] = z.toString().substring(var5, z.length());
                     ++var11;
                     var5 = z.length();
                     var4 = 0;
                     var6 = -1;
                     var10 = 0;
                  } else if(tag.equals("lt")) {
                     var4 += a('<');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 60];
                     }

                     var10 = 60;
                  } else if(tag.equals("gt")) {
                     var4 += a('>');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 62];
                     }

                     var10 = 62;
                  } else if(tag.equals("nbsp")) {
                     var4 += a('\u00a0');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 160];
                     }

                     var10 = 160;
                  } else if(tag.equals("shy")) {
                     var4 += a('\u00ad');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 173];
                     }

                     var10 = 173;
                  } else if(tag.equals("times")) {
                     var4 += a('\u00d7');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 215];
                     }

                     var10 = 215;
                  } else if(tag.equals("euro")) {
                     var4 += a('\u20ac');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 128];
                     }

                     var10 = 8364;
                  } else if(tag.equals("copy")) {
                     var4 += a('\u00a9');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 169];
                     }

                     var10 = 169;
                  } else if(tag.equals("reg")) {
                     var4 += a('\u00ae');
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + 174];
                     }

                     var10 = 174;
                  } else if(tag.startsWith("img=")) {
                     try {
                        int var16 = wm.a(tag.substring(4), (byte)117);
                        var4 += r[var16].a;
                        var10 = 0;
                     } catch (Exception ignored) {
					 }
                  }

                  var14 = 0;
               }

               if(var9 == -1) {
                  if(var14 != 0) {
                     z.append(var14);
                     var14 = (char)(ej.a(var14, -8213) & 255);
                     var4 += y[var14];
                     if(u != null && var10 != 0) {
                        var4 += u[(var10 << 8) + var14];
                     }

                     var10 = var14;
                  }

                  if(var14 == 32) {
                     var6 = z.length();
                     var7 = var4;
                     var8 = 1;
                  }

                  if(var2 != null && var4 > var2[var11 < var2.length?var11:var2.length - 1] && var6 >= 0) {
                     strings[var11] = z.toString().substring(var5, var6 - var8);
                     ++var11;
                     var5 = var6;
                     var6 = -1;
                     var4 -= var7;
                     var10 = 0;
                  }

                  if(var14 == 45) {
                     var6 = z.length();
                     var7 = var4;
                     var8 = 0;
                  }
               }
            }
         }

         if(z.length() > var5) {
            strings[var11] = z.toString().substring(var5, z.length());
            ++var11;
         }

         return var11;
      }
   }

   final void a(String var1, int var2, int var3, int var4, int var5, int var6) {
      if(var1 != null) {
		  a(var4, var5, var6);
		  a(var1, var2 - a(var1) / 2, var3);
      }
   }

   private void b(String var1, int var2) {
      int var3 = 0;
      boolean var4 = false;
      int var5 = var1.length();

      for(int var6 = 0; var6 < var5; ++var6) {
         char var7 = var1.charAt(var6);
         if(var7 == 60) {
            var4 = true;
         } else if(var7 == 62) {
            var4 = false;
         } else if(!var4 && var7 == 32) {
            ++var3;
         }
      }

      if(var3 > 0) {
         F = (var2 - a(var1) << 8) / var3;
      }

   }

   final void a(qi[] var1, int[] var2) {
      if(var2 != null && var2.length != var1.length) {
         throw new IllegalArgumentException();
      } else {
		  r = var1;
		  w = var2;
      }
   }

   private void a(int var1, int var2, int var3) {
      B = -1;
      J = -1;
      I = var2;
      E = var2;
      H = var1;
      K = var1;
      A = var3;
      G = var3;
      F = 0;
      C = 0;
   }

   private static int a(byte[][] var0, byte[][] var1, int[] var2, int[] var3, int[] var4, int var5, int var6) {
      int var7 = var2[var5];
      int var8 = var7 + var4[var5];
      int var9 = var2[var6];
      int var10 = var9 + var4[var6];
      int var11 = var7;
      if(var9 > var7) {
         var11 = var9;
      }

      int var12 = var8;
      if(var10 < var8) {
         var12 = var10;
      }

      int var13 = var3[var5];
      if(var3[var6] < var13) {
         var13 = var3[var6];
      }

      byte[] var14 = var1[var5];
      byte[] var15 = var0[var6];
      int var16 = var11 - var7;
      int var17 = var11 - var9;

      for(int var18 = var11; var18 < var12; ++var18) {
         int var19 = var14[var16++] + var15[var17++];
         if(var19 < var13) {
            var13 = var19;
         }
      }

      return -var13;
   }

   final int a(char var1) {
      return y[ej.a(var1, -8213) & 255];
   }

   public static void a() {
      z = null;
      D = null;
   }

   final void a(String s, int var2, int var3, int var4, int var5) {
      if(s != null) {
		  b(var4, var5);
		  a(s, var2, var3);
      }
   }

   final int a(String string) {
      if(string == null) {
         return 0;
      } else {
         int var2 = -1;
         char var3 = 0;
         int var4 = 0;
         int len = string.length();

         for(int i = 0; i < len; ++i) {
            char c = string.charAt(i);
            if(c == 60) {
               var2 = i;
            } else {
               if(c == 62 && var2 != -1) {
                  String tag = string.substring(var2 + 1, i).toLowerCase();
                  var2 = -1;
                  if(tag.equals("lt")) {
                     c = 60;
                  } else if(tag.equals("gt")) {
                     c = 62;
                  } else if(tag.equals("nbsp")) {
                     c = 160;
                  } else if(tag.equals("shy")) {
                     c = 173;
                  } else if(tag.equals("times")) {
                     c = 215;
                  } else if(tag.equals("euro")) {
                     c = 8364;
                  } else if(tag.equals("copy")) {
                     c = 169;
                  } else {
                     if(!tag.equals("reg")) {
                        if(tag.startsWith("img=")) {
                           try {
                              int imgID = wm.a(tag.substring(4), (byte)117);
                              var4 += r[imgID].a;
                              var3 = 0;
                           } catch (Exception ignored) {
						   }
                        }
                        continue;
                     }

                     c = 174;
                  }
               }

               if(var2 == -1) {
                  c = (char)(ej.a(c, -8213) & 255);
                  var4 += y[c];
                  if(u != null && var3 != 0) {
                     var4 += u[(var3 << 8) + c];
                  }

                  var3 = c;
               }
            }
         }

         return var4;
      }
   }

   private void a(String text, int var2, int var3) {
      var3 -= n;
      int var4 = -1;
      char var5 = 0;
      int len = text.length();

      for(int i = 0; i < len; ++i) {
         char ch = text.charAt(i);
         if(ch == 60) {
            var4 = i;
         } else {
            int imgID;
            if(ch == 62 && var4 != -1) {
               String var9 = text.substring(var4 + 1, i).toLowerCase();
               var4 = -1;
               if(var9.equals("lt")) {
                  ch = 60;
               } else if(var9.equals("gt")) {
                  ch = 62;
               } else if(var9.equals("nbsp")) {
                  ch = 160;
               } else if(var9.equals("shy")) {
                  ch = 173;
               } else if(var9.equals("times")) {
                  ch = 215;
               } else if(var9.equals("euro")) {
                  ch = 8364;
               } else if(var9.equals("copy")) {
                  ch = 169;
               } else {
                  if(!var9.equals("reg")) {
                     if(var9.startsWith("img=")) {
                        try {
                           imgID = wm.a(var9.substring(4), (byte)117);
                           qi var15 = r[imgID];
                           int var12 = w != null? w[imgID]:var15.j;
                           if(G == 256) {
                              var15.b(var2, var3 + n - var12);
                           } else {
                              var15.a(var2, var3 + n - var12, G);
                           }

                           var2 += var15.a;
                           var5 = 0;
                        } catch (Exception ignored) {
						}
                     } else {
						 c(var9);
                     }
                     continue;
                  }

                  ch = 174;
               }
            }

            if(var4 == -1) {
               ch = (char)(ej.a(ch, -8213) & 255);
               if(u != null && var5 != 0) {
                  var2 += u[(var5 << 8) + ch];
               }

               int var14 = p[ch];
               imgID = v[ch];
               int var11 = var2;
               if(ch != 32) {
                  if(G == 256) {
                     if(E != -1) {
						 a(ch, var2 + o[ch] + 1, var3 + q[ch] + 1, var14, imgID, E, true);
                     }

					  a(ch, var2 + o[ch], var3 + q[ch], var14, imgID, K, false);
                  } else {
                     if(E != -1) {
						 a(ch, var2 + o[ch] + 1, var3 + q[ch] + 1, var14, imgID, E, G, true);
                     }

					  a(ch, var2 + o[ch], var3 + q[ch], var14, imgID, K, G, false);
                  }
               } else if(F > 0) {
                  C += F;
                  var2 += C >> 8;
                  C &= 255;
               }

               var2 += y[ch];
               if(B != -1) {
                  cd.d(var11, var3 + (int)((double) n * 0.7D), var2 - var11, B);
               }

               if(J != -1) {
                  cd.d(var11, var3 + n + 1, var2 - var11, J);
               }

               var5 = ch;
            }
         }
      }

   }

   abstract void a(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7);

   final void b(String var1, int var2, int var3, int var4, int var5) {
      if(var1 != null) {
		  b(var4, var5);
		  a(var1, var2 - a(var1), var3);
      }
   }

   abstract void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8);

   final void c(String text, int var2, int var3, int var4, int var5) {
      if(text != null) {
		  b(var4, var5);
		  a(text, var2 - a(text) / 2, var3);
      }
   }

   static final String b(String var0) {
      int var1 = var0.length();
      int var2 = 0;

      for(int var3 = 0; var3 < var1; ++var3) {
         char var4 = var0.charAt(var3);
         if(var4 == 60 || var4 == 62) {
            var2 += 3;
         }
      }

      StringBuffer var7 = new StringBuffer(var1 + var2);

      for(int var5 = 0; var5 < var1; ++var5) {
         char var6 = var0.charAt(var5);
         if(var6 == 60) {
            var7.append("<lt>");
         } else if(var6 == 62) {
            var7.append("<gt>");
         } else {
            var7.append(var6);
         }
      }

      return var7.toString();
   }

   final int c(String var1, int var2) {
      return a(var1, new int[]{var2}, D);
   }

   final void b(String var1, int var2, int var3, int var4, int var5, int var6) {
      if(var1 != null) {
		  a(var4, var5, var6);
		  a(var1, var2 - a(var1), var3);
      }
   }

   final int a(String str, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
      if(str == null) {
         return 0;
      } else {
		  a(var6, var7, var8);
         if(var11 == 0) {
            var11 = n;
         }

         int[] var12 = new int[]{var4};
         if(var5 < t + s + var11 && var5 < var11 + var11) {
            var12 = null;
         }

         int var13 = a(str, var12, D);
         if(var10 == 3 && var13 == 1) {
            var10 = 1;
         }

         int var14;
         int var15;
         if(var10 == 0) {
            var14 = var3 + t;
         } else if(var10 == 1) {
            var14 = var3 + t + (var5 - t - s - (var13 - 1) * var11) / 2;
         } else if(var10 == 2) {
            var14 = var3 + var5 - s - (var13 - 1) * var11;
         } else {
            var15 = (var5 - t - s - (var13 - 1) * var11) / (var13 + 1);
            if(var15 < 0) {
               var15 = 0;
            }

            var14 = var3 + t + var15;
            var11 += var15;
         }

         for(var15 = 0; var15 < var13; ++var15) {
            if(var9 == 0) {
				a(D[var15], var2, var14);
            } else if(var9 == 1) {
				a(D[var15], var2 + (var4 - a(D[var15])) / 2, var14);
            } else if(var9 == 2) {
				a(D[var15], var2 + var4 - a(D[var15]), var14);
            } else if(var15 == var13 - 1) {
				a(D[var15], var2, var14);
            } else {
				b(D[var15], var4);
				a(D[var15], var2, var14);
               F = 0;
            }

            var14 += var11;
         }

         return var13;
      }
   }

   bk(byte[] var1, int[] var2, int[] var3, int[] var4, int[] var5) {
	   o = var2;
	   q = var3;
	   p = var4;
	   v = var5;
	   a(var1);
      int var6 = Integer.MAX_VALUE;
      int var7 = Integer.MIN_VALUE;

      for(int var8 = 0; var8 < 256; ++var8) {
         if(q[var8] < var6 && v[var8] != 0) {
            var6 = q[var8];
         }

         if(q[var8] + v[var8] > var7) {
            var7 = q[var8] + v[var8];
         }
      }

	   t = n - var6;
	   s = var7 - n;
	   x = n - q[88];
   }

   protected bk() {}

   final void c(String var1, int var2, int var3, int var4, int var5, int var6) {
      if(var1 != null) {
		  a(var4, var5, var6);
		  a(var1, var2, var3);
      }
   }

   private void c(String var1) {
      try {
         if(var1.startsWith("col=")) {
            K = tg.a(-125, var1.substring(4), 16);
            return;
         }

         if(var1.equals("/col")) {
            K = H;
            return;
         }

         if(var1.startsWith("trans=")) {
            G = wm.a(var1.substring(6), (byte)117);
            return;
         }

         if(var1.equals("/trans")) {
            G = A;
            return;
         }

         if(var1.startsWith("str=")) {
            B = tg.a(-128, var1.substring(4), 16);
            return;
         }

         if(var1.equals("str")) {
            B = 8388608;
            return;
         }

         if(var1.equals("/str")) {
            B = -1;
            return;
         }

         if(var1.startsWith("u=")) {
            J = tg.a(-128, var1.substring(2), 16);
            return;
         }

         if(var1.equals("u")) {
            J = 0;
            return;
         }

         if(var1.equals("/u")) {
            J = -1;
            return;
         }

         if(var1.startsWith("shad=")) {
            E = tg.a(-123, var1.substring(5), 16);
            return;
         }

         if(var1.equals("shad")) {
            E = 0;
            return;
         }

         if(var1.equals("/shad")) {
            E = I;
            return;
         }

         if(var1.equals("br")) {
			 a(H, I, A);
            return;
         }
      } catch (Exception ignored) {
	  }

   }

   final int a(String var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
      return a(var1, var2, var3, var4, var5, var6, var7, 256, var8, var9, var10);
   }

   static final String a(bk var0, String var1, int var2) {
      if(var0.a(var1) <= var2) {
         return var1;
      } else {
         int var3 = var0.a("...");
         int var4 = var2 - var3;
         int var5 = 0;

         for(int var6 = 0; var6 < var1.length(); ++var6) {
            int var7 = var0.a(var1.charAt(var6));
            if(var5 + var7 > var4) {
               return var1.substring(0, var6 - 1) + "...";
            }

            var5 += var7;
         }

         return null;
      }
   }

   final int b(String var1, int var2, int var3) {
      if(var3 == 0) {
         var3 = n;
      }

      int var4 = a(var1, new int[]{var2}, D);
      int var5 = (var4 - 1) * var3;
      return t + var5 + s;
   }

   private void b(int var1, int var2) {
      B = -1;
      J = -1;
      I = var2;
      E = var2;
      H = var1;
      K = var1;
      A = 256;
      G = 256;
      F = 0;
      C = 0;
   }

}
