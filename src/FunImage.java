import java.awt.*;
import java.awt.image.PixelGrabber;

class FunImage extends al {

	int[] pixels;

	FunImage(int oWidth, int oHeight, int var3, int var4, int width, int height, int[] pixels) {
		super.originalWidth = oWidth;
		super.originalHeight = oHeight;
		super.r = var3;
		super.v = var4;
		super.width = width;
		super.height = height;
		this.pixels = pixels;
	}

	FunImage(int width, int height) {
		pixels = new int[width * height];
		super.width = super.originalWidth = width;
		super.height = super.originalHeight = height;
		super.r = super.v = 0;
	}

	FunImage(byte[] imageData, Component component) {
		try {
			Image image = Toolkit.getDefaultToolkit().createImage(imageData);
			MediaTracker tracker = new MediaTracker(component);
			tracker.addImage(image, 0);
			tracker.waitForAll();
			super.width = image.getWidth(component);
			super.height = image.getHeight(component);
			super.originalWidth = super.width;
			super.originalHeight = super.height;
			super.r = 0;
			super.v = 0;
			pixels = new int[super.width * super.height];
			PixelGrabber var5 = new PixelGrabber(image, 0, 0, super.width, super.height, pixels, 0, super.width);
			var5.grabPixels();
		} catch (InterruptedException ignored) {
		}
	}

	final void a(int var1, int var2, int var3, int var4) {
		int var5 = super.originalWidth << 3;
		int var6 = super.originalHeight << 3;
		var1 = (var1 << 4) + (var5 & 15);
		var2 = (var2 << 4) + (var6 & 15);
		a(var5, var6, var1, var2, var3, var4);
	}

	final void b(int var1, int var2, int var3, int var4) {
		if (var3 <= super.originalWidth && var4 <= super.originalHeight) {
			int var5 = var1 + super.r * var3 / super.originalWidth;
			int var6 = var1 + ((super.r + super.width) * var3 + super.originalWidth - 1) / super.originalWidth;
			int var7 = var2 + super.v * var4 / super.originalHeight;
			int var8 = var2 + ((super.v + super.height) * var4 + super.originalHeight - 1) / super.originalHeight;
			if (var5 < cd.l) {
				var5 = cd.l;
			}

			if (var6 > cd.j) {
				var6 = cd.j;
			}

			if (var7 < cd.g) {
				var7 = cd.g;
			}

			if (var8 > cd.e) {
				var8 = cd.e;
			}

			if (var5 < var6 && var7 < var8) {
				int var9 = var7 * cd.width + var5;
				int var10 = cd.width - (var6 - var5);

				for (int var11 = var7; var11 < var8; ++var11) {
					for (int var12 = var5; var12 < var6; ++var12) {
						int var13 = var12 - var1 << 4;
						int var14 = var11 - var2 << 4;
						int var15 = var13 * super.originalWidth / var3 - (super.r << 4);
						int var16 = (var13 + 16) * super.originalWidth / var3 - (super.r << 4);
						int var17 = var14 * super.originalHeight / var4 - (super.v << 4);
						int var18 = (var14 + 16) * super.originalHeight / var4 - (super.v << 4);
						int var19 = (var16 - var15) * (var18 - var17);
						if (var19 != 0) {
							if (var15 < 0) {
								var15 = 0;
							}

							if (var16 > super.width << 4) {
								var16 = super.width << 4;
							}

							if (var17 < 0) {
								var17 = 0;
							}

							if (var18 > super.height << 4) {
								var18 = super.height << 4;
							}

							--var16;
							--var18;
							int var20 = 16 - (var15 & 15);
							int var21 = (var16 & 15) + 1;
							int var22 = 16 - (var17 & 15);
							int var23 = (var18 & 15) + 1;
							var15 >>= 4;
							var16 >>= 4;
							var17 >>= 4;
							var18 >>= 4;
							int var24 = 0;
							int var25 = 0;
							int var26 = 0;
							int var27 = 0;
							int var28 = cd.pixels[var9];

							int var30;
							for (int var29 = var17; var29 <= var18; ++var29) {
								var30 = 16;
								if (var29 == var17) {
									var30 = var22;
								}

								if (var29 == var18) {
									var30 = var23;
								}

								for (int var31 = var15; var31 <= var16; ++var31) {
									int var32 = pixels[var29 * super.width + var31];
									if (var32 == 0) {
										var32 = var28;
									}

									int var33;
									if (var31 == var15) {
										var33 = var30 * var20;
									} else if (var31 == var16) {
										var33 = var30 * var21;
									} else {
										var33 = var30 << 4;
									}

									var27 += var33;
									var24 += (var32 >> 16 & 255) * var33;
									var25 += (var32 >> 8 & 255) * var33;
									var26 += (var32 & 255) * var33;
								}
							}

							if (var27 < var19) {
								var30 = var19 - var27;
								var24 += (var28 >> 16 & 255) * var30;
								var25 += (var28 >> 8 & 255) * var30;
								var26 += (var28 & 255) * var30;
							}

							var30 = (var24 / var19 << 16) + (var25 / var19 << 8) + var26 / var19;
							if (var30 == 0) {
								var30 = 1;
							}

							cd.pixels[var9] = var30;
							++var9;
						}
					}

					var9 += var10;
				}

			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	final FunImage a() {
		FunImage image = new FunImage(super.width, super.height);
		image.originalWidth = super.originalWidth;
		image.originalHeight = super.originalHeight;
		image.r = super.originalWidth - super.width - super.r;
		image.v = super.v;

		for (int h = 0; h < super.height; ++h) {
			for (int w = 0; w < super.width; ++w) {
				image.pixels[h * super.width + w] = pixels[h * super.width + super.width - 1 - w];
			}
		}

		return image;
	}

	private static void a(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
		int var13 = var3;

		for (int var14 = -var8; var14 < 0; ++var14) {
			int var15 = (var4 >> 16) * var11;

			for (int var16 = -var7; var16 < 0; ++var16) {
				var2 = var1[(var3 >> 16) + var15];
				if (var2 != 0) {
					int var17 = (var2 & 16711935) * var12 & -16711936;
					int var18 = (var2 & '\uff00') * var12 & 16711680;
					var0[var5++] = (var17 | var18) >>> 8;
				} else {
					++var5;
				}

				var3 += var9;
			}

			var4 += var10;
			var3 = var13;
			var5 += var6;
		}

	}

	private static void a(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
		int var9 = -(var5 >> 2);
		var5 = -(var5 & 3);

		for (int var10 = -var6; var10 < 0; ++var10) {
			for (int var11 = var9; var11 < 0; ++var11) {
				if (var1[var3++] != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}

				if (var1[var3++] != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}

				if (var1[var3++] != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}

				if (var1[var3++] != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}
			}

			for (int var12 = var5; var12 < 0; ++var12) {
				if (var1[var3++] != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}
			}

			var4 += var7;
			var3 += var8;
		}

	}

	final void b() {
		int[] pixels = new int[super.width * super.height];
		int i = 0;

		int y;
		for (int x = 0; x < super.width; ++x) {
			for (y = super.height - 1; y >= 0; --y) {
				pixels[i++] = this.pixels[x + y * super.width];
			}
		}

		this.pixels = pixels;
		y = super.v;
		super.v = super.r;
		super.r = super.originalHeight - super.height - y;
		y = super.height;
		super.height = super.width;
		super.width = y;
		y = super.originalHeight;
		super.originalHeight = super.originalWidth;
		super.originalWidth = y;
	}

	private static void a(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
		int var12 = var11 & 16711935;
		int var13 = var11 >> 8 & 255;

		for (var6 = -var8; var6 < 0; ++var6) {
			for (var5 = -var7; var5 < 0; ++var5) {
				var2 = var1[var3++];
				if (var2 != 0) {
					if (var2 >> 8 == (var2)) {
						var2 &= 255;
						var0[var4++] = (var2 * var12 >> 8 & 16711934) + (var2 * var13 & '\uff00') + 1;
					} else {
						var0[var4++] = var2;
					}
				} else {
					++var4;
				}
			}

			var4 += var9;
			var3 += var10;
		}

	}

	private static void a(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
		for (int var8 = -var5; var8 < 0; ++var8) {
			int var9;
			for (var9 = var3 + var4 - 3; var3 < var9; var0[var3++] = var1[var2++]) {
				var0[var3++] = var1[var2++];
				var0[var3++] = var1[var2++];
				var0[var3++] = var1[var2++];
			}

			for (var9 += 3; var3 < var9; var0[var3++] = var1[var2++]) {
			}

			var3 += var6;
			var2 += var7;
		}

	}

	final void a(int var1, int var2, int var3) {
		var1 += super.r;
		var2 += super.v;
		int var4 = var1 + var2 * cd.width;
		int var5 = 0;
		int var6 = super.height;
		int var7 = super.width;
		int var8 = cd.width - var7;
		int var9 = 0;
		int var10;
		if (var2 < cd.g) {
			var10 = cd.g - var2;
			var6 -= var10;
			var2 = cd.g;
			var5 += var10 * var7;
			var4 += var10 * cd.width;
		}

		if (var2 + var6 > cd.e) {
			var6 -= var2 + var6 - cd.e;
		}

		if (var1 < cd.l) {
			var10 = cd.l - var1;
			var7 -= var10;
			var1 = cd.l;
			var5 += var10;
			var4 += var10;
			var9 += var10;
			var8 += var10;
		}

		if (var1 + var7 > cd.j) {
			var10 = var1 + var7 - cd.j;
			var7 -= var10;
			var9 += var10;
			var8 += var10;
		}

		if (var7 > 0 && var6 > 0) {
			a(cd.pixels, pixels, var3, var5, var4, var7, var6, var8, var9);
		}
	}

	void b(int var1, int var2) {
		var1 += super.r >> 1;
		var2 += super.v >> 1;
		int var3 = var1 < cd.l ? cd.l - var1 << 1 : 0;
		int var4 = var1 + (super.width >> 1) > cd.j ? cd.j - var1 << 1 : super.width;
		int var5 = var2 < cd.g ? cd.g - var2 << 1 : 0;
		int var6 = var2 + (super.height >> 1) > cd.e ? cd.e - var2 << 1 : super.height;
		a(pixels, var5 * super.width + var3, (var2 + (var5 >> 1)) * cd.width + var1 + (var3 >> 1), (super.width << 1) - (var4 - var3) + (super.width & 1),
				cd.width - (var4 - var3 >> 1), super.width, var4 - var3 >> 1, var6 - var5 >> 1);
	}

	final FunImage copy() {
		FunImage var1 = new FunImage(super.width, super.height);
		var1.originalWidth = super.originalWidth;
		var1.originalHeight = super.originalHeight;
		var1.r = super.r;
		var1.v = super.v;
		int var2 = pixels.length;
		System.arraycopy(pixels, 0, var1.pixels, 0, var2);
		return var1;
	}

	private static void b(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
		int var13 = var3;

		for (int var14 = -var8; var14 < 0; ++var14) {
			int var15 = (var4 >> 16) * var11;

			for (int var16 = -var7; var16 < 0; ++var16) {
				var2 = var1[(var3 >> 16) + var15];
				if (var2 != 0) {
					var0[var5++] = var12;
				} else {
					++var5;
				}

				var3 += var9;
			}

			var4 += var10;
			var3 = var13;
			var5 += var6;
		}

	}

	void b(int var1, int var2, int var3) {
		var1 += super.r;
		var2 += super.v;
		int var4 = var1 + var2 * cd.width;
		int var5 = 0;
		int var6 = super.height;
		int var7 = super.width;
		int var8 = cd.width - var7;
		int var9 = 0;
		int var10;
		if (var2 < cd.g) {
			var10 = cd.g - var2;
			var6 -= var10;
			var2 = cd.g;
			var5 += var10 * var7;
			var4 += var10 * cd.width;
		}

		if (var2 + var6 > cd.e) {
			var6 -= var2 + var6 - cd.e;
		}

		if (var1 < cd.l) {
			var10 = cd.l - var1;
			var7 -= var10;
			var1 = cd.l;
			var5 += var10;
			var4 += var10;
			var9 += var10;
			var8 += var10;
		}

		if (var1 + var7 > cd.j) {
			var10 = var1 + var7 - cd.j;
			var7 -= var10;
			var9 += var10;
			var8 += var10;
		}

		if (var7 > 0 && var6 > 0) {
			b(cd.pixels, pixels, 0, var5, var4, var7, var6, var8, var9, var3);
		}
	}

	final void c(int var1, int var2, int var3, int var4) {
		if (var3 <= super.originalWidth && var4 <= super.originalHeight) {
			int var5 = var1 + super.r * var3 / super.originalWidth;
			int var6 = var1 + ((super.r + super.width) * var3 + super.originalWidth - 1) / super.originalWidth;
			int var7 = var2 + super.v * var4 / super.originalHeight;
			int var8 = var2 + ((super.v + super.height) * var4 + super.originalHeight - 1) / super.originalHeight;
			if (var5 < cd.l) {
				var5 = cd.l;
			}

			if (var6 > cd.j) {
				var6 = cd.j;
			}

			if (var7 < cd.g) {
				var7 = cd.g;
			}

			if (var8 > cd.e) {
				var8 = cd.e;
			}

			if (var5 < var6 && var7 < var8) {
				int var9 = var7 * cd.width + var5;
				int var10 = cd.width - (var6 - var5);

				for (int var11 = var7; var11 < var8; ++var11) {
					for (int var12 = var5; var12 < var6; ++var12) {
						int var13 = var12 - var1 << 4;
						int var14 = var11 - var2 << 4;
						int var15 = var13 * super.originalWidth / var3 - (super.r << 4);
						int var16 = (var13 + 16) * super.originalWidth / var3 - (super.r << 4);
						int var17 = var14 * super.originalHeight / var4 - (super.v << 4);
						int var18 = (var14 + 16) * super.originalHeight / var4 - (super.v << 4);
						int var19 = (var16 - var15) * (var18 - var17) >> 1;
						if (var19 != 0) {
							if (var15 < 0) {
								var15 = 0;
							}

							if (var16 > super.width << 4) {
								var16 = super.width << 4;
							}

							if (var17 < 0) {
								var17 = 0;
							}

							if (var18 > super.height << 4) {
								var18 = super.height << 4;
							}

							--var16;
							--var18;
							int var20 = 16 - (var15 & 15);
							int var21 = (var16 & 15) + 1;
							int var22 = 16 - (var17 & 15);
							int var23 = (var18 & 15) + 1;
							var15 >>= 4;
							var16 >>= 4;
							var17 >>= 4;
							var18 >>= 4;
							int var24 = 0;
							int var25 = 0;
							int var26 = 0;
							int var27 = 0;

							int var29;
							for (int var28 = var17; var28 <= var18; ++var28) {
								var29 = 16;
								if (var28 == var17) {
									var29 = var22;
								}

								if (var28 == var18) {
									var29 = var23;
								}

								for (int var30 = var15; var30 <= var16; ++var30) {
									int var31 = pixels[var28 * super.width + var30];
									if (var31 != 0) {
										int var32;
										if (var30 == var15) {
											var32 = var29 * var20;
										} else if (var30 == var16) {
											var32 = var29 * var21;
										} else {
											var32 = var29 << 4;
										}

										var27 += var32;
										var24 += (var31 >> 16 & 255) * var32;
										var25 += (var31 >> 8 & 255) * var32;
										var26 += (var31 & 255) * var32;
									}
								}
							}

							if (var27 >= var19) {
								var29 = (var24 / var27 << 16) + (var25 / var27 << 8) + var26 / var27;
								if (var29 == 0) {
									var29 = 1;
								}

								cd.pixels[var9] = var29;
							}

							++var9;
						}
					}

					var9 += var10;
				}

			}
		} else {
			throw new IllegalArgumentException();
		}
	}



	final void a(int var1, int var2, int var3, int var4, int var5, int var6) {
		if (var6 != 0) {
			var1 -= super.r << 4;
			var2 -= super.v << 4;
			double var7 = (double) (var5) * 9.587379924285257E-5D;
			int var9 = (int) Math.floor(Math.sin(var7) * (double) var6 + 0.5D);
			int var10 = (int) Math.floor(Math.cos(var7) * (double) var6 + 0.5D);
			int var11 = -var1 * var10 + -var2 * var9;
			int var12 = var1 * var9 + -var2 * var10;
			int var13 = ((super.width << 4) - var1) * var10 + -var2 * var9;
			int var14 = -((super.width << 4) - var1) * var9 + -var2 * var10;
			int var15 = -var1 * var10 + ((super.height << 4) - var2) * var9;
			int var16 = var1 * var9 + ((super.height << 4) - var2) * var10;
			int var17 = ((super.width << 4) - var1) * var10 + ((super.height << 4) - var2) * var9;
			int var18 = -((super.width << 4) - var1) * var9 + ((super.height << 4) - var2) * var10;
			int var19;
			int var20;
			if (var11 < var13) {
				var19 = var11;
				var20 = var13;
			} else {
				var19 = var13;
				var20 = var11;
			}

			if (var15 < var19) {
				var19 = var15;
			}

			if (var17 < var19) {
				var19 = var17;
			}

			if (var15 > var20) {
				var20 = var15;
			}

			if (var17 > var20) {
				var20 = var17;
			}

			int var21;
			int var22;
			if (var12 < var14) {
				var21 = var12;
				var22 = var14;
			} else {
				var21 = var14;
				var22 = var12;
			}

			if (var16 < var21) {
				var21 = var16;
			}

			if (var18 < var21) {
				var21 = var18;
			}

			if (var16 > var22) {
				var22 = var16;
			}

			if (var18 > var22) {
				var22 = var18;
			}

			var19 >>= 12;
			var20 = var20 + 4095 >> 12;
			var21 >>= 12;
			var22 = var22 + 4095 >> 12;
			var19 += var3;
			var20 += var3;
			var21 += var4;
			var22 += var4;
			var19 >>= 4;
			var20 = var20 + 15 >> 4;
			var21 >>= 4;
			var22 = var22 + 15 >> 4;
			if (var19 < cd.l) {
				var19 = cd.l;
			}

			if (var20 > cd.j) {
				var20 = cd.j;
			}

			if (var21 < cd.g) {
				var21 = cd.g;
			}

			if (var22 > cd.e) {
				var22 = cd.e;
			}

			var20 = var19 - var20;
			if (var20 < 0) {
				var22 = var21 - var22;
				if (var22 < 0) {
					int var23 = var21 * cd.width + var19;
					int var24 = cd.width + var20;
					double var25 = 1.6777216E7D / (double) var6;
					int var27 = (int) Math.floor(Math.sin(var7) * var25 + 0.5D);
					int var28 = (int) Math.floor(Math.cos(var7) * var25 + 0.5D);
					int var29 = (var19 << 4) + 8 - var3;
					int var30 = (var21 << 4) + 8 - var4;
					int var31 = (var1 << 8) - 2048 - (var30 * var27 >> 4);
					int var32 = (var2 << 8) - 2048 + (var30 * var28 >> 4);
					int var34;
					int var35;
					int var33;
					int var38;
					int var39;
					int var36;
					int var37;
					if (var28 < 0) {
						if (var27 < 0) {
							for (var36 = var22; var36 < 0; var23 += var24) {
								label258:
								{
									var37 = var31 + (var29 * var28 >> 4);
									var38 = var32 + (var29 * var27 >> 4);
									var39 = var20;
									if ((var35 = var37 - (super.width << 12)) >= 0) {
										if (var28 == 0) {
											var23 -= var20;
											break label258;
										}

										var35 = (var28 - var35) / var28;
										var39 = var20 + var35;
										var37 += var28 * var35;
										var38 += var27 * var35;
										var23 += var35;
									}

									if ((var35 = var38 - (super.height << 12)) >= 0) {
										if (var27 == 0) {
											var23 -= var39;
											break label258;
										}

										var35 = (var27 - var35) / var27;
										var39 += var35;
										var37 += var28 * var35;
										var38 += var27 * var35;
										var23 += var35;
									}

									while (var39 < 0 && var37 >= -4096 && var38 >= -4096) {
										var33 = var37 >> 12;
										var34 = var38 >> 12;
										a(var23, var33, var34, var37, var38);
										++var39;
										var37 += var28;
										var38 += var27;
										++var23;
									}

									var23 -= var39;
								}

								++var36;
								var31 -= var27;
								var32 += var28;
							}

						} else {
							for (var36 = var22; var36 < 0; var23 += var24) {
								label261:
								{
									var37 = var31 + (var29 * var28 >> 4);
									var38 = var32 + (var29 * var27 >> 4);
									var39 = var20;
									if ((var35 = var37 - (super.width << 12)) >= 0) {
										if (var28 == 0) {
											var23 -= var20;
											break label261;
										}

										var35 = (var28 - var35) / var28;
										var39 = var20 + var35;
										var37 += var28 * var35;
										var38 += var27 * var35;
										var23 += var35;
									}

									if ((var35 = var38 + 4096) < 0) {
										if (var27 == 0) {
											var23 -= var39;
											break label261;
										}

										var35 = (var27 - 1 - var35) / var27;
										var39 += var35;
										var37 += var28 * var35;
										var38 += var27 * var35;
										var23 += var35;
									}

									while (var39 < 0 && var37 >= -4096 && (var34 = var38 >> 12) < super.height) {
										var33 = var37 >> 12;
										a(var23, var33, var34, var37, var38);
										++var39;
										var37 += var28;
										var38 += var27;
										++var23;
									}

									var23 -= var39;
								}

								++var36;
								var31 -= var27;
								var32 += var28;
							}

						}
					} else if (var27 < 0) {
						for (var36 = var22; var36 < 0; var23 += var24) {
							label264:
							{
								var37 = var31 + (var29 * var28 >> 4);
								var38 = var32 + (var29 * var27 >> 4);
								var39 = var20;
								if ((var35 = var37 + 4096) < 0) {
									if (var28 == 0) {
										var23 -= var20;
										break label264;
									}

									var35 = (var28 - 1 - var35) / var28;
									var39 = var20 + var35;
									var37 += var28 * var35;
									var38 += var27 * var35;
									var23 += var35;
								}

								if ((var35 = var38 - (super.height << 12)) >= 0) {
									if (var27 == 0) {
										var23 -= var39;
										break label264;
									}

									var35 = (var27 - var35) / var27;
									var39 += var35;
									var37 += var28 * var35;
									var38 += var27 * var35;
									var23 += var35;
								}

								while (var39 < 0 && var38 >= -4096 && (var33 = var37 >> 12) < super.width) {
									var34 = var38 >> 12;
									a(var23, var33, var34, var37, var38);
									++var39;
									var37 += var28;
									var38 += var27;
									++var23;
								}

								var23 -= var39;
							}

							++var36;
							var31 -= var27;
							var32 += var28;
						}

					} else {
						for (var36 = var22; var36 < 0; var23 += var24) {
							label267:
							{
								var37 = var31 + (var29 * var28 >> 4);
								var38 = var32 + (var29 * var27 >> 4);
								var39 = var20;
								if ((var35 = var37 + 4096) < 0) {
									if (var28 == 0) {
										var23 -= var20;
										break label267;
									}

									var35 = (var28 - 1 - var35) / var28;
									var39 = var20 + var35;
									var37 += var28 * var35;
									var38 += var27 * var35;
									var23 += var35;
								}

								if ((var35 = var38 + 4096) < 0) {
									if (var27 == 0) {
										var23 -= var39;
										break label267;
									}

									var35 = (var27 - 1 - var35) / var27;
									var39 += var35;
									var37 += var28 * var35;
									var38 += var27 * var35;
									var23 += var35;
								}

								while (var39 < 0 && (var33 = var37 >> 12) < super.width && (var34 = var38 >> 12) < super.height) {
									a(var23, var33, var34, var37, var38);
									++var39;
									var37 += var28;
									var38 += var27;
									++var23;
								}

								var23 -= var39;
							}

							++var36;
							var31 -= var27;
							var32 += var28;
						}

					}
				}
			}
		}
	}

	private static void a(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		for (int var10 = -var6; var10 < 0; ++var10) {
			for (int var11 = -var5; var11 < 0; ++var11) {
				var2 = var1[var3++];
				if (var2 != 0) {
					int var12 = (var2 & 16711935) * var9 & -16711936;
					int var13 = (var2 & '\uff00') * var9 & 16711680;
					var0[var4++] = (var12 | var13) >>> 8;
				} else {
					++var4;
				}
			}

			var4 += var7;
			var3 += var8;
		}

	}

	private final void a(int var1, int var2, int var3, int var4, int var5) {
		int var6 = var3 * super.width + var2;
		var4 &= 4095;
		var5 &= 4095;
		int var7;
		int var8;
		int var11;
		int var12;
		if (var3 >= 0) {
			if (var2 >= 0) {
				var7 = pixels[var6];
				var11 = var7 != 0 ? (4096 - var4) * (4096 - var5) : 0;
			} else {
				var11 = 0;
				var7 = 0;
			}

			if (var2 < super.width - 1) {
				var8 = pixels[var6 + 1];
				var12 = var8 != 0 ? var4 * (4096 - var5) : 0;
			} else {
				var12 = 0;
				var8 = 0;
			}
		} else {
			var12 = 0;
			var11 = 0;
			var8 = 0;
			var7 = 0;
		}

		int var9;
		int var10;
		int var13;
		int var14;
		if (var3 < super.height - 1) {
			if (var2 >= 0) {
				var9 = pixels[var6 + super.width];
				var13 = var9 != 0 ? (4096 - var4) * var5 : 0;
			} else {
				var13 = 0;
				var9 = 0;
			}

			if (var2 < super.width - 1) {
				var10 = pixels[var6 + super.width + 1];
				var14 = var10 != 0 ? var4 * var5 : 0;
			} else {
				var14 = 0;
				var10 = 0;
			}
		} else {
			var14 = 0;
			var13 = 0;
			var10 = 0;
			var9 = 0;
		}

		var11 >>= 16;
		var12 >>= 16;
		var13 >>= 16;
		var14 >>= 16;
		int var15 = var11 + var12 + var13 + var14;
		int var17;
		int var16;
		int var18;
		if (var15 >= 256) {
			var16 = (var7 & 16711935) * var11 + (var8 & 16711935) * var12;
			var16 += (var9 & 16711935) * var13 + (var10 & 16711935) * var14;
			var17 = (var7 & '\uff00') * var11 + (var8 & '\uff00') * var12;
			var17 += (var9 & '\uff00') * var13 + (var10 & '\uff00') * var14;
			var18 = (var16 >>> 8 & 16711935) + (var17 >>> 8 & '\uff00');
			if (var18 == 0) {
				var18 = 1;
			}

			cd.pixels[var1] = var18;
		} else {
			if (var15 >= 128) {
				var16 = (var7 & 16711935) * var11 + (var8 & 16711935) * var12;
				var16 += (var9 & 16711935) * var13 + (var10 & 16711935) * var14;
				var17 = (var7 & '\uff00') * var11 + (var8 & '\uff00') * var12;
				var17 += (var9 & '\uff00') * var13 + (var10 & '\uff00') * var14;
				var18 = ((var16 >>> 16) / var15 << 16) + (var17 / var15 & '\uff00') + (var16) / var15;
				if (var18 == 0) {
					var18 = 1;
				}

				cd.pixels[var1] = var18;
			}

		}
	}

	private static void c(int[] buf, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
		int var13 = 256 - var12;
		int var14 = var3;

		for (int var15 = -var8; var15 < 0; ++var15) {
			int var16 = (var4 >> 16) * var11;

			for (int var17 = -var7; var17 < 0; ++var17) {
				var2 = var1[(var3 >> 16) + var16];
				if (var2 != 0) {
					int var18 = buf[var5];
					buf[var5++] = ((var2 & 16711935) * var12 + (var18 & 16711935) * var13 & -16711936) + ((var2 & '\uff00') * var12 + (var18 & '\uff00') * var13 & 16711680) >> 8;
				} else {
					++var5;
				}

				var3 += var9;
			}

			var4 += var10;
			var3 = var14;
			var5 += var6;
		}

	}

	private static void a(int var0, int var1, int var2, int[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13) {
		for (var8 = -var10; var8 < 0; ++var8) {
			for (var6 = -var9; var6 < 0; ++var6) {
				var0 = var4[var5++];
				if (var0 != 0) {
					var1 = (var0 & 16711935) * var13;
					var0 = (var1 & -16711936) + (var0 * var13 - var1 & 16711680) >>> 8;
					var1 = var3[var7];
					var2 = var0 + var1;
					var0 = (var0 & 16711935) + (var1 & 16711935);
					var1 = (var0 & 16777472) + (var2 - var0 & 65536);
					var3[var7++] = var2 - var1 | var1 - (var1 >>> 8);
				} else {
					++var7;
				}
			}

			var7 += var11;
			var5 += var12;
		}

	}

	private static void b(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		int var10 = 256 - var9;

		for (int var11 = -var6; var11 < 0; ++var11) {
			for (int var12 = -var5; var12 < 0; ++var12) {
				var2 = var1[var3++];
				if (var2 != 0) {
					int var13 = var0[var4];
					var0[var4++] = ((var2 & 16711935) * var9 + (var13 & 16711935) * var10 & -16711936) + ((var2 & '\uff00') * var9 + (var13 & '\uff00') * var10 & 16711680) >> 8;
				} else {
					++var4;
				}
			}

			var4 += var7;
			var3 += var8;
		}

	}

	void c(int var1, int var2, int var3) {
		var1 += super.r;
		var2 += super.v;
		int var4 = var1 + var2 * cd.width;
		int var5 = 0;
		int var6 = super.height;
		int var7 = super.width;
		int var8 = cd.width - var7;
		int var9 = 0;
		int var10;
		if (var2 < cd.g) {
			var10 = cd.g - var2;
			var6 -= var10;
			var2 = cd.g;
			var5 += var10 * var7;
			var4 += var10 * cd.width;
		}

		if (var2 + var6 > cd.e) {
			var6 -= var2 + var6 - cd.e;
		}

		if (var1 < cd.l) {
			var10 = cd.l - var1;
			var7 -= var10;
			var1 = cd.l;
			var5 += var10;
			var4 += var10;
			var9 += var10;
			var8 += var10;
		}

		if (var1 + var7 > cd.j) {
			var10 = var1 + var7 - cd.j;
			var7 -= var10;
			var9 += var10;
			var8 += var10;
		}

		if (var7 > 0 && var6 > 0) {
			c(cd.pixels, pixels, 0, var5, var4, var7, var6, var8, var9, var3);
		}
	}

	void d(int var1, int var2, int var3) {
		var1 += super.r;
		var2 += super.v;
		int var4 = var1 + var2 * cd.width;
		int var5 = 0;
		int var6 = super.height;
		int var7 = super.width;
		int var8 = cd.width - var7;
		int var9 = 0;
		int var10;
		if (var2 < cd.g) {
			var10 = cd.g - var2;
			var6 -= var10;
			var2 = cd.g;
			var5 += var10 * var7;
			var4 += var10 * cd.width;
		}

		if (var2 + var6 > cd.e) {
			var6 -= var2 + var6 - cd.e;
		}

		if (var1 < cd.l) {
			var10 = cd.l - var1;
			var7 -= var10;
			var1 = cd.l;
			var5 += var10;
			var4 += var10;
			var9 += var10;
			var8 += var10;
		}

		if (var1 + var7 > cd.j) {
			var10 = var1 + var7 - cd.j;
			var7 -= var10;
			var9 += var10;
			var8 += var10;
		}

		if (var7 > 0 && var6 > 0) {
			if (var3 == 256) {
				a(0, 0, 0, cd.pixels, pixels, var5, 0, var4, 0, var7, var6, var8, var9);
			} else {
				a(0, 0, 0, cd.pixels, pixels, var5, 0, var4, 0, var7, var6, var8, var9, var3);
			}
		}
	}

	void c(int var1, int var2) {
		var1 += super.r;
		var2 += super.v;
		int var3 = var1 + var2 * cd.width;
		int var4 = 0;
		int var5 = super.height;
		int var6 = super.width;
		int var7 = cd.width - var6;
		int var8 = 0;
		int var9;
		if (var2 < cd.g) {
			var9 = cd.g - var2;
			var5 -= var9;
			var2 = cd.g;
			var4 += var9 * var6;
			var3 += var9 * cd.width;
		}

		if (var2 + var5 > cd.e) {
			var5 -= var2 + var5 - cd.e;
		}

		if (var1 < cd.l) {
			var9 = cd.l - var1;
			var6 -= var9;
			var1 = cd.l;
			var4 += var9;
			var3 += var9;
			var8 += var9;
			var7 += var9;
		}

		if (var1 + var6 > cd.j) {
			var9 = var1 + var6 - cd.j;
			var6 -= var9;
			var8 += var9;
			var7 += var9;
		}

		if (var6 > 0 && var5 > 0) {
			b(cd.pixels, pixels, 0, var4, var3, var6, var5, var7, var8);
		}
	}

	void d(int var1, int var2) {
		var1 += super.r;
		var2 += super.v;
		int var3 = var1 + var2 * cd.width;
		int var4 = 0;
		int var5 = super.height;
		int var6 = super.width;
		int var7 = cd.width - var6;
		int var8 = 0;
		int var9;
		if (var2 < cd.g) {
			var9 = cd.g - var2;
			var5 -= var9;
			var2 = cd.g;
			var4 += var9 * var6;
			var3 += var9 * cd.width;
		}

		if (var2 + var5 > cd.e) {
			var5 -= var2 + var5 - cd.e;
		}

		if (var1 < cd.l) {
			var9 = cd.l - var1;
			var6 -= var9;
			var1 = cd.l;
			var4 += var9;
			var3 += var9;
			var8 += var9;
			var7 += var9;
		}

		if (var1 + var6 > cd.j) {
			var9 = var1 + var6 - cd.j;
			var6 -= var9;
			var8 += var9;
			var7 += var9;
		}

		if (var6 > 0 && var5 > 0) {
			a(cd.pixels, pixels, var4, var3, var6, var5, var7, var8);
		}
	}

	private static void b(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
		int var12 = var3;

		for (int var13 = -var8; var13 < 0; ++var13) {
			int var14 = (var4 >> 16) * var11;

			for (int var15 = -var7; var15 < 0; ++var15) {
				var2 = var1[(var3 >> 16) + var14];
				if (var2 != 0) {
					var0[var5++] = var2;
				} else {
					++var5;
				}

				var3 += var9;
			}

			var4 += var10;
			var3 = var12;
			var5 += var6;
		}

	}

	final void d() {
		if (super.width != super.originalWidth || super.height != super.originalHeight) {
			int[] var1 = new int[super.originalWidth * super.originalHeight];

			for (int var2 = 0; var2 < super.height; ++var2) {
				for (int var3 = 0; var3 < super.width; ++var3) {
					var1[(var2 + super.v) * super.originalWidth + var3 + super.r] = pixels[var2 * super.width + var3];
				}
			}

			pixels = var1;
			super.width = super.originalWidth;
			super.height = super.originalHeight;
			super.r = 0;
			super.v = 0;
		}
	}

	final void e() {
		int var1;
		int var2;
		int var3;
		label107:
		for (var1 = super.height - 1; var1 >= 0; --var1) {
			var2 = var1 * super.width;

			for (var3 = 0; var3 < super.width; ++var3) {
				if (pixels[var2 + var3] != 0) {
					break label107;
				}
			}
		}

		int var4;
		label95:
		for (var2 = 0; var2 < var1; ++var2) {
			var3 = var2 * super.width;

			for (var4 = 0; var4 < super.width; ++var4) {
				if (pixels[var3 + var4] != 0) {
					break label95;
				}
			}
		}

		label83:
		for (var3 = super.width - 1; var3 >= 0; --var3) {
			for (var4 = var2; var4 <= var1; ++var4) {
				if (pixels[var4 * super.width + var3] != 0) {
					break label83;
				}
			}
		}

		int var5;
		label71:
		for (var4 = 0; var4 < var3; ++var4) {
			for (var5 = var2; var5 <= var1; ++var5) {
				if (pixels[var5 * super.width + var4] != 0) {
					break label71;
				}
			}
		}

		if (var4 != 0 || var3 != super.width - 1 || var2 != 0 || var1 != super.height - 1) {
			var5 = var3 + 1 - var4;
			int var6 = var1 + 1 - var2;
			int[] var7 = new int[var5 * var6];

			for (int var8 = 0; var8 < var6; ++var8) {
				for (int var9 = 0; var9 < var5; ++var9) {
					var7[var8 * var5 + var9] = pixels[(var8 + var2) * super.width + var9 + var4];
				}
			}

			pixels = var7;
			super.width = var5;
			super.height = var6;
			super.r += var4;
			super.v += var2;
		}
	}

	private static void c(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		int var10 = var9 >> 16 & 255;
		int var11 = var9 >> 8 & 255;
		int var12 = var9 & 255;
		int var13 = -(var5 >> 2);
		var5 = -(var5 & 3);
		int var14 = var13 + var13 + var13 + var13 + var5;

		for (int var15 = -var6; var15 < 0; ++var15) {
			for (int var16 = var14; var16 < 0; ++var16) {
				var2 = var1[var3++];
				if (var2 != 0) {
					int var17 = var2 >> 16 & 255;
					int var18 = var2 >> 8 & 255;
					int var19 = var2 & 255;
					if (var17 == var18 && var18 == var19) {
						if (var17 <= 128) {
							var0[var4++] = (var17 * var10 >> 7 << 16) + (var18 * var11 >> 7 << 8) + (var19 * var12 >> 7);
						} else {
							var0[var4++] = (var10 * (256 - var17) + 255 * (var17 - 128) >> 7 << 16) + (var11 * (256 - var18) + 255 * (var18 - 128) >> 7 << 8) + (var12 * (256 -
									var19) + 255 * (var19 - 128) >> 7);
						}
					} else {
						var0[var4++] = var2;
					}
				} else {
					++var4;
				}
			}

			var4 += var7;
			var3 += var8;
		}

	}

	private static void a(int[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
		for (int var8 = 0; var8 < var7; var2 += var4) {
			for (int var9 = 0; var9 < var6; var1 += 2) {
				int var11 = cd.pixels[var2] & 16711935;
				int var12 = cd.pixels[var2] & '\uff00';
				byte var13 = 0;
				byte var14 = 0;
				int var16;
				int var10;
				int var15;
				if ((var10 = var0[var1]) == 0) {
					var15 = var13 + var11;
					var16 = var14 + var12;
				} else {
					var15 = var13 + (var10 & 16711935);
					var16 = var14 + (var10 & '\uff00');
				}

				if ((var10 = var0[var1 + 1]) == 0) {
					var15 += var11;
					var16 += var12;
				} else {
					var15 += var10 & 16711935;
					var16 += var10 & '\uff00';
				}

				if ((var10 = var0[var1 + var5]) == 0) {
					var15 += var11;
					var16 += var12;
				} else {
					var15 += var10 & 16711935;
					var16 += var10 & '\uff00';
				}

				if ((var10 = var0[var1 + var5 + 1]) == 0) {
					var15 += var11;
					var16 += var12;
				} else {
					var15 += var10 & 16711935;
					var16 += var10 & '\uff00';
				}

				cd.pixels[var2++] = (var15 & 66847740 | var16 & 261120) >> 2;
				++var9;
			}

			++var8;
			var1 += var3;
		}

	}

	final void f() {
		cd.a(pixels, super.width, super.height);
	}

	void e(int var1, int var2, int var3) {
		if (var3 == 256) {
			c(var1, var2);
		} else {
			var1 += super.r;
			var2 += super.v;
			int var4 = var1 + var2 * cd.width;
			int var5 = 0;
			int height = super.height;
			int width = super.width;
			int var8 = cd.width - width;
			int var9 = 0;
			int var10;
			if (var2 < cd.g) {
				var10 = cd.g - var2;
				height -= var10;
				var2 = cd.g;
				var5 += var10 * width;
				var4 += var10 * cd.width;
			}

			if (var2 + height > cd.e) {
				height -= var2 + height - cd.e;
			}

			if (var1 < cd.l) {
				var10 = cd.l - var1;
				width -= var10;
				var1 = cd.l;
				var5 += var10;
				var4 += var10;
				var9 += var10;
				var8 += var10;
			}

			if (var1 + width > cd.j) {
				var10 = var1 + width - cd.j;
				width -= var10;
				var9 += var10;
				var8 += var10;
			}

			if (width > 0 && height > 0) {
				a(cd.pixels, pixels, 0, var5, var4, width, height, var8, var9, var3);
			}
		}
	}

	void b(int var1, int var2, int var3, int var4, int var5) {
		if (var3 > 0 && var4 > 0) {
			if (var5 == 256) {
				d(var1, var2, var3, var4);
			} else {
				int var6 = super.width;
				int var7 = super.height;
				int var8 = 0;
				int var9 = 0;
				int var10 = super.originalWidth;
				int var11 = super.originalHeight;
				int var12 = (var10 << 16) / var3;
				int var13 = (var11 << 16) / var4;
				int var14;
				if (super.r > 0) {
					var14 = ((super.r << 16) + var12 - 1) / var12;
					var1 += var14;
					var8 += var14 * var12 - (super.r << 16);
				}

				if (super.v > 0) {
					var14 = ((super.v << 16) + var13 - 1) / var13;
					var2 += var14;
					var9 += var14 * var13 - (super.v << 16);
				}

				if (var6 < var10) {
					var3 = ((var6 << 16) - var8 + var12 - 1) / var12;
				}

				if (var7 < var11) {
					var4 = ((var7 << 16) - var9 + var13 - 1) / var13;
				}

				var14 = var1 + var2 * cd.width;
				int var15 = cd.width - var3;
				if (var2 + var4 > cd.e) {
					var4 -= var2 + var4 - cd.e;
				}

				int var16;
				if (var2 < cd.g) {
					var16 = cd.g - var2;
					var4 -= var16;
					var14 += var16 * cd.width;
					var9 += var13 * var16;
				}

				if (var1 + var3 > cd.j) {
					var16 = var1 + var3 - cd.j;
					var3 -= var16;
					var15 += var16;
				}

				if (var1 < cd.l) {
					var16 = cd.l - var1;
					var3 -= var16;
					var14 += var16;
					var8 += var12 * var16;
					var15 += var16;
				}

				a(cd.pixels, pixels, 0, var8, var9, var14, var15, var3, var4, var12, var13, var6, var5);
			}
		}
	}

	private static void a(int var0, int[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		for (int var10 = -var7; var10 < 0; ++var10) {
			for (int var11 = -var6; var11 < 0; ++var11) {
				var3 = var2[var4++];
				if (var3 != 0) {
					var0 = var1[var5];
					if (var0 != 0) {
						int var12 = ((var3 & 16711680) >>> 16) * ((var0 & 16711680) >>> 16) >>> 8;
						int var13 = (var3 & '\uff00') * (var0 & '\uff00') >>> 24;
						int var14 = (var3 & 255) * (var0 & 255) >>> 8;
						var1[var5++] = (var12 << 16) + (var13 << 8) + var14;
					} else {
						++var5;
					}
				} else {
					++var5;
				}
			}

			var5 += var8;
			var4 += var9;
		}

	}

	private static void a(int var0, int var1, int var2, int[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
		for (var8 = -var10; var8 < 0; ++var8) {
			for (var6 = -var9; var6 < 0; ++var6) {
				var0 = var4[var5++];
				if (var0 != 0) {
					var1 = var3[var7];
					var2 = var0 + var1;
					var0 = (var0 & 16711935) + (var1 & 16711935);
					var1 = (var0 & 16777472) + (var2 - var0 & 65536);
					var3[var7++] = var2 - var1 | var1 - (var1 >>> 8);
				} else {
					++var7;
				}
			}

			var7 += var11;
			var5 += var12;
		}

	}

	void f(int var1, int var2, int var3) {
		var1 += super.r;
		var2 += super.v;
		int var4 = var1 + var2 * cd.width;
		int var5 = 0;
		int var6 = super.height;
		int var7 = super.width;
		int var8 = cd.width - var7;
		int var9 = 0;
		int var10;
		if (var2 < cd.g) {
			var10 = cd.g - var2;
			var6 -= var10;
			var2 = cd.g;
			var5 += var10 * var7;
			var4 += var10 * cd.width;
		}

		if (var2 + var6 > cd.e) {
			var6 -= var2 + var6 - cd.e;
		}

		if (var1 < cd.l) {
			var10 = cd.l - var1;
			var7 -= var10;
			var1 = cd.l;
			var5 += var10;
			var4 += var10;
			var9 += var10;
			var8 += var10;
		}

		if (var1 + var7 > cd.j) {
			var10 = var1 + var7 - cd.j;
			var7 -= var10;
			var9 += var10;
			var8 += var10;
		}

		if (var7 > 0 && var6 > 0) {
			a(cd.pixels, pixels, 0, var5, var4, 0, 0, var7, var6, var8, var9, var3);
		}
	}

	private static void b(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
		int var9 = -(var5 >> 2);
		var5 = -(var5 & 3);

		for (int var10 = -var6; var10 < 0; ++var10) {
			for (int var11 = var9; var11 < 0; ++var11) {
				var2 = var1[var3++];
				if (var2 != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}

				var2 = var1[var3++];
				if (var2 != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}

				var2 = var1[var3++];
				if (var2 != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}

				var2 = var1[var3++];
				if (var2 != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}
			}

			for (int var12 = var5; var12 < 0; ++var12) {
				var2 = var1[var3++];
				if (var2 != 0) {
					var0[var4++] = var2;
				} else {
					++var4;
				}
			}

			var4 += var7;
			var3 += var8;
		}

	}

	void d(int var1, int var2, int var3, int var4) {
		if (var3 > 0 && var4 > 0) {
			int var5 = super.width;
			int var6 = super.height;
			int var7 = 0;
			int var8 = 0;
			int var9 = super.originalWidth;
			int var10 = super.originalHeight;
			int var11 = (var9 << 16) / var3;
			int var12 = (var10 << 16) / var4;
			int var13;
			if (super.r > 0) {
				var13 = ((super.r << 16) + var11 - 1) / var11;
				var1 += var13;
				var7 += var13 * var11 - (super.r << 16);
			}

			if (super.v > 0) {
				var13 = ((super.v << 16) + var12 - 1) / var12;
				var2 += var13;
				var8 += var13 * var12 - (super.v << 16);
			}

			if (var5 < var9) {
				var3 = ((var5 << 16) - var7 + var11 - 1) / var11;
			}

			if (var6 < var10) {
				var4 = ((var6 << 16) - var8 + var12 - 1) / var12;
			}

			var13 = var1 + var2 * cd.width;
			int var14 = cd.width - var3;
			if (var2 + var4 > cd.e) {
				var4 -= var2 + var4 - cd.e;
			}

			int var15;
			if (var2 < cd.g) {
				var15 = cd.g - var2;
				var4 -= var15;
				var13 += var15 * cd.width;
				var8 += var12 * var15;
			}

			if (var1 + var3 > cd.j) {
				var15 = var1 + var3 - cd.j;
				var3 -= var15;
				var14 += var15;
			}

			if (var1 < cd.l) {
				var15 = cd.l - var1;
				var3 -= var15;
				var13 += var15;
				var7 += var11 * var15;
				var14 += var15;
			}

			b(cd.pixels, pixels, 0, var7, var8, var13, var14, var3, var4, var11, var12, var5);
		}
	}

	final void c(int var1, int var2, int var3, int var4, int var5) {
		if (var3 > 0 && var4 > 0) {
			if (var3 == super.width && var4 == super.height) {
				a(var1, var2, var5);
			} else {
				int var6 = super.width;
				int var7 = super.height;
				int var8 = 0;
				int var9 = 0;
				int var10 = super.originalWidth;
				int var11 = super.originalHeight;
				int var12 = (var10 << 16) / var3;
				int var13 = (var11 << 16) / var4;
				int var14;
				if (super.r > 0) {
					var14 = ((super.r << 16) + var12 - 1) / var12;
					var1 += var14;
					var8 += var14 * var12 - (super.r << 16);
				}

				if (super.v > 0) {
					var14 = ((super.v << 16) + var13 - 1) / var13;
					var2 += var14;
					var9 += var14 * var13 - (super.v << 16);
				}

				if (var6 < var10) {
					var3 = ((var6 << 16) - var8 + var12 - 1) / var12;
				}

				if (var7 < var11) {
					var4 = ((var7 << 16) - var9 + var13 - 1) / var13;
				}

				var14 = var1 + var2 * cd.width;
				int var15 = cd.width - var3;
				if (var2 + var4 > cd.e) {
					var4 -= var2 + var4 - cd.e;
				}

				int var16;
				if (var2 < cd.g) {
					var16 = cd.g - var2;
					var4 -= var16;
					var14 += var16 * cd.width;
					var9 += var13 * var16;
				}

				if (var1 + var3 > cd.j) {
					var16 = var1 + var3 - cd.j;
					var3 -= var16;
					var15 += var16;
				}

				if (var1 < cd.l) {
					var16 = cd.l - var1;
					var3 -= var16;
					var14 += var16;
					var8 += var12 * var16;
					var15 += var16;
				}

				b(cd.pixels, pixels, 0, var8, var9, var14, var15, var3, var4, var12, var13, var6, var5);
			}
		}
	}

	void d(int var1, int var2, int var3, int var4, int var5) {
		if (var3 > 0 && var4 > 0) {
			int width = super.width;
			int height = super.height;
			int var8 = 0;
			int var9 = 0;
			int oWidth = super.originalWidth;
			int oHeight = super.originalHeight;
			int var12 = (oWidth << 16) / var3;
			int var13 = (oHeight << 16) / var4;
			int var14;
			if (super.r > 0) {
				var14 = ((super.r << 16) + var12 - 1) / var12;
				var1 += var14;
				var8 += var14 * var12 - (super.r << 16);
			}

			if (super.v > 0) {
				var14 = ((super.v << 16) + var13 - 1) / var13;
				var2 += var14;
				var9 += var14 * var13 - (super.v << 16);
			}

			if (width < oWidth) {
				var3 = ((width << 16) - var8 + var12 - 1) / var12;
			}

			if (height < oHeight) {
				var4 = ((height << 16) - var9 + var13 - 1) / var13;
			}

			var14 = var1 + var2 * cd.width;
			int var15 = cd.width - var3;
			if (var2 + var4 > cd.e) {
				var4 -= var2 + var4 - cd.e;
			}

			int var16;
			if (var2 < cd.g) {
				var16 = cd.g - var2;
				var4 -= var16;
				var14 += var16 * cd.width;
				var9 += var13 * var16;
			}

			if (var1 + var3 > cd.j) {
				var16 = var1 + var3 - cd.j;
				var3 -= var16;
				var15 += var16;
			}

			if (var1 < cd.l) {
				var16 = cd.l - var1;
				var3 -= var16;
				var14 += var16;
				var8 += var12 * var16;
				var15 += var16;
			}

			c(cd.pixels, pixels, 0, var8, var9, var14, var15, var3, var4, var12, var13, width, var5);
		}
	}

	final void e(int var1, int var2) {
		var1 += super.r;
		var2 += super.v;
		int var3 = var1 + var2 * cd.width;
		int var4 = 0;
		int var5 = super.height;
		int var6 = super.width;
		int var7 = cd.width - var6;
		int var8 = 0;
		int var9;
		if (var2 < cd.g) {
			var9 = cd.g - var2;
			var5 -= var9;
			var2 = cd.g;
			var4 += var9 * var6;
			var3 += var9 * cd.width;
		}

		if (var2 + var5 > cd.e) {
			var5 -= var2 + var5 - cd.e;
		}

		if (var1 < cd.l) {
			var9 = cd.l - var1;
			var6 -= var9;
			var1 = cd.l;
			var4 += var9;
			var3 += var9;
			var8 += var9;
			var7 += var9;
		}

		if (var1 + var6 > cd.j) {
			var9 = var1 + var6 - cd.j;
			var6 -= var9;
			var8 += var9;
			var7 += var9;
		}

		if (var6 > 0 && var5 > 0) {
			a(0, cd.pixels, pixels, 0, var4, var3, var6, var5, var7, var8);
		}
	}
}
