
final class ne extends FunNode {

   private int f;
   private int g;
   private static int h;
   private static int i;
   private int j;
   private boolean k;
   private static boolean l = false;
   private static float[] m;
   private static eb[] n;
   private static float[] o;
   private static float[] p;
   private int q;
   private float[] r;
   private static int s;
   private static int[] t;
   private static int[] u;
   private static mi[] v;
   private boolean w;
   private static boolean[] x;
   private static float[] y;
   private static int z;
   private static float[] A;
   private int B;
   private static la[] C;
   private int D;
   private byte[][] E;
   static b[] F;
   private int G;
   private int H;
   private static float[] I;
   private static int[] J;
   private static float[] K;
   private static byte[] L;
   private byte[] M;


   public static void a() {
      L = null;
      F = null;
      C = null;
      v = null;
      n = null;
      x = null;
      t = null;
      K = null;
      I = null;
      A = null;
      o = null;
      m = null;
      y = null;
      p = null;
      u = null;
      J = null;
   }

   private static final void a(byte[] var0, int var1) {
      L = var0;
      s = var1;
      i = 0;
   }

   private final void a(byte[] var1) {
      ByteBuffer var2 = new ByteBuffer(var1);
      this.g = var2.getInt(255);
      this.f = var2.getInt(255);
      this.D = var2.getInt(255);
      this.j = var2.getInt(255);
      if(this.j < 0) {
         this.j = ~this.j;
         this.w = true;
      }

      int var3 = var2.getInt(255);
      this.E = new byte[var3][];

      for(int var4 = 0; var4 < var3; ++var4) {
         int var5 = 0;

         int var6;
         do {
            var6 = var2.getInt(false);
            var5 += var6;
         } while(var6 >= 255);

         byte[] var7 = new byte[var5];
         var2.a(var7, var5, 0, -109);
         this.E[var4] = var7;
      }

   }

   static final int a(int var0) {
      int var1 = 0;

      int var2;
      int var3;
      for(var2 = 0; var0 >= 8 - i; var0 -= var3) {
         var3 = 8 - i;
         int var4 = (1 << var3) - 1;
         var1 += (L[s] >> i & var4) << var2;
         i = 0;
         ++s;
         var2 += var3;
      }

      if(var0 > 0) {
         var3 = (1 << var0) - 1;
         var1 += (L[s] >> i & var3) << var2;
         i += var0;
      }

      return var1;
   }

   final hg a(int[] var1) {
      if(var1 != null && var1[0] <= 0) {
         return null;
      } else {
         if(this.M == null) {
            this.B = 0;
            this.r = new float[z];
            this.M = new byte[this.f];
            this.G = 0;
            this.H = 0;
         }

         for(; this.H < this.E.length; ++this.H) {
            if(var1 != null && var1[0] <= 0) {
               return null;
            }

            float[] var2 = this.e(this.H);
            if(var2 != null) {
               int var3 = this.G;
               int var4 = var2.length;
               if(var4 > this.f - var3) {
                  var4 = this.f - var3;
               }

               for(int var5 = 0; var5 < var4; ++var5) {
                  int var6 = (int)(128.0F + var2[var5] * 128.0F);
                  if((var6 & -256) != 0) {
                     var6 = ~var6 >> 31;
                  }

                  this.M[var3++] = (byte)(var6 - 128);
               }

               if(var1 != null) {
                  var1[0] -= var3 - this.G;
               }

               this.G = var3;
            }
         }

         this.r = null;
         byte[] var7 = this.M;
         this.M = null;
         return new hg(this.g, var7, this.D, this.j, this.w);
      }
   }

   static final float d(int var0) {
      int var1 = var0 & 2097151;
      int var2 = var0 & Integer.MIN_VALUE;
      int var3 = (var0 & 2145386496) >> 21;
      if(var2 != 0) {
         var1 = -var1;
      }

      return (float)((double)var1 * Math.pow(2.0D, (double)(var3 - 788)));
   }

   static final ne a(nf var0, int var1, int var2) {
      if(!a(var0)) {
         var0.a(-2, var1, var2);
         return null;
      } else {
         byte[] var3 = var0.c(var1, var2, -126);
         return var3 == null?null:new ne(var3);
      }
   }

   private final float[] e(int var1) {
      a(this.E[var1], 0);
      b();
      int var2 = a(ji.a(t.length - 1, 4));
      boolean var3 = x[var2];
      int var4 = var3?z:h;
      boolean var5 = false;
      boolean var6 = false;
      if(var3) {
         var5 = b() != 0;
         var6 = b() != 0;
      }

      int var7 = var4 >> 1;
      int var8;
      int var9;
      int var10;
      if(var3 && !var5) {
         var8 = (var4 >> 2) - (h >> 2);
         var9 = (var4 >> 2) + (h >> 2);
         var10 = h >> 1;
      } else {
         var8 = 0;
         var9 = var7;
         var10 = var4 >> 1;
      }

      int var11;
      int var12;
      int var13;
      if(var3 && !var6) {
         var11 = var4 - (var4 >> 2) - (h >> 2);
         var12 = var4 - (var4 >> 2) + (h >> 2);
         var13 = h >> 1;
      } else {
         var11 = var7;
         var12 = var4;
         var13 = var4 >> 1;
      }

      eb var14 = n[t[var2]];
      int var16 = var14.a;
      int var17 = var14.c[var16];
      boolean var15 = !C[var17].a();
      boolean var18 = var15;

      float[] var21;
      for(int var19 = 0; var19 < var14.d; ++var19) {
         mi var20 = v[var14.b[var19]];
         var21 = K;
         var20.a(var21, var4 >> 1, var18);
      }

      int var47;
      int var49;
      if(!var15) {
         var47 = var14.a;
         var49 = var14.c[var47];
         C[var49].a(K, var4 >> 1);
      }

      int var22;
      if(var15) {
         for(var47 = var4 >> 1; var47 < var4; ++var47) {
            K[var47] = 0.0F;
         }
      } else {
         var47 = var4 >> 1;
         var49 = var4 >> 2;
         var22 = var4 >> 3;
         float[] var23 = K;

         for(int var24 = 0; var24 < var47; ++var24) {
            var23[var24] *= 0.5F;
         }

         for(int var25 = var47; var25 < var4; ++var25) {
            var23[var25] = -var23[var4 - var25 - 1];
         }

         float[] var26 = var3?m:I;
         float[] var27 = var3?y:A;
         float[] var28 = var3?p:o;
         int[] var29 = var3?J:u;

         float var34;
         float var32;
         float var33;
         for(int var30 = 0; var30 < var49; ++var30) {
            float var31 = var23[4 * var30] - var23[var4 - 4 * var30 - 1];
            var32 = var23[4 * var30 + 2] - var23[var4 - 4 * var30 - 3];
            var33 = var26[2 * var30];
            var34 = var26[2 * var30 + 1];
            var23[var4 - 4 * var30 - 1] = var31 * var33 - var32 * var34;
            var23[var4 - 4 * var30 - 3] = var31 * var34 + var32 * var33;
         }

         for(int var51 = 0; var51 < var22; ++var51) {
            var32 = var23[var47 + 3 + 4 * var51];
            var33 = var23[var47 + 1 + 4 * var51];
            var34 = var23[4 * var51 + 3];
            float var35 = var23[4 * var51 + 1];
            var23[var47 + 3 + 4 * var51] = var32 + var34;
            var23[var47 + 1 + 4 * var51] = var33 + var35;
            float var36 = var26[var47 - 4 - 4 * var51];
            float var37 = var26[var47 - 3 - 4 * var51];
            var23[4 * var51 + 3] = (var32 - var34) * var36 - (var33 - var35) * var37;
            var23[4 * var51 + 1] = (var33 - var35) * var36 + (var32 - var34) * var37;
         }

         int var53 = ji.a(var4 - 1, 4);

         int var38;
         int var39;
         float var42;
         float var43;
         int var40;
         float var41;
         float var44;
         float var45;
         int var55;
         int var54;
         int var58;
         int var56;
         for(int var52 = 0; var52 < var53 - 3; ++var52) {
            var56 = var4 >> var52 + 2;
            var55 = 8 << var52;

            for(var54 = 0; var54 < 2 << var52; ++var54) {
               var58 = var4 - var56 * 2 * var54;
               var38 = var4 - var56 * (2 * var54 + 1);

               for(var39 = 0; var39 < var4 >> var52 + 4; ++var39) {
                  var40 = 4 * var39;
                  var41 = var23[var58 - 1 - var40];
                  var42 = var23[var58 - 3 - var40];
                  var43 = var23[var38 - 1 - var40];
                  var44 = var23[var38 - 3 - var40];
                  var23[var58 - 1 - var40] = var41 + var43;
                  var23[var58 - 3 - var40] = var42 + var44;
                  var45 = var26[var39 * var55];
                  float var46 = var26[var39 * var55 + 1];
                  var23[var38 - 1 - var40] = (var41 - var43) * var45 - (var42 - var44) * var46;
                  var23[var38 - 3 - var40] = (var42 - var44) * var45 + (var41 - var43) * var46;
               }
            }
         }

         float var57;
         for(var56 = 1; var56 < var22 - 1; ++var56) {
            var55 = var29[var56];
            if(var56 < var55) {
               var54 = 8 * var56;
               var58 = 8 * var55;
               var57 = var23[var54 + 1];
               var23[var54 + 1] = var23[var58 + 1];
               var23[var58 + 1] = var57;
               var57 = var23[var54 + 3];
               var23[var54 + 3] = var23[var58 + 3];
               var23[var58 + 3] = var57;
               var57 = var23[var54 + 5];
               var23[var54 + 5] = var23[var58 + 5];
               var23[var58 + 5] = var57;
               var57 = var23[var54 + 7];
               var23[var54 + 7] = var23[var58 + 7];
               var23[var58 + 7] = var57;
            }
         }

         for(var55 = 0; var55 < var47; ++var55) {
            var23[var55] = var23[2 * var55 + 1];
         }

         for(var54 = 0; var54 < var22; ++var54) {
            var23[var4 - 1 - 2 * var54] = var23[4 * var54];
            var23[var4 - 2 - 2 * var54] = var23[4 * var54 + 1];
            var23[var4 - var49 - 1 - 2 * var54] = var23[4 * var54 + 2];
            var23[var4 - var49 - 2 - 2 * var54] = var23[4 * var54 + 3];
         }

         for(var58 = 0; var58 < var22; ++var58) {
            var57 = var28[2 * var58];
            float var61 = var28[2 * var58 + 1];
            float var60 = var23[var47 + 2 * var58];
            var41 = var23[var47 + 2 * var58 + 1];
            var42 = var23[var4 - 2 - 2 * var58];
            var43 = var23[var4 - 1 - 2 * var58];
            var44 = var61 * (var60 - var42) + var57 * (var41 + var43);
            var23[var47 + 2 * var58] = (var60 + var42 + var44) * 0.5F;
            var23[var4 - 2 - 2 * var58] = (var60 + var42 - var44) * 0.5F;
            var44 = var61 * (var41 + var43) - var57 * (var60 - var42);
            var23[var47 + 2 * var58 + 1] = (var41 - var43 + var44) * 0.5F;
            var23[var4 - 1 - 2 * var58] = (-var41 + var43 + var44) * 0.5F;
         }

         for(var38 = 0; var38 < var49; ++var38) {
            var23[var38] = var23[2 * var38 + var47] * var27[2 * var38] + var23[2 * var38 + 1 + var47] * var27[2 * var38 + 1];
            var23[var47 - 1 - var38] = var23[2 * var38 + var47] * var27[2 * var38 + 1] - var23[2 * var38 + 1 + var47] * var27[2 * var38];
         }

         for(var39 = 0; var39 < var49; ++var39) {
            var23[var4 - var49 + var39] = -var23[var39];
         }

         for(var40 = 0; var40 < var49; ++var40) {
            var23[var40] = var23[var49 + var40];
         }

         for(int var59 = 0; var59 < var49; ++var59) {
            var23[var49 + var59] = -var23[var49 - var59 - 1];
         }

         for(int var63 = 0; var63 < var49; ++var63) {
            var23[var47 + var63] = var23[var4 - var63 - 1];
         }

         for(int var64 = var8; var64 < var9; ++var64) {
            var44 = (float)Math.sin(((double)(var64 - var8) + 0.5D) / (double)var10 * 0.5D * 3.141592653589793D);
            K[var64] *= (float)Math.sin(1.5707963267948966D * (double)var44 * (double)var44);
         }

         for(int var62 = var11; var62 < var12; ++var62) {
            var45 = (float)Math.sin(((double)(var62 - var11) + 0.5D) / (double)var13 * 0.5D * 3.141592653589793D + 1.5707963267948966D);
            K[var62] *= (float)Math.sin(1.5707963267948966D * (double)var45 * (double)var45);
         }
      }

      float[] var50 = null;
      if(this.B > 0) {
         var49 = this.B + var4 >> 2;
         var50 = new float[var49];
         int var48;
         if(!this.k) {
            for(var22 = 0; var22 < this.q; ++var22) {
               var48 = (this.B >> 1) + var22;
               var50[var22] += this.r[var48];
            }
         }

         if(!var15) {
            for(var22 = var8; var22 < var4 >> 1; ++var22) {
               var48 = var50.length - (var4 >> 1) + var22;
               var50[var48] += K[var22];
            }
         }
      }

      var21 = this.r;
      this.r = K;
      K = var21;
      this.B = var4;
      this.q = var12 - (var4 >> 1);
      this.k = var15;
      return var50;
   }

   static final int b() {
      int var0 = L[s] >> i & 1;
      ++i;
      s += i >> 3;
      i &= 7;
      return var0;
   }

   private ne(byte[] var1) {
      this.a(var1);
   }

   private static final void b(byte[] var0) {
      a(var0, 0);
      h = 1 << a(4);
      z = 1 << a(4);
      K = new float[z];

      int var2;
      int var3;
      int var4;
      int var5;
      int var7;
      int var9;
      int var11;
      int var13;
      for(int var1 = 0; var1 < 2; ++var1) {
         var2 = var1 != 0?z:h;
         var3 = var2 >> 1;
         var4 = var2 >> 2;
         var5 = var2 >> 3;
         float[] var6 = new float[var3];

         for(var7 = 0; var7 < var4; ++var7) {
            var6[2 * var7] = (float)Math.cos((double)(4 * var7) * 3.141592653589793D / (double)var2);
            var6[2 * var7 + 1] = -((float)Math.sin((double)(4 * var7) * 3.141592653589793D / (double)var2));
         }

         float[] var8 = new float[var3];

         for(var9 = 0; var9 < var4; ++var9) {
            var8[2 * var9] = (float)Math.cos((double)(2 * var9 + 1) * 3.141592653589793D / (double)(2 * var2));
            var8[2 * var9 + 1] = (float)Math.sin((double)(2 * var9 + 1) * 3.141592653589793D / (double)(2 * var2));
         }

         float[] var10 = new float[var4];

         for(var11 = 0; var11 < var5; ++var11) {
            var10[2 * var11] = (float)Math.cos((double)(4 * var11 + 2) * 3.141592653589793D / (double)var2);
            var10[2 * var11 + 1] = -((float)Math.sin((double)(4 * var11 + 2) * 3.141592653589793D / (double)var2));
         }

         int[] var12 = new int[var5];
         var13 = ji.a(var5 - 1, 4);

         for(int var14 = 0; var14 < var5; ++var14) {
            var12[var14] = qc.a(var13, -128, var14);
         }

         if(var1 != 0) {
            m = var6;
            y = var8;
            p = var10;
            J = var12;
         } else {
            I = var6;
            A = var8;
            o = var10;
            u = var12;
         }
      }

      var2 = a(8) + 1;
      F = new b[var2];

      for(var3 = 0; var3 < var2; ++var3) {
         F[var3] = new b();
      }

      var4 = a(6) + 1;

      for(var5 = 0; var5 < var4; ++var5) {
         a(16);
      }

      int var15 = a(6) + 1;
      C = new la[var15];

      for(var7 = 0; var7 < var15; ++var7) {
         C[var7] = new la();
      }

      int var16 = a(6) + 1;
      v = new mi[var16];

      for(var9 = 0; var9 < var16; ++var9) {
         v[var9] = new mi();
      }

      int var17 = a(6) + 1;
      n = new eb[var17];

      for(var11 = 0; var11 < var17; ++var11) {
         n[var11] = new eb();
      }

      int var18 = a(6) + 1;
      x = new boolean[var18];
      t = new int[var18];

      for(var13 = 0; var13 < var18; ++var13) {
         x[var13] = b() != 0;
         a(16);
         a(16);
         t[var13] = a(8);
      }

      l = true;
   }

   private static final boolean a(nf var0) {
      if(!l) {
         byte[] var1 = var0.c(0, 0, -123);
         if(var1 == null) {
            return false;
         }

         b(var1);
      }

      return true;
   }

}
