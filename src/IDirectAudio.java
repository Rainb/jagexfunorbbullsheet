import java.awt.Component;

interface IDirectAudio {

   int a(int var1, boolean var2);

   void a(boolean var1, int var2, int var3) throws Exception;

   void a(int var1, int var2);

   void a(boolean var1, Component var2, int var3, int var4) throws Exception;

   void stop(int var1, byte var2);

   void a(int var1, int[] var2);
}
