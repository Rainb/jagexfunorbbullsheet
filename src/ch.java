import java.applet.Applet;
import java.net.URL;

final class ch {

   static FunImage a;
   static FunImage b;
   static pm c;
   static boolean d;
   static int e;
   static boolean f;
   static String g = "Unable to add friend - system busy";
   static int[] h;
   static int i;


   static void a(int var0, int var1, int var2, Applet var3, int var4, String[] var5) {
      int var8 = client.L;

      try {
         ha.t = var3.getParameter("overxgames");
         if(ha.t == null) {
            ha.t = "0";
         }

         wf.M = var3.getParameter("overxachievements");
         if(wf.M == null) {
            wf.M = "0";
         }

         label67: {
            String currency = var3.getParameter("currency");
            if(currency == null || !FunNodeList.a(123, currency)) {
               qj.c = 2;
               if(var8 == 0) {
                  break label67;
               }
            }

            qj.c = wm.a(currency, (byte)117);
         }

         hn.j = var1;
         if(var0 != -18367) {
            a(83, -69, -51, null, -24, null);
         }

         oh.b = var2;
         tl.n = var4;
         ng.H = new FunImage[var5.length];
         int var7 = 0;
         if(var8 != 0) {
            ng.H[var7] = new FunImage(317, 34);
            ++var7;
         }

         while(~var7 > ~var5.length) {
            ng.H[var7] = new FunImage(317, 34);
            ++var7;
         }

         qc.g = var5;
      } catch (RuntimeException var9) {
         throw pl.a(var9, "ch.C(" + var0 + ',' + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ',' + (var5 != null?"{...}":"null") + ')');
      }
   }

   static URL a(int var0, URL url, String var2, String var3, int var4) {
      int var9 = client.L;

      try {
         String urlFile = url.getFile();
         if(var0 < 87) {
            return null;
         } else {
            int x = 0;

            while(true) {
               int var7;
               while(true) {
                  do {
                     while(urlFile.regionMatches(x, "/l=", 0, 3)) {
                        var7 = urlFile.indexOf(47, x - -1);
                        if(~var7 > -1) {
                           break;
                        }

                        if(~var4 > -1) {
                           x = var7;
                           if(var9 == 0) {
                              continue;
                           }
                        }

                        urlFile = urlFile.substring(0, x) + urlFile.substring(var7);
                        if(var9 != 0) {
                           break;
                        }
                     }

                     if(!urlFile.regionMatches(x, "/a=", 0, 3)) {
                        break;
                     }

                     var7 = urlFile.indexOf(47, 1 + x);
                     if(var7 < 0) {
                        break;
                     }

                     x = var7;
                  } while(var9 == 0);

                  if(!urlFile.regionMatches(x, "/p=", 0, 3)) {
                     break;
                  }

                  var7 = urlFile.indexOf(47, 1 + x);
                  if(~var7 > -1) {
                     break;
                  }

                  if(var2 != null) {
                     urlFile = urlFile.substring(0, x) + urlFile.substring(var7);
                     if(var9 == 0) {
                        continue;
                     }
                  }

                  x = var7;
                  if(var9 != 0) {
                     break;
                  }
               }

               if(!urlFile.regionMatches(x, "/s=", 0, 3) && !urlFile.regionMatches(x, "/c=", 0, 3)) {
                  break;
               }

               var7 = urlFile.indexOf(47, 1 + x);
               if(var7 < 0) {
                  break;
               }

               if(var3 != null) {
                  urlFile = urlFile.substring(0, x) + urlFile.substring(var7);
                  if(var9 == 0) {
                     continue;
                  }
               }

               x = var7;
               if(var9 != 0) {
                  break;
               }
            }

            StringBuilder urlBuilder = new StringBuilder(x);
            urlBuilder.append(urlFile.substring(0, x));
            if(var4 > 0) {
               urlBuilder.append("/l=");
               urlBuilder.append(Integer.toString(var4));
            }

            if(var2 != null && var2.length() > 0) {
               urlBuilder.append("/p=");
               urlBuilder.append(var2);
            }

            if(var3 != null && ~var3.length() < -1) {
               urlBuilder.append("/s=");
               urlBuilder.append(var3);
            }

            label71: {
               if(urlFile.length() > x) {
                  urlBuilder.append(urlFile.substring(x, urlFile.length()));
                  if(var9 == 0) {
                     break label71;
                  }
               }

               urlBuilder.append('/');
            }

            try {
               return new URL(url, urlBuilder.toString());
            } catch (Exception var10) {
               var10.printStackTrace();
               return url;
            }
         }
      } catch (RuntimeException var11) {
         throw pl.a(var11, "ch.B(" + var0 + ',' + (url != null?"{...}":"null") + ',' + (var2 != null?"{...}":"null") + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

   public static void a(byte var0) {
      try {
         b = null;
         g = null;
         a = null;
         c = null;
         if(var0 == -100) {
            h = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ch.A(" + var0 + ')');
      }
   }

   static boolean a(int var0) {
      try {
         if(gn.g) {
            return true;
         } else if(!ce.g.a((byte)-85, "benefits")) {
            return false;
         } else {
            FunImage benefitBorders = fj.a("benefits", "borders", ce.g, 0);
            FunImage benefitPrice = fj.a("benefits", "price", ce.g, 0);
            FunImage benefitsLogo = fj.a("benefits", "logo", ce.g, 0);
            FunImage[] screenShotBenefits = rm.a("screenshots", "benefits", ce.g, 28166);
            rm.a(20986, benefitBorders, ik.e);
            jh.a(-76, 15, 35, 103, 2, benefitPrice, -2400, 13, 92);
            wa.a(screenShotBenefits, (byte)104);
            mc.a(benefitsLogo, true);
            gn.g = true;
            if(var0 != 3) {
               a = null;
            }

            return true;
         }
      } catch (RuntimeException var5) {
         throw pl.a(var5, "ch.D(" + var0 + ')');
      }
   }

}
