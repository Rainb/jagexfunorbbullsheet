
final class nj {

   static int a;
   private int[] b = new int[2];
   private int[][][] c = new int[2][2][4];
   int[] d = new int[2];
   private int[][][] e = new int[2][2][4];
   private static float f;
   static int[][] g = new int[2][8];
   private static float[][] h = new float[2][8];


   public static void a() {
      h = null;
      g = null;
   }

   final int a(int var1, float var2) {
      float var3;
      if(var1 == 0) {
         var3 = (float)this.b[0] + (float)(this.b[1] - this.b[0]) * var2;
         var3 *= 0.0030517578F;
         f = (float)Math.pow(0.1D, (double)(var3 / 20.0F));
         a = (int)(f * 65536.0F);
      }

      if(this.d[var1] == 0) {
         return 0;
      } else {
         var3 = this.a(var1, 0, var2);
         h[var1][0] = -2.0F * var3 * (float)Math.cos((double)this.b(var1, 0, var2));
         h[var1][1] = var3 * var3;

         for(int var4 = 1; var4 < this.d[var1]; ++var4) {
            var3 = this.a(var1, var4, var2);
            float var5 = -2.0F * var3 * (float)Math.cos((double)this.b(var1, var4, var2));
            float var6 = var3 * var3;
            h[var1][var4 * 2 + 1] = h[var1][var4 * 2 - 1] * var6;
            h[var1][var4 * 2] = h[var1][var4 * 2 - 1] * var5 + h[var1][var4 * 2 - 2] * var6;

            for(int var7 = var4 * 2 - 1; var7 >= 2; --var7) {
               h[var1][var7] += h[var1][var7 - 1] * var5 + h[var1][var7 - 2] * var6;
            }

            h[var1][1] += h[var1][0] * var5 + var6;
            h[var1][0] += var5;
         }

         int var8;
         if(var1 == 0) {
            for(var8 = 0; var8 < this.d[0] * 2; ++var8) {
               h[0][var8] *= f;
            }
         }

         for(var8 = 0; var8 < this.d[var1] * 2; ++var8) {
            g[var1][var8] = (int)(h[var1][var8] * 65536.0F);
         }

         return this.d[var1] * 2;
      }
   }

   final void a(ByteBuffer var1, wh var2) {
      int var3 = var1.getInt(false);
      this.d[0] = var3 >> 4;
      this.d[1] = var3 & 15;
      if(var3 == 0) {
         this.b[0] = this.b[1] = 0;
      } else {
         this.b[0] = var1.j(1808469224);
         this.b[1] = var1.j(1808469224);
         int var4 = var1.getInt(false);

         int var6;
         for(int var5 = 0; var5 < 2; ++var5) {
            for(var6 = 0; var6 < this.d[var5]; ++var6) {
               this.c[var5][0][var6] = var1.j(1808469224);
               this.e[var5][0][var6] = var1.j(1808469224);
            }
         }

         for(var6 = 0; var6 < 2; ++var6) {
            for(int var7 = 0; var7 < this.d[var6]; ++var7) {
               if((var4 & 1 << var6 * 4 << var7) != 0) {
                  this.c[var6][1][var7] = var1.j(1808469224);
                  this.e[var6][1][var7] = var1.j(1808469224);
               } else {
                  this.c[var6][1][var7] = this.c[var6][0][var7];
                  this.e[var6][1][var7] = this.e[var6][0][var7];
               }
            }
         }

         if(var4 != 0 || this.b[1] != this.b[0]) {
            var2.a(var1);
         }

      }
   }

   private final float a(int var1, int var2, float var3) {
      float var4 = (float)this.e[var1][0][var2] + var3 * (float)(this.e[var1][1][var2] - this.e[var1][0][var2]);
      var4 *= 0.0015258789F;
      return 1.0F - (float)Math.pow(10.0D, (double)(-var4 / 20.0F));
   }

   private static final float a(float var0) {
      float var1 = 32.703197F * (float)Math.pow(2.0D, (double)var0);
      return var1 * 3.1415927F / 11025.0F;
   }

   private final float b(int var1, int var2, float var3) {
      float var4 = (float)this.c[var1][0][var2] + var3 * (float)(this.c[var1][1][var2] - this.c[var1][0][var2]);
      var4 *= 1.2207031E-4F;
      return a(var4);
   }

}
