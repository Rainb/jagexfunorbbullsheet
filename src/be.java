import java.util.Date;

final class be extends FunRB {

   static int F;
   static String G = "This option cannot be combined with the current settings for:  ";
   static volatile boolean windowHasFocus = true;
   static oj I;
   static boolean J;
   static om K;


   private be(int var1, int var2, int var3, int var4, vg var5, aa var6, bn var7) {
      super(var1, var2, var3, var4, var5, var6);

      try {
         super.w = var7;
      } catch (RuntimeException var9) {
         throw pl.a(var9, "be.<init>(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + (var5 != null?"{...}":"null") + ',' + (var6 != null?"{...}":"null") + ',' + (var7 != null?"{...}":"null") + ')');
      }
   }

   static void h(int var0) {
      try {
         FunM.u = FunAB.a((byte) 104);
         FunQ.v = var0;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "be.B(" + var0 + ')');
      }
   }

   static String a(int var0, long time) {
      try {
         bl.calendar.setTime(new Date(time));
         int day = bl.calendar.get(7);
         int date = bl.calendar.get(5);
         int month = bl.calendar.get(2);
         int year = bl.calendar.get(1);
         if(var0 >= -3) {
            windowHasFocus = false;
         }

         int hour = bl.calendar.get(11);
         int min = bl.calendar.get(12);
         int second = bl.calendar.get(13);
         return gk.y[-1 + day] + ", " + date / 10 + date % 10 + "-" + eh.monthAbv[month] + "-" + year + " " + hour / 10 + hour % 10 + ":" + min / 10 + min % 10 + ":" + second / 10 + second % 10 + " GMT";
      } catch (RuntimeException var10) {
         throw pl.a(var10, "be.C(" + var0 + ',' + time + ')');
      }
   }

   public static void i(int var0) {
      try {
         I = null;
         G = null;
         K = null;
         if(var0 != -5011) {
            i(79);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "be.A(" + var0 + ')');
      }
   }

   final void a(int var1, int var2, int var3, int var4, int var5, bn var6) {
      try {
         super.a(var1, 113, var3, var4, var5, var6);
         if(var2 <= 70) {
            I = null;
         }

         fd var7 = ug.h;
         if(var7 != null && a(1, var3, var1, var4, var5)) {
            if(!(super.listener instanceof si)) {
               if(var7.listener instanceof si) {
                  ((si)var7.listener).a(-25446, var7, this);
                  ug.h = null;
			   }
            } else {
               ((si)super.listener).a(-25446, var7, this);
               ug.h = null;
            }

         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "be.IA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + (var6 != null?"{...}":"null") + ')');
      }
   }

}
