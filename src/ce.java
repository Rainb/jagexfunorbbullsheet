import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.URL;

public abstract class ce extends Applet implements Runnable, FocusListener, WindowListener {

   static nf a;
   static int b;
   static FunImage c;
   static ph d;
   static String e = "Spectate";
   boolean f = false;
   static nf g;
   public static boolean h;
   public static int i;
   public static int j;
   public static int k;
   public static boolean l;
   public static boolean m;
   public static int n;


   abstract void a(int var1);

   public final void windowActivated(WindowEvent var1) {}

   static int a(int var0, int var1) {
      try {
         return var0 | var1;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.Q(" + var0 + ',' + var1 + ')');
      }
   }

   public final void focusLost(FocusEvent var1) {
      try {
         be.windowHasFocus = false;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.focusLost(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public static void b(int var0) {
      try {
         if(var0 == 31) {
            c = null;
            a = null;
            d = null;
            e = null;
            g = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.F(" + var0 + ')');
      }
   }

   private void c(int var1) {
      try {
         long var2 = FunAB.a((byte) 101);
         long var4 = pl.p[mh.f];
         pl.p[mh.f] = var2;
         boolean var10000;
		  var10000 = var4 != 0L && ~var2 < ~var4;

         mh.f = 31 & mh.f + var1;
         synchronized(this){}

		  FunNodeList.f = be.windowHasFocus;

		  e(22970);
      } catch (RuntimeException var10) {
         throw pl.a(var10, "ce.D(" + var1 + ')');
      }
   }

   public final void windowDeactivated(WindowEvent var1) {}

   static FunImage[] d(int var0) {
      int var8 = client.L;

      try {
         FunImage[] images = new FunImage[bl.w];
         int i = 0;
         if(var0 > -27) {
            b(111);
            if(var8 == 0 && i >= bl.w) {
               fk.dispose(-97);
               return images;
            }
         } else if(i >= bl.w) {
            fk.dispose(-97);
            return images;
         }

         do {
            label69: {
               int var3 = uj.J[i] * fi.f[i];
               byte[] var4 = FunCanvas.b[i];
               if(FunT.a[i]) {
                  byte[] var5 = fk.j[i];
                  int[] var6 = new int[var3];
                  int var7 = 0;
                  if(var8 != 0 || ~var3 < ~var7) {
                     do {
                        var6[var7] = a(FunE.e[wm.a(255, var4[var7])], wm.a(-16777216, var5[var7] << 24));
                        ++var7;
                     } while(~var3 < ~var7);
                  }

                  images[i] = new kh(im.e, be.F, ec.k[i], RandomAccessWrapper.g[i], fi.f[i], uj.J[i], var6);
                  if(var8 == 0) {
                     break label69;
                  }
               }

               int[] var10 = new int[var3];
               int var11 = 0;
               if(var8 != 0 || ~var3 < ~var11) {
                  do {
                     var10[var11] = FunE.e[wm.a(255, var4[var11])];
                     ++var11;
                  } while(~var3 < ~var11);
               }

               images[i] = new FunImage(im.e, be.F, ec.k[i], RandomAccessWrapper.g[i], fi.f[i], uj.J[i], var10);
            }

            ++i;
         } while(i < bl.w);

         fk.dispose(-97);
         return images;
      } catch (RuntimeException var9) {
         throw pl.a(var9, "ce.K(" + var0 + ')');
      }
   }

   public final String getParameter(String var1) {
      try {
		  if(ByteBuffer.g == null) {
			  if(fd.loaderApplet != null && fd.loaderApplet != this) {
				  return fd.loaderApplet.getParameter(var1);

			  }
			  return super.getParameter(var1);
		  }
		  return null;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.getParameter(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   abstract void e(int var1);

   public final void focusGained(FocusEvent var1) {
      try {
         be.windowHasFocus = true;
         en.m = true;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.focusGained(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final void windowIconified(WindowEvent var1) {}

   public final void destroy() {
      try {
         if(this == ga.a && !tl.m) {
            ef.c = FunAB.a((byte) 48);
            mc.a(5000L, 102);
            FunP.e = null;
			 a(0, false);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.destroy(" + ')');
      }
   }

   static void a(ByteBuffer var0, BigInteger var1, byte var2, ByteBuffer var3, BigInteger var4) {
      try {
         mn.a(var4, (byte)126, var0.buffer, var1, 0, var0.offset, var3);
         if(var2 < 105) {
            a(-5, -25, false);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "ce.L(" + (var0 != null?"{...}":"null") + ',' + (var1 != null?"{...}":"null") + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + (var4 != null?"{...}":"null") + ')');
      }
   }

   public final void windowOpened(WindowEvent var1) {}

   public final AppletContext getAppletContext() {
      try {
         return ByteBuffer.g != null?null: fd.loaderApplet != null && fd.loaderApplet != this?fd.loaderApplet.getAppletContext():super.getAppletContext();
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.getAppletContext(" + ')');
      }
   }

   public static void provideLoaderApplet(Applet applet) {
      try {
         fd.loaderApplet = applet;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.provideLoaderApplet(" + (applet != null?"{...}":"null") + ')');
      }
   }

   final void init(int var1, int var2, String var3, int var4, byte var5, int var6, int var7) {
      int var9 = client.L;

      try {
         try {
            if(ga.a != null) {
               ++FunP.a;
               if(~FunP.a <= -4) {
				   showErrorDocument(51, "alreadyloaded");
               } else {
				   getAppletContext().showDocument(getDocumentBase(), "_self");
               }
            } else {
               FunDB.Sb = 0;
               ed.g = var4;
               af.f = var4;
               ga.a = this;
               FunPB.N = 0;
               if(var5 > -76) {
				   a(78);
               }

               FunNB.xb = var7;
               ca.u = var7;
               pj.A = var1;
               nc.e = fd.loaderApplet;
               FunP.e = lf.s = new om(var6, var3, var2, fd.loaderApplet != null);
               ObjectStore var8 = lf.s.a(1, false, this);
               if(var9 != 0) {
                  mc.a(10L, -33);
               }

               while(~var8.f == -1) {
                  mc.a(10L, -33);
               }

            }
         } catch (Throwable var10) {
            dj.a(null, 0, var10);
			 showErrorDocument(95, "crash");
         }
      } catch (RuntimeException var11) {
         throw pl.a(var11, "ce.H(" + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
      }
   }

   abstract void f(int var1);

   private void a(int var1, boolean clean) {
      try {
		  if(var1 != 0) {
			  f = true;
		  }
		  if(tl.m) {
			 return;
		  }
		  tl.m = true;

		  if(fd.loaderApplet != null) {
            fd.loaderApplet.destroy();
         }

         try {
			 f(-104);
         } catch (Exception ignored) {
		 }

         if(qa.canvas != null) {
            try {
               qa.canvas.removeFocusListener(this);
               qa.canvas.getParent().remove(qa.canvas);
            } catch (Exception ignored) {
			}
         }

         if(lf.s != null) {
            try {
               lf.s.close((byte) 22);
            } catch (Exception ignored) {
			}
         }

		  g(-120);
         if(ByteBuffer.g != null) {
            try {
               System.exit(0);
            } catch (Throwable ignored) {
			}
         }

         System.out.println("Shutdown complete - clean:" + clean);
      } catch (RuntimeException var16) {
         throw pl.a(var16, "ce.E(" + var1 + ',' + clean + ')');
      }
   }

   final void showErrorDocument(int var1, String page) {
      try {
         if(!f) {
			 f = true;
            System.out.println("error_game_" + page);

            try {
               if(var1 < 3) {
                  return;
               }

               JSHelper.exec("loggedout", 73, fd.loaderApplet);
            } catch (Throwable ignored) {
			}

            try {
				getAppletContext().showDocument(new URL(getCodeBase(), "error_game_" + page + ".ws"), "_top");
            } catch (Exception ignored) {
			}
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "ce.J(" + var1 + ',' + (page != null?"{...}":"null") + ')');
      }
   }

   private void a(byte var1) {
      try {
         long var2 = FunAB.a((byte) -126);
         long var4 = em.c[bh.e];
         em.c[bh.e] = var2;
         bh.e = bh.e + 1 & 31;
         if(~var4 != -1L && ~var2 < ~var4) {
            int var6 = (int)(-var4 + var2);
            fa.framesPerSecond = ((var6 >> 1) + 32000) / var6;
         }

         if(~lk.b++ < -51) {
            label27: {
               lk.b -= 50;
               en.m = true;
               qa.canvas.setSize(FunNB.xb, ed.g);
               qa.canvas.setVisible(true);
               if(ByteBuffer.g != null && FunCanvas.d == null) {
                  Insets var8 = ByteBuffer.g.getInsets();
                  qa.canvas.setLocation(var8.left - -FunPB.N, FunDB.Sb + var8.top);
                  if(client.L == 0) {
                     break label27;
                  }
               }

               qa.canvas.setLocation(FunPB.N, FunDB.Sb);
            }
         }

		  d((byte)-8);
         if(var1 >= -70) {
			 windowDeiconified(null);
         }
      } catch (RuntimeException var7) {
         throw pl.a(var7, "ce.A(" + var1 + ')');
      }
   }

   public final void start() {
      try {
         if(this == ga.a && !tl.m) {
            ef.c = 0L;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.start(" + ')');
      }
   }

   public final synchronized void paint(Graphics graphics) {
      try {
         if(ga.a == this && !tl.m) {
            en.m = true;
            if(FunD.b && ~(FunAB.a((byte) -127) + -aj.wb) < -1001L) {//year() = getGameTime
               Rectangle bounds = graphics.getClipBounds();
               if(bounds == null || ca.u <= bounds.width && bounds.height >= af.f) {
                  cm.c = true;
               }

            }
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.paint(" + (graphics != null?"{...}":"null") + ')');
      }
   }

	static final boolean skip = true;
   final boolean b(byte var1) {
	   if(skip) {
		   return true;
	   }
       
      try {
         String host = getDocumentBase().getHost().toLowerCase();
         if(!host.equals("jagex.com") && !host.endsWith(".jagex.com")) {
            if(!host.equals("funorb.com") && !host.endsWith(".funorb.com")) {
               if(host.endsWith("127.0.0.1")) {
                  return true;
               } else {
                  while(~host.length() < -1 && ~host.charAt(host.length() - 1) <= -49 && ~host.charAt(host.length() - 1) >= -58) {
                     host = host.substring(0, host.length() - 1);
                  }

                  if(host.endsWith("192.168.1.")) {
                     return true;
                  } else {
                     return true; 
                     //this.achievementsThisGame(40, "invalidhost");//<-- lol wut
                     //return false;
                  }
               }
            } else {
               return true;
            }
         } else {
            return true;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "ce.O(" + var1 + ')');
      }
   }

   final synchronized void a(boolean var1) {
      int var4 = client.L;

      try {
         if(qa.canvas != null) {
            qa.canvas.removeFocusListener(this);
            qa.canvas.getParent().remove(qa.canvas);
         }

         Object var2;
         label45: {
            if(FunCanvas.d != null) {
               var2 = FunCanvas.d;
               if(var4 == 0) {
                  break label45;
               }
            }

            if(ByteBuffer.g != null) {
               var2 = ByteBuffer.g;
               if(var4 == 0) {
                  break label45;
               }
            }

            if(fd.loaderApplet != null) {
               var2 = fd.loaderApplet;
               if(var4 == 0) {
                  break label45;
               }
            }

            var2 = ga.a;
         }

         label26: {
            ((Container)var2).setLayout(null);
            qa.canvas = new FunSecondCanvas(this);
            ((Container)var2).add(qa.canvas);
            qa.canvas.setSize(FunNB.xb, ed.g);
            qa.canvas.setVisible(true);
            if(var2 != ByteBuffer.g) {
               qa.canvas.setLocation(FunPB.N, FunDB.Sb);
               if(var4 == 0) {
                  break label26;
               }
            }

            Insets var3 = ByteBuffer.g.getInsets();
            qa.canvas.setLocation(var3.left - -FunPB.N, var3.top + FunDB.Sb);
         }

         qa.canvas.addFocusListener(this);
         qa.canvas.requestFocus();
         FunNodeList.f = true;
         be.windowHasFocus = true;
         en.m = true;
         cm.c = var1;
         aj.wb = FunAB.a((byte) -127);
      } catch (RuntimeException var5) {
         throw pl.a(var5, "ce.G(" + var1 + ')');
      }
   }

   public final void stop() {
      try {
         if(ga.a == this && !tl.m) {
            ef.c = FunAB.a((byte) -126) - -4000L;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.stop(" + ')');
      }
   }

   public final void windowClosed(WindowEvent var1) {}

   static int c(byte var0) {
      try {
         int var1 = -55 % ((var0 - 83) / 41);
         bh.f.a(-13220);
         return !jd.d.b((byte)-16)?jf.a((byte)106):0;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.C(" + var0 + ')');
      }
   }

   public final void update(Graphics var1) {
      try {
		  paint(var1);
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.update(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public abstract void init();

   public final URL getDocumentBase() {
      try {
         return ByteBuffer.g != null?null: fd.loaderApplet != null && fd.loaderApplet != this?fd.loaderApplet.getDocumentBase():super.getDocumentBase();
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ce.getDocumentBase(" + ')');
      }
   }

   public final URL getCodeBase() {
      try {
         return ByteBuffer.g != null?null: fd.loaderApplet != null && fd.loaderApplet != this?fd.loaderApplet.getCodeBase():super.getCodeBase();
      } catch (RuntimeException var2) {
         throw var2;
      }
   }

   abstract void g(int var1);

   public final void windowDeiconified(WindowEvent var1) {}

   public final void run() {
      int var4 = client.L;

      try {
         try {
            label117: {
               if(om.javaVendor != null) {
                  label122: {
                     String var1 = om.javaVendor.toLowerCase();
                     if(var1.indexOf("sun") != -1 || ~var1.indexOf("apple") != 0) {
                        String var2 = om.t;
                        if(!var2.equals("1.1") && !var2.startsWith("1.1.") && !var2.equals("1.2") && !var2.startsWith("1.2.")) {
                           break label122;
                        }

						 showErrorDocument(101, "wrongjava");
                        if(var4 == 0) {
                           break label117;
                        }
                     }

                     if(var1.indexOf("ibm") != -1 && (om.t == null || om.t.equals("1.4.2"))) {
						 showErrorDocument(120, "wrongjava");
                        if(var4 == 0) {
                           break label117;
                        }
                     }
                  }
               }

               if(om.t != null && om.t.startsWith("1.")) {
                  int var8 = 2;
                  int var10 = 0;
                  if(var4 != 0 || om.t.length() > var8) {
                     do {
                        char var3 = om.t.charAt(var8);
                        if(var3 < 48 || ~var3 < -58) {
                           break;
                        }

                        var10 = -48 - (-var3 - var10 * 10);
                        ++var8;
                     } while(om.t.length() > var8);
                  }

                  if(~var10 <= -6) {
                     FunD.b = true;
                     
                  }
               }

               Object var9 = ga.a;
               if(fd.loaderApplet != null) {
                  var9 = fd.loaderApplet;
               }

               Method var11 = om.focusCycleRootMethod;
               if(var11 != null) {
                  try {
                     var11.invoke(var9, new Object[]{Boolean.TRUE});
                  } catch (Throwable ignored) {
				  }
               }

               ca.f(-14040);
				a(false);//crash crash crash
               wm.g = dk.a(qa.canvas, (byte)-29, ed.g, FunNB.xb);
				a(28528);
               ah.eb = FunCC.b(-118);
               if(var4 != 0 || ef.c == 0L || ~FunAB.a((byte) -26) > ~ef.c) {
                  do {
                     ik.f = ah.eb.a(el.e, (byte)-118);
                     int var12 = 0;
                     if(var4 != 0) {
						 c(1);
                        ++var12;
                     }

                     while(ik.f > var12) {
						 c(1);
                        ++var12;
                     }

					  a((byte)-71);
                     pa.a(true, lf.s, qa.canvas);
                  } while(ef.c == 0L || ~FunAB.a((byte) -26) > ~ef.c);
               }
            }
         } catch (Throwable var6) {
            dj.a(null, 0, var6);
			 showErrorDocument(48, "crash");
         }

		  a(0, true);
      } catch (RuntimeException var7) {
         throw pl.a(var7, "ce.run(" + ')');
      }
   }

   public final void windowClosing(WindowEvent var1) {
      try {
		  destroy();
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ce.windowClosing(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   abstract void d(byte var1);

   static void a(int var0, int var1, boolean var2) {
      try {
         fj var3 = dk.a(6, var2, var1);
         if(var3 != null) {
            vc.e.a(false, (byte)94);
            FunCB var4 = vc.e;
            String var5 = mg.m;
            var4.j.a(2040, 5, var5);
            FunCB var6 = vc.e;
            int var7 = ah.gb;
            int var8 = FunImageProducer.r;
            var6.j.b(0, var8, var7, 0, 126);
         }

         lj.a(-127, var2, var1);
         if(var0 != 12938) {
            provideLoaderApplet(null);
         }
      } catch (RuntimeException var9) {
         throw pl.a(var9, "ce.N(" + var0 + ',' + var1 + ',' + var2 + ')');
      }
   }

}
