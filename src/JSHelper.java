import netscape.javascript.JSObject;

import java.applet.Applet;

final class JSHelper {

	static Object exec(String method, int var1, Applet applet) throws Throwable {
		return var1 <= 35 ? null : JSObject.getWindow(applet).call(method, null);
	}

	static void eval(boolean var0, Applet applet, String code) throws Throwable {
		JSObject.getWindow(applet).eval(code);
	}

	static Object a(String var0, int var1, Object[] var2, Applet var3) throws Throwable {
		return var1 > -48 ? null : JSObject.getWindow(var3).call(var0, var2);
	}
}
