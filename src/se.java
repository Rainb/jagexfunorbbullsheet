
final class se extends FunNode {

   int f;
   boolean g;
   private int[] h;
   static String removeName = "Remove name";
   private FunImage j;
   static String k = "Please wait while we search.<br>Games usually start within a minute, provided the server is busy enough.<br><br>The longer you are forced to wait, the earlier in the list of players you are likely to appear.<br><br>If the game doesn\'t start, click \'Cancel\' and then try choosing \'Don\'t mind\' for more options or switching to a busier lobby.";
   static int[] l;


   public static void a(boolean var0) {
      try {
         l = null;
         removeName = null;
         if(!var0) {
            removeName = null;
         }

         k = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "se.D(" + var0 + ')');
      }
   }

   private final void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, boolean var15, boolean var16) {
      int var17 = var10;
      int var18 = -var8;
      int var19 = j.pixels[var12 * j.originalWidth + var11];
      int var20 = (var19 & 24) >> 3;
      FunImage[] var21 = null;
      g var22 = null;
      int var23 = 0;
      int var24 = 2;
      int var25 = Math.abs(var10);
      if(var20 == 1) {
         if(var11 > 0 && ((j.pixels[var12 * j.originalWidth + var11 - 1] ^ var19) & 31) == 0) {
            ++var23;
         }

         if(var11 < j.originalWidth - 1 && ((j.pixels[var12 * j.originalWidth + var11 + 1] ^ var19) & 31) == 0) {
            var23 += 2;
         }

         if(var12 > 0 && ((j.pixels[(var12 - 1) * j.originalWidth + var11] ^ var19) & 31) == 0) {
            var23 += 4;
         }

         if(var12 < j.originalHeight - 1 && ((j.pixels[(var12 + 1) * j.originalWidth + var11] ^ var19) & 31) == 0) {
            var23 += 8;
         }

         var21 = fa.l[var1][var19 & 7];
         var22 = qj.e[var23];
      }

      int var26 = h[var12 * j.originalWidth + var11];
      byte var27 = (byte)var26;
      byte var28 = (byte)(var26 >> 8);
      int var29 = var26 >> 16 & 255;
      int var30 = var26 >> 24;
      int var31 = (var14 + var30 - 60) * 1000 / 60;
      int var32;
      if(var15) {
         var32 = (int)(333.0D * Math.sin((double)(ol.b + var30) * 0.045D));
         if(var31 > var32) {
            var31 = var32;
         }
      }

      var31 = var31 * var31 / 1000;
      var32 = (var14 + var30 / 2 - 60) * 2000 / 60;
      if(var15 && var32 > 0) {
         var32 = 0;
      } else {
         var32 = var32 * var32 / 2000;
         var32 = var32 * var32 / 2000;
         var32 = var32 * var32 / 2000;
      }

      int var33 = var29 * (var31 + var32) / 10;
      int var34 = var33 * var27 / ((j.originalWidth + j.originalHeight) * 4);
      int var35 = var33 * var28 / ((j.originalWidth + j.originalHeight) * 4) + (int)(100.0D * Math.sin((double)(ol.b + var30 * 4) * 0.07D));
      int var36 = 4000000 / var4;
      int var37 = (var34 * var8 + var33 * var10) / (var36 * 2);
      int var38 = (var34 * var10 - var33 * var8) / (var36 * 2);
      var5 += var37;
      var6 += var35;
      var7 += var38;
      if(var21 != null) {
         if(var25 < var8 * 2) {
            var24 = 1 + ((var24 - 1) * var25 + var8 / 2) / var8;
         }

         int var39 = Integer.MAX_VALUE;
         int var40 = Integer.MIN_VALUE;
         int var41 = Integer.MAX_VALUE;
         int var42 = Integer.MIN_VALUE;

         for(int var43 = -var24; var43 <= var24; var43 += 2) {
            int var46 = j.originalWidth * var24 * 2;
            int var47 = var5 + (var11 * var8 * var24 * 2 + var17 * var43) / var46;
            int var48 = var5 + ((var11 + 1) * var8 * var24 * 2 + var17 * var43) / var46;
            int var49 = var6 + var12 * var9 / j.originalHeight;
            int var50 = var6 + (var12 + 1) * var9 / j.originalHeight;
            int var51 = var7 + (var11 * var10 * var24 * 2 + var18 * var43) / var46;
            int var52 = var7 + ((var11 + 1) * var10 * var24 * 2 + var18 * var43) / var46;
            if(var51 >= 500 && var52 >= 500) {
               int var53 = var2 + var47 * var4 / var51;
               int var54 = var2 + var48 * var4 / var52;
               int var55 = var3 + var49 * var4 / var51;
               int var56 = var3 + var49 * var4 / var52;
               int var57 = var3 + var50 * var4 / var51;
               int var58 = var3 + var50 * var4 / var52;
               if(var16) {
                  if(var53 < var39) {
                     var39 = var53;
                  }

                  if(var54 > var40) {
                     var40 = var54;
                  }

                  if(var55 < var41) {
                     var41 = var55;
                  }

                  if(var56 < var41) {
                     var41 = var56;
                  }

                  if(var57 > var42) {
                     var42 = var57;
                  }

                  if(var58 > var42) {
                     var42 = var58;
                  }
               } else {
                  FunImage var59 = var21[var43 == var24?var23:var23 & 12];
                  int var60 = 0;
                  if(var11 < var13) {
                     var60 = (var24 - var43) * var59.originalWidth / (var24 * 2);
                  }

                  if(var11 > var13) {
                     var60 = (var24 + var43) * var59.originalWidth / (var24 * 2);
                  }

                  wj.a(var59, var22, var53, var54, var55, var56, var57, var58, var43 < var24, false, var60, -1);
               }
            }
         }

         if(var39 != Integer.MAX_VALUE) {
            cd.d(var39 - 2, var41 - 2, var40 - var39 + 4, var42 - var41 + 4, 2, 0);
         }
      }

   }

   private final void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, boolean var9, boolean var10, int var11, int var12, int var13) {
      int var22 = client.L;

      try {
         int var14;
         int var15;
         int var16;
         label152: {
            var2 -= var8;
            var5 -= var1;
            var7 -= var13;
            var14 = FunUC.a(var5 * var5 + var7 * var7, 0, j.originalWidth * -(var1 * var5 + var13 * var7));
            var15 = FunUC.a(var2, 0, -var8 * j.originalHeight);
            var16 = var14;
            if(var15 < 0) {
               var15 = 0;
               if(var22 == 0) {
                  break label152;
               }
            }

            if(var15 > j.originalHeight) {
               var15 = j.originalHeight;
            }
         }

         label123: {
            if(~var14 <= -1) {
               if(var14 <= j.originalWidth) {
                  break label123;
               }

               var14 = j.originalWidth;
               if(var22 == 0) {
                  break label123;
               }
            }

            var14 = 0;
         }

         int var17 = 0;
         int var18;
         if(var22 != 0) {
            var18 = 0;
            if(var22 == 0 && var18 >= j.originalHeight) {
               ++var17;
            } else {
               while(true) {
				   a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var17, var18, var16, var4, var9, true);
                  ++var18;
                  if(var18 >= j.originalHeight) {
                     ++var17;
                     break;
                  }
               }
            }
         }

         while(var17 < j.originalWidth) {
            var18 = 0;
            if(var22 == 0 && var18 >= j.originalHeight) {
               ++var17;
            } else {
               do {
				   a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var17, var18, var16, var4, var9, true);
                  ++var18;
               } while(var18 < j.originalHeight);

               ++var17;
            }
         }

         if(!var10) {
			 h = null;
         }

         var18 = 0;
         int var19;
         int var20;
         if(var22 != 0 || ~var18 > ~var14) {
            do {
               var19 = 0;
               if(var22 != 0) {
				   a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var18, var19, var16, var4, var9, false);
                  ++var19;
               }

               while(var15 > var19) {
				   a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var18, var19, var16, var4, var9, false);
                  ++var19;
               }

               var20 = -1 + j.originalHeight;
               if(var22 != 0 || var15 <= var20) {
                  do {
					  a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var18, var20, var16, var4, var9, false);
                     --var20;
                  } while(var15 <= var20);

                  ++var18;
               } else {
                  ++var18;
               }
            } while(~var18 > ~var14);
         }

         var19 = -1 + j.originalWidth;
         if(var22 != 0 || ~var14 >= ~var19) {
            do {
               var20 = 0;
               if(var22 != 0) {
				   a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var19, var20, var16, var4, var9, false);
                  ++var20;
               }

               while(var15 > var20) {
				   a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var19, var20, var16, var4, var9, false);
                  ++var20;
               }

               int var21 = j.originalHeight - 1;
               if(var22 != 0 || ~var15 >= ~var21) {
                  do {
					  a(var12, var3, var11, var6, var13, var8, var1, var7, var2, var5, var19, var21, var16, var4, var9, false);
                     --var21;
                  } while(~var15 >= ~var21);

                  --var19;
               } else {
                  --var19;
               }
            } while(~var14 >= ~var19);

         }
      } catch (RuntimeException var23) {
         throw pl.a(var23, "se.A(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ',' + var9 + ',' + var10 + ',' + var11 + ',' + var12 + ',' + var13 + ')');
      }
   }

   final void a(int var1, boolean var2, int var3, int var4, int var5, int var6, boolean var7) {
      try {
         int var8 = 2000;
         int var9 = 1500;
         if(var2) {
            var9 = 600;
         }

         label28: {
            if(~(var8 * j.originalHeight) > ~(var9 * j.originalWidth)) {
               var9 = var8 * j.originalHeight / j.originalWidth;
               if(client.L == 0) {
                  break label28;
               }
            }

            var8 = j.originalWidth * var9 / j.originalHeight;
         }

         int var10 = !var2?0:5 * f;
		  a(10000, var9 + -var10, var4, var5, 10000, var3, var8, -var9 + -var10, var2, var7, var1, var6, -var8);
      } catch (RuntimeException var11) {
         throw pl.a(var11, "se.C(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
      }
   }

   se(String var1, int var2, boolean var3) {

	   super();
      int var27 = client.L;
	   f = 0;

      try {
		  g = var3;
         int width = FunVB.w.a(var1, Integer.MAX_VALUE);
         --width;
         int height = FunVB.w.b(var1, Integer.MAX_VALUE, 8);
		  j = new FunImage(width, height);
         int[] var6 = cd.pixels;
         int var7 = cd.width;
         int var8 = cd.height;
		  j.f();
         FunVB.w.a(var1, 0, 0, width - -1, height, var2, -1, 1, 1, 8);
         cd.a(var6, var7, var8);
		  h = new int[height * width];
         int[] nPixels = new int[height * width];
         int h = 0;
         if(var27 != 0 || h < height) {
            do {
               int var11 = 0;
               if(var27 == 0 && ~var11 <= ~width) {
                  ++h;
               } else {
                  do {
                     if(this.h[var11 + h * width] == 0) {
                        int pixel = j.pixels[var11 + width * h];
                        if((pixel & 24) != 0) {
                           int var13 = var11;
                           int var14 = var11;
                           int var15 = h;
                           int var16 = h;
                           int var17 = 0;
                           int i = 1;
                           nPixels[0] = var11 + h * width;
							j.pixels[width * h - -var11] += Integer.MIN_VALUE;
                           int var19;
                           int var21;
                           int w;
                           if((pixel & 24) == 8) {
                              var19 = pixel & 31;
                              if(var27 != 0 || ~i < ~var17) {
                                 do {
                                    w = nPixels[var17++];
                                    var21 = w % width;
                                    if(~var21 < ~var14) {
                                       var14 = var21;
                                    }

                                    if(var21 < var13) {
                                       var13 = var21;
                                    }

                                    int var22 = w / width;
                                    if(~var22 > ~var15) {
                                       var15 = var22;
                                    }

                                    if(var22 > var16) {
                                       var16 = var22;
                                    }

                                    if(~var21 < -1 && ~(j.pixels[w + -1] & -2147483617) == ~var19) {
                                       nPixels[i++] = w - 1;
										j.pixels[w - 1] += Integer.MIN_VALUE;
                                    }

                                    if(width - 1 > var21 && var19 == (-2147483617 & j.pixels[w - -1])) {
                                       nPixels[i++] = 1 + w;
										j.pixels[w + 1] += Integer.MIN_VALUE;
                                    }

                                    if(var22 > 0 && ~(j.pixels[w + -width] & -2147483617) == ~var19) {
                                       nPixels[i++] = -width + w;
										j.pixels[-width + w] += Integer.MIN_VALUE;
                                    }

                                    if(var22 < height - 1 && ~(j.pixels[width + w] & -2147483617) == ~var19) {
                                       nPixels[i++] = w + width;
										j.pixels[width + w] += Integer.MIN_VALUE;
                                    }
                                 } while(~i < ~var17);
                              }
                           }

                           var19 = -width + var13 + var14 + 1;
                           w = 1 + var16 + var15 + -height;
                           var21 = 150 + FunHB.a(-126, 51, re.e);
                           int var23 = FunHB.a(-121, 17, re.e) + -8;
                           int var24 = (var23 << 24) + (var21 << 16) + (var19 & 255) - -('\uff00' & w << 8);
                           int var25 = 0;
                           if(var27 != 0 || ~var25 > ~i) {
                              do {
                                 int var26 = nPixels[var25];
								  j.pixels[var26] -= Integer.MIN_VALUE;
								  this.h[var26] = var24;
                                 ++var25;
                              } while(~var25 > ~i);
                           }
                        }
                     }

                     ++var11;
                  } while(~var11 > ~width);

                  ++h;
               }
            } while(h < height);

         }
      } catch (RuntimeException var28) {
         throw pl.a(var28, "se.<init>(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ')');
      }
   }

}
