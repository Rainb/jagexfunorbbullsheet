import java.applet.Applet;
import java.net.MalformedURLException;
import java.net.URL;

final class ec extends FunNode {

	int f;
	int g;
	int[] h;
	static String[] i = new String[8];
	static int[] j = new int[4];
	static int[] k;
	int l;
	int m;
	static String invitingFormat;
	static dd o;
	int p;
	static ph q;
	int r;
	static FunImage s;
	static String[] waitingForTranslation;
	static String u;


	static void showDocument(Applet applet, boolean var1, String var2, int var3) {
		try {
			if (!om.os.startsWith("win") || !dn.a(var2, var3 ^ -14441)) {
				try {
					applet.getAppletContext().showDocument(new URL(var2), "_blank");
					if (var3 != 14358) {
						showDocument(null, true, null, 127);
					}
				} catch (MalformedURLException var5) {
					dj.a("MGR1: " + var2, var3 + -14358, null);
				}
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "ec.C(" + (applet != null ? "{...}" : "null") + ',' + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
		}
	}

	static w a(int var0, byte var1, int var2, nf var3, nf var4) {
		try {
			return !FunNB.a(var2, var4, var0, false) ? null : var1 >= -82 ? null : ac.a(var3.c(var2, var0, -124), -22168);
		} catch (RuntimeException var6) {
			throw pl.a(var6, "ec.E(" + var0 + ',' + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + (var4 != null ? "{...}" : "null") + ')');
		}
	}

	public static void a(byte var0) {
		try {
			i = null;
			s = null;
			invitingFormat = null;
			u = null;
			k = null;
			j = null;
			o = null;
			waitingForTranslation = null;
			q = null;
		} catch (RuntimeException var2) {
			throw pl.a(var2, "ec.D(" + var0 + ')');
		}
	}

	static void a(byte var0, int var1, boolean var2, boolean var3) {
		int var5 = client.L;

		try {
			MainCacheFile.c(0);
			if (var0 != -21) {
				u = null;
			}

			label97:
			{
				if (~gn.f > -1) {
					ie.c.W = null;
					if (var5 == 0) {
						break label97;
					}
				}

				ie.c.W = FunO.a(qa.t, new String[] { Integer.toString(gn.f) }, (byte) -116);
			}

			wc.e.a((byte) 79, var3 && !var2 && !ge.k && ka.y == null && wf.R == null);
			jh.r.a((byte) 38, var3 && !var2 && !ge.k && ka.y == null && wf.R == null);
			uk.s.a((byte) 90, var3 && !var2 && ge.k);
			wm.a.a.c((byte) 57);
			if (~ae.d.yb != -1) {
				be.J = true;
			}

			if (mc.c.yb != 0) {
				label110:
				{
					if (qi.m || ~sh.s < -1 || jc.h >= 2 && ed.e[12]) {
						lc.a(var1, false);
						if (var5 == 0) {
							break label110;
						}
					}

					ii.W = true;
				}
			}

			if (~in.a.yb != -1) {
				if (ea.g.length == 1 && ea.g[0] != gf.m) {
					gf.m = ea.g[0];
				}

				ac.a(0, gf.m, true, var1, false, jd.h);
			}

			if (~mf.E.yb != -1) {
				ge.k = false;
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "ec.B(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ')');
		}
	}

	static void a(byte var0, String var1) {
		try {
			int var2 = jn.a;
			if (var0 < -3) {
				int var3 = pc.Hb;
				int var4 = cg.b.I.c(var1, 500);
				int var5 = 6 + cg.b.I.a(var1, 500);
				int var6 = 2 + FunRB.v * var4;
				int var7 = FunH.a(12, var2, (byte) -116, var5);
				int var8 = jd.a(var3, (byte) 110, var6, 20);
				cd.a(var7, var8, var5, var6, 0);
				cd.d(1 + var7, var8 + 1, -2 + var5, -2 + var6, 16777088);
				cg.b.I.a(var1, 3 + var7, 1 + var8 + FunBB.f + -cg.b.I.t, 500, 1000, 0, -1, 0, 0, FunRB.v);
			}
		} catch (RuntimeException var9) {
			throw pl.a(var9, "ec.F(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ')');
		}
	}

	final void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int[] var8) {
		try {
			r = var5;
			g = var3;
			f = var2;
			l = var7;
			p = var4;
			m = var1;
			h = var8;
			if (var6 != 4) {
				l = -85;
			}
		} catch (RuntimeException var10) {
			throw pl.a(var10, "ec.A(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + (var8 != null ? "{...}" : "null") + ')');
		}
	}

	static {
		i[5] = "undersea";
		i[0] = "fruit";
		i[7] = "eightbit";
		i[4] = "flowers";
		i[2] = "breakfast";
		i[1] = "animals";
		i[6] = "city";
		i[3] = "bugs";
		invitingFormat = "Inviting <%0>";
		waitingForTranslation = new String[] { "Waiting for text", "Warte auf Text", "En attente du texte", "Aguardando textos", "Op tekst wachten", "Esperando a texto" };
		o = new dd(13, 0, 1, 0);
		u = "To Customer Support";
	}
}
