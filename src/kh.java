
final class kh extends FunImage {

   final void e(int var1, int var2, int var3) {
      if(var3 == 256) {
		  c(var1, var2);
      } else {
         var1 += super.r;
         var2 += super.v;
         int var4 = var1 + var2 * cd.width;
         int var5 = 0;
         int var6 = super.height;
         int var7 = super.width;
         int var8 = cd.width - var7;
         int var9 = 0;
         int var10;
         if(var2 < cd.g) {
            var10 = cd.g - var2;
            var6 -= var10;
            var2 = cd.g;
            var5 += var10 * var7;
            var4 += var10 * cd.width;
         }

         if(var2 + var6 > cd.e) {
            var6 -= var2 + var6 - cd.e;
         }

         if(var1 < cd.l) {
            var10 = cd.l - var1;
            var7 -= var10;
            var1 = cd.l;
            var5 += var10;
            var4 += var10;
            var9 += var10;
            var8 += var10;
         }

         if(var1 + var7 > cd.j) {
            var10 = var1 + var7 - cd.j;
            var7 -= var10;
            var9 += var10;
            var8 += var10;
         }

         if(var7 > 0 && var6 > 0) {
            e(cd.pixels, super.pixels, 0, var5, var4, var7, var6, var8, var9, var3);
         }
      }
   }

   private static void d(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
      int var13 = var3;

      for(int var14 = -var8; var14 < 0; ++var14) {
         int var15 = (var4 >> 16) * var11;

         for(int var16 = -var7; var16 < 0; ++var16) {
            int var17 = var1[(var3 >> 16) + var15];
            int var18 = var0[var5];
            int var19 = (var17 >>> 24) * var12 >> 8;
            int var20 = 256 - var19;
            var0[var5++] = ((var17 & 16711935) * var19 + (var18 & 16711935) * var20 & -16711936) + ((var17 & '\uff00') * var19 + (var18 & '\uff00') * var20 & 16711680) >>> 8;
            var3 += var9;
         }

         var4 += var10;
         var3 = var13;
         var5 += var6;
      }

   }

   private static void d(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
      for(int var10 = -var6; var10 < 0; ++var10) {
         for(int var11 = -var5; var11 < 0; ++var11) {
            int var12 = (var1[var3] >>> 24) * var9 >> 8;
            int var13 = 256 - var12;
            int var14 = var1[var3++];
            int var15 = var0[var4];
            var0[var4++] = ((var14 & 16711935) * var12 + (var15 & 16711935) * var13 & -16711936) + ((var14 & '\uff00') * var12 + (var15 & '\uff00') * var13 & 16711680) >>> 8;
         }

         var4 += var7;
         var3 += var8;
      }

   }

   private static void c(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
      int var12 = var11 & 16711935;
      int var13 = var11 >> 8 & 255;

      for(var6 = -var8; var6 < 0; ++var6) {
         for(var5 = -var7; var5 < 0; ++var5) {
            var2 = var1[var3++];
            int var14 = var2 >>> 24;
            var2 &= 16777215;
            if(var14 != 0) {
				int var18;
               if(var2 >> 8 == var2) {
                  var2 &= 255;
                  var18 = (var2 * var12 >> 8 & 16711934) + (var2 * var13 & '\uff00') + 1;
               } else {
                  var18 = var2;
               }

               int var16 = 256 - var14;
               int var17 = var0[var4];
               var0[var4++] = ((var18 & 16711935) * var14 + (var17 & 16711935) * var16 & -16711936) + ((var18 & '\uff00') * var14 + (var17 & '\uff00') * var16 & 16711680) >>> 8;
            } else {
               ++var4;
            }
         }

         var4 += var9;
         var3 += var10;
      }

   }

   final void b(int var1, int var2) {
      int var3 = super.width >> 1;
      int var4 = super.height >> 1;
      var1 += super.r / 2;
      var2 += super.v / 2;
      int var5 = var1 < cd.l?cd.l - var1 << 1:0;
      int var6 = var1 + var3 > cd.j?(cd.j - var1 << 1) - 2:super.width - 2;
      int var7 = var2 < cd.g?cd.g - var2 << 1:0;
      int var8 = var2 + var4 > cd.e?(cd.e - var2 << 1) - 2:super.height - 2;

      for(int var9 = var7; var9 <= var8; var9 += 2) {
         int var10 = var9 * super.width + var5;
         int var11 = (var2 + (var9 >> 1)) * cd.width + var1 + (var5 >> 1);

         for(int var12 = var5; var12 <= var6; var10 += 2) {
			 int var15 = 0;
            int var16 = 0;
            int var17 = 0;
            int var18 = 0;

            for(int var19 = 0; var19 < 4; ++var19) {
               int var23 = super.pixels[var10 + (var19 & 1) + ((var19 & 2) == 0?super.width :0)];
               int var24 = var23 >>> 24;
               var18 += var24;
               var15 += var24 * (var23 >> 16 & 255);
               var16 += var24 * (var23 >> 8 & 255);
               var17 += var24 * (var23 & 255);
            }

            if(var18 != 0) {
               var15 = (var15 / var18 << 16) + var17 / var18;
               var16 = var16 / var18 << 8;
               int var20 = var18 >> 2;
               int var21 = 256 - var20;
               int var22 = cd.pixels[var11];
               cd.pixels[var11] = (var20 * var15 + var21 * (var22 & 16711935) & -16711936) + (var20 * var16 + var21 * (var22 & '\uff00') & 16711680) >>> 8;
            }

            var12 += 2;
            ++var11;
         }
      }

   }

   final void d(int var1, int var2, int var3, int var4, int var5) {
      if(var3 > 0 && var4 > 0) {
         int var6 = super.width;
         int var7 = super.height;
         int var8 = 0;
         int var9 = 0;
         int var10 = super.originalWidth;
         int var11 = super.originalHeight;
         int var12 = (var10 << 16) / var3;
         int var13 = (var11 << 16) / var4;
         int var14;
         if(super.r > 0) {
            var14 = ((super.r << 16) + var12 - 1) / var12;
            var1 += var14;
            var8 += var14 * var12 - (super.r << 16);
         }

         if(super.v > 0) {
            var14 = ((super.v << 16) + var13 - 1) / var13;
            var2 += var14;
            var9 += var14 * var13 - (super.v << 16);
         }

         if(var6 < var10) {
            var3 = ((var6 << 16) - var8 + var12 - 1) / var12;
         }

         if(var7 < var11) {
            var4 = ((var7 << 16) - var9 + var13 - 1) / var13;
         }

         var14 = var1 + var2 * cd.width;
         int var15 = cd.width - var3;
         if(var2 + var4 > cd.e) {
            var4 -= var2 + var4 - cd.e;
         }

         int var16;
         if(var2 < cd.g) {
            var16 = cd.g - var2;
            var4 -= var16;
            var14 += var16 * cd.width;
            var9 += var13 * var16;
         }

         if(var1 + var3 > cd.j) {
            var16 = var1 + var3 - cd.j;
            var3 -= var16;
            var15 += var16;
         }

         if(var1 < cd.l) {
            var16 = cd.l - var1;
            var3 -= var16;
            var14 += var16;
            var8 += var12 * var16;
            var15 += var16;
         }

         d(cd.pixels, super.pixels, 0, var8, var9, var14, var15, var3, var4, var12, var13, var6, var5);
      }
   }

   final void b(int var1, int var2, int var3, int var4, int var5) {
      if(var5 == 256) {
		  d(var1, var2, var3, var4);
      } else if(var3 > 0 && var4 > 0) {
         int var6 = super.width;
         int var7 = super.height;
         int var8 = 0;
         int var9 = 0;
         int var10 = super.originalWidth;
         int var11 = super.originalHeight;
         int var12 = (var10 << 16) / var3;
         int var13 = (var11 << 16) / var4;
         int var14;
         if(super.r > 0) {
            var14 = ((super.r << 16) + var12 - 1) / var12;
            var1 += var14;
            var8 += var14 * var12 - (super.r << 16);
         }

         if(super.v > 0) {
            var14 = ((super.v << 16) + var13 - 1) / var13;
            var2 += var14;
            var9 += var14 * var13 - (super.v << 16);
         }

         if(var6 < var10) {
            var3 = ((var6 << 16) - var8 + var12 - 1) / var12;
         }

         if(var7 < var11) {
            var4 = ((var7 << 16) - var9 + var13 - 1) / var13;
         }

         var14 = var1 + var2 * cd.width;
         int var15 = cd.width - var3;
         if(var2 + var4 > cd.e) {
            var4 -= var2 + var4 - cd.e;
         }

         int var16;
         if(var2 < cd.g) {
            var16 = cd.g - var2;
            var4 -= var16;
            var14 += var16 * cd.width;
            var9 += var13 * var16;
         }

         if(var1 + var3 > cd.j) {
            var16 = var1 + var3 - cd.j;
            var3 -= var16;
            var15 += var16;
         }

         if(var1 < cd.l) {
            var16 = cd.l - var1;
            var3 -= var16;
            var14 += var16;
            var8 += var12 * var16;
            var15 += var16;
         }

         e(cd.pixels, super.pixels, 0, var8, var9, var14, var15, var3, var4, var12, var13, var6, var5);
      }
   }

   kh(int width, int height) {
      super(width, height);
   }

   final void d(int var1, int var2, int var3, int var4) {
      if(var3 > 0 && var4 > 0) {
         int var5 = super.width;
         int var6 = super.height;
         int var7 = 0;
         int var8 = 0;
         int var9 = super.originalWidth;
         int var10 = super.originalHeight;
         int var11 = (var9 << 16) / var3;
         int var12 = (var10 << 16) / var4;
         int var13;
         if(super.r > 0) {
            var13 = ((super.r << 16) + var11 - 1) / var11;
            var1 += var13;
            var7 += var13 * var11 - (super.r << 16);
         }

         if(super.v > 0) {
            var13 = ((super.v << 16) + var12 - 1) / var12;
            var2 += var13;
            var8 += var13 * var12 - (super.v << 16);
         }

         if(var5 < var9) {
            var3 = ((var5 << 16) - var7 + var11 - 1) / var11;
         }

         if(var6 < var10) {
            var4 = ((var6 << 16) - var8 + var12 - 1) / var12;
         }

         var13 = var1 + var2 * cd.width;
         int var14 = cd.width - var3;
         if(var2 + var4 > cd.e) {
            var4 -= var2 + var4 - cd.e;
         }

         int var15;
         if(var2 < cd.g) {
            var15 = cd.g - var2;
            var4 -= var15;
            var13 += var15 * cd.width;
            var8 += var12 * var15;
         }

         if(var1 + var3 > cd.j) {
            var15 = var1 + var3 - cd.j;
            var3 -= var15;
            var14 += var15;
         }

         if(var1 < cd.l) {
            var15 = cd.l - var1;
            var3 -= var15;
            var13 += var15;
            var7 += var11 * var15;
            var14 += var15;
         }

         d(cd.pixels, super.pixels, 0, var7, var8, var13, var14, var3, var4, var11, var12, var5);
      }
   }

   final void c(int var1, int var2) {
      var1 += super.r;
      var2 += super.v;
      int var3 = var1 + var2 * cd.width;
      int var4 = 0;
      int var5 = super.height;
      int var6 = super.width;
      int var7 = cd.width - var6;
      int var8 = 0;
      int var9;
      if(var2 < cd.g) {
         var9 = cd.g - var2;
         var5 -= var9;
         var2 = cd.g;
         var4 += var9 * var6;
         var3 += var9 * cd.width;
      }

      if(var2 + var5 > cd.e) {
         var5 -= var2 + var5 - cd.e;
      }

      if(var1 < cd.l) {
         var9 = cd.l - var1;
         var6 -= var9;
         var1 = cd.l;
         var4 += var9;
         var3 += var9;
         var8 += var9;
         var7 += var9;
      }

      if(var1 + var6 > cd.j) {
         var9 = var1 + var6 - cd.j;
         var6 -= var9;
         var8 += var9;
         var7 += var9;
      }

      if(var6 > 0 && var5 > 0) {
         c(cd.pixels, super.pixels, 0, var4, var3, var6, var5, var7, var8);
      }
   }

   final void b(int var1, int var2, int var3) {
      var1 += super.r;
      var2 += super.v;
      int var4 = var1 + var2 * cd.width;
      int var5 = 0;
      int var6 = super.height;
      int var7 = super.width;
      int var8 = cd.width - var7;
      int var9 = 0;
      int var10;
      if(var2 < cd.g) {
         var10 = cd.g - var2;
         var6 -= var10;
         var2 = cd.g;
         var5 += var10 * var7;
         var4 += var10 * cd.width;
      }

      if(var2 + var6 > cd.e) {
         var6 -= var2 + var6 - cd.e;
      }

      if(var1 < cd.l) {
         var10 = cd.l - var1;
         var7 -= var10;
         var1 = cd.l;
         var5 += var10;
         var4 += var10;
         var9 += var10;
         var8 += var10;
      }

      if(var1 + var7 > cd.j) {
         var10 = var1 + var7 - cd.j;
         var7 -= var10;
         var9 += var10;
         var8 += var10;
      }

      if(var7 > 0 && var6 > 0) {
         d(cd.pixels, super.pixels, 0, var5, var4, var7, var6, var8, var9, var3);
      }
   }

   final void f(int var1, int var2, int var3) {
      var1 += super.r;
      var2 += super.v;
      int var4 = var1 + var2 * cd.width;
      int var5 = 0;
      int var6 = super.height;
      int var7 = super.width;
      int var8 = cd.width - var7;
      int var9 = 0;
      int var10;
      if(var2 < cd.g) {
         var10 = cd.g - var2;
         var6 -= var10;
         var2 = cd.g;
         var5 += var10 * var7;
         var4 += var10 * cd.width;
      }

      if(var2 + var6 > cd.e) {
         var6 -= var2 + var6 - cd.e;
      }

      if(var1 < cd.l) {
         var10 = cd.l - var1;
         var7 -= var10;
         var1 = cd.l;
         var5 += var10;
         var4 += var10;
         var9 += var10;
         var8 += var10;
      }

      if(var1 + var7 > cd.j) {
         var10 = var1 + var7 - cd.j;
         var7 -= var10;
         var9 += var10;
         var8 += var10;
      }

      if(var7 > 0 && var6 > 0) {
         c(cd.pixels, super.pixels, 0, var5, var4, 0, 0, var7, var6, var8, var9, var3);
      }
   }

   private static void e(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
      int var13 = var3;

      for(int var14 = -var8; var14 < 0; ++var14) {
         int var15 = (var4 >> 16) * var11;

         for(int var16 = -var7; var16 < 0; ++var16) {
            var2 = var1[(var3 >> 16) + var15];
            int var17 = var2 >>> 24;
            if(var17 != 0) {
               int var18 = var0[var5];
               int var19 = 256 - var17;
               int var20 = (var2 & 16711935) * var12 & -16711936;
               int var21 = (var2 & '\uff00') * var12 & 16711680;
               var2 = (var20 | var21) >>> 8;
               var0[var5++] = ((var2 & 16711935) * var17 + (var18 & 16711935) * var19 & -16711936) + ((var2 & '\uff00') * var17 + (var18 & '\uff00') * var19 & 16711680) >>> 8;
            } else {
               ++var5;
            }

            var3 += var9;
         }

         var4 += var10;
         var3 = var13;
         var5 += var6;
      }

   }

   private static void c(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      int var9 = -var5;

      for(int var10 = -var6; var10 < 0; ++var10) {
         for(int var11 = var9; var11 < 0; ++var11) {
            var2 = var1[var3++];
            int var12 = var2 >>> 24;
            if(var12 != 0) {
               int var13 = 256 - var12;
               int var14 = var0[var4];
               var0[var4++] = ((var2 & 16711935) * var12 + (var14 & 16711935) * var13 & -16711936) + ((var2 & '\uff00') * var12 + (var14 & '\uff00') * var13 & 16711680) >>> 8;
            } else {
               ++var4;
            }
         }

         var4 += var7;
         var3 += var8;
      }

   }

   final void c(int var1, int var2, int var3) {
      var1 += super.r;
      var2 += super.v;
      int var4 = var1 + var2 * cd.width;
      int var5 = 0;
      int var6 = super.height;
      int var7 = super.width;
      int var8 = cd.width - var7;
      int var9 = 0;
      int var10;
      if(var2 < cd.g) {
         var10 = cd.g - var2;
         var6 -= var10;
         var2 = cd.g;
         var5 += var10 * var7;
         var4 += var10 * cd.width;
      }

      if(var2 + var6 > cd.e) {
         var6 -= var2 + var6 - cd.e;
      }

      if(var1 < cd.l) {
         var10 = cd.l - var1;
         var7 -= var10;
         var1 = cd.l;
         var5 += var10;
         var4 += var10;
         var9 += var10;
         var8 += var10;
      }

      if(var1 + var7 > cd.j) {
         var10 = var1 + var7 - cd.j;
         var7 -= var10;
         var9 += var10;
         var8 += var10;
      }

      if(var7 > 0 && var6 > 0) {
         f(cd.pixels, super.pixels, 0, var5, var4, var7, var6, var8, var9, var3);
      }
   }

   private static void e(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
      for(int var10 = -var6; var10 < 0; ++var10) {
         for(int var11 = -var5; var11 < 0; ++var11) {
            var2 = var1[var3++];
            int var12 = var2 >>> 24;
            if(var12 != 0) {
               int var13 = var0[var4];
               int var14 = 256 - var12;
               int var15 = (var2 & 16711935) * var9 & -16711936;
               int var16 = (var2 & '\uff00') * var9 & 16711680;
               var2 = (var15 | var16) >>> 8;
               var0[var4++] = ((var2 & 16711935) * var12 + (var13 & 16711935) * var14 & -16711936) + ((var2 & '\uff00') * var12 + (var13 & '\uff00') * var14 & 16711680) >>> 8;
            } else {
               ++var4;
            }
         }

         var4 += var7;
         var3 += var8;
      }

   }

   final void d(int var1, int var2) {
      var1 += super.r;
      var2 += super.v;
      int var3 = var1 + var2 * cd.width;
      int var4 = 0;
      int var5 = super.height;
      int var6 = super.width;
      int var7 = cd.width - var6;
      int var8 = 0;
      int var9;
      if(var2 < cd.g) {
         var9 = cd.g - var2;
         var5 -= var9;
         var2 = cd.g;
         var4 += var9 * var6;
         var3 += var9 * cd.width;
      }

      if(var2 + var5 > cd.e) {
         var5 -= var2 + var5 - cd.e;
      }

      if(var1 < cd.l) {
         var9 = cd.l - var1;
         var6 -= var9;
         var1 = cd.l;
         var4 += var9;
         var3 += var9;
         var8 += var9;
         var7 += var9;
      }

      if(var1 + var6 > cd.j) {
         var9 = var1 + var6 - cd.j;
         var6 -= var9;
         var8 += var9;
         var7 += var9;
      }

      if(var6 > 0 && var5 > 0) {
         c(cd.pixels, super.pixels, 0, var4, var3, var6, var5, var7, var8);
      }
   }

   kh(int var1, int var2, int var3, int var4, int var5, int var6, int[] var7) {
      super(var1, var2, var3, var4, var5, var6, var7);
   }

   private static void f(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
      int var10 = var9 >> 16 & 255;
      int var11 = var9 >> 8 & 255;
      int var12 = var9 & 255;
      int var13 = -(var5 >> 2);
      var5 = -(var5 & 3);
      int var14 = var13 + var13 + var13 + var13 + var5;

      for(int var15 = -var6; var15 < 0; ++var15) {
         for(int var16 = var14; var16 < 0; ++var16) {
            var2 = var1[var3++];
            int var17 = var2 >>> 24;
            if(var17 != 0) {
               int var19 = var2 >> 16 & 255;
               int var20 = var2 >> 8 & 255;
               int var21 = var2 & 255;
               int var18;
               if(var19 == var20 && var20 == var21) {
                  if(var19 <= 128) {
                     var18 = (var19 * var10 >> 7 << 16) + (var20 * var11 >> 7 << 8) + (var21 * var12 >> 7);
                  } else {
                     var18 = (var10 * (256 - var19) + 255 * (var19 - 128) >> 7 << 16) + (var11 * (256 - var20) + 255 * (var20 - 128) >> 7 << 8) + (var12 * (256 - var21) + 255 * (var21 - 128) >> 7);
                  }
               } else {
                  var18 = var2;
               }

               int var22 = 256 - var17;
               int var23 = var0[var4];
               var0[var4++] = ((var18 & 16711935) * var17 + (var23 & 16711935) * var22 & -16711936) + ((var18 & '\uff00') * var17 + (var23 & '\uff00') * var22 & 16711680) >>> 8;
            } else {
               ++var4;
            }
         }

         var4 += var7;
         var3 += var8;
      }

   }

   private static void d(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
      int var12 = var3;

      for(int var13 = -var8; var13 < 0; ++var13) {
         int var14 = (var4 >> 16) * var11;

         for(int var15 = -var7; var15 < 0; ++var15) {
            var2 = var1[(var3 >> 16) + var14];
            int var16 = var2 >>> 24;
            if(var16 != 0) {
               int var17 = 256 - var16;
               int var18 = var0[var5];
               var0[var5++] = ((var2 & 16711935) * var16 + (var18 & 16711935) * var17 & -16711936) + ((var2 & '\uff00') * var16 + (var18 & '\uff00') * var17 & 16711680) >>> 8;
            } else {
               ++var5;
            }

            var3 += var9;
         }

         var4 += var10;
         var3 = var12;
         var5 += var6;
      }

   }

   private static void b(int var0, int var1, int var2, int[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13) {
      for(var8 = -var10; var8 < 0; ++var8) {
         for(var6 = -var9; var6 < 0; ++var6) {
            var0 = var4[var5++];
            if(var0 != 0) {
               int var14 = var13 * (var0 >>> 24) >> 8 & 255;
               var1 = (var0 & 16711935) * var14;
               var0 = (var1 & -16711936) + (var0 * var14 - var1 & 16711680) >>> 8;
               var1 = var3[var7];
               var2 = var0 + var1;
               var0 = (var0 & 16711935) + (var1 & 16711935);
               var1 = (var0 & 16777472) + (var2 - var0 & 65536);
               var3[var7++] = var2 - var1 | var1 - (var1 >>> 8);
            } else {
               ++var7;
            }
         }

         var7 += var11;
         var5 += var12;
      }

   }

   final void d(int var1, int var2, int var3) {
      var1 += super.r;
      var2 += super.v;
      int var4 = var1 + var2 * cd.width;
      int var5 = 0;
      int var6 = super.height;
      int var7 = super.width;
      int var8 = cd.width - var7;
      int var9 = 0;
      int var10;
      if(var2 < cd.g) {
         var10 = cd.g - var2;
         var6 -= var10;
         var2 = cd.g;
         var5 += var10 * var7;
         var4 += var10 * cd.width;
      }

      if(var2 + var6 > cd.e) {
         var6 -= var2 + var6 - cd.e;
      }

      if(var1 < cd.l) {
         var10 = cd.l - var1;
         var7 -= var10;
         var1 = cd.l;
         var5 += var10;
         var4 += var10;
         var9 += var10;
         var8 += var10;
      }

      if(var1 + var7 > cd.j) {
         var10 = var1 + var7 - cd.j;
         var7 -= var10;
         var9 += var10;
         var8 += var10;
      }

      if(var7 > 0 && var6 > 0) {
         b(0, 0, 0, cd.pixels, super.pixels, var5, 0, var4, 0, var7, var6, var8, var9, var3);
      }
   }
}
