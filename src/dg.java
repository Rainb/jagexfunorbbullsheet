
final class dg implements vg {

   static String loadingGraphics = "Loading graphics";
   private int b;
   static ph c;
   static String winner = "Winner!";
   private int e;
   private int f;
   static String solidShapesSEnt = "Solid shapes are sent to the next player!";
   private bk h;
   static int[] i;
   static og j = new og();
   static int[] k;
   private int l;
   private int m;
   static String n = "Enter name of player to add to list";
   private int o;
   static FunImage[][] p = new FunImage[8][];
   private int q;
   static FunL r = new FunL();
   static int s;
   static long t;


   static void a(FunImage var0, int var1, FunImage var2) {
      int var16 = client.L;

      try {
         float var4 = 1.0F;
         if(var1 == 127) {
            int var6 = var2.width;
            int var7 = var2.height;
            int[] var8 = var2.pixels;
            int[] var9 = var0.pixels;
            int var10 = 0;
            int var11 = 0;
            if(var16 != 0 || var7 > var11) {
               do {
                  int var12 = 0;
                  if(var16 == 0 && var12 >= var6) {
                     ++var11;
                  } else {
                     do {
                        int var13 = 255 & var8[var10];
                        int var14 = var13 / 5 + var12 - -var11 - -ol.b & 127;
                        float var5 = (float)var13 / 255.0F;
                        float var3 = (float)var14 / 128.0F;
                        int var15 = rk.a(var4, var5, (byte)123, var3);
                        var9[var10] = var15;
                        ++var10;
                        ++var12;
                     } while(var12 < var6);

                     ++var11;
                  }
               } while(var7 > var11);

            }
         }
      } catch (RuntimeException var17) {
         throw pl.a(var17, "dg.B(" + (var0 != null?"{...}":"null") + ',' + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   static void a(int var0) {
      try {
         ec var1 = (ec)FunE.d.e(126);
         if(var1 == null) {
            throw new IllegalStateException();
         } else {
            cd.a(var1.h, var1.g, var1.f);
            cd.b(var1.r, var1.l, var1.m, var1.p);
            var1.h = null;
            if(var0 != 900) {
               b(-56);
            }

            FunQ.n.a(var0 ^ -1018, var1);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "dg.C(" + var0 + ')');
      }
   }

   public final void a(boolean var1, bn var2, int var3, boolean var4, int var5) {
      try {
         rf var6 = var2 instanceof rf?(rf)var2:null;
         cd.d(var3 + var2.i, var2.p + var5, var2.n, var2.t, l);
         int var7 = var6.H + var2.i + var3;
         int var8 = var2.p + var5 + var6.J;
         cd.e(var7, var8, var6.L, b);
         double var9;
         int var11;
         int var12;
         if(var6.K != -1) {
            var9 = 2.0D * (double)var6.K * 3.141592653589793D / (double)var6.N;
            var11 = (int)(-Math.sin(var9) * (double)var6.L);
            var12 = (int)(Math.cos(var9) * (double)var6.L);
            cd.e(var11 + var7, var8 + var12, 1, o);
         }

         cd.e(var7, var8, 2, 1);
         var9 = (double)var6.I * 3.141592653589793D * 2.0D / (double)var6.N;
         var11 = (int)(-Math.sin(var9) * (double)var6.L);
         var12 = (int)(Math.cos(var9) * (double)var6.L);
         cd.e(var7, var8, var7 + var11, var12 + var8, 1);
         if(h != null) {
            int var13 = var6.H + (var6.L - -m);
			 h.a(var2.text, var13 + var2.i + var3, var5 + (var2.p - -q), var2.n - m - var13, var2.t + -(m << 2089081793), e, f, 1, 1, 0);
         }

         if(!var4) {
            c = null;
         }
      } catch (RuntimeException var14) {
         throw pl.a(var14, "dg.A(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

   static void b(int var0) {
      try {
         FunImage var1;
         FunImage var2;
         boolean var3;
         boolean var4;
         int var5;
         label33: {
            if(!na.k) {
               var3 = FunF.e;
               var4 = kc.h;
               var5 = sj.b * sj.b * 320 / 900;
               var2 = FunNB.Ab;
               var1 = FunVB.y;
               if(client.L == 0) {
                  break label33;
               }
            }

            var2 = FunVB.y;
            var1 = FunNB.Ab;
            var5 = -(sj.b * sj.b * 320 / 900) + 320;
            var3 = kc.h;
            var4 = FunF.e;
         }

         cd.b(0, 0, 640, -var5 + 160);
         if(var3 && var4) {
            var4 = false;
            var3 = false;
         }

         int var6 = 128 - -(128 * var5 / 320);
         og.a(var4, var2, 256, -var5, (byte)92);
         cd.b(0, -var5 + 160, 213, 320 - var5);
         og.a(var4, var2, 256, -var5, (byte)39);
         cd.b(427, 160 + -var5, 640, 320 + -var5);
         og.a(var4, var2, 256, -var5, (byte)32);
         cd.b(0, 320 + -var5, 213, 320 - -var5);
         og.a(var3, var1, var6, 0, (byte)73);
         cd.b(213, 160 - var5, 427, var5 + 160);
         og.a(var3, var1, var6, 0, (byte)33);
         cd.b(427, 320 + -var5, 640, var5 + 320);
         if(var0 >= -101) {
            a(-114);
         }

         og.a(var3, var1, var6, 0, (byte)76);
         cd.b(213, 160 + var5, 427, 320 - -var5);
         og.a(var4, var2, 256, var5, (byte)32);
         cd.b(0, 320 + var5, 640, 480);
         og.a(var4, var2, 256, var5, (byte)106);
         cd.c();
         if(kc.h && FunF.e && !qd.g) {
            rg.a(0, 0, 256, 25693);
         }
      } catch (RuntimeException var7) {
         throw pl.a(var7, "dg.E(" + var0 + ')');
      }
   }

   public dg() {}

   public static void a(boolean var0) {
      try {
         loadingGraphics = null;
         p = null;
         c = null;
         n = null;
         winner = null;
         r = null;
         solidShapesSEnt = null;
         if(var0) {
            j = null;
         }

         k = null;
         j = null;
         i = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "dg.D(" + var0 + ')');
      }
   }

   dg(bk var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      try {
		  f = var5;
		  q = var3;
		  h = var1;
		  l = var8;
		  o = var7;
		  m = var2;
		  e = var4;
		  b = var6;
      } catch (RuntimeException var10) {
         throw pl.a(var10, "dg.<init>(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ')');
      }
   }

}
