
final class pg extends gb {

   private int j;
   private int k;
   private int l;
   private int m;
   private int n;
   private int o;
   private int p;
   private int q;
   private int r;
   private int s;
   private int t;
   private boolean u;
   private int v;
   private int w;
   private int x;


   private static final int a(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, pg var10) {
      var3 >>= 8;
      var9 >>= 8;
      var5 <<= 2;
      var6 <<= 2;
      if((var7 = var4 + var3 - (var9 - 1)) > var8) {
         var7 = var8;
      }

      var4 <<= 1;
      var7 <<= 1;

      byte var11;
      int var10001;
      for(var7 -= 6; var4 < var7; var2[var10001] += var11 * var6) {
         var11 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
         var2[var10001] += var11 * var6;
         var11 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
         var2[var10001] += var11 * var6;
         var11 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
         var2[var10001] += var11 * var6;
         var11 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
      }

      for(var7 += 6; var4 < var7; var2[var10001] += var11 * var6) {
         var11 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
      }

      var10.m = var3 << 8;
      return var4 >> 1;
   }

   private static final int a(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, pg var9) {
      var2 >>= 8;
      var8 >>= 8;
      var4 <<= 2;
      var5 <<= 2;
      if((var6 = var3 + var2 - (var8 - 1)) > var7) {
         var6 = var7;
      }

      var9.k += var9.q * (var6 - var3);
      var9.o += var9.t * (var6 - var3);

      int var10001;
      for(var6 -= 3; var3 < var6; var4 += var5) {
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
         var4 += var5;
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
         var4 += var5;
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
         var4 += var5;
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
      }

      for(var6 += 3; var3 < var6; var4 += var5) {
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
      }

      var9.l = var4 >> 2;
      var9.m = var2 << 8;
      return var3;
   }

   private final void e() {
      if(this.r != 0) {
         if(this.j == Integer.MIN_VALUE) {
            this.j = 0;
         }

         this.r = 0;
         this.g();
      }

   }

   private final synchronized void a(int var1, int var2) {
      this.j = var1;
      this.v = var2;
      this.r = 0;
      this.g();
   }

   final synchronized void b(int var1, int var2) {
      this.w = var1;
      this.x = var2;
   }

   private final synchronized int f() {
      return this.v < 0?-1:this.v;
   }

   final synchronized void d(int var1) {
      var1 <<= 8;
      int var2 = ((hg)super.h).i.length << 8;
      if(var1 < -1) {
         var1 = -1;
      }

      if(var1 > var2) {
         var1 = var2;
      }

      this.m = var1;
   }

   static final pg a(hg var0, int var1, int var2, int var3) {
      return var0.i != null && var0.i.length != 0?new pg(var0, var1, var2 << 6, var3 << 6):null;
   }

   final synchronized void a(int var1) {
      if(this.r > 0) {
         if(var1 >= this.r) {
            if(this.j == Integer.MIN_VALUE) {
               this.j = 0;
               this.l = this.k = this.o = 0;
               this.b(true);
               var1 = this.r;
            }

            this.r = 0;
            this.g();
         } else {
            this.l += this.s * var1;
            this.k += this.q * var1;
            this.o += this.t * var1;
            this.r -= var1;
         }
      }

      hg var2 = (hg)super.h;
      int var3 = this.w << 8;
      int var4 = this.x << 8;
      int var5 = var2.i.length << 8;
      int var6 = var4 - var3;
      if(var6 <= 0) {
         this.p = 0;
      }

      if(this.m < 0) {
         if(this.n <= 0) {
            this.e();
            this.b(true);
            return;
         }

         this.m = 0;
      }

      if(this.m >= var5) {
         if(this.n >= 0) {
            this.e();
            this.b(true);
            return;
         }

         this.m = var5 - 1;
      }

      this.m += this.n * var1;
      if(this.p < 0) {
         if(!this.u) {
            if(this.n < 0) {
               if(this.m < var3) {
                  this.m = var4 - 1 - (var4 - 1 - this.m) % var6;
               }
            } else if(this.m >= var4) {
               this.m = var3 + (this.m - var3) % var6;
            }
         } else {
            if(this.n < 0) {
               if(this.m >= var3) {
                  return;
               }

               this.m = var3 + var3 - 1 - this.m;
               this.n = -this.n;
            }

            while(this.m >= var4) {
               this.m = var4 + var4 - 1 - this.m;
               this.n = -this.n;
               if(this.m >= var3) {
                  return;
               }

               this.m = var3 + var3 - 1 - this.m;
               this.n = -this.n;
            }

         }
      } else {
         if(this.p > 0) {
            if(this.u) {
               label126: {
                  if(this.n < 0) {
                     if(this.m >= var3) {
                        return;
                     }

                     this.m = var3 + var3 - 1 - this.m;
                     this.n = -this.n;
                     if(--this.p == 0) {
                        break label126;
                     }
                  }

                  do {
                     if(this.m < var4) {
                        return;
                     }

                     this.m = var4 + var4 - 1 - this.m;
                     this.n = -this.n;
                     if(--this.p == 0) {
                        break;
                     }

                     if(this.m >= var3) {
                        return;
                     }

                     this.m = var3 + var3 - 1 - this.m;
                     this.n = -this.n;
                  } while(--this.p != 0);
               }
            } else {
               int var7;
               if(this.n < 0) {
                  if(this.m >= var3) {
                     return;
                  }

                  var7 = (var4 - 1 - this.m) / var6;
                  if(var7 < this.p) {
                     this.m += var6 * var7;
                     this.p -= var7;
                     return;
                  }

                  this.m += var6 * this.p;
                  this.p = 0;
               } else {
                  if(this.m < var4) {
                     return;
                  }

                  var7 = (this.m - var3) / var6;
                  if(var7 < this.p) {
                     this.m -= var6 * var7;
                     this.p -= var7;
                     return;
                  }

                  this.m -= var6 * this.p;
                  this.p = 0;
               }
            }
         }

         if(this.n < 0) {
            if(this.m < 0) {
               this.m = -1;
               this.e();
               this.b(true);
               return;
            }
         } else if(this.m >= var5) {
            this.m = var5;
            this.e();
            this.b(true);
         }

      }
   }

   private static final int a(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, pg var8) {
      var2 >>= 8;
      var7 >>= 8;
      var4 <<= 2;
      if((var5 = var3 + var2 - (var7 - 1)) > var6) {
         var5 = var6;
      }

      int var10001;
      for(var5 -= 3; var3 < var5; var1[var10001] += var0[var2--] * var4) {
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
         var10001 = var3++;
         var1[var10001] += var0[var2--] * var4;
         var10001 = var3++;
      }

      for(var5 += 3; var3 < var5; var1[var10001] += var0[var2--] * var4) {
         var10001 = var3++;
      }

      var8.m = var2 << 8;
      return var3;
   }

   static final pg a(hg var0, int var1, int var2) {
      return var0.i != null && var0.i.length != 0?new pg(var0, var1, var2 << 6):null;
   }

   private final synchronized void e(int var1) {
      this.a(var1, this.f());
   }

   static final pg b(hg var0, int var1, int var2, int var3) {
      return var0.i != null && var0.i.length != 0?new pg(var0, var1, var2, var3):null;
   }

   final int d() {
      return this.j == 0 && this.r == 0?0:1;
   }

   private final int a(int[] var1, int var2, int var3, int var4, int var5) {
      do {
         if(this.r <= 0) {
            if(this.n == -256 && (this.m & 255) == 0) {
               if(bi.twoChannels) {
                  return a(0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, 0, var4, var3, this);
               }

               return a(((hg)super.h).i, var1, this.m, var2, this.l, 0, var4, var3, this);
            }

            if(bi.twoChannels) {
               return d(0, 0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, 0, var4, var3, this, this.n, var5);
            }

            return b(0, 0, ((hg)super.h).i, var1, this.m, var2, this.l, 0, var4, var3, this, this.n, var5);
         }

         int var6 = var2 + this.r;
         if(var6 > var4) {
            var6 = var4;
         }

         this.r += var2;
         if(this.n == -256 && (this.m & 255) == 0) {
            if(bi.twoChannels) {
               var2 = b(0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, this.q, this.t, 0, var6, var3, this);
            } else {
               var2 = a(((hg)super.h).i, var1, this.m, var2, this.l, this.s, 0, var6, var3, this);
            }
         } else if(bi.twoChannels) {
            var2 = b(0, 0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, this.q, this.t, 0, var6, var3, this, this.n, var5);
         } else {
            var2 = b(0, 0, ((hg)super.h).i, var1, this.m, var2, this.l, this.s, 0, var6, var3, this, this.n, var5);
         }

         this.r -= var2;
         if(this.r != 0) {
            return var2;
         }
      } while(!this.h());

      return var4;
   }

   private static final int b(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, pg var9) {
      var2 >>= 8;
      var8 >>= 8;
      var4 <<= 2;
      var5 <<= 2;
      if((var6 = var3 + var8 - var2) > var7) {
         var6 = var7;
      }

      var9.k += var9.q * (var6 - var3);
      var9.o += var9.t * (var6 - var3);

      int var10001;
      for(var6 -= 3; var3 < var6; var4 += var5) {
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
         var4 += var5;
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
         var4 += var5;
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
         var4 += var5;
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
      }

      for(var6 += 3; var3 < var6; var4 += var5) {
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
      }

      var9.l = var4 >> 2;
      var9.m = var2 << 8;
      return var3;
   }

   private static final int c(int var0, int var1) {
      return var1 < 0?-var0:(int)((double)var0 * Math.sqrt((double)var1 * 1.220703125E-4D) + 0.5D);
   }

   private static final int a(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, pg var11, int var12, int var13) {
      var11.k -= var11.q * var5;
      var11.o -= var11.t * var5;
      if(var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 257) / var12) > var9) {
         var8 = var9;
      }

      int var10001;
      byte var14;
      while(var5 < var8) {
         var1 = var4 >> 8;
         var14 = var2[var1];
         var10001 = var5++;
         var3[var10001] += ((var14 << 8) + (var2[var1 + 1] - var14) * (var4 & 255)) * var6 >> 6;
         var6 += var7;
         var4 += var12;
      }

      if(var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 1) / var12) > var9) {
         var8 = var9;
      }

      for(var1 = var13; var5 < var8; var4 += var12) {
         var14 = var2[var4 >> 8];
         var10001 = var5++;
         var3[var10001] += ((var14 << 8) + (var1 - var14) * (var4 & 255)) * var6 >> 6;
         var6 += var7;
      }

      var11.k += var11.q * var5;
      var11.o += var11.t * var5;
      var11.l = var6;
      var11.m = var4;
      return var5;
   }

   private static final int a(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, pg var13, int var14, int var15) {
      var13.l -= var13.s * var5;
      if(var14 == 0 || (var10 = var5 + (var12 - var4 + var14 - 257) / var14) > var11) {
         var10 = var11;
      }

      var5 <<= 1;

      int var10001;
      byte var16;
      for(var10 <<= 1; var5 < var10; var4 += var14) {
         var1 = var4 >> 8;
         var16 = var2[var1];
         var0 = (var16 << 8) + (var2[var1 + 1] - var16) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var6 += var8;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
         var7 += var9;
      }

      if(var14 == 0 || (var10 = (var5 >> 1) + (var12 - var4 + var14 - 1) / var14) > var11) {
         var10 = var11;
      }

      var10 <<= 1;

      for(var1 = var15; var5 < var10; var4 += var14) {
         var16 = var2[var4 >> 8];
         var0 = (var16 << 8) + (var1 - var16) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var6 += var8;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
         var7 += var9;
      }

      var5 >>= 1;
      var13.l += var13.s * var5;
      var13.k = var6;
      var13.o = var7;
      var13.m = var4;
      return var5;
   }

   private static final int a(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, pg var10, int var11, int var12) {
      if(var11 == 0 || (var7 = var5 + (var9 - var4 + var11 - 257) / var11) > var8) {
         var7 = var8;
      }

      int var10001;
      byte var13;
      while(var5 < var7) {
         var1 = var4 >> 8;
         var13 = var2[var1];
         var10001 = var5++;
         var3[var10001] += ((var13 << 8) + (var2[var1 + 1] - var13) * (var4 & 255)) * var6 >> 6;
         var4 += var11;
      }

      if(var11 == 0 || (var7 = var5 + (var9 - var4 + var11 - 1) / var11) > var8) {
         var7 = var8;
      }

      for(var1 = var12; var5 < var7; var4 += var11) {
         var13 = var2[var4 >> 8];
         var10001 = var5++;
         var3[var10001] += ((var13 << 8) + (var1 - var13) * (var4 & 255)) * var6 >> 6;
      }

      var10.m = var4;
      return var5;
   }

   static final pg c(hg var0, int var1, int var2, int var3) {
      return var0.i != null && var0.i.length != 0?new pg(var0, (int)((long)var0.h * 256L * (long)var1 / (long)(100 * qc.f)), var2, var3):null;
   }

   final synchronized void a(int var1, int var2, int var3) {
      this.b(var1, var2 << 6, var3 << 6);
   }

   private static final int b(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, pg var11, int var12, int var13) {
      var11.k -= var11.q * var5;
      var11.o -= var11.t * var5;
      if(var12 == 0 || (var8 = var5 + (var10 + 256 - var4 + var12) / var12) > var9) {
         var8 = var9;
      }

      int var10001;
      while(var5 < var8) {
         var1 = var4 >> 8;
         byte var14 = var2[var1 - 1];
         var10001 = var5++;
         var3[var10001] += ((var14 << 8) + (var2[var1] - var14) * (var4 & 255)) * var6 >> 6;
         var6 += var7;
         var4 += var12;
      }

      if(var12 == 0 || (var8 = var5 + (var10 - var4 + var12) / var12) > var9) {
         var8 = var9;
      }

      var0 = var13;

      for(var1 = var12; var5 < var8; var4 += var1) {
         var10001 = var5++;
         var3[var10001] += ((var0 << 8) + (var2[var4 >> 8] - var0) * (var4 & 255)) * var6 >> 6;
         var6 += var7;
      }

      var11.k += var11.q * var5;
      var11.o += var11.t * var5;
      var11.l = var6;
      var11.m = var4;
      return var5;
   }

   private final void g() {
      this.l = this.j;
      this.k = e(this.j, this.v);
      this.o = c(this.j, this.v);
   }

   private final int b(int[] var1, int var2, int var3, int var4, int var5) {
      do {
         if(this.r <= 0) {
            if(this.n == 256 && (this.m & 255) == 0) {
               if(bi.twoChannels) {
                  return b(0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, 0, var4, var3, this);
               }

               return b(((hg)super.h).i, var1, this.m, var2, this.l, 0, var4, var3, this);
            }

            if(bi.twoChannels) {
               return c(0, 0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, 0, var4, var3, this, this.n, var5);
            }

            return a(0, 0, ((hg)super.h).i, var1, this.m, var2, this.l, 0, var4, var3, this, this.n, var5);
         }

         int var6 = var2 + this.r;
         if(var6 > var4) {
            var6 = var4;
         }

         this.r += var2;
         if(this.n == 256 && (this.m & 255) == 0) {
            if(bi.twoChannels) {
               var2 = a(0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, this.q, this.t, 0, var6, var3, this);
            } else {
               var2 = b(((hg)super.h).i, var1, this.m, var2, this.l, this.s, 0, var6, var3, this);
            }
         } else if(bi.twoChannels) {
            var2 = a(0, 0, ((hg)super.h).i, var1, this.m, var2, this.k, this.o, this.q, this.t, 0, var6, var3, this, this.n, var5);
         } else {
            var2 = a(0, 0, ((hg)super.h).i, var1, this.m, var2, this.l, this.s, 0, var6, var3, this, this.n, var5);
         }

         this.r -= var2;
         if(this.r != 0) {
            return var2;
         }
      } while(!this.h());

      return var4;
   }

   private final boolean h() {
      int var1 = this.j;
      int var2;
      int var3;
      if(var1 == Integer.MIN_VALUE) {
         var3 = 0;
         var2 = 0;
         var1 = 0;
      } else {
         var2 = e(var1, this.v);
         var3 = c(var1, this.v);
      }

      if(this.l == var1 && this.k == var2 && this.o == var3) {
         if(this.j == Integer.MIN_VALUE) {
            this.j = 0;
            this.l = this.k = this.o = 0;
            this.b(true);
            return true;
         } else {
            this.g();
            return false;
         }
      } else {
         if(this.l < var1) {
            this.s = 1;
            this.r = var1 - this.l;
         } else if(this.l > var1) {
            this.s = -1;
            this.r = this.l - var1;
         } else {
            this.s = 0;
         }

         if(this.k < var2) {
            this.q = 1;
            if(this.r == 0 || this.r > var2 - this.k) {
               this.r = var2 - this.k;
            }
         } else if(this.k > var2) {
            this.q = -1;
            if(this.r == 0 || this.r > this.k - var2) {
               this.r = this.k - var2;
            }
         } else {
            this.q = 0;
         }

         if(this.o < var3) {
            this.t = 1;
            if(this.r == 0 || this.r > var3 - this.o) {
               this.r = var3 - this.o;
            }
         } else if(this.o > var3) {
            this.t = -1;
            if(this.r == 0 || this.r > this.o - var3) {
               this.r = this.o - var3;
            }
         } else {
            this.t = 0;
         }

         return false;
      }
   }

   final synchronized void a(boolean var1) {
      this.u = var1;
   }

   final int a() {
      int var1 = this.l * 3 >> 6;
      var1 = (var1 ^ var1 >> 31) + (var1 >>> 31);
      if(this.p == 0) {
         var1 -= var1 * this.m / (((hg)super.h).i.length << 8);
      } else if(this.p >= 0) {
         var1 -= var1 * this.w / ((hg)super.h).i.length;
      }

      return var1 > 255?255:var1;
   }

   final gb b() {
      return null;
   }

   private static final int b(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, pg var13, int var14, int var15) {
      var13.l -= var13.s * var5;
      if(var14 == 0 || (var10 = var5 + (var12 + 256 - var4 + var14) / var14) > var11) {
         var10 = var11;
      }

      var5 <<= 1;

      int var10001;
      for(var10 <<= 1; var5 < var10; var4 += var14) {
         var1 = var4 >> 8;
         byte var16 = var2[var1 - 1];
         var0 = (var16 << 8) + (var2[var1] - var16) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var6 += var8;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
         var7 += var9;
      }

      if(var14 == 0 || (var10 = (var5 >> 1) + (var12 - var4 + var14) / var14) > var11) {
         var10 = var11;
      }

      var10 <<= 1;

      for(var1 = var15; var5 < var10; var4 += var14) {
         var0 = (var1 << 8) + (var2[var4 >> 8] - var1) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var6 += var8;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
         var7 += var9;
      }

      var5 >>= 1;
      var13.l += var13.s * var5;
      var13.k = var6;
      var13.o = var7;
      var13.m = var4;
      return var5;
   }

   private static final int b(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, pg var10, int var11, int var12) {
      if(var11 == 0 || (var7 = var5 + (var9 + 256 - var4 + var11) / var11) > var8) {
         var7 = var8;
      }

      int var10001;
      while(var5 < var7) {
         var1 = var4 >> 8;
         byte var13 = var2[var1 - 1];
         var10001 = var5++;
         var3[var10001] += ((var13 << 8) + (var2[var1] - var13) * (var4 & 255)) * var6 >> 6;
         var4 += var11;
      }

      if(var11 == 0 || (var7 = var5 + (var9 - var4 + var11) / var11) > var8) {
         var7 = var8;
      }

      var0 = var12;

      for(var1 = var11; var5 < var7; var4 += var1) {
         var10001 = var5++;
         var3[var10001] += ((var0 << 8) + (var2[var4 >> 8] - var0) * (var4 & 255)) * var6 >> 6;
      }

      var10.m = var4;
      return var5;
   }

   final synchronized void d(int var1, int var2) {
      this.b(var1, var2, this.f());
   }

   private static final int b(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, pg var8) {
      var2 >>= 8;
      var7 >>= 8;
      var4 <<= 2;
      if((var5 = var3 + var7 - var2) > var6) {
         var5 = var6;
      }

      int var10001;
      for(var5 -= 3; var3 < var5; var1[var10001] += var0[var2++] * var4) {
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
         var10001 = var3++;
         var1[var10001] += var0[var2++] * var4;
         var10001 = var3++;
      }

      for(var5 += 3; var3 < var5; var1[var10001] += var0[var2++] * var4) {
         var10001 = var3++;
      }

      var8.m = var2 << 8;
      return var3;
   }

   final synchronized void b(int var1, int var2, int var3) {
      if(var1 == 0) {
         this.a(var2, var3);
      } else {
         int var4 = e(var2, var3);
         int var5 = c(var2, var3);
         if(this.k == var4 && this.o == var5) {
            this.r = 0;
         } else {
            int var6 = var2 - this.l;
            if(this.l - var2 > var6) {
               var6 = this.l - var2;
            }

            if(var4 - this.k > var6) {
               var6 = var4 - this.k;
            }

            if(this.k - var4 > var6) {
               var6 = this.k - var4;
            }

            if(var5 - this.o > var6) {
               var6 = var5 - this.o;
            }

            if(this.o - var5 > var6) {
               var6 = this.o - var5;
            }

            if(var1 > var6) {
               var1 = var6;
            }

            this.r = var1;
            this.j = var2;
            this.v = var3;
            this.s = (var2 - this.l) / var1;
            this.q = (var4 - this.k) / var1;
            this.t = (var5 - this.o) / var1;
         }
      }
   }

   final synchronized void f(int var1) {
      this.p = var1;
   }

   private static final int e(int var0, int var1) {
      return var1 < 0?var0:(int)((double)var0 * Math.sqrt((double)(16384 - var1) * 1.220703125E-4D) + 0.5D);
   }

   private static final int a(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, pg var12) {
      var3 >>= 8;
      var11 >>= 8;
      var5 <<= 2;
      var6 <<= 2;
      var7 <<= 2;
      var8 <<= 2;
      if((var9 = var4 + var11 - var3) > var10) {
         var9 = var10;
      }

      var12.l += var12.s * (var9 - var4);
      var4 <<= 1;
      var9 <<= 1;

      int var10001;
      byte var13;
      for(var9 -= 6; var4 < var9; var6 += var8) {
         var13 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
         var6 += var8;
         var13 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
         var6 += var8;
         var13 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
         var6 += var8;
         var13 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
      }

      for(var9 += 6; var4 < var9; var6 += var8) {
         var13 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
      }

      var12.k = var5 >> 2;
      var12.o = var6 >> 2;
      var12.m = var3 << 8;
      return var4 >> 1;
   }

   final synchronized void g(int var1) {
      if(var1 == 0) {
         this.e(0);
         this.b(true);
      } else if(this.k == 0 && this.o == 0) {
         this.r = 0;
         this.j = 0;
         this.l = 0;
         this.b(true);
      } else {
         int var2 = -this.l;
         if(this.l > var2) {
            var2 = this.l;
         }

         if(-this.k > var2) {
            var2 = -this.k;
         }

         if(this.k > var2) {
            var2 = this.k;
         }

         if(-this.o > var2) {
            var2 = -this.o;
         }

         if(this.o > var2) {
            var2 = this.o;
         }

         if(var1 > var2) {
            var1 = var2;
         }

         this.r = var1;
         this.j = Integer.MIN_VALUE;
         this.s = -this.l / var1;
         this.q = -this.k / var1;
         this.t = -this.o / var1;
      }
   }

   private static final int c(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, pg var11, int var12, int var13) {
      if(var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 257) / var12) > var9) {
         var8 = var9;
      }

      var5 <<= 1;

      int var10001;
      byte var14;
      for(var8 <<= 1; var5 < var8; var4 += var12) {
         var1 = var4 >> 8;
         var14 = var2[var1];
         var0 = (var14 << 8) + (var2[var1 + 1] - var14) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
      }

      if(var12 == 0 || (var8 = (var5 >> 1) + (var10 - var4 + var12 - 1) / var12) > var9) {
         var8 = var9;
      }

      var8 <<= 1;

      for(var1 = var13; var5 < var8; var4 += var12) {
         var14 = var2[var4 >> 8];
         var0 = (var14 << 8) + (var1 - var14) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
      }

      var11.m = var4;
      return var5 >> 1;
   }

   final synchronized void a(int[] var1, int var2, int var3) {
      if(this.j == 0 && this.r == 0) {
         this.a(var3);
      } else {
         hg var4 = (hg)super.h;
         int var5 = this.w << 8;
         int var6 = this.x << 8;
         int var7 = var4.i.length << 8;
         int var8 = var6 - var5;
         if(var8 <= 0) {
            this.p = 0;
         }

         int var9 = var2;
         var3 += var2;
         if(this.m < 0) {
            if(this.n <= 0) {
               this.e();
               this.b(true);
               return;
            }

            this.m = 0;
         }

         if(this.m >= var7) {
            if(this.n >= 0) {
               this.e();
               this.b(true);
               return;
            }

            this.m = var7 - 1;
         }

         if(this.p < 0) {
            if(this.u) {
               if(this.n < 0) {
                  var9 = this.a(var1, var2, var5, var3, var4.i[this.w]);
                  if(this.m >= var5) {
                     return;
                  }

                  this.m = var5 + var5 - 1 - this.m;
                  this.n = -this.n;
               }

               while(true) {
                  var9 = this.b(var1, var9, var6, var3, var4.i[this.x - 1]);
                  if(this.m < var6) {
                     return;
                  }

                  this.m = var6 + var6 - 1 - this.m;
                  this.n = -this.n;
                  var9 = this.a(var1, var9, var5, var3, var4.i[this.w]);
                  if(this.m >= var5) {
                     return;
                  }

                  this.m = var5 + var5 - 1 - this.m;
                  this.n = -this.n;
               }
            } else if(this.n < 0) {
               while(true) {
                  var9 = this.a(var1, var9, var5, var3, var4.i[this.x - 1]);
                  if(this.m >= var5) {
                     return;
                  }

                  this.m = var6 - 1 - (var6 - 1 - this.m) % var8;
               }
            } else {
               while(true) {
                  var9 = this.b(var1, var9, var6, var3, var4.i[this.w]);
                  if(this.m < var6) {
                     return;
                  }

                  this.m = var5 + (this.m - var5) % var8;
               }
            }
         } else {
            if(this.p > 0) {
               if(this.u) {
                  label133: {
                     if(this.n < 0) {
                        var9 = this.a(var1, var2, var5, var3, var4.i[this.w]);
                        if(this.m >= var5) {
                           return;
                        }

                        this.m = var5 + var5 - 1 - this.m;
                        this.n = -this.n;
                        if(--this.p == 0) {
                           break label133;
                        }
                     }

                     do {
                        var9 = this.b(var1, var9, var6, var3, var4.i[this.x - 1]);
                        if(this.m < var6) {
                           return;
                        }

                        this.m = var6 + var6 - 1 - this.m;
                        this.n = -this.n;
                        if(--this.p == 0) {
                           break;
                        }

                        var9 = this.a(var1, var9, var5, var3, var4.i[this.w]);
                        if(this.m >= var5) {
                           return;
                        }

                        this.m = var5 + var5 - 1 - this.m;
                        this.n = -this.n;
                     } while(--this.p != 0);
                  }
               } else {
                  int var10;
                  if(this.n < 0) {
                     while(true) {
                        var9 = this.a(var1, var9, var5, var3, var4.i[this.x - 1]);
                        if(this.m >= var5) {
                           return;
                        }

                        var10 = (var6 - 1 - this.m) / var8;
                        if(var10 >= this.p) {
                           this.m += var8 * this.p;
                           this.p = 0;
                           break;
                        }

                        this.m += var8 * var10;
                        this.p -= var10;
                     }
                  } else {
                     while(true) {
                        var9 = this.b(var1, var9, var6, var3, var4.i[this.w]);
                        if(this.m < var6) {
                           return;
                        }

                        var10 = (this.m - var5) / var8;
                        if(var10 >= this.p) {
                           this.m -= var8 * this.p;
                           this.p = 0;
                           break;
                        }

                        this.m -= var8 * var10;
                        this.p -= var10;
                     }
                  }
               }
            }

            if(this.n < 0) {
               this.a(var1, var9, 0, var3, 0);
               if(this.m < 0) {
                  this.m = -1;
                  this.e();
                  this.b(true);
                  return;
               }
            } else {
               this.b(var1, var9, var7, var3, 0);
               if(this.m >= var7) {
                  this.m = var7;
                  this.e();
                  this.b(true);
               }
            }

         }
      }
   }

   private static final int b(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, pg var10) {
      var3 >>= 8;
      var9 >>= 8;
      var5 <<= 2;
      var6 <<= 2;
      if((var7 = var4 + var9 - var3) > var8) {
         var7 = var8;
      }

      var4 <<= 1;
      var7 <<= 1;

      byte var11;
      int var10001;
      for(var7 -= 6; var4 < var7; var2[var10001] += var11 * var6) {
         var11 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
         var2[var10001] += var11 * var6;
         var11 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
         var2[var10001] += var11 * var6;
         var11 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
         var2[var10001] += var11 * var6;
         var11 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
      }

      for(var7 += 6; var4 < var7; var2[var10001] += var11 * var6) {
         var11 = var1[var3++];
         var10001 = var4++;
         var2[var10001] += var11 * var5;
         var10001 = var4++;
      }

      var10.m = var3 << 8;
      return var4 >> 1;
   }

   final synchronized void h(int var1) {
      if(this.n < 0) {
         this.n = -var1;
      } else {
         this.n = var1;
      }
   }

   final gb c() {
      return null;
   }

   private static final int d(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, pg var11, int var12, int var13) {
      if(var12 == 0 || (var8 = var5 + (var10 + 256 - var4 + var12) / var12) > var9) {
         var8 = var9;
      }

      var5 <<= 1;

      int var10001;
      for(var8 <<= 1; var5 < var8; var4 += var12) {
         var1 = var4 >> 8;
         byte var14 = var2[var1 - 1];
         var0 = (var14 << 8) + (var2[var1] - var14) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
      }

      if(var12 == 0 || (var8 = (var5 >> 1) + (var10 - var4 + var12) / var12) > var9) {
         var8 = var9;
      }

      var8 <<= 1;

      for(var1 = var13; var5 < var8; var4 += var12) {
         var0 = (var1 << 8) + (var2[var4 >> 8] - var1) * (var4 & 255);
         var10001 = var5++;
         var3[var10001] += var0 * var6 >> 6;
         var10001 = var5++;
         var3[var10001] += var0 * var7 >> 6;
      }

      var11.m = var4;
      return var5 >> 1;
   }

   private static final int b(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, pg var12) {
      var3 >>= 8;
      var11 >>= 8;
      var5 <<= 2;
      var6 <<= 2;
      var7 <<= 2;
      var8 <<= 2;
      if((var9 = var4 + var3 - (var11 - 1)) > var10) {
         var9 = var10;
      }

      var12.l += var12.s * (var9 - var4);
      var4 <<= 1;
      var9 <<= 1;

      int var10001;
      byte var13;
      for(var9 -= 6; var4 < var9; var6 += var8) {
         var13 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
         var6 += var8;
         var13 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
         var6 += var8;
         var13 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
         var6 += var8;
         var13 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
      }

      for(var9 += 6; var4 < var9; var6 += var8) {
         var13 = var1[var3--];
         var10001 = var4++;
         var2[var10001] += var13 * var5;
         var5 += var7;
         var10001 = var4++;
         var2[var10001] += var13 * var6;
      }

      var12.k = var5 >> 2;
      var12.o = var6 >> 2;
      var12.m = var3 << 8;
      return var4 >> 1;
   }

   private pg(hg var1, int var2, int var3) {
      super.h = var1;
      this.w = var1.k;
      this.x = var1.j;
      this.u = var1.g;
      this.n = var2;
      this.j = var3;
      this.v = 8192;
      this.m = 0;
      this.g();
   }

   private pg(hg var1, int var2, int var3, int var4) {
      super.h = var1;
      this.w = var1.k;
      this.x = var1.j;
      this.u = var1.g;
      this.n = var2;
      this.j = var3;
      this.v = var4;
      this.m = 0;
      this.g();
   }
}
