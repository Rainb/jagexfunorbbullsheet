import java.applet.Applet;
import java.net.URL;

final class en extends ai {

   static String i;
   static String j = "This option is restricted. Your rating is currently <%0>.<br>Can you achieve the qualifying rating of <%1>?";
   static int k;
   private String l;
   static volatile boolean m = true;


   static final boolean a(byte var0) {
      try {
         if(~FunRB.D == 0) {
            if(!qi.a(127, 1)) {
               return false;
            }

            FunRB.D = ta.o.getInt(false);
            ta.o.offset = 0;
         }

         if(var0 <= 114) {
            i = null;
         }

         if(FunRB.D == -2) {
            if(!qi.a(74, 2)) {
               return false;
            }

            FunRB.D = ta.o.j(1808469224);
            ta.o.offset = 0;
         }

         return qi.a(113, FunRB.D);
      } catch (RuntimeException var2) {
         throw pl.a(var2, "en.A(" + var0 + ')');
      }
   }

   final ol a(boolean var1) {
      try {
         return var1?null:em.e;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "en.B(" + var1 + ')');
      }
   }

   static final String a(boolean var0, int var1, int var2, int var3) {
      try {
         if(var1 <= 0) {
            throw new IllegalArgumentException("Can only get ordinal for positive numbers");
         } else if(~var3 == -1) {
            int var4 = var1 % 100;
            if(~var4 <= -12 && var4 <= 13) {
               return var1 + "th";
            } else {
               int var5 = var1 % 10;
               return ~var5 == -2?var1 + "st":(var5 == 2?var1 + "nd":(~var5 == -4?var1 + "rd":var1 + "th"));
            }
         } else if(~var3 == -2) {
            return var1 + ".";
         } else if(~var3 == -3) {
            return ~var1 == -2?(var2 != 2?var1 + "er":var1 + "ère"):var1 + "ème";
         } else {
            if(!var0) {
               i = null;
            }

            if(var3 == 3) {
               return ~var2 == -3?var1 + "ª":var1 + "º";
            } else if(~var3 == -5) {
               return var1 + "e";
            } else {
               throw new IllegalArgumentException("Unsupported language " + var3);
            }
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "en.J(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ')');
      }
   }

   final void a(byte var1, ByteBuffer var2) {
      try {
         var2.putJString(-1192815152, this.l);
         if(var1 != -3) {
            this.a((byte)53, (ByteBuffer)null);
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "en.C(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   static final void a(int var0, int var1, boolean var2) {
      try {
         if(var0 == 3) {
            if(tg.w != null) {
               int var3 = tg.w.c(1, var2);
               if(var3 != -2) {
                  if(~var3 != 0) {
                     boolean var4 = tg.w.c((byte)105);
                     tl.a(var4, var3, tg.w.m(26341), tg.w.Sb, var1, true);
                  }

                  tg.w = null;
                  ie.b(-99);
                  return;
               }
            }

         }
      } catch (RuntimeException var5) {
         throw pl.a(var5, "en.L(" + var0 + ',' + var1 + ',' + var2 + ')');
      }
   }

   static final void a(oj var0, byte var1) {
      try {
         if(var0 != null) {
            if(var1 == -96) {
               kn.c = var0;
               he.d.qb.b(false);
               he.d.a(14939, (ph)kn.c);
               wi.g = true;
            }
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "en.I(" + (var0 != null?"{...}":"null") + ',' + var1 + ')');
      }
   }

   public static void a(int var0) {
      try {
         int var1 = -74 / ((-51 - var0) / 51);
         j = null;
         i = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "en.M(" + var0 + ')');
      }
   }

   en(String var1) {
      try {
         this.l = var1;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "en.<init>(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   static final void a(byte var0, Applet var1) {
      try {
         try {
            if(var0 == 36) {
               URL var2 = new URL(var1.getCodeBase(), "quit.ws");
               var1.getAppletContext().showDocument(wg.a(var2, var1, (byte)-123), "_top");
            }
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "en.K(" + var0 + ',' + (var1 != null?"{...}":"null") + ')');
      }
   }

}
