import java.awt.*;
import java.awt.image.*;
import java.util.Hashtable;

public final class FunSomeImage extends jl {

	private Component component;


	final void init(Component component, int width, int height, byte var4) {
		this.width = width;
		pixels = new int[width * height - -1];
		this.height = height;
		DataBufferInt dataBufferInt = new DataBufferInt(pixels, pixels.length);
		DirectColorModel colorModel = new DirectColorModel(32, 16711680, '\uff00', 255);
		WritableRaster raster = Raster.createWritableRaster(colorModel.createCompatibleSampleModel(this.width, this.height), dataBufferInt, null);
		image = new BufferedImage(colorModel, raster, false, new Hashtable());
		this.component = component;
		a(true);
		if (var4 < 42) {
			this.component = null;
		}
	}

	final void draw(int x, Graphics graphics, int var3, int y) {
		graphics.drawImage(image, x, y, component);
		if (var3 != -16711898) {
			component = null;
		}
	}
}
