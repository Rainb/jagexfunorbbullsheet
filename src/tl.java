import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

final class tl implements Runnable {

	private int a;
	private Socket b;
	private OutputStream c;
	private int d;
	private om e;
	private ObjectStore f;
	private byte[] g;
	static pf h = new pf();
	private boolean i;
	private int j;
	private InputStream k;
	private boolean l;
	static boolean m = false;
	static int n;
	static int[] o;
	static volatile int p = 0;
	static String q = "Waiting for <%0> to start the game...";
	static ph r;
	static FunImage[] s;


	public final void run() {
		int var5 = client.L;

		try {
			try {
				while (true) {
					int var1;
					int var2;
					if (d == a) {
						if (l) {
							break;
						}

						try {
							wait();
						} catch (InterruptedException ignored) {
						}
					}
					label143:
					{
						if (a <= d) {
							var1 = d + -a;
							if (var5 == 0) {
								break label143;
							}
						}

						var1 = j + -a;
					}
					var2 = a;

					if (~var1 < -1) {
						try {
							c.write(g, var2, var1);
						} catch (IOException var15) {
							i = true;
						}

						a = (var1 + a) % j;

						try {
							if (d == a) {
								c.flush();
							}
						} catch (IOException var14) {
							i = true;
						}
					}
				}

				try {
					if (k != null) {
						k.close();
					}

					if (c != null) {
						c.close();
					}

					if (b != null) {
						b.close();
					}
				} catch (IOException ignored) {
				}

				g = null;
			} catch (Exception var18) {
				dj.a(null, 0, var18);
			}
		} catch (RuntimeException var19) {
			throw pl.a(var19, "tl.run(" + ')');
		}
	}

	final void a(int var1, int var2, byte[] var3, int var4) throws IOException {
		try {
			if (var4 == 30186) {
				if (!l) {
					while (var1 > 0) {
						int var5 = k.read(var3, var2, var1);
						if (var5 <= 0) {
							throw new EOFException();
						}

						var2 += var5;
						var1 -= var5;
					}

				}
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "tl.J(" + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ')');
		}
	}

	protected final void finalize() {
		try {
			a(-13);
		} catch (RuntimeException var2) {
			throw pl.a(var2, "tl.finalize(" + ')');
		}
	}

	tl(Socket var1, om var2) throws IOException {
		this(var1, var2, 5000);
	}

	static void a(boolean var0, int var1, String var2, long var3, int var5, boolean var6) {
		try {
			bf.f.a(var5, var6);
			++bf.f.offset;
			int offset = bf.f.offset;
			bf.f.putLong(var3, -268435456);
			bf.f.putJString(-1192815152, var2);
			bf.f.a(var1, 126);
			bf.f.a(!var0 ? 0 : 1, -84);
			bf.f.d(-offset + bf.f.offset, (byte) 73);
		} catch (RuntimeException var8) {
			throw pl.a(var8, "tl.I(" + var0 + ',' + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ',' + var5 + ',' + var6 + ')');
		}
	}

	static void a(bi var0, byte var1) {
		int var4 = client.L;

		try {
			if (var0.a != null) {
				int var2;
				if (var0.k != 0 || var0.j != 0) {
					var2 = 0;
					if (var4 != 0 || var2 < FunIOException.f) {
						do {
							bi var3 = af.j[var2];
							if (var3.r == 2 && ~var3.k == ~var0.k && ~var3.j == ~var0.j) {
								return;
							}

							++var2;
						} while (var2 < FunIOException.f);
					}
				}

				if (var0.m != null) {
					ln.g = var0.c;
					FunImageProducer.m = var0.m;
					mk.Ib = var0.g;
					FunJ.n = var0.r;
				}
				kc.a(var0, (byte) -125);
			}
		} catch (RuntimeException var5) {
			throw pl.a(var5, "tl.D(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ')');
		}
	}

	static void setAppletSession(String session, int var1) {
		try {
			if (var1 != -12294) {
				r = null;
			}

			hh.appletSession = session;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "tl.C(" + (session != null ? "{...}" : "null") + ',' + var1 + ')');
		}
	}

	public static void a(byte var0) {
		try {
			r = null;
			s = null;
			o = null;
			if (var0 != -88) {
				o = null;
			}

			h = null;
			q = null;
		} catch (RuntimeException var2) {
			throw pl.a(var2, "tl.F(" + var0 + ')');
		}
	}

	final void a(int var1) {
		int var3 = client.L;

		try {
			if (!l) {
				l = true;
				notifyAll();

				if (f != null) {
					if (var3 != 0) {
						mc.a(1L, 95);
					}

					while (f.f == 0) {
						mc.a(1L, 95);
					}

					if (~f.f == -2) {
						try {
							((Thread) f.object).join();
						} catch (InterruptedException ignored) {
						}
					}
				}

				f = null;
			}
		} catch (RuntimeException var9) {
			throw pl.a(var9, "tl.A(" + var1 + ')');
		}
	}

	final void b(int var1) throws IOException {
		try {
			if (!l) {
				if (i) {
					i = false;
					throw new IOException();
				} else if (var1 != 0) {
					b = null;
				}
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "tl.E(" + var1 + ')');
		}
	}

	final void a(int var1, byte var2, int var3, byte[] var4) throws IOException {
		int var8 = client.L;

		try {
			if (!l) {
				if (i) {
					i = false;
					throw new IOException();
				} else {
					if (g == null) {
						g = new byte[j];
					}

					if (var2 < 13) {
						s = null;
					}
					int var7 = 0;
					if (var8 == 0 && var7 >= var1) {
						if (f == null) {
							f = e.a(3, false, this);
						}

						notifyAll();
					} else {
						while (true) {
							g[d] = var4[var3 + var7];
							d = (d + 1) % j;
							if ((-100 + (a - -j)) % j == d) {
								throw new IOException();
							}

							++var7;
							if (var7 >= var1) {
								if (f == null) {
									f = e.a(3, false, this);
								}

								notifyAll();
								break;
							}
						}
					}

				}
			}
		} catch (RuntimeException var12) {
			throw pl.a(var12, "tl.H(" + var1 + ',' + var2 + ',' + var3 + ',' + (var4 != null ? "{...}" : "null") + ')');
		}
	}

	final int read(byte var1) throws IOException {
		try {
			if (l) {
				return 0;
			} else {
				return k.read();
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "tl.B(" + var1 + ')');
		}
	}

	final int available(int var1) throws IOException {
		try {
			if (l) {
				return 0;
			} else {
				if (var1 != 0) {
					o = null;
				}

				return k.available();
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "tl.G(" + var1 + ')');
		}
	}

	private tl(Socket var1, om var2, int var3) throws IOException {
		d = 0;
		a = 0;
		i = false;
		l = false;

		try {
			e = var2;
			b = var1;
			b.setSoTimeout(30000);
			b.setTcpNoDelay(true);
			k = b.getInputStream();
			c = b.getOutputStream();
			j = var3;
		} catch (RuntimeException var5) {
			throw pl.a(var5, "tl.<init>(" + (var1 != null ? "{...}" : "null") + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
		}
	}

}
