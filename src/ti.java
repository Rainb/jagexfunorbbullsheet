import com.ms.awt.WComponentPeer;
import com.ms.com.IUnknown;
import com.ms.directX.DDSurfaceDesc;
import com.ms.directX.DirectDraw;
import com.ms.directX.IEnumModesCallback;
import com.ms.win32.User32;
import java.awt.Frame;

final class ti implements IEnumModesCallback {

   private DirectDraw directDraw;
   private static int b;
   private static int[] c;


   final void a(boolean var1, Frame var2) {
	   if(var1) {
		   a(-13, -57, 119, true, null, -82);
	   }
	   directDraw.restoreDisplayMode();
	   directDraw.setCooperativeLevel(var2, 8);
   }

   final int[] a(byte var1) {
	   directDraw.enumDisplayModes(0, null, null, this);
	   c = new int[b];
	   b = 0;
	   directDraw.enumDisplayModes(0, null, null, this);
	   int[] var2 = c;
	   c = null;
	   if(var1 < 45) {
		  return null;
	   } else {
		  b = 0;
		  return var2;
	   }
   }

   public ti() {
	   directDraw = new DirectDraw();
	   directDraw.initialize(null);
   }

   final void a(int var1, int var2, int var3, boolean var4, Frame frame, int var6) {
	   frame.setVisible(true);
	   WComponentPeer peer = (WComponentPeer)frame.getPeer();
	   int var8 = peer.getHwnd();
	   User32.SetWindowLong(var8, -16, Integer.MIN_VALUE);
	   User32.SetWindowLong(var8, -20, 8);
	   directDraw.setCooperativeLevel(frame, 17);
	   directDraw.setDisplayMode(var2, var6, var1, var3, 0);
	   frame.setBounds(0, 0, var2, var6);
	   frame.toFront();
	   frame.requestFocus();
	   if(var4) {
		   callbackEnumModes(null, null);
	   }
   }

   public final void callbackEnumModes(DDSurfaceDesc var1, IUnknown var2) {
	   if(c != null) {
		  c[b++] = var1.width;
		  c[b++] = var1.height;
		  c[b++] = var1.rgbBitCount;
		  c[b++] = var1.refreshRate;
	   } else {
		  b += 4;
	   }
   }
}
