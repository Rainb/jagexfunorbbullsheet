import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

final class fl {

   static boolean a = false;
   static int[] b = new int[8];
   static tl c;
   static String d = "Unfortunately your configuration doesn\'t support fullscreen mode.";
   static String e = "Average rating";
   static byte[][] f = new byte[50][];
   static String g = "You cannot join this game - it is in progress";


   static final void a(ByteBuffer var0, int var1, boolean var2, int var3, int var4) {
      try {
         if(var2) {
            d = null;
         }

         var0.a((int)12, (int)-17);
         var0.b((byte)-70, (int)17);
         var0.b((byte)-114, var4);
         var0.b((byte)-36, var3);
         var0.a(var1, (int)-68);
      } catch (RuntimeException var6) {
         throw pl.a(var6, "fl.G(" + (var0 != null?"{...}":"null") + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   static final void a(ByteBuffer var0, int var1, qk var2, boolean var3) {
      int var9 = client.L;

      try {
         if(var3) {
            var2.Ob = var0.getInt(false);
         }

         var2.uc = var0.getInt(false);
         var2.vc = var0.getInt(false);
         int var4 = var0.getInt(false);
         var2.Nb = (128 & var4) != 0;
         var2.oc = (var4 & 32) != 0;
         var2.Kb = (var4 & 8) != 0;
         if(var1 < 70) {
            b = null;
         }

         boolean var6;
         label83: {
            var2.dc = (var4 & 16) == 0?1:2;
            var2.Qb = var2.Nb && ~var2.uc < ~var2.Ob;
            var2.Wb = (64 & var4) != 0;
            boolean var5 = (var4 & 4) != 0;
            var6 = ~(var4 & 2) != -1;
            var0.a(var2.jc, var2.jc.length, 0, -104);
            var2.kc = var0.j(1808469224);
            var2.Xb = FunAB.a((byte) -7) - (long)var0.getInt(255);
            if(!var5) {
               var2.gc = -1;
               if(var9 == 0) {
                  break label83;
               }
            }

            var2.gc = var0.getInt(255);
         }

         var2.ic = var0.e(16711680);
         int var7 = var0.offset;
         var2.rc = var0.getString((byte) -80);
         if(!var6) {
            var2.nc = null;
         } else {
            var0.offset = var7;
            var2.nc = new String[var2.Ob];
            int var8 = 0;
            if(var9 != 0) {
               var2.nc[var8] = var0.getString((byte) -80);
               ++var8;
            }

            while(~var2.Ob < ~var8) {
               var2.nc[var8] = var0.getString((byte) -80);
               ++var8;
            }

         }
      } catch (RuntimeException var10) {
         throw pl.a(var10, "fl.D(" + (var0 != null?"{...}":"null") + ',' + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

   public static void a(int var0) {
      try {
         if(var0 == 26921) {
            e = null;
            c = null;
            d = null;
            g = null;
            b = null;
            f = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "fl.H(" + var0 + ')');
      }
   }

   static final int b(int var0) {
      try {
         if(var0 != 48) {
            a(126, (byte)-7, (byte[])null, 109);
         }

         return pn.g;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "fl.F(" + var0 + ')');
      }
   }

   static final void a(byte var0) {
      try {
         if(var0 >= -33) {
            a((byte)-108);
         }

         ue.c((byte)63);
      } catch (RuntimeException var2) {
         throw pl.a(var2, "fl.B(" + var0 + ')');
      }
   }

   static final boolean b(byte var0) {
      try {
         if(var0 != 78) {
            a = true;
         }

         return ~ci.a <= -11 && ah.hb >= 13;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "fl.E(" + var0 + ')');
      }
   }

   static final String a(Throwable var0, int var1) throws IOException {
      int var13 = client.L;

      try {
         String var2;
         label45: {
            if(var0 instanceof gn) {
               gn var3 = (gn)var0;
               var0 = var3.d;
               var2 = var3.b + " | ";
               if(var13 == 0) {
                  break label45;
               }
            }

            var2 = "";
         }

         StringWriter var15 = new StringWriter();
         PrintWriter var4 = new PrintWriter(var15);
         var0.printStackTrace(var4);
         if(var1 > -73) {
            f = null;
         }

         var4.close();
         String var5 = var15.toString();
         BufferedReader var6 = new BufferedReader(new StringReader(var5));
         String var7 = var6.readLine();

         do {
            String var8 = var6.readLine();
            if(var8 == null) {
               break;
            }

            int var9;
            int var10;
            String var11;
            label36: {
               var9 = var8.indexOf(40);
               var10 = var8.indexOf(41, 1 + var9);
               if(~var9 != 0) {
                  var11 = var8.substring(0, var9);
                  if(var13 == 0) {
                     break label36;
                  }
               }

               var11 = var8;
            }

            var11 = var11.trim();
            var11 = var11.substring(1 + var11.lastIndexOf(32));
            var11 = var11.substring(var11.lastIndexOf(9) + 1);
            var2 = var2 + var11;
            if(~var9 != 0 && ~var10 != 0) {
               int var12 = var8.indexOf(".java:", var9);
               if(~var12 <= -1) {
                  var2 = var2 + var8.substring(5 + var12, var10);
               }
            }

            var2 = var2 + ' ';
         } while(var13 == 0);

         var2 = var2 + "| " + var7;
         return var2;
      } catch (RuntimeException var14) {
         throw var14;
      }
   }

   static final byte[] a(int var0, byte var1, byte[] var2, int var3) {
      int var8 = client.L;

      try {
         byte[] var4;
         label35: {
            if(var0 > 0) {
               var4 = new byte[var3];
               int var5 = 0;
               if(var8 != 0 || var5 < var3) {
                  do {
                     var4[var5] = var2[var0 + var5];
                     ++var5;
                  } while(var5 < var3);
               }

               if(var8 == 0) {
                  break label35;
               }
            }

            var4 = var2;
         }

         rk var10 = new rk();
         var10.a(false);
         int var6 = -47 / ((-24 - var1) / 46);
         var10.a(-19153, var4, (long)(var3 * 8));
         byte[] var7 = new byte[64];
         var10.a(0, 37, var7);
         return var7;
      } catch (RuntimeException var9) {
         throw pl.a(var9, "fl.A(" + var0 + ',' + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

}
