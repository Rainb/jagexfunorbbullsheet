final class kg extends gi {

	private int[] h;
	private int[] i;
	private int[] j;
	private int k = 0;
	private int l = 64;
	private int m;
	private int n;
	private int o = -1;
	private int[] p;
	private int q;
	private dk r;
	private int[] s;
	private int t = -1;
	private int[] u;
	private int[] v;
	private boolean[] w;
	private int x;
	private int[] y;
	private int z;
	private int[] A;
	private int[] B;
	private int[] C;
	private int[] D;
	private int[] E;
	private int[] F;
	private int[] G;
	private int[] H;
	private int[] I;
	private int[] J;
	private int[] L;
	private int[] M;
	private static int[] N = new int[8000];
	private int[] O;
	private int P;
	private int[] Q;
	private int[] R;
	private static int[] S = new int[68];
	private int T = 0;
	private int U = 256;
	private int[] V;
	private dk W = null;
	private int[] X;
	private boolean Y = true;
	private int Z = 0;
	private int[] ab;
	private int[] bb;
	private int[] cb;
	private int[] db;
	private int[] eb;
	private pg[] fb;
	private int gb = 0;
	private int[] hb;
	private int ib;
	private int jb = 0;
	private int[] kb;
	private int lb = 0;
	private int mb = -1;


	final int a(gd var1) {
		boolean var2 = d();
		if (var2 && !Y) {
			return -1;
		} else {
			int var3 = qc.f;
			int var4 = (var3 << 9) + (var3 << 7);
			return var4 / (n * U);
		}
	}

	final synchronized void a(int var1) {
		U = var1;
	}

	final synchronized void a(dk var1, int var2, int var3, int var4) {
		T = var4;
		if (var1 != null) {
			W = var1;
		}

		mb = var2;
		o = var3;
	}

	private void a(int var1, int var2, int var3, int var4, int var5, int var6) {
		d(var1);
		if (var4 > N[7999] && var4 < N[0]) {
			if (var5 < 0) {
				var5 = 0;
			} else if (var5 > 64) {
				var5 = 64;
			}

			if (var6 < 0) {
				var6 = 0;
			} else if (var6 > 255) {
				var6 = 255;
			}

			pg var7 = pg.a(r.c[var2], var4 * 256 / qc.f, var3 != 0 ? 0 : var5 * P * l >> 12, var6);
			var7.b(r.f[var2], r.b[var2]);
			var7.f(r.m[var2] != 0 ? -1 : 0);
			var7.a(r.m[var2] == 2);
			if (var3 != 0) {
				var7.d(var3);
				var7.a(qc.f >> 7, var5 * P * l >> 12, var6);
			}

			fb[var1] = var7;
			if (super.g != null) {
				super.g.b(var7);

			}
		}
	}

	private void d(int var1) {
		if (fb[var1] != null) {
			fb[var1].g(qc.f >> 7);
			fb[var1] = null;
		}

	}

	private void c() {
		Z = 0;
		k = 0;
		m = 0;
		x = 0;
		ib = 0;
		z = 0;
		q = r.o;
		n = r.u;
		P = 64;

		for (int var1 = 0; var1 < r.J; ++var1) {
			p[var1] = 0;
			s[var1] = 0;
			bb[var1] = 0;
			V[var1] = 0;
			y[var1] = 0;
			B[var1] = 0;
			F[var1] = 0;
			O[var1] = 0;
			A[var1] = 0;
			G[var1] = 0;
			j[var1] = 0;
			I[var1] = 0;
			i[var1] = 0;
			eb[var1] = 0;
			h[var1] = 0;
			cb[var1] = 0;
			J[var1] = -1;
			ab[var1] = 0;
			X[var1] = 0;
			R[var1] = 0;
			kb[var1] = 0;
			Q[var1] = 0;
			L[var1] = 0;
			M[var1] = 0;
			u[var1] = 0;
			D[var1] = 0;
			H[var1] = 0;
			db[var1] = 0;
			C[var1] = 128;
			hb[var1] = 0;
			v[var1] = -1;
		}

		T = 0;
		W = null;
		mb = -1;
		o = -1;
		gb = 0;
		lb = 0;
		t = -1;
		jb = 0;

		for (int var2 = 0; var2 < r.J; ++var2) {
			fb[var2] = null;
			w[var2] = false;
		}

		k = 1;
	}

	private boolean d() {
		byte var3 = 0;
		if (k != 1 && k != 2) {
			Z = 0;
			return false;
		} else {
			boolean var14 = false;
			int var15;
			int var17;
			int var16;
			if (Z == 0) {
				if (k == 1) {
					k = 2;
					ib = 0;
					x = 0;
					z = r.H[ib];
					m = 0;
				}

				if (x >= r.n[z] || jb == 1 || gb != 0 && x % gb == 0 || T > 0 && x % T == 0) {
					if (gb != 0) {
						if (lb <= ib) {
							var14 = true;
						}

						ib = lb;
					} else {
						++ib;
					}

					if (t != -1) {
						ib = t;
						t = -1;
					}

					if (ib >= r.g) {
						ib = 0;
						var14 = true;
					}

					x = 0;
					if (jb == 1) {
						lb = lb / 16 * 10 + lb % 16;
						if (lb >= r.n[z]) {
							lb = 0;
						}

						x = lb;
					}

					if (mb >= 0 && (T >= 0 || var14)) {
						var15 = mb;
						var16 = o;
						if (W != null) {
							for (var17 = 0; var17 < r.J; ++var17) {
								d(var17);
							}

							r = W;
							e();
							k = 2;
						}

						ib = var15;
						x = var16;
						var14 = false;
						T = 0;
						mb = -1;
						o = -1;
						W = null;
					}

					z = r.H[ib];
					m = 0;

					for (int var10 = 0; var10 < x; ++var10) {
						for (var15 = 0; var15 < r.J; ++var15) {
							var16 = r.s[z][m] & 255;
							++m;
							if (var16 < 128) {
								--m;
								var16 = 31;
							} else {
								var16 -= 128;
							}

							if ((var16 & 1) == 1) {
								++m;
							}

							if ((var16 & 2) == 2) {
								++m;
							}

							if ((var16 & 4) == 4) {
								++m;
							}

							if ((var16 & 8) == 8) {
								++m;
							}

							if ((var16 & 16) == 16) {
								++m;
							}
						}
					}

					t = -1;
					jb = 0;
					gb = 0;
				}

				++x;
			}

			int var18;
			for (var15 = 0; var15 < r.J; ++var15) {
				if (Z == 0) {
					var16 = m;
				} else {
					if (J[var15] < 0) {
						continue;
					}

					var16 = J[var15];
				}

				J[var15] = -1;
				var17 = var16;
				var18 = -1;
				int var19 = -1;
				int var20 = -1;
				int var21 = -1;
				int var22 = -1;
				int var23 = 0;
				int var24 = r.s[z][var16] & 255;
				++var16;
				if (var24 < 128) {
					--var16;
					var24 = 31;
				} else {
					var24 -= 128;
				}

				if ((var24 & 1) == 1) {
					var18 = r.s[z][var16] & 255;
					++var16;
				}

				if ((var24 & 2) == 2) {
					var19 = (r.s[z][var16] & 255) - 1;
					++var16;
				}

				if ((var24 & 4) == 4) {
					var20 = (r.s[z][var16] & 255) - 16;
					++var16;
				}

				if ((var24 & 8) == 8) {
					var22 = r.s[z][var16] & 255;
					++var16;
				}

				if ((var24 & 16) == 16) {
					var23 = r.s[z][var16] & 255;
					++var16;
				}

				if (Z == 0) {
					m = var16;
				}

				if (!w[var15]) {
					if (var22 == 14) {
						var22 = var22 * 16 + var23 / 16;
						var23 &= 15;
					}

					if (var22 == 236) {
						if (var23 == 0) {
							var18 = 97;
						} else if (Z == var23) {
							var18 = 97;
							var22 = -1;
							var21 = -1;
							var20 = -1;
							var19 = -1;
							var23 = 0;
						} else {
							J[var15] = var17;
						}
					}

					if (var22 == 237 && Z < var23) {
						J[var15] = var17;
					} else {
						if (var20 >= 80 && var20 < 96) {
							u[var15] = 2;
							p[var15] = 80 - var20;
							var20 = -1;
						}

						if (var20 >= 96 && var20 < 112) {
							u[var15] = 2;
							p[var15] = var20 - 96;
							var20 = -1;
						}

						if (var20 >= 112 && var20 < 128) {
							var20 = db[var15] - (var20 - 112);
							if (var20 < 0) {
								var20 = 0;
							} else if (var20 > 64) {
								var20 = 64;
							}
						}

						if (var20 >= 128 && var20 < 136) {
							var20 = db[var15] + var20 - 128;
							if (var20 < 0) {
								var20 = 0;
							} else if (var20 > 64) {
								var20 = 64;
							}
						}

						if (var20 >= 176 && var20 < 192) {
							var21 = (var20 - 176) * 17;
							var20 = -1;
						}

						if (var20 >= 192 && var20 < 208) {
							D[var15] = 2;
							s[var15] = 192 - var20;
							var20 = -1;
						}

						if (var20 >= 208 && var20 < 224) {
							D[var15] = 2;
							s[var15] = var20 - 208;
							var20 = -1;
						}

						if (var20 > 64) {
							var20 = -1;
						}

						if (var22 == 13) {
							if (gb <= 1) {
								jb = 1;
								lb = var23;
							} else {
								gb = 1;
							}
						}

						if (var22 == 15) {
							if (var23 < 32) {
								q = var23;
							} else {
								n = var23;
							}
						}

						if (var22 == 16) {
							P = var23;
						}

						if (var22 == 12) {
							var20 = var23;
						}

						if (var22 == 8) {
							var21 = var23;
						}

						if (var22 == 11 && gb <= 1) {
							gb = 1;
							lb = var23;
						}

						if (var22 == 20) {
							var18 = 97;
						}

						if (var22 == 21) {
							ab[var15] = var23;
							if (ab[var15] >= r.B[var3][r.B[var3].length - 1]) {
								ab[var15] = r.B[var3][r.B[var3].length - 1] - 1;
							}

							X[var15] = var23;
							if (X[var15] >= r.d[var3][r.d[var3].length - 1]) {
								X[var15] = r.d[var3][r.d[var3].length - 1] - 1;
							}
						}

						if (var19 >= 0 && var18 <= 96) {
							ab[var15] = 0;
							X[var15] = 0;
							kb[var15] = 1;
							Q[var15] = 0;
						}

						if (var22 == 3 && var20 < 0 && var19 != -1) {
							var20 = r.q[H[var15]];
						}

						if (var22 == 3 && var21 < 0 && var19 != -1) {
							var21 = r.h[H[var15]];
						}

						int var25;
						if (var18 >= 0 && var18 <= 96 && var22 != 3) {
							if (var19 == -1 && var20 < 0) {
								var20 = db[var15];
							}

							if (var19 == -1 && var21 < 0) {
								var21 = C[var15];
							}

							if (var19 == -1) {
								var19 = H[var15];
							} else {
								v[var15] = var19;
								if (var18 < 96) {
									var19 = r.w[var19][var18];
								} else {
									var19 = r.w[var19][95];
								}
							}

							if (var20 < 0) {
								var20 = r.q[var19];
							}

							if (var21 < 0) {
								var21 = r.h[var19];
							}

							var25 = 7680 - (var18 + r.p[var19]) * 16 * 4 - r.N[var19] / 2;
							if (var25 < 0) {
								var25 = 0;
							} else if (var25 > 7999) {
								var25 = 7999;
							}

							h[var15] = var25;
							int var26 = N[var25];
							H[var15] = var19;
							db[var15] = var20;
							C[var15] = var21;
							hb[var15] = var25;
							int var1 = 0;
							if (var22 == 9) {
								if (var23 * 256 > r.E[var19]) {
									var1 = r.E[var19];
								} else {
									var1 = var23 * 256;
								}
							}

							a(var15, var19, var1, var26, var20, var21);
							y[var15] = 0;
						} else if (var18 > 96) {
							if (R[var15] == 1) {
								kb[var15] = 0;
							} else {
								d(var15);
							}
						} else if (var20 >= 0 || var21 >= 0) {
							if (var20 >= 0) {
								db[var15] = var20;
							}

							if (var21 >= 0) {
								C[var15] = var21;
							}
						}

						if (var22 == 3) {
							L[var15] = 1;
							if (var18 >= 0 && var18 <= 96) {
								var19 = H[var15];
								var25 = 7680 - (var18 + r.p[var19]) * 16 * 4 - r.N[var19] / 2;
								if (var25 < 0) {
									var25 = 0;
								} else if (var25 > 7999) {
									var25 = 7999;
								}

								h[var15] = var25;
							}

							if (var23 != 0) {
								cb[var15] = var23;
								if (r.C == 0) {
									cb[var15] *= 2;
								}
							}
						} else if (var22 != 5) {
							L[var15] = 0;
						}

						if (var22 == 4) {
							B[var15] = 1;
							if (var23 / 16 > 0) {
								bb[var15] = var23 / 16;
							}

							if ((var23 & 15) > 0) {
								V[var15] = var23 & 15;
							}
						} else if (var22 != 6) {
							if (B[var15] != 0) {
								var25 = N[hb[var15]];
								a(var15, var25);
							}

							y[var15] = 0;
							B[var15] = 0;
						}

						if (var22 == 17) {
							M[var15] = 1;
							if (var23 != 0) {
								E[var15] = (var23 & 240) / 16 - (var23 & 15);
							}
						} else {
							M[var15] = 0;
						}

						if (var22 != 10 && var22 != 6 && var22 != 5) {
							if (u[var15] == 2) {
								u[var15] = 1;
							} else {
								u[var15] = 0;
							}
						} else {
							u[var15] = 1;
							if (var23 != 0) {
								p[var15] = (var23 & 240) / 16 - (var23 & 15);
							}
						}

						if (var22 == 25) {
							D[var15] = 1;
							if (var23 != 0) {
								s[var15] = (var23 & 240) / 16 - (var23 & 15);
							}
						} else if (D[var15] == 2) {
							D[var15] = 1;
						} else {
							D[var15] = 0;
						}

						if (var22 == 234) {
							if (var23 == 0) {
								var23 = F[var15];
							} else {
								F[var15] = var23;
							}

							db[var15] += var23;
							if (db[var15] < 0) {
								db[var15] = 0;
							} else if (db[var15] > 64) {
								db[var15] = 64;
							}
						}

						if (var22 == 235) {
							if (var23 == 0) {
								var23 = G[var15];
							} else {
								G[var15] = var23;
							}

							db[var15] -= var23;
							if (db[var15] < 0) {
								db[var15] = 0;
							} else if (db[var15] > 64) {
								db[var15] = 64;
							}
						}

						if (var22 == 1) {
							if (var23 != 0) {
								i[var15] = var23;
							}
						} else {
							i[var15] = 0;
						}

						if (var22 == 2) {
							if (var23 != 0) {
								eb[var15] = var23;
							}
						} else {
							eb[var15] = 0;
						}

						if (var22 == 225) {
							if (var23 == 0) {
								var23 = O[var15];
							} else {
								O[var15] = var23;
							}

							hb[var15] -= var23 * 4;
							if (hb[var15] < 0) {
								hb[var15] = 0;
							} else if (hb[var15] > 7999) {
								hb[var15] = 7999;
							}

							var25 = N[hb[var15]];
							a(var15, var25);
						}

						if (var22 == 226) {
							if (var23 == 0) {
								var23 = j[var15];
							} else {
								j[var15] = var23;
							}

							hb[var15] += var23 * 4;
							if (hb[var15] < 0) {
								hb[var15] = 0;
							} else if (hb[var15] > 7999) {
								hb[var15] = 7999;
							}

							var25 = N[hb[var15]];
							a(var15, var25);
						}

						if (var22 == 33) {
							if (var23 / 16 == 1) {
								var23 &= 15;
								if (var23 == 0) {
									var23 = A[var15];
								} else {
									A[var15] = var23;
								}

								hb[var15] -= var23;
								if (hb[var15] < 0) {
									hb[var15] = 0;
								} else if (hb[var15] > 7999) {
									hb[var15] = 7999;
								}

								var25 = N[hb[var15]];
								a(var15, var25);
							} else {
								var23 &= 15;
								if (var23 == 0) {
									var23 = I[var15];
								} else {
									I[var15] = var23;
								}

								hb[var15] += var23;
								if (hb[var15] < 0) {
									hb[var15] = 0;
								} else if (hb[var15] > 7999) {
									hb[var15] = 7999;
								}

								var25 = N[hb[var15]];
								a(var15, var25);
							}
						}
					}
				}
			}

			if (Z > 0) {
				for (var16 = 0; var16 < r.J; ++var16) {
					if (!w[var16]) {
						if (M[var16] != 0) {
							P += E[var16];
							if (P < 0) {
								P = 0;
							} else if (P > 64) {
								P = 64;
							}
						}

						if (u[var16] != 0 || D[var16] != 0) {
							db[var16] += p[var16];
							if (db[var16] < 0) {
								db[var16] = 0;
							} else if (db[var16] > 64) {
								db[var16] = 64;
							}

							C[var16] += s[var16];
							if (C[var16] < 0) {
								C[var16] = 0;
							} else if (C[var16] > 255) {
								C[var16] = 255;
							}
						}

						if (B[var16] == 1) {
							y[var16] = (y[var16] + bb[var16]) % 68;
							int var2 = S[y[var16]] * V[var16] >> 8;
							var17 = hb[var16] + var2;
							a(var16, N[var17]);
						}

						if (i[var16] > 0) {
							hb[var16] -= i[var16] * 4;
							if (hb[var16] < 0) {
								hb[var16] = 0;
							} else if (hb[var16] > 7999) {
								hb[var16] = 7999;
							}

							var17 = N[hb[var16]];
							a(var16, var17);
						}

						if (eb[var16] > 0) {
							hb[var16] += eb[var16] * 4;
							if (hb[var16] < 0) {
								hb[var16] = 0;
							} else if (hb[var16] > 7999) {
								hb[var16] = 7999;
							}

							var17 = N[hb[var16]];
							a(var16, var17);
						}

						if (L[var16] > 0) {
							if (hb[var16] < h[var16]) {
								hb[var16] += cb[var16] * 4;
								if (hb[var16] > h[var16]) {
									hb[var16] = h[var16];
								}
							}

							if (hb[var16] > h[var16]) {
								hb[var16] -= cb[var16] * 4;
								if (hb[var16] < h[var16]) {
									hb[var16] = h[var16];
								}
							}

							if (hb[var16] < 0) {
								hb[var16] = 0;
							} else if (hb[var16] > 7999) {
								hb[var16] = 7999;
							}

							var17 = N[hb[var16]];
							a(var16, var17);
						}
					}
				}
			}

			for (var16 = 0; var16 < r.J; ++var16) {
				if (!w[var16]) {
					var17 = db[var16];
					var18 = C[var16];
					R[var16] = 0;
					if (v[var16] >= 0) {
						int var27 = v[var16];
						int var4;
						int var5;
						int var6;
						int var7;
						int var8;
						int var9;
						if ((r.t[v[var16]] & 1) == 1) {
							R[var16] = 1;
							var4 = ab[var16];

							for (var5 = 0; r.B[var27][var5 + 1] < var4; ++var5) {
							}

							var6 = r.B[var27][var5];
							var8 = r.B[var27][var5 + 1];
							var7 = r.G[var27][var5];
							var9 = r.G[var27][var5 + 1];
							if (var8 == var6) {
								++var8;
							}

							int var11 = ((var8 - var4) * var7 + (var4 - var6) * var9) / (var8 - var6);
							int var12 = '\u8000' - Q[var16];
							if (var12 < 0) {
								var12 = 0;
							}

							var17 = var17 * var11 * var12 >> 21;
							if ((r.t[var27] & 2) == 2 && kb[var16] == 1 && ab[var16] == r.B[var27][r.j[var27]]) {
								--ab[var16];
							}

							if (ab[var16] == r.B[var27][r.B[var27].length - 1]) {
								--ab[var16];
							}

							if (kb[var16] == 0) {
								Q[var16] += r.a[var27];
							}

							++ab[var16];
							if ((r.t[var27] & 4) == 4 && ab[var16] == r.B[var27][r.D[var27]]) {
								ab[var16] = r.B[var27][r.I[var27]];
							}
						}

						if ((r.l[v[var16]] & 1) == 1) {
							var4 = X[var16];

							for (var5 = 0; r.d[var27][var5 + 1] < var4; ++var5) {
							}

							var6 = r.d[var27][var5];
							var8 = r.d[var27][var5 + 1];
							var7 = r.z[var27][var5];
							var9 = r.z[var27][var5 + 1];
							if (var8 == var6) {
								++var8;
							}

							int var13 = ((var8 - var4) * var7 + (var4 - var6) * var9) / (var8 - var6);
							var18 += (var13 - 32) * (128 - Math.abs(var18 - 128)) >> 5;
							if ((r.l[var27] & 2) == 2 && kb[var16] == 1 && X[var16] == r.d[var27][r.x[var27]]) {
								--X[var16];
							}

							if (X[var16] == r.d[var27][r.d[var27].length - 1]) {
								--X[var16];
							}

							++X[var16];
							if ((r.l[var27] & 4) == 4 && X[var16] == r.d[var27][r.k[var27]]) {
								X[var16] = r.d[var27][r.y[var27]];
							}
						}
					}

					a(var16, var17, var18);
				}
			}

			++Z;
			if (Z >= q) {
				Z = 0;
			}

			return var14;
		}
	}

	final void b() {
		for (int var1 = 0; var1 < r.J; ++var1) {
			if (fb[var1] != null) {
				super.g.a(fb[var1]);
			}
		}

	}

	final void a() {
		for (int var1 = 0; var1 < r.J; ++var1) {
			if (fb[var1] != null) {
				super.g.b(fb[var1]);
			}
		}

	}

	private void a(int var1, int var2, int var3) {
		if (fb[var1] != null) {
			if (var2 < 0) {
				var2 = 0;
			} else if (var2 > 64) {
				var2 = 64;
			}

			if (var3 < 0) {
				var3 = 0;
			} else if (var3 > 256) {
				var3 = 256;
			}

			fb[var1].a(qc.f >> 7, var2 * P * l >> 12, var3);
		}

	}

	private void a(int var1, int var2) {
		if (fb[var1] != null) {
			fb[var1].h(var2 * 256 / qc.f);
		}

	}

	private void e() {
		db = new int[r.J];
		C = new int[r.J];
		hb = new int[r.J];
		H = new int[r.J];
		v = new int[r.J];
		E = new int[r.J];
		p = new int[r.J];
		s = new int[r.J];
		bb = new int[r.J];
		V = new int[r.J];
		y = new int[r.J];
		B = new int[r.J];
		F = new int[r.J];
		O = new int[r.J];
		A = new int[r.J];
		G = new int[r.J];
		j = new int[r.J];
		I = new int[r.J];
		i = new int[r.J];
		eb = new int[r.J];
		h = new int[r.J];
		cb = new int[r.J];
		J = new int[r.J];
		ab = new int[r.J];
		X = new int[r.J];
		R = new int[r.J];
		kb = new int[r.J];
		Q = new int[r.J];
		L = new int[r.J];
		u = new int[r.J];
		D = new int[r.J];
		M = new int[r.J];
		w = new boolean[r.J];
		fb = new pg[r.J];
		c();
	}

	public static void f() {
		N = null;
		S = null;
	}

	final synchronized void e(int var1) {
		l = var1;
	}

	kg(dk var1) {
		r = var1;
		e();
	}

	static {
		for (int var0 = 0; var0 < 8000; ++var0) {
			N[var0] = (int) (8363.0D * Math.pow(2.0D, (double) (4608 - var0) / 768.0D));
		}

		for (int var1 = 0; var1 < 68; ++var1) {
			S[var1] = (int) (-2048.0D * Math.sin((double) var1 * 0.0923998D));
		}

	}
}
