import java.io.EOFException;
import java.io.IOException;

final class MainCacheIndex {

   private int len = '\ufde8';
   private MainCacheFile index = null;
   private int id;
   static String d = "Left";
   static int e = 0;
   static String f = "<%0> has dropped out.";
   private MainCacheFile cache = null;
   static an h;
   static bk i;


   public static void a(int var0) {
      try {
         f = null;
         d = null;
         if(var0 == 14426) {
            h = null;
            i = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "MainCacheIndex.A(" + var0 + ')');
      }
   }

   public final String toString() {
      try {
         return "Cache:" + id;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "MainCacheIndex.toString(" + ')');
      }
   }

   static int[] a(int[] var0, int[] var1, int var2) {
      int var5 = client.L;

      try {
         int[] var3 = new int[8];
         if(var2 <= 107) {
            return null;
         } else {
            int var4 = 0;
            if(var5 == 0 && var4 >= 8) {
               return var3;
            } else {
               do {
                  var3[var4] = wm.a(var1[var4], var0[var4]);
                  ++var4;
               } while(var4 < 8);

               return var3;
            }
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "MainCacheIndex.C(" + (var0 != null?"{...}":"null") + ',' + (var1 != null?"{...}":"null") + ',' + var2 + ')');
      }
   }

   final boolean a(int var1, byte[] var2, boolean var3, int var4) {
      try {
         boolean var5;
		  if(var4 < 0 || ~len > ~var4) {
			 throw new IllegalArgumentException();
		  }
		  boolean var8 = a(var3, var2, -124, var4, var1);
		  if(!var8) {
			 var8 = a(false, var2, -115, var4, var1);
		  }
		  var5 = var8;

		  return var5;
      } catch (RuntimeException var12) {
         throw pl.a(var12, "MainCacheIndex.B(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ',' + var4 + ')');
      }
   }

   private boolean a(boolean var1, byte[] var2, int var3, int var4, int var5) {
      int var16 = client.L;

      try {
		  boolean var6;
		  try {
			 int var9;
			 if(var1) {
				if(~((long)(var5 * 6 - -6)) < ~index.a((byte) -108)) {
				   var6 = false;
				   return var6;
				}

				 index.setOffset((long) (var5 * 6), (byte) 90);
				 index.a(0, 6, 0, hi.a);
				var9 = (hi.a[5] & 255) + ((255 & hi.a[4]) << 8) + (hi.a[3] << 16 & 16711680);
				if(~var9 >= -1 || ~((long)var9) < ~(cache.a((byte) 115) / 520L)) {
				   var6 = false;
				   return var6;
				}
			 } else {
				var9 = (int)((cache.a((byte) -126) + 519L) / 520L);
				if(var9 == 0) {
				   var9 = 1;
				}
			 }

			 hi.a[0] = (byte)(var4 >> 16);
			 hi.a[4] = (byte)(var9 >> 8);
			 hi.a[5] = (byte)var9;
			 hi.a[2] = (byte)var4;
			 hi.a[3] = (byte)(var9 >> 16);
			 hi.a[1] = (byte)(var4 >> 8);
			  index.setOffset((long) (var5 * 6), (byte) -8);
			  index.a(0, 6, true, hi.a);
			 int var10 = 0;
			 if(var3 > -105) {
				 cache = null;
			 }

			 int var11 = 0;
			 if(var16 != 0 || var4 > var10) {
				do {
				   int var12 = 0;
				   int var13;
				   if(var1) {
					   cache.setOffset((long) (520 * var9), (byte) -60);

					  try {
						  cache.a(0, 8, 0, hi.a);
					  } catch (EOFException var21) {
						 if(var16 == 0) {
							break;
						 }
					  }

					  var13 = ((255 & hi.a[0]) << 8) - -(255 & hi.a[1]);
					  int var14 = (255 & hi.a[3]) + (hi.a[2] << 8 & '\uff00');
					  var12 = (16711680 & hi.a[4] << 16) + ((hi.a[5] & 255) << 8) - -(255 & hi.a[6]);
					  int var15 = 255 & hi.a[7];
					  if(var5 != var13 || ~var14 != ~var11 || var15 != id) {
						 var6 = false;
						 return var6;
					  }

					  if(~var12 > -1 || (long)var12 > cache.a((byte) 29) / 520L) {
						 var6 = false;
						 return var6;
					  }
				   }

				   if(~var12 == -1) {
					  var12 = (int)((519L + cache.a((byte) 56)) / 520L);
					  var1 = false;
					  if(~var12 == -1) {
						 ++var12;
					  }

					  if(var9 == var12) {
						 ++var12;
					  }
				   }

				   hi.a[0] = (byte)(var5 >> 8);
				   hi.a[3] = (byte)var11;
				   if(var4 + -var10 <= 512) {
					  var12 = 0;
				   }

				   hi.a[1] = (byte)var5;
				   hi.a[2] = (byte)(var11 >> 8);
				   hi.a[7] = (byte) id;
				   hi.a[4] = (byte)(var12 >> 16);
				   hi.a[6] = (byte)var12;
				   hi.a[5] = (byte)(var12 >> 8);
					cache.setOffset((long) (var9 * 520), (byte) -17);
					cache.a(0, 8, true, hi.a);
				   var13 = var4 - var10;
				   if(var13 > 512) {
					  var13 = 512;
				   }

					cache.a(var10, var13, true, var2);
				   var10 += var13;
				   ++var11;
				   var9 = var12;
				} while(var4 > var10);
			 }

			 var6 = true;
			 return var6;
		  } catch (IOException var22) {
			 var6 = false;
			 return var6;
		  }
	  } catch (RuntimeException var24) {
         throw pl.a(var24, "MainCacheIndex.F(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

   static void setLoadingText(float var0, int var1, String text) {
      try {
         if(var1 > -74) {
            a(null, null, -64);
         }

         rg.v = text;
         gk.K = var0;
      } catch (RuntimeException var4) {
         throw pl.a(var4, "MainCacheIndex.setLoadingText(" + var0 + ',' + var1 + ',' + (text != null?"{...}":"null") + ')');
      }
   }

   final byte[] a(int var1, int offset) {
      int var17 = client.L;

      try {
         Object var3;
		  try {
			 if((long)(6 * offset + 6) > index.a((byte) -119)) {
				var3 = null;
				return (byte[])var3;
			 }

			  index.setOffset((long) (6 * offset), (byte) 104);
			  index.a(0, 6, 0, hi.a);
			 int var6 = ('\uff00' & hi.a[1] << 8) + ((hi.a[0] & 255) << 16) - -(hi.a[2] & 255);
			 if(var1 != -24237) {
				var3 = null;
				return (byte[])var3;
			 }

			 int var7 = (hi.a[5] & 255) + ((hi.a[3] & 255) << 16) + ((hi.a[4] & 255) << 8);
			 if(var6 < 0 || ~var6 < ~len) {
				var3 = null;
				return (byte[])var3;
			 }

			 if(~var7 < -1 && cache.a((byte) 44) / 520L >= (long)var7) {
				byte[] var8 = new byte[var6];
				int var9 = 0;
				int var10 = 0;
				byte[] var24;
				if(var17 == 0 && ~var9 <= ~var6) {
				   var24 = var8;
				   return var24;
				}

				do {
				   if(~var7 == -1) {
					  var3 = null;
					  return (byte[])var3;
				   }

					cache.setOffset((long) (var7 * 520), (byte) 105);
				   int var11 = var6 - var9;
				   if(var11 > 512) {
					  var11 = 512;
				   }

					cache.a(0, var11 + 8, 0, hi.a);
				   int var12 = (hi.a[1] & 255) + ((hi.a[0] & 255) << 8);
				   int var13 = ((255 & hi.a[2]) << 8) - -(255 & hi.a[3]);
				   int var14 = (16711680 & hi.a[4] << 16) + ((hi.a[5] & 255) << 8) - -(255 & hi.a[6]);
				   int var15 = 255 & hi.a[7];
				   if(~offset == ~var12 && var13 == var10 && ~id == ~var15) {
					  if(~var14 <= -1 && ~((long)var14) >= ~(cache.a((byte) 48) / 520L)) {
						 int var16 = 0;
						 if(var17 != 0 || var16 < var11) {
							do {
							   var8[var9++] = hi.a[var16 + 8];
							   ++var16;
							} while(var16 < var11);

							++var10;
							var7 = var14;
							continue;
						 }

						 ++var10;
						 var7 = var14;
						 continue;
					  }

					  var3 = null;
					  return (byte[])var3;
				   }

				   var3 = null;
				   return (byte[])var3;
				} while(~var9 > ~var6);

				var24 = var8;
				return var24;
			 }

			 var3 = null;
		  } catch (IOException var21) {
			 var3 = null;
			 return (byte[])var3;
		  }

		  return (byte[])var3;
      } catch (RuntimeException var23) {
         throw pl.a(var23, "MainCacheIndex.D(" + var1 + ',' + offset + ')');
      }
   }

   MainCacheIndex(int id, MainCacheFile cache, MainCacheFile index, int var4) {
      try {
		  this.index = index;
		  this.id = id;
		  this.cache = cache;
		  len = var4;
      } catch (RuntimeException var6) {
         throw pl.a(var6, "MainCacheIndex.<init>(" + id + ',' + (cache != null?"{...}":"null") + ',' + (index != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

}
