import java.util.Hashtable;

class pj extends bn {

   static volatile int mouseX = -1;
   boolean w;
   boolean x;
   private boolean y;
   static String[] z;
   static int A;
   static boolean B = false;
   static String[] C = new String[]{"Before playing a multiplayer game, please learn the ropes by completing the first 3 stages of Stamina Mode. It will only take a few minutes.", "Congratulations! The multiplayer game is now unlocked.<br><br>If you wish, you can continue playing Stamina Mode, or you can end this game any time and enter the multiplayer lobby. Press \'ESC\' for the Options Menu."};
   static String D = "<%0> wants to join";
   static FunImage E;
   private boolean F;
   static int G;


   boolean a(bn var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      try {
         if(this.w && this.a(1, var5, var4, var3, var7)) {
            this.a(-11129, var1);
            super.r = var2;
            if(super.listener != null && super.listener instanceof ke) {
               ((ke)super.listener).a(var5, var7, var3, var4, this, true, var2);
            }

            return true;
         } else {
            return var6 != -16581;
         }
      } catch (RuntimeException var9) {
         throw pl.a(var9, "pj.MA(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
      }
   }

   static final void a(int var0, kc var1, int var2) {
      try {
         int var4 = 37 / ((3 - var0) / 56);
         FunN var3 = bf.f;
         var3.a(var2, true);
         var3.a((int)5, (int)-108);
         var3.a((int)0, (int)23);
         var3.b((byte)-116, var1.p);
         var3.a(var1.i, (int)-64);
         var3.a(var1.r, (int)0);
      } catch (RuntimeException var5) {
         throw pl.a(var5, "pj.CB(" + var0 + ',' + (var1 != null?"{...}":"null") + ',' + var2 + ')');
      }
   }

   final boolean e(int var1) {
      try {
         return var1 != Integer.MAX_VALUE?false:this.y;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "pj.DA(" + var1 + ')');
      }
   }

   boolean a(int var1, byte var2, bn var3, char var4) {
      try {
         if(this.e(var2 ^ 2147483586) && (var1 == 84 || ~var1 == -84)) {
            this.b(-1, -1, 1, -1);
            return true;
         } else {
            return var2 != 61?false:false;
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "pj.E(" + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

   final StringBuffer a(int var1, Hashtable table, byte b, StringBuffer buffer) {
      try {
         if(b != 99) {
            return null;
         } else {
            if(this.a(b + -99, buffer, var1, table)) {
               this.b(115, buffer, var1, table);
               if(this.x) {
                  buffer.append(" active");
               }

               if(!this.w) {
                  buffer.append(" disabled");
               }
            }

            return buffer;
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "pj.GA(" + var1 + ',' + (table != null?"{...}":"null") + ',' + b + ',' + (buffer != null?"{...}":"null") + ')');
      }
   }

   final void a(int var1, int var2, int var3, int var4, int var5, bn var6) {
      try {
         if(super.listener != null && super.listener instanceof ke) {
            ((ke)super.listener).a(var3, this, var5, true, var4, var1);
         }

         super.r = 0;
         if(var2 <= 70) {
            G = -64;
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "pj.IA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + (var6 != null?"{...}":"null") + ')');
      }
   }

   pj(String var1, vg var2, aa var3) {
      super(var1, var2, var3);
      this.y = false;
      this.w = true;
      this.F = true;
   }

   static final String a(String var0, byte var1) {
      try {
         if(var1 > -44) {
            a((String)null, (byte)-2);
         }

         return ed.a(var0, 1097122945, false);
      } catch (RuntimeException var3) {
         throw pl.a(var3, "pj.DB(" + (var0 != null?"{...}":"null") + ',' + var1 + ')');
      }
   }

   final void c(boolean var1) {
      try {
         if(this.y) {
            this.y = false;
            if(super.listener != null && super.listener instanceof le) {
               ((le)super.listener).a(119, this, this.y);
            }
         }

         if(var1) {
            a((String)null, (byte)15);
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "pj.NA(" + var1 + ')');
      }
   }

   boolean a(int var1, bn var2) {
      try {
         if(this.w && this.F) {
            var2.c(false);
            if(var1 != -11129) {
               return false;
            } else {
               this.y = true;
               if(super.listener != null && super.listener instanceof le) {
                  ((le)super.listener).a(var1 ^ -11041, this, this.y);
               }

               return true;
            }
         } else {
            return false;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "pj.PA(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   pj(String var1, aa var2) {
      this(var1, vc.a.c, var2);
   }

   void b(int var1, int var2, int var3, int var4) {
      try {
         if(var4 != -1) {
            z = null;
         }

         if(super.listener != null && super.listener instanceof pd) {
            ((pd)super.listener).a(var3, var1, this, var2, 5);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "pj.J(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   public static void g(int var0) {
      try {
         if(var0 != 84) {
            E = null;
         }

         E = null;
         D = null;
         z = null;
         C = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "pj.EB(" + var0 + ')');
      }
   }

   void a(bn var1, int var2, int var3, int var4) {
      try {
         super.a(var1, var2, 80, var4);
         if(super.r != 0 && gf.l != super.r) {
            if(this.a(1, var4, pc.Hb, var2, jn.a) && gf.l == 0) {
               this.b(-var2 + pc.Hb, jn.a - var4, super.r, -1);
            }

            this.a(pc.Hb, 93, var4, var2, jn.a, var1);
         }

         int var5 = -13 / ((var3 - 40) / 38);
      } catch (RuntimeException var6) {
         throw pl.a(var6, "pj.D(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   public pj() {
      this.y = false;
      this.w = true;
      this.F = true;

      try {
         super.renderer = vc.a.i;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "pj.<init>(" + ')');
      }
   }

}
