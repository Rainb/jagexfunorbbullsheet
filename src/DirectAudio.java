import com.ms.com.ComFailException;
import com.ms.directX.*;

import java.awt.*;

public final class DirectAudio implements IDirectAudio {

	private DirectSound directSound;
	private int b;
	private WaveFormatEx waveFormatEx;
	private DirectSoundBuffer[] soundBuffers = new DirectSoundBuffer[2];
	private boolean[] activeBuffer = new boolean[2];
	private byte[][] f = new byte[2][];
	private int blockAlign;
	private int[] h = new int[2];
	private int channels;
	private DSBufferDesc[] dsBufferDescs = new DSBufferDesc[2];
	private int[] k;
	private int[] l = new int[2];
	private DSCursors[] dsCursors = new DSCursors[2];


	public final void a(int var1, int var2) {
		if (soundBuffers[var2] != null) {
			try {
				soundBuffers[var2].stop();
			} catch (ComFailException ignored) {
			}

			soundBuffers[var2] = null;
		}
		if (var1 != -2359) {
			dsCursors = null;
		}
	}

	public final void stop(int index, byte var2) {
		try {
			soundBuffers[index].stop();
			if (var2 >= -28) {
				blockAlign = 14;
			}

			activeBuffer[index] = false;
			soundBuffers[index].setCurrentPosition(0);
			h[index] = 0;
		} catch (ComFailException e) {
			e.printStackTrace();
		}
	}

	public final void a(int soundBuffer, int[] var2) {
		int var3 = var2.length;
		if (var3 != 256 * channels) {
			throw new IllegalArgumentException();
		} else {
			int var4 = h[soundBuffer] * blockAlign;

			for (int var5 = 0; var5 < var3; ++var5) {
				int var6 = var2[var5];
				if ((var6 + 8388608 & -16777216) != 0) {
					var6 = 8388607 ^ var6 >> 31;
				}

				f[soundBuffer][var4 + var5 * 2] = (byte) (var6 >> 8);
				f[soundBuffer][var4 + var5 * 2 + 1] = (byte) (var6 >> 16);
			}

			soundBuffers[soundBuffer].writeBuffer(var4, var3 * 2, f[soundBuffer], 0);
			h[soundBuffer] = h[soundBuffer] + var3 / channels;
			if (!activeBuffer[soundBuffer]) {
				soundBuffers[soundBuffer].play(1);
				activeBuffer[soundBuffer] = true;
			}

		}
	}

	public final void a(boolean active, int var2, int index) throws Exception {
		if (~b != -1 && soundBuffers[index] == null) {
			int var4 = blockAlign * 65536;
			if (f[index] == null || ~var4 != ~f[index].length) {
				f[index] = new byte[var4];
				dsBufferDescs[index].bufferBytes = var4;
			}

			soundBuffers[index] = directSound.createSoundBuffer(dsBufferDescs[index], waveFormatEx);
			activeBuffer[index] = active;
			h[index] = 0;
			l[index] = var2;
		} else {
			throw new IllegalStateException();
		}
	}

	public final int a(int index, boolean var2) {
		if (!activeBuffer[index]) {
			return 0;
		} else {
			soundBuffers[index].getCurrentPosition(dsCursors[index]);
			if (var2) {
				dsCursors = null;
			}

			int var3 = dsCursors[index].write / blockAlign;
			int var4 = h[index] - var3;
			if (l[index] < var4) {
				for (int var5 = -h[index] + var3; var5 > 0; var5 -= 256) {
					a(index, k);
				}

				var4 = -var3 + h[index];
			}

			return var4;
		}
	}

	public final void a(boolean doubleChannel, Component component, int sampleRate, int var4) throws Exception {
		if (~b == -1) {
			if (~sampleRate <= -8001 && sampleRate <= '\ubb80') {
				blockAlign = doubleChannel ? 4 : 2;
				channels = doubleChannel ? 2 : 1;
				k = new int[256 * channels];
				directSound.initialize(null);
				directSound.setCooperativeLevel(component, var4);

				for (int index = 0; ~index > -3; ++index) {
					dsBufferDescs[index].flags = 16384;
				}

				waveFormatEx.samplesPerSec = sampleRate;
				waveFormatEx.bitsPerSample = 16;
				waveFormatEx.formatTag = 1;
				waveFormatEx.channels = channels;
				waveFormatEx.blockAlign = blockAlign;
				b = sampleRate;
				waveFormatEx.avgBytesPerSec = blockAlign * sampleRate;
			} else {
				throw new IllegalArgumentException();
			}
		}
	}

	public DirectAudio() throws Exception {
		directSound = new DirectSound();
		waveFormatEx = new WaveFormatEx();
		for (int index = 0; ~index > -3; ++index) {
			dsBufferDescs[index] = new DSBufferDesc();
		}
		for (int index = 0; index < 2; ++index) {
			dsCursors[index] = new DSCursors();
		}
	}
}
