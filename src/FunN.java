import java.awt.Canvas;
import java.awt.Graphics;
import java.math.BigInteger;

final class FunN extends ByteBuffer {

   static ph p;
   private int q;
   static BigInteger r = new BigInteger("5718356335657395518331821700519095443885622059651704259708802406811661091006861135294892374371095523138870720024266040385056460362796623317915157483226371");
   private int s;
   static boolean t;
   private rm u;
   static String eliminateScoreText = "Eliminate Score: ";
   static String returnToOptionsMenu = "Return to Options Menu";


   final int c(byte var1, int var2) {
      int var7 = client.L;

      try {
         int var4 = -33 % ((-34 - var1) / 45);
         int var3 = s >> 1537175683;
         int var5 = -(7 & s) + 8;
		  s += var2;
         int var6 = 0;
         if(var7 != 0) {
            var6 += (super.buffer[var3++] & cg.e[var5]) << var2 + -var5;
            var2 -= var5;
            var5 = 8;
         }

         while(~var5 > ~var2) {
            var6 += (super.buffer[var3++] & cg.e[var5]) << var2 + -var5;
            var2 -= var5;
            var5 = 8;
         }

         if(var2 == var5) {
            var6 += cg.e[var5] & super.buffer[var3];
            if(var7 == 0) {
               return var6;
            }
         }

         var6 += super.buffer[var3] >> -var2 + var5 & cg.e[var2];
         return var6;
      } catch (RuntimeException var8) {
         throw pl.a(var8, "FunN.WA(" + var1 + ',' + var2 + ')');
      }
   }

   static String a(boolean var0, String str) {
      try {
         int var2 = gg.a(pa.D, jc.h, -27472);
         if(var2 == 1) {
            str = "<img=0>" + str;
         }

         if(!var0) {
            t = false;
         }

         if(~var2 == -3) {
            str = "<img=1>" + str;
         }

         return str;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "FunN.FB(" + var0 + ',' + (str != null?"{...}":"null") + ')');
      }
   }

   final void a(int[] var1, int var2) {
      try {
         if(var2 < -26) {
			 u = new rm(var1);
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "FunN.VA(" + (var1 != null?"{...}":"null") + ',' + var2 + ')');
      }
   }

   FunN(int var1) {
      super(var1);
   }

   static void draw(int someInt, int x, Canvas canvas, int y) {
      try {
         try {
            Graphics graphics = canvas.getGraphics();
            wm.g.draw(x, graphics, -16711898, y);
            graphics.dispose();
            if(someInt < 83) {
               r = null;
            }
         } catch (Exception var5) {
            canvas.repaint();
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "FunN.RA(" + someInt + ',' + x + ',' + (canvas != null?"{...}":"null") + ',' + y + ')');
      }
   }

   FunN(byte[] var1) {
      super(var1);
   }

   final void l(int var1) {
      try {
         if(var1 != 17675) {
			 a((byte)2, -88, -43);
         }

         super.offset = (7 + s) / 8;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "FunN.BB(" + var1 + ')');
      }
   }

   public static void m(int var0) {
      try {
         p = null;
         returnToOptionsMenu = null;
         if(var0 != -240) {
            r = null;
         }

         eliminateScoreText = null;
         r = null;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "FunN.TA(" + var0 + ')');
      }
   }

   final void d(byte var1) {
      try {
		  s = 8 * super.offset;
         if(var1 != -41) {
			 u = null;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "FunN.EB(" + var1 + ')');
      }
   }

   final void e(byte var1) {
      try {
         if(var1 >= -32) {
			 c((byte)-40, 91);
         }

         if(q < 8) {
			 q = 8;
            ++super.offset;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "FunN.CB(" + var1 + ')');
      }
   }

   final void e(boolean var1) {
      try {
         if(var1) {
            super.buffer[super.offset] = 0;
			 q = 8;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "FunN.AB(" + var1 + ')');
      }
   }

   final void a(byte var1, int var2, int var3) {
      int var5 = client.L;

      try {
         var3 &= cg.e[var2];
         int var10001;
         byte[] var10000;
         if(var5 != 0) {
            var2 -= q;
            var10000 = super.buffer;
            var10001 = super.offset++;
            var10000[var10001] = (byte)(var10000[var10001] + (var3 >>> var2));
            super.buffer[super.offset] = 0;
			 q = 8;
         }

         while(~q > ~var2) {
            var2 -= q;
            var10000 = super.buffer;
            var10001 = super.offset++;
            var10000[var10001] = (byte)(var10000[var10001] + (var3 >>> var2));
            super.buffer[super.offset] = 0;
			 q = 8;
         }

         label23: {
            if(var2 != q) {
				q -= var2;
               super.buffer[super.offset] = (byte)(super.buffer[super.offset] + (var3 << q));
               if(var5 == 0) {
                  break label23;
               }
            }

            var10000 = super.buffer;
            var10001 = super.offset++;
            var10000[var10001] = (byte)(var10000[var10001] + var3);
            super.buffer[super.offset] = 0;
			 q = 8;
         }

         if(var1 <= 46) {
            n(-54);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "FunN.GB(" + var1 + ',' + var2 + ',' + var3 + ')');
      }
   }

   final void a(int var1, int var2, byte[] var3, int var4) {
      int var6 = client.L;

      try {
         int var5 = 0;
         if(var2 != -10661) {
            returnToOptionsMenu = null;
            if(var6 == 0 && var1 <= var5) {
               return;
            }
         } else if(var1 <= var5) {
            return;
         }

         do {
            var3[var5 - -var4] = (byte)(super.buffer[super.offset++] + -u.a(-28747));
            ++var5;
         } while(var1 > var5);

      } catch (RuntimeException var7) {
         throw pl.a(var7, "FunN.QA(" + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

   static void n(int var0) {
      try {
         if(!ba.b) {
            throw new IllegalStateException();
         } else {
            if(var0 != -943862777) {
               returnToOptionsMenu = null;
            }

            if(ah.lb != null) {
               ah.lb.k((byte)-91);
            }

            String var1 = on.a(true);
            FunNodeList.i = new vl(var1, null, true, false, false);
            od.Qb.b((byte)112, kj.a);
            kj.a.a(FunNodeList.i, 21843);
            kj.a.k(var0 ^ -943862610);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "FunN.DB(" + var0 + ')');
      }
   }

   final void a(int var1, boolean var2) {
      try {
         super.buffer[super.offset++] = (byte)(u.a(-28747) + var1);
         if(!var2) {
            returnToOptionsMenu = null;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "FunN.UA(" + var1 + ',' + var2 + ')');
      }
   }

   final int o(int var1) {
      try {
         if(var1 != 255) {
            n(22);
         }

         return 255 & super.buffer[super.offset++] + -u.a(-28747);
      } catch (RuntimeException var3) {
         throw pl.a(var3, "FunN.SA(" + var1 + ')');
      }
   }

}
