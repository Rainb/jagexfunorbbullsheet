final class FunKB extends ph {

	static boolean Hb;
	static boolean Ib;
	static uf Jb;
	private ph Kb;
	static String Lb = "Kick";
	static int[] Mb = new int[] { 0, 0, 5, 9, 12, 14, 15, 15, 14, 12, 8, 4, 2, 1 };
	private ph Nb;
	static String Ob = "To server list";
	static String[][] Pb = new String[][] { { "Earthquake", "Causes stacks of loose pieces to collapse." }, { "Drill", "Individually pops every piece in its path." }, { "Bomb",
			"When you pop a shape touching a bomb, everything in your bucket of the same colour will explode." }, { "Power Drill", "Pops every entire shape, loose or solid, " +
			"in its path. Loose shapes take touching solid shapes with them!" }, { "Water Capsule", "Turns every solid shape in your bucket into loose pieces. Can be quite " +
			"spectacular." }, { "Poison", "Turns all the loose pieces in your bucket into solid shapes. Not good!" }, { "Wildcard",
			"Can be used in place of any other loose piece." } };
	static String Qb = "Go Back";


	public static void c(byte var0) {
		try {
			Jb = null;
			int var1 = -30 / ((33 - var0) / 44);
			Lb = null;
			Pb = null;
			Qb = null;
			Ob = null;
			Mb = null;
		} catch (RuntimeException var2) {
			throw pl.a(var2, "FunKB.F(" + var0 + ')');
		}
	}

	final int b(int var1, byte var2) {
		try {
			int var3 = -81 / ((var2 - 61) / 47);
			return Nb.i(Integer.MAX_VALUE) - (-var1 - Kb.i(Integer.MAX_VALUE));
		} catch (RuntimeException var4) {
			throw pl.a(var4, "FunKB.A(" + var1 + ',' + var2 + ')');
		}
	}

	final void a(int var1, int var2, int var3, int var4, int var5, byte var6) {
		try {
			int var7 = 123 % ((var6 - -18) / 41);
			a(var2, -30, var5, var1, var4);
			c(var3, true);
		} catch (RuntimeException var8) {
			throw pl.a(var8, "FunKB.E(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ')');
		}
	}

	FunKB(long var1, FunImage var3, FunImage var4, int var5, ph var6, String var7) {
		this(var1, null, var6, var7);

		try {
			Nb.zb = var5;
			Nb.S = var3;
			Nb.Y = var4;
		} catch (RuntimeException var9) {
			throw pl.a(var9, "FunKB.<init>(" + var1 + ',' + (var3 != null ? "{...}" : "null") + ',' + (var4 != null ? "{...}" : "null") + ',' + var5 + ',' + (var6 != null ? "{...}" : "null") + ',' + (var7 != null ? "{...}" : "null") + ')');
		}
	}

	static FunImage[] a(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, byte var8) {
		int var19 = client.L;

		try {
			int var9 = var6 + var2 + var5;
			FunImage[] var10 = new FunImage[] { new FunImage(var9, var9), new FunImage(var7, var9), new FunImage(var9, var9), new FunImage(var9, var7), new FunImage(64, 64),
					new FunImage(var9, var7), new FunImage(var9, var9), new FunImage(var7, var9), new FunImage(var9, var9) };

			int var13 = 0;
			FunImage var14;
			int var15;
			if (var19 != 0) {
				var14 = var10[var13];
				var15 = 0;
				if (var19 != 0) {
					var14.pixels[var15] = var1;
					++var15;
				}

				while (true) {
					if (~var15 <= ~var14.pixels.length) {
						++var13;
						break;
					}

					var14.pixels[var15] = var1;
					++var15;
				}
			}

			while (var13 < var10.length) {
				var14 = var10[var13];
				var15 = 0;
				if (var19 != 0) {
					var14.pixels[var15] = var1;
					++var15;
				}

				while (~var15 > ~var14.pixels.length) {
					var14.pixels[var15] = var1;
					++var15;
				}

				++var13;
			}

			int var21 = 0;
			if (var19 != 0 || var21 < var5) {
				do {
					var15 = 0;
					if (var19 == 0 && ~var9 >= ~var15) {
						++var21;
					} else {
						do {
							var10[6].pixels[var9 * (var9 - var21 - 1) - -var15] = var0;
							var10[8].pixels[var15 + (-1 + var9 - var21) * var9] = var0;
							var10[2].pixels[-var21 + var9 + -1 + var15 * var9] = var0;
							var10[8].pixels[-1 + var9 + -var21 + var9 * var15] = var0;
							++var15;
						} while (~var9 < ~var15);

						++var21;
					}
				} while (var21 < var5);
			}

			var15 = 0;
			int var16;
			if (var19 != 0) {
				var16 = 0;
				if (var19 == 0 && var9 <= var16) {
					++var15;
				} else {
					while (true) {
						var10[0].pixels[var15 * var9 + var16] = var4;
						var10[0].pixels[var9 * var16 - -var15] = var4;
						if (var9 - var15 > var16) {
							var10[2].pixels[var9 * var15 + var16] = var4;
							var10[6].pixels[var15 + var16 * var9] = var4;
						}

						++var16;
						if (var9 <= var16) {
							++var15;
							break;
						}
					}
				}
			}

			while (~var5 < ~var15) {
				var16 = 0;
				if (var19 == 0 && var9 <= var16) {
					++var15;
				} else {
					do {
						var10[0].pixels[var15 * var9 + var16] = var4;
						var10[0].pixels[var9 * var16 - -var15] = var4;
						if (var9 - var15 > var16) {
							var10[2].pixels[var9 * var15 + var16] = var4;
							var10[6].pixels[var15 + var16 * var9] = var4;
						}

						++var16;
					} while (var9 > var16);

					++var15;
				}
			}

			var16 = 0;
			int var17;
			if (var19 != 0 || var16 < var7) {
				do {
					var17 = 0;
					if (var19 == 0 && ~var5 >= ~var17) {
						++var16;
					} else {
						do {
							var10[7].pixels[var7 * (var9 + -var17 - 1) + var16] = var0;
							var10[5].pixels[var9 * var16 + -var17 + -1 + var9] = var0;
							var10[1].pixels[var16 + var17 * var7] = var4;
							var10[3].pixels[var16 * var9 - -var17] = var4;
							++var17;
						} while (~var5 < ~var17);

						++var16;
					}
				} while (var16 < var7);
			}

			var17 = 0;
			if (var19 == 0 && ~var17 <= ~(var7 >> 2144145281)) {
				return var10;
			} else {
				do {
					int var18 = 0;
					if (var19 == 0 && ~var18 <= ~var6) {
						++var17;
					} else {
						do {
							var10[1].pixels[var17 + (-1 + -var18 + var9) * var7] = var3;
							var10[3].pixels[-var18 + var9 + -1 + var17 * var9] = var3;
							var10[7].pixels[var17 + var7 * var18] = var3;
							var10[5].pixels[var18 + var17 * var9] = var3;
							++var18;
						} while (~var18 > ~var6);

						++var17;
					}
				} while (~var17 > ~(var7 >> 2144145281));

				return var10;
			}
		} catch (RuntimeException var20) {
			throw pl.a(var20, "FunKB.D(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ')');
		}
	}

	static void a(boolean var0, int var1) {
		try {
			FunN var2 = bf.f;
			var2.a(var1, !var0);
			var2.a(1, 27);
			if (var0) {
				c((byte) -113);
			}

			var2.a(0, 125);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunKB.G(" + var0 + ',' + var1 + ')');
		}
	}

	static void k(int var0) {
		int var3 = client.L;

		try {
			wf.L = 0;
			FunCB.i = 0;
			vk.E = null;
			ch.c.b(false);
			hf.e.b(false);
			ud var1 = FunWB.P.a(var0);
			if (var3 != 0 || var1 != null) {
				do {
					var1.a(-118);
					var1 = FunWB.P.b((int) -17825);
				} while (var1 != null);
			}

			ud var2 = ig.d.a(var0);
			if (var3 == 0 && var2 == null) {
				ii.Z = 0;
			} else {
				do {
					var2.a(var0 ^ 90);
					var2 = ig.d.b(-17825);
				} while (var2 != null);

				ii.Z = 0;
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "FunKB.B(" + var0 + ')');
		}
	}

	FunKB(long var1, FunKB var3, String var4) {
		this(var1, var3.Nb, var3.Kb, var4);
	}

	private FunKB(long var1, ph var3, ph var4, String var5) {
		super(var1, null);

		try {
			Nb = new ph(0L, var3);
			Kb = new ph(0L, var4);
			Kb.W = var5;
			a(14939, Nb);
			a(14939, Kb);
			j(0);
		} catch (RuntimeException var7) {
			throw pl.a(var7, "FunKB.<init>(" + var1 + ',' + (var3 != null ? "{...}" : "null") + ',' + (var4 != null ? "{...}" : "null") + ',' + (var5 != null ? "{...}" : "null") + ')');
		}
	}

	private void c(int var1, boolean var2) {
		try {
			Nb.a(0, -105, 0, super.ob, Nb.i(Integer.MAX_VALUE));
			if (!var2) {
				Hb = true;
			}

			int var3 = Nb.rb - -var1;
			Kb.a(var3, 97, 0, super.ob, super.rb + -var3);
		} catch (RuntimeException var4) {
			throw pl.a(var4, "FunKB.C(" + var1 + ',' + var2 + ')');
		}
	}

}
