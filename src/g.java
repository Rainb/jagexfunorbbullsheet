final class g extends qi {

	int[] o;
	byte[] p;


	final void b(int var1, int var2) {
		var1 += super.i;
		var2 += super.h;
		int var3 = var1 + var2 * cd.width;
		int var4 = 0;
		int var5 = super.g;
		int var6 = super.e;
		int var7 = cd.width - var6;
		int var8 = 0;
		int var9;
		if (var2 < cd.g) {
			var9 = cd.g - var2;
			var5 -= var9;
			var2 = cd.g;
			var4 += var9 * var6;
			var3 += var9 * cd.width;
		}

		if (var2 + var5 > cd.e) {
			var5 -= var2 + var5 - cd.e;
		}

		if (var1 < cd.l) {
			var9 = cd.l - var1;
			var6 -= var9;
			var1 = cd.l;
			var4 += var9;
			var3 += var9;
			var8 += var9;
			var7 += var9;
		}

		if (var1 + var6 > cd.j) {
			var9 = var1 + var6 - cd.j;
			var6 -= var9;
			var8 += var9;
			var7 += var9;
		}

		if (var6 > 0 && var5 > 0) {
			b(cd.pixels, p, o, 0, var4, var3, var6, var5, var7, var8);
		}
	}

	private static void a(int[] var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		int var10 = 256 - var9;

		for (int var11 = -var6; var11 < 0; ++var11) {
			for (int var12 = -var5; var12 < 0; ++var12) {
				byte var13 = var1[var3++];
				if (var13 != 0) {
					int var15 = var2[var13 & 255];
					int var14 = var0[var4];
					var0[var4++] = ((var15 & 16711935) * var9 + (var14 & 16711935) * var10 & -16711936) + ((var15 & '\uff00') * var9 + (var14 & '\uff00') * var10 & 16711680) >> 8;
				} else {
					++var4;
				}
			}

			var4 += var7;
			var3 += var8;
		}

	}

	private static void b(int[] var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
		int var10 = -(var6 >> 2);
		var6 = -(var6 & 3);

		for (int var11 = -var7; var11 < 0; ++var11) {
			byte var14;
			for (int var12 = var10; var12 < 0; ++var12) {
				var14 = var1[var4++];
				if (var14 != 0) {
					var0[var5++] = var2[var14 & 255];
				} else {
					++var5;
				}

				var14 = var1[var4++];
				if (var14 != 0) {
					var0[var5++] = var2[var14 & 255];
				} else {
					++var5;
				}

				var14 = var1[var4++];
				if (var14 != 0) {
					var0[var5++] = var2[var14 & 255];
				} else {
					++var5;
				}

				var14 = var1[var4++];
				if (var14 != 0) {
					var0[var5++] = var2[var14 & 255];
				} else {
					++var5;
				}
			}

			for (int var13 = var6; var13 < 0; ++var13) {
				var14 = var1[var4++];
				if (var14 != 0) {
					var0[var5++] = var2[var14 & 255];
				} else {
					++var5;
				}
			}

			var5 += var8;
			var4 += var9;
		}

	}

	final g a() {
		g var1 = new g(super.e, super.g, o.length);
		var1.a = super.a;
		var1.j = super.j;
		var1.i = super.i;
		var1.h = super.h;
		int var2 = p.length;

		System.arraycopy(p, 0, var1.p, 0, var2);

		var2 = o.length;

		System.arraycopy(o, 0, var1.o, 0, var2);

		return var1;
	}

	g(int var1, int var2, int var3, int var4, int var5, int var6, byte[] var7, int[] var8) {
		super.a = var1;
		super.j = var2;
		super.i = var3;
		super.h = var4;
		super.e = var5;
		super.g = var6;
		p = var7;
		o = var8;
	}

	final void b() {
		if (super.e != super.a || super.g != super.j) {
			byte[] var1 = new byte[super.a * super.j];
			int var2 = 0;

			for (int var3 = 0; var3 < super.g; ++var3) {
				for (int var4 = 0; var4 < super.e; ++var4) {
					var1[var4 + super.i + (var3 + super.h) * super.a] = p[var2++];
				}
			}

			p = var1;
			super.e = super.a;
			super.g = super.j;
			super.i = 0;
			super.h = 0;
		}
	}

	final void a(int var1, int var2, int var3) {
		var1 += super.i;
		var2 += super.h;
		int var4 = var1 + var2 * cd.width;
		int var5 = 0;
		int var6 = super.g;
		int var7 = super.e;
		int var8 = cd.width - var7;
		int var9 = 0;
		int var10;
		if (var2 < cd.g) {
			var10 = cd.g - var2;
			var6 -= var10;
			var2 = cd.g;
			var5 += var10 * var7;
			var4 += var10 * cd.width;
		}

		if (var2 + var6 > cd.e) {
			var6 -= var2 + var6 - cd.e;
		}

		if (var1 < cd.l) {
			var10 = cd.l - var1;
			var7 -= var10;
			var1 = cd.l;
			var5 += var10;
			var4 += var10;
			var9 += var10;
			var8 += var10;
		}

		if (var1 + var7 > cd.j) {
			var10 = var1 + var7 - cd.j;
			var7 -= var10;
			var9 += var10;
			var8 += var10;
		}

		if (var7 > 0 && var6 > 0) {
			a(cd.pixels, p, o, var5, var4, var7, var6, var8, var9, var3);
		}
	}

	g(int var1, int var2, int var3) {
		super.a = super.e = var1;
		super.j = super.g = var2;
		super.i = super.h = 0;
		p = new byte[var1 * var2];
		o = new int[var3];
	}
}
