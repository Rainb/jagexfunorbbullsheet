import java.nio.ByteBuffer;

final class FunDirectByteBuffer extends AbstractByteBuffer {

	private ByteBuffer byteBuffer;


	final void set(int var1, byte[] buf) {
		byteBuffer = ByteBuffer.allocateDirect(buf.length);
		byteBuffer.position(0);
		byteBuffer.put(buf);
		if (var1 != 111) {
			set(109, null);
		}
	}

	final byte[] get(int var1) {
		byte[] bytes = new byte[byteBuffer.capacity()];
		byteBuffer.position(0);
		byteBuffer.get(bytes);
		if (var1 >= -38) {
			get(125);
		}
		return bytes;
	}

}
