import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

final class AppletMouseListener implements MouseListener, MouseMotionListener, FocusListener {

   static long lastTime;
   static nf b;
   static String c;


   static void a(boolean var0) {
      try {
         an.a[7] = rl.eightBit;
         an.a[1] = mg.animals;
         an.a[2] = jf.breakfast;
         an.a[5] = ByteBuffer.underTheSea;
         an.a[0] = wj.d;
         an.a[3] = ca.x;
         an.a[4] = wc.c;
         an.a[6] = lc.c;
         FunTB.Hb[FunTB.Hb.length - 2] = na.n;
         if(var0) {
            c = null;
         }

         FunTB.Hb[FunTB.Hb.length - 1] = jc.e;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "AppletMouseListener.C(" + var0 + ')');
      }
   }

   public final synchronized void mouseDragged(MouseEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            vm.N = 0;
            pj.mouseX = var1.getX();
            pc.mouseY = var1.getY();
            bm.n = true;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mouseDragged(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final void mouseClicked(MouseEvent even) {
      try {
         if(even.isPopupTrigger()) {
            even.consume();
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mouseClicked(" + (even != null?"{...}":"null") + ')');
      }
   }

   public static void a(int var0) {
      try {
         b = null;
         if(var0 == 243) {
            c = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "AppletMouseListener.B(" + var0 + ')');
      }
   }

   public final void focusGained(FocusEvent var1) {}

   public final synchronized void mouseMoved(MouseEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            vm.N = 0;
            pj.mouseX = var1.getX();
            pc.mouseY = var1.getY();
            bm.n = true;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mouseMoved(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void mouseReleased(MouseEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            vm.N = 0;
            fn.g = 0;
            bm.n = true;
            int var2 = var1.getModifiers();
         }

         if(var1.isPopupTrigger()) {
            var1.consume();
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mouseReleased(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void mouseEntered(MouseEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            vm.N = 0;
            pj.mouseX = var1.getX();
            pc.mouseY = var1.getY();
            bm.n = true;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mouseEntered(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void focusLost(FocusEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            fn.g = 0;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.focusLost(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void mouseExited(MouseEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            vm.N = 0;
            pj.mouseX = -1;
            pc.mouseY = -1;
            bm.n = true;
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mouseExited(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   public final synchronized void mousePressed(MouseEvent var1) {
      try {
         if(nh.appletMouseListener != null) {
            label28: {
               vm.N = 0;
               FunPB.F = var1.getX();
               aj.Bb = var1.getY();
               if(!var1.isMetaDown()) {
                  tl.p = 1;
                  fn.g = 1;
                  if(client.L == 0) {
                     break label28;
                  }
               }

               tl.p = 2;
               fn.g = 2;
            }

            int var2 = var1.getModifiers();
            bm.n = true;
         }

         if(var1.isPopupTrigger()) {
            var1.consume();
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "AppletMouseListener.mousePressed(" + (var1 != null?"{...}":"null") + ')');
      }
   }

   static void b(int var0) {
      int var4 = client.L;

      try {
         cd.a(243, 197, 369, 143, 16777215);
         cd.d(244, 198, 367, 141, 0);
         if(var0 != 29047) {
            c = null;
         }

         if(ge.f == null) {
            if(dn.f != null) {
               dn.f.a(gk.P, 245, 199, 365, 139, 16777215, -1, 1, 1, 0);
            }

         } else {
            FunImage var1;
            label70: {
               var1 = ge.f[uf.db];
               if(var1 != null) {
                  var1.c(245, 199);
                  if(var4 == 0) {
                     break label70;
                  }
               }

               if(dn.f != null) {
                  dn.f.a(gk.P, 245, 199, 365, 139, 16777215, -1, 1, 1, 0);
               }
            }

            int var3;
            if(hm.b < sn.g) {
               var1 = ge.f[hf.h];
               if(var1 != null) {
                  label63: {
                     int var2 = (var1.originalWidth + 60) * hm.b / sn.g;
                     var3 = var2 + -30;
                     if(!el.o) {
                        FunO.a(245, (byte) 127, var1, -256 * var3 / 30, 199, 256 * (var1.originalWidth + -var3) / 30);
                        if(var4 == 0) {
                           break label63;
                        }
                     }

                     FunO.a(245, (byte) 126, var1, 256 * (-var3 + var1.originalWidth) / 30, 199, -256 * var3 / 30);
                  }
               }
            }

            if(dn.f != null) {
               String var6 = null;

               for(var3 = 0; ~ge.f.length < ~var3; ++var3) {
                  var1 = ge.f[var3];
                  if(var1 != null && (~var1.originalWidth != -366 || var1.originalHeight != 139)) {
                     if(var6 == null) {
                        var6 = Integer.toString(var3);
                        if(var4 == 0) {
                           continue;
                        }
                     }

                     var6 = var6 + ", " + var3;
                  }
               }

               if(var6 != null) {
                  dn.f.a("Screenshot(s) " + var6 + " is/are the wrong size! Should be " + 365 + "<times>" + 139, 245, 199, 365, 139, 16737843, 0, 1, 1, 0);
                  return;
               }
            }

         }
      } catch (RuntimeException var5) {
         throw pl.a(var5, "AppletMouseListener.A(" + var0 + ')');
      }
   }
}
