
final class sa {

   private int a;
   private int b;
   private ob[] c = new ob[10];


   final hg a() {
      byte[] var1 = this.b();
      return new hg(22050, var1, 22050 * this.a / 1000, 22050 * this.b / 1000);
   }

   static final sa a(nf var0, int var1, int var2) {
      byte[] var3 = var0.c(var1, var2, -104);
      return var3 == null?null:new sa(new ByteBuffer(var3));
   }

   private final byte[] b() {
      int var1 = 0;

      for(int var2 = 0; var2 < 10; ++var2) {
         if(this.c[var2] != null && this.c[var2].n + this.c[var2].m > var1) {
            var1 = this.c[var2].n + this.c[var2].m;
         }
      }

      if(var1 == 0) {
         return new byte[0];
      } else {
         int var3 = 22050 * var1 / 1000;
         byte[] var4 = new byte[var3];

         for(int var5 = 0; var5 < 10; ++var5) {
            if(this.c[var5] != null) {
               int var6 = this.c[var5].n * 22050 / 1000;
               int var7 = this.c[var5].m * 22050 / 1000;
               int[] var8 = this.c[var5].a(var6, this.c[var5].n);

               for(int var9 = 0; var9 < var6; ++var9) {
                  int var10 = var4[var9 + var7] + (var8[var9] >> 8);
                  if((var10 + 128 & -256) != 0) {
                     var10 = var10 >> 31 ^ 127;
                  }

                  var4[var9 + var7] = (byte)var10;
               }
            }
         }

         return var4;
      }
   }

   private sa(ByteBuffer var1) {
      for(int var2 = 0; var2 < 10; ++var2) {
         int var3 = var1.getInt(false);
         if(var3 != 0) {
            --var1.offset;
            this.c[var2] = new ob();
            this.c[var2].a(var1);
         }
      }

      this.a = var1.j(1808469224);
      this.b = var1.j(1808469224);
   }

   private sa() {}
}
