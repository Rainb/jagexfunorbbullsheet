import java.io.File;
import java.io.RandomAccessFile;
import java.util.Hashtable;

public class rc {

   private static Hashtable a = new Hashtable(16);
   private static boolean b = false;
   private static String userHome;
   private static String d;
   private static int e;


   public static File a(boolean var0, String var1) {
	   if(!var0) {
		  a(true, null);
	   }
	   return a(d, e, var1, 74);
   }

   public static void a(int var0, String var1, byte var2) {
	   d = var1;
	   e = var0;
	   try {
		  userHome = System.getProperty("user.home");
		  if(userHome != null) {
			 userHome = userHome + "/";
		  }

		  if(var2 != -8) {
			 userHome = null;
		  }
	   } catch (Exception ignored) {
	   }
	   b = true;
	   if(userHome == null) {
		  userHome = "~/";
	   }
   }

   public static File a(String filename, int var1, String var2, int var3) {
	   if(!b) {
		  throw new RuntimeException("");
	   } else {
		  File f1 = (File)a.get(var2);
		  if(f1 != null) {
			 return f1;
		  } else {
			 int var7 = -123 % ((-34 - var3) / 46);
			 String[] paths = new String[]{"c:/rscache/", "/rscache/", "c:/windows/", "c:/winnt/", "c:/", userHome, "/tmp/", ""};
			 String[] var6 = new String[]{".jagex_cache_" + var1, ".file_store_" + var1};

			 for(int var8 = 0; var8 < 2; ++var8) {
				for(int var9 = 0; ~var6.length < ~var9; ++var9) {
					for (String path : paths) {
						String f = path + var6[var9] + "/" + (filename != null ? filename + "/" : "") + var2;
						RandomAccessFile accessFile = null;

						try {
							File file = new File(f);
							if (~var8 != -1 || file.exists()) {
								if (~var8 != -2 || path.length() <= 0 || new File(path).exists()) {
									new File(path + var6[var9]).mkdir();
									if (filename != null) {
										new File(path + var6[var9] + "/" + filename).mkdir();
									}

									accessFile = new RandomAccessFile(file, "rw");
									int r = accessFile.read();
									accessFile.seek(0L);
									accessFile.write(r);
									accessFile.seek(0L);
									accessFile.close();
									a.put(var2, file);
									return file;
								}
							}
						} catch (Exception var17) {
							try {
								if (accessFile != null) {
									accessFile.close();
									accessFile = null;
								}
							} catch (Exception ignored) {
							}
						}
					}
				}
			 }

			 throw new RuntimeException();
		  }
	   }
   }

}
