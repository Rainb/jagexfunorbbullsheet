final class cd {

	static int width;
	static int height;
	static int[] c;
	static int[] d;
	static int e = 0;
	private static int[] f;
	static int g = 0;
	private static int[] h;
	private static int[] i;
	static int j = 0;
	static int[] pixels;
	static int l = 0;


	static void a(int[] var0) {
		l = var0[0];
		g = var0[1];
		j = var0[2];
		e = var0[3];
		d();
	}

	static void a(int[] pixels, int width, int height) {
		cd.pixels = pixels;
		cd.width = width;
		cd.height = height;
		b(0, 0, width, height);
	}

	private static void a(int[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
		int var8 = 16384 / (2 * var3 + 1);
		int var9 = 1 + var3 - var5 - var4;
		if (var9 > 0) {
			var9 = 0;
		}

		int var10 = width - var4 - var5 - var3;
		if (var10 > 0) {
			var10 = 0;
		}

		int var11 = 0;
		int var12 = var4 + var3 + 1;
		if (width < var12) {
			var11 = var12 - width;
			var12 = width;
		}

		for (int var13 = -var7; var13 < 0; ++var13) {
			int var14 = 0;
			int var15 = 0;
			int var16 = 0;
			int var17 = var2 - var3;
			int var18 = var17 - (var3 << 1) - 1;
			int var19 = var4 - var3;
			if (var19 < 0) {
				var17 -= var19;
				var18 -= var19;
				var19 = 0;
			}

			int var20;
			for (var20 = var12 - var19; var19 < var12; ++var19) {
				var1 = var0[var17];
				var14 += var1 >> 16 & 255;
				var15 += var1 >> 8 & 255;
				var16 += var1 & 255;
				++var17;
				++var18;
			}

			var18 += var11;
			var0[var2++] = (var14 / var20 << 16) + (var15 / var20 << 8) + var16 / var20;

			int var21;
			int var23;
			int var22;
			for (var19 = 1 - var5; var19 < var9; ++var19) {
				++var18;
				if (var4 + var5 + var19 + var3 < j) {
					var1 = var0[var17];
					++var17;
					var14 += var1 >> 16 & 255;
					var15 += var1 >> 8 & 255;
					var16 += var1 & 255;
					++var20;
				}

				var21 = var14 / var20;
				var22 = var15 / var20;
				var23 = var16 / var20;
				var0[var2++] = (var21 << 16) + (var22 << 8) + var23;
			}

			while (var19 < var10) {
				var1 = var0[var18++];
				var14 -= var1 >> 16 & 255;
				if (var14 < 0) {
					var14 = 0;
				}

				var15 -= var1 >> 8 & 255;
				if (var15 < 0) {
					var15 = 0;
				}

				var16 -= var1 & 255;
				if (var16 < 0) {
					var16 = 0;
				}

				var1 = var0[var17];
				++var17;
				var14 += var1 >> 16 & 255;
				var15 += var1 >> 8 & 255;
				var16 += var1 & 255;
				var21 = var14 * var8 >> 14;
				var22 = var15 * var8 >> 14;
				var23 = var16 * var8 >> 14;
				if (var21 > 255) {
					var21 = 255;
				}

				if (var22 > 255) {
					var22 = 255;
				}

				if (var23 > 255) {
					var23 = 255;
				}

				var0[var2++] = (var21 << 16) + (var22 << 8) + var23;
				++var19;
			}

			while (var19 < 0) {
				var1 = var0[var18++];
				var14 -= var1 >> 16 & 255;
				var15 -= var1 >> 8 & 255;
				var16 -= var1 & 255;
				--var20;
				var21 = var14 / var20;
				var22 = var15 / var20;
				var23 = var16 / var20;
				if (var21 < 0) {
					var21 = 0;
				} else if (var21 > 255) {
					var21 = 255;
				}

				if (var22 < 0) {
					var22 = 0;
				} else if (var22 > 255) {
					var22 = 255;
				}

				if (var23 < 0) {
					var23 = 0;
				} else if (var23 > 255) {
					var23 = 255;
				}

				var0[var2++] = (var21 << 16) + (var22 << 8) + var23;
				++var19;
			}

			var2 += var6;
		}

	}

	static void a() {
		int var0 = 0;

		int var1;
		for (var1 = width * height - 7; var0 < var1; pixels[var0++] = 0) {
			pixels[var0++] = 0;
			pixels[var0++] = 0;
			pixels[var0++] = 0;
			pixels[var0++] = 0;
			pixels[var0++] = 0;
			pixels[var0++] = 0;
			pixels[var0++] = 0;
		}

		for (var1 += 7; var0 < var1; pixels[var0++] = 0) {
			;
		}

	}

	static void a(int var0, int var1, int var2, int var3, int var4) {
		d(var0, var1, var2, var4);
		d(var0, var1 + var3 - 1, var2, var4);
		c(var0, var1, var3, var4);
		c(var0 + var2 - 1, var1, var3, var4);
	}

	static void a(int var0, int var1, int var2, int var3, int var4, int var5) {
		a(pixels, 0, var2 + var3 * width, var0, var2, var4, width - var4, var5);
		a(pixels, 0, var2 + var3 * width, var1, var3, var5, width - var4, var2, var4);
	}

	static void a(int var0, int var1, int var2, int var3, int var4, int var5, int var6) {
		if (var6 == 256) {
			d(var0, var1, var2, var3, var4, var5);
		} else if (var4 == 0) {
			b(var0, var1, var2, var3, var5, var6);
		} else {
			int var7 = 256 - var6;
			var5 = ((var5 & 16711935) * var6 >> 8 & 16711935) + ((var5 & '\uff00') * var6 >> 8 & '\uff00');
			if (var4 < 0) {
				var4 = -var4;
			}

			int var8 = var0 + var4;
			int var9 = var1 + var4;
			int var10 = var1;
			if (var1 < g) {
				var10 = g;
			}

			int var11 = var1 + var3;
			if (var11 > e) {
				var11 = e;
			}

			int var12 = var2 - var4 - var4 - 1;
			int var13 = var10;
			int var14 = var4 * var4;
			int var15 = 0;
			int var16 = var9 - var10;
			int var17 = var16 * var16;
			int var18 = var17 - var16;
			if (var9 > var11) {
				var9 = var11;
			}

			int var19;
			int var21;
			int var20;
			int var23;
			int var22;
			while (var13 < var9) {
				while (var18 <= var14 || var17 <= var14) {
					var17 += var15 + var15;
					var18 += var15++ + var15;
				}

				var19 = var8 - var15 + 1;
				if (var19 < l) {
					var19 = l;
				}

				var20 = var8 + var12 + var15;
				if (var20 > j) {
					var20 = j;
				}

				var21 = var19 + var13 * width;

				for (var22 = var19; var22 < var20; ++var22) {
					var23 = pixels[var21];
					var23 = ((var23 & 16711935) * var7 >> 8 & 16711935) + ((var23 & '\uff00') * var7 >> 8 & '\uff00');
					pixels[var21++] = var5 + var23;
				}

				++var13;
				var17 -= var16-- + var16;
				var18 -= var16 + var16;
			}

			var19 = var0;
			if (var0 < l) {
				var19 = l;
			}

			var20 = var0 + var2;
			if (var20 > j) {
				var20 = j;
			}

			var21 = var19 + var13 * width;
			var22 = width + var19 - var20;
			var23 = var1 + var3 - var4 - 1;
			if (var23 > e) {
				var23 = e;
			}

			int var25;
			int var24;
			while (var13 < var23) {
				for (var24 = var19; var24 < var20; ++var24) {
					var25 = pixels[var21];
					var25 = ((var25 & 16711935) * var7 >> 8 & 16711935) + ((var25 & '\uff00') * var7 >> 8 & '\uff00');
					pixels[var21++] = var5 + var25;
				}

				++var13;
				var21 += var22;
			}

			var16 = 0;
			var15 = var4;
			var18 = var16 * var16 + var14;
			var17 = var18 - var4;

			for (var18 -= var16; var13 < var11; var17 += var16++ + var16) {
				while (var18 > var14 && var17 > var14) {
					var18 -= var15-- + var15;
					var17 -= var15 + var15;
				}

				var24 = var8 - var15;
				if (var24 < l) {
					var24 = l;
				}

				var25 = var8 + var12 + var15;
				if (var25 > j - 1) {
					var25 = j - 1;
				}

				int var26 = var24 + var13 * width;

				for (int var27 = var24; var27 <= var25; ++var27) {
					int var28 = pixels[var26];
					var28 = ((var28 & 16711935) * var7 >> 8 & 16711935) + ((var28 & '\uff00') * var7 >> 8 & '\uff00');
					pixels[var26++] = var5 + var28;
				}

				++var13;
				var18 += var16 + var16;
			}

		}
	}

	static void a(int var0, int var1, int var2, int var3) {
		if (l < var0) {
			l = var0;
		}

		if (g < var1) {
			g = var1;
		}

		if (j > var2) {
			j = var2;
		}

		if (e > var3) {
			e = var3;
		}

		d();
	}

	static void b(int var0, int var1, int var2, int var3, int var4, int var5) {
		if (var0 < l) {
			var2 -= l - var0;
			var0 = l;
		}

		if (var1 < g) {
			var3 -= g - var1;
			var1 = g;
		}

		if (var0 + var2 > j) {
			var2 = j - var0;
		}

		if (var1 + var3 > e) {
			var3 = e - var1;
		}

		var4 = ((var4 & 16711935) * var5 >> 8 & 16711935) + ((var4 & '\uff00') * var5 >> 8 & '\uff00');
		int var6 = 256 - var5;
		int var7 = width - var2;
		int var8 = var0 + var1 * width;

		for (int var9 = 0; var9 < var3; ++var9) {
			for (int var10 = -var2; var10 < 0; ++var10) {
				int var11 = pixels[var8];
				var11 = ((var11 & 16711935) * var6 >> 8 & 16711935) + ((var11 & '\uff00') * var6 >> 8 & '\uff00');
				pixels[var8++] = var4 + var11;
			}

			var8 += var7;
		}

	}

	public static void dispose() {
		pixels = null;
		d = null;
		c = null;
		f = null;
		i = null;
		h = null;
	}

	static void b(int x, int y, int width, int height) {
		if (x < 0) {
			x = 0;
		}

		if (y < 0) {
			y = 0;
		}

		if (width > cd.width) {
			width = cd.width;
		}

		if (height > cd.height) {
			height = cd.height;
		}

		l = x;
		g = y;
		j = width;
		e = height;
		d();
	}

	static void c(int var0, int var1, int var2, int var3, int var4, int var5) {
		int var6 = 0;
		int var7 = 65536 / var3;
		if (var0 < l) {
			var2 -= l - var0;
			var0 = l;
		}

		if (var1 < g) {
			var6 += (g - var1) * var7;
			var3 -= g - var1;
			var1 = g;
		}

		if (var0 + var2 > j) {
			var2 = j - var0;
		}

		if (var1 + var3 > e) {
			var3 = e - var1;
		}

		int var8 = width - var2;
		int var9 = var0 + var1 * width;

		for (int var10 = -var3; var10 < 0; ++var10) {
			int var11 = 65536 - var6 >> 8;
			int var12 = var6 >> 8;
			int var13 = ((var4 & 16711935) * var11 + (var5 & 16711935) * var12 & -16711936) + ((var4 & '\uff00') * var11 + (var5 & '\uff00') * var12 & 16711680) >>> 8;

			for (int var14 = -var2; var14 < 0; ++var14) {
				pixels[var9++] = var13;
			}

			var9 += var8;
			var6 += var7;
		}

	}

	static void c(int var0, int var1, int var2, int var3) {
		if (var0 >= l && var0 < j) {
			if (var1 < g) {
				var2 -= g - var1;
				var1 = g;
			}

			if (var1 + var2 > e) {
				var2 = e - var1;
			}

			int var4 = var0 + var1 * width;

			for (int var5 = 0; var5 < var2; var4 += width) {
				pixels[var4] = var3;
				++var5;
			}

		}
	}

	static void d(int var0, int var1, int var2, int var3, int var4, int var5) {
		if (var4 == 0) {
			d(var0, var1, var2, var3, var5);
		} else {
			if (var4 < 0) {
				var4 = -var4;
			}

			int var6 = var0 + var4;
			int var7 = var1 + var4;
			int var8 = var1;
			if (var1 < g) {
				var8 = g;
			}

			int var9 = var1 + var3;
			if (var9 > e) {
				var9 = e;
			}

			int var10 = var2 - var4 - var4 - 1;
			int var11 = var8;
			int var12 = var4 * var4;
			int var13 = 0;
			int var14 = var7 - var8;
			int var15 = var14 * var14;
			int var16 = var15 - var14;
			if (var7 > var9) {
				var7 = var9;
			}

			int var17;
			int var19;
			int var18;
			int var20;
			while (var11 < var7) {
				while (var16 <= var12 || var15 <= var12) {
					var15 += var13 + var13;
					var16 += var13++ + var13;
				}

				var17 = var6 - var13 + 1;
				if (var17 < l) {
					var17 = l;
				}

				var18 = var6 + var10 + var13;
				if (var18 > j) {
					var18 = j;
				}

				var19 = var17 + var11 * width;

				for (var20 = var17; var20 < var18; ++var20) {
					pixels[var19++] = var5;
				}

				++var11;
				var15 -= var14-- + var14;
				var16 -= var14 + var14;
			}

			var17 = var0;
			if (var0 < l) {
				var17 = l;
			}

			var18 = var0 + var2;
			if (var18 > j) {
				var18 = j;
			}

			var19 = var17 + var11 * width;
			var20 = width + var17 - var18;
			int var21 = var1 + var3 - var4 - 1;
			if (var21 > e) {
				var21 = e;
			}

			int var22;
			while (var11 < var21) {
				for (var22 = var17; var22 < var18; ++var22) {
					pixels[var19++] = var5;
				}

				++var11;
				var19 += var20;
			}

			var14 = 0;
			var13 = var4;
			var16 = var14 * var14 + var12;
			var15 = var16 - var4;

			for (var16 -= var14; var11 < var9; var15 += var14++ + var14) {
				while (var16 > var12 && var15 > var12) {
					var16 -= var13-- + var13;
					var15 -= var13 + var13;
				}

				var22 = var6 - var13;
				if (var22 < l) {
					var22 = l;
				}

				int var23 = var6 + var10 + var13;
				if (var23 > j - 1) {
					var23 = j - 1;
				}

				int var24 = var22 + var11 * width;

				for (int var25 = var22; var25 <= var23; ++var25) {
					pixels[var24++] = var5;
				}

				++var11;
				var16 += var14 + var14;
			}

		}
	}

	static void b(int var0, int var1, int var2, int var3, int var4) {
		if (var1 >= g && var1 < e) {
			if (var0 < l) {
				var2 -= l - var0;
				var0 = l;
			}

			if (var0 + var2 > j) {
				var2 = j - var0;
			}

			int var5 = 256 - var4;
			int var6 = (var3 >> 16 & 255) * var4;
			int var7 = (var3 >> 8 & 255) * var4;
			int var8 = (var3 & 255) * var4;
			int var12 = var0 + var1 * width;

			for (int var13 = 0; var13 < var2; ++var13) {
				int var9 = (pixels[var12] >> 16 & 255) * var5;
				int var10 = (pixels[var12] >> 8 & 255) * var5;
				int var11 = (pixels[var12] & 255) * var5;
				int var14 = (var6 + var9 >> 8 << 16) + (var7 + var10 >> 8 << 8) + (var8 + var11 >> 8);
				pixels[var12++] = var14;
			}

		}
	}

	private static void a(int var0, int[] var1, int var2, int var3, int var4, int var5) {
		while (var4 < 0) {
			for (var2 = var0 + var3 - 7; var0 < var2; ++var0) {
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
				++var0;
				var1[var0] = (var1[var0] & 16711422) >> 1;
			}

			for (var2 += 7; var0 < var2; ++var0) {
				var1[var0] = (var1[var0] & 16711422) >> 1;
			}

			var0 += var5;
			++var4;
		}

	}

	private static void c(int var0, int var1, int var2, int var3, int var4) {
		if (var0 >= l && var0 < j) {
			if (var1 < g) {
				var2 -= g - var1;
				var1 = g;
			}

			if (var1 + var2 > e) {
				var2 = e - var1;
			}

			int var5 = 256 - var4;
			int var6 = (var3 >> 16 & 255) * var4;
			int var7 = (var3 >> 8 & 255) * var4;
			int var8 = (var3 & 255) * var4;
			int var12 = var0 + var1 * width;

			for (int var13 = 0; var13 < var2; ++var13) {
				int var9 = (pixels[var12] >> 16 & 255) * var5;
				int var10 = (pixels[var12] >> 8 & 255) * var5;
				int var11 = (pixels[var12] & 255) * var5;
				int var14 = (var6 + var9 >> 8 << 16) + (var7 + var10 >> 8 << 8) + (var8 + var11 >> 8);
				pixels[var12] = var14;
				var12 += width;
			}

		}
	}

	static void d(int var0, int var1, int var2, int var3) {
		if (var1 >= g && var1 < e) {
			if (var0 < l) {
				var2 -= l - var0;
				var0 = l;
			}

			if (var0 + var2 > j) {
				var2 = j - var0;
			}

			int var4 = var0 + var1 * width;

			for (int var5 = 0; var5 < var2; ++var5) {
				pixels[var4 + var5] = var3;
			}

		}
	}

	static void d(int var0, int var1, int var2, int var3, int var4) {
		if (var0 < l) {
			var2 -= l - var0;
			var0 = l;
		}

		if (var1 < g) {
			var3 -= g - var1;
			var1 = g;
		}

		if (var0 + var2 > j) {
			var2 = j - var0;
		}

		if (var1 + var3 > e) {
			var3 = e - var1;
		}

		int var5 = width - var2;
		int var6 = var0 + var1 * width;

		for (int var7 = -var3; var7 < 0; ++var7) {
			for (int var8 = -var2; var8 < 0; ++var8) {
				pixels[var6++] = var4;
			}

			var6 += var5;
		}

	}

	private static void a(int[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
		if (f == null || f.length < var8) {
			f = new int[var8];
			i = new int[var8];
			h = new int[var8];
		}

		int[] var9 = f;
		int[] var10 = i;
		int[] var11 = h;
		rd.a(var9, 0, var8);
		rd.a(var10, 0, var8);
		rd.a(var11, 0, var8);
		int var12 = 16384 / (2 * var3 + 1);
		int var13 = var4 - var3;
		if (var13 < 0) {
			var13 = 0;
		}

		int var14 = var7 + var13 * width;
		int var15 = var4 + var3;
		int var16 = 0;
		if (var15 >= height) {
			var16 = var15 - height + 1;
			var15 = height - 1;
		}

		int var17;
		int var18;
		for (var17 = var15 - var13 + 1; var13 <= var15; ++var13) {
			for (var18 = 0; var18 < var8; ++var18) {
				var1 = var0[var14++];
				var9[var18] += var1 >> 16 & 255;
				var10[var18] += var1 >> 8 & 255;
				var11[var18] += var1 & 255;
			}

			var14 += var6;
		}

		var14 += var16 * width;

		for (var18 = 0; var18 < var8; ++var18) {
			var0[var2++] = (var9[var18] / var17 << 16) + (var10[var18] / var17 << 8) + var11[var18] / var17;
		}

		var2 += var6;
		var13 = 1 - var5;
		int var19 = 1 + var3 - var5 - var4;
		if (var19 > 0) {
			var19 = 0;
		}

		int var20 = var7 + (var4 - var3) * width;
		if (var13 < var19) {
			var20 += (var19 - var13) * width;
		}

		int var21;
		int var23;
		int var22;
		int var24;
		while (var13 < var19) {
			if (var13 + var4 + var5 + var3 < e) {
				for (var21 = 0; var21 < var8; ++var21) {
					var1 = var0[var14++];
					var9[var21] += var1 >> 16 & 255;
					var10[var21] += var1 >> 8 & 255;
					var11[var21] += var1 & 255;
				}

				var14 += var6;
				++var17;
			} else {
				var14 += width;
			}

			for (var21 = 0; var21 < var8; ++var21) {
				var22 = var9[var21] / var17;
				var23 = var10[var21] / var17;
				var24 = var11[var21] / var17;
				var0[var2++] = (var22 << 16) + (var23 << 8) + var24;
			}

			var2 += var6;
			++var13;
		}

		var19 = height - var4 - var5 - var3;
		if (var19 > 0) {
			var19 = 0;
		}

		int var25;
		while (var13 < var19) {
			for (var21 = 0; var21 < var8; ++var21) {
				var1 = var0[var20++];
				var22 = var9[var21] - (var1 >> 16 & 255);
				var9[var21] = var22 < 0 ? 0 : var22;
				var22 = var10[var21] - (var1 >> 8 & 255);
				var10[var21] = var22 < 0 ? 0 : var22;
				var22 = var11[var21] - (var1 & 255);
				var11[var21] = var22 < 0 ? 0 : var22;
			}

			var20 += var6;

			for (var22 = 0; var22 < var8; ++var22) {
				var1 = var0[var14++];
				var9[var22] += var1 >> 16 & 255;
				var10[var22] += var1 >> 8 & 255;
				var11[var22] += var1 & 255;
			}

			var14 += var6;

			for (var23 = 0; var23 < var8; ++var23) {
				var24 = var9[var23] * var12 >> 14;
				var25 = var10[var23] * var12 >> 14;
				int var26 = var11[var23] * var12 >> 14;
				if (var24 > 255) {
					var24 = 255;
				}

				if (var25 > 255) {
					var25 = 255;
				}

				if (var26 > 255) {
					var26 = 255;
				}

				var0[var2++] = (var24 << 16) + (var25 << 8) + var26;
			}

			var2 += var6;
			++var13;
		}

		while (var13 < 0) {
			for (var21 = 0; var21 < var8; ++var21) {
				var1 = var0[var20++];
				var9[var21] -= var1 >> 16 & 255;
				var10[var21] -= var1 >> 8 & 255;
				var11[var21] -= var1 & 255;
			}

			var20 += var6;
			--var17;

			for (var22 = 0; var22 < var8; ++var22) {
				var23 = var9[var22] / var17;
				var24 = var10[var22] / var17;
				var25 = var11[var22] / var17;
				if (var23 < 0) {
					var23 = 0;
				} else if (var23 > 255) {
					var23 = 255;
				}

				if (var24 < 0) {
					var24 = 0;
				} else if (var24 > 255) {
					var24 = 255;
				}

				if (var25 < 0) {
					var25 = 0;
				} else if (var25 > 255) {
					var25 = 255;
				}

				var0[var2++] = (var23 << 16) + (var24 << 8) + var25;
			}

			var2 += var6;
			++var13;
		}

	}

	static void b(int[] var0) {
		var0[0] = l;
		var0[1] = g;
		var0[2] = j;
		var0[3] = e;
	}

	static void c() {
		l = 0;
		g = 0;
		j = width;
		e = height;
		d();
	}

	static void e(int var0, int var1, int var2, int var3, int var4) {
		var2 -= var0;
		var3 -= var1;
		if (var3 == 0) {
			if (var2 >= 0) {
				d(var0, var1, var2 + 1, var4);
			} else {
				d(var0 + var2, var1, -var2 + 1, var4);
			}
		} else if (var2 == 0) {
			if (var3 >= 0) {
				c(var0, var1, var3 + 1, var4);
			} else {
				c(var0, var1 + var3, -var3 + 1, var4);
			}
		} else {
			if (var2 + var3 < 0) {
				var0 += var2;
				var2 = -var2;
				var1 += var3;
				var3 = -var3;
			}

			int var5;
			int var6;
			if (var2 > var3) {
				var1 <<= 16;
				var1 += '\u8000';
				var3 <<= 16;
				var5 = (int) Math.floor((double) var3 / (double) var2 + 0.5D);
				var2 += var0;
				if (var0 < l) {
					var1 += var5 * (l - var0);
					var0 = l;
				}

				if (var2 >= j) {
					var2 = j - 1;
				}

				while (var0 <= var2) {
					var6 = var1 >> 16;
					if (var6 >= g && var6 < e) {
						pixels[var0 + var6 * width] = var4;
					}

					var1 += var5;
					++var0;
				}

			} else {
				var0 <<= 16;
				var0 += '\u8000';
				var2 <<= 16;
				var5 = (int) Math.floor((double) var2 / (double) var3 + 0.5D);
				var3 += var1;
				if (var1 < g) {
					var0 += var5 * (g - var1);
					var1 = g;
				}

				if (var3 >= e) {
					var3 = e - 1;
				}

				while (var1 <= var3) {
					var6 = var0 >> 16;
					if (var6 >= l && var6 < j) {
						pixels[var6 + var1 * width] = var4;
					}

					var0 += var5;
					++var1;
				}

			}
		}
	}

	private static void d() {
		d = null;
		c = null;
	}

	static void e(int var0, int var1, int var2, int var3) {
		if (var2 == 0) {
			a(var0, var1, var3);
		} else {
			if (var2 < 0) {
				var2 = -var2;
			}

			int var4 = var1 - var2;
			if (var4 < g) {
				var4 = g;
			}

			int var5 = var1 + var2 + 1;
			if (var5 > e) {
				var5 = e;
			}

			int var6 = var4;
			int var7 = var2 * var2;
			int var8 = 0;
			int var9 = var1 - var4;
			int var10 = var9 * var9;
			int var11 = var10 - var9;
			if (var1 > var5) {
				var1 = var5;
			}

			int var12;
			int var13;
			int var14;
			int var15;
			while (var6 < var1) {
				while (var11 <= var7 || var10 <= var7) {
					var10 += var8 + var8;
					var11 += var8++ + var8;
				}

				var12 = var0 - var8 + 1;
				if (var12 < l) {
					var12 = l;
				}

				var13 = var0 + var8;
				if (var13 > j) {
					var13 = j;
				}

				var14 = var12 + var6 * width;

				for (var15 = var12; var15 < var13; ++var15) {
					pixels[var14++] = var3;
				}

				++var6;
				var10 -= var9-- + var9;
				var11 -= var9 + var9;
			}

			var8 = var2;
			var9 = var6 - var1;
			var11 = var9 * var9 + var7;
			var10 = var11 - var2;

			for (var11 -= var9; var6 < var5; var10 += var9++ + var9) {
				while (var11 > var7 && var10 > var7) {
					var11 -= var8-- + var8;
					var10 -= var8 + var8;
				}

				var12 = var0 - var8;
				if (var12 < l) {
					var12 = l;
				}

				var13 = var0 + var8;
				if (var13 > j - 1) {
					var13 = j - 1;
				}

				var14 = var12 + var6 * width;

				for (var15 = var12; var15 <= var13; ++var15) {
					pixels[var14++] = var3;
				}

				++var6;
				var11 += var9 + var9;
			}

		}
	}

	static void f(int var0, int var1, int var2, int var3) {
		if (var0 < l) {
			var2 -= l - var0;
			var0 = l;
		}

		if (var1 < g) {
			var3 -= g - var1;
			var1 = g;
		}

		if (var0 + var2 > j) {
			var2 = j - var0;
		}

		if (var1 + var3 > e) {
			var3 = e - var1;
		}

		int var4 = width - var2;
		int var5 = var0 + var1 * width;
		a(var5, pixels, 0, var2, -var3, var4);
	}

	static void a(int var0, int var1, int var2) {
		if (var0 >= l && var1 >= g && var0 < j && var1 < e) {
			pixels[var0 + var1 * width] = var2;
		}
	}

	static void g(int var0, int var1, int var2, int var3) {
		if (var0 < l) {
			var2 -= l - var0;
			var0 = l;
		}

		if (var0 + var2 > j) {
			var2 = j - var0;
		}

		if (var1 < g) {
			var3 -= g - var1;
			var1 = g;
		}

		if (var1 + var3 > e) {
			var3 = e - var1;
		}

		int var4 = var0 + var1 * width;
		if (var2 > 0 && var3 > 0) {
			for (int var5 = 0; var5 < var3; ++var5) {
				for (int var6 = 0; var6 < var2; ++var6) {
					int var7 = pixels[var4];
					int var8 = var7 >> 15 & 510;
					int var9 = var7 >> 8 & 255;
					int var10 = var7 & 255;
					int var11 = (var10 + var8) / 3 + var9 >> 1;
					pixels[var4++] = (var11 << 16) + (var11 << 8) + var11;
				}

				var4 += width - var2;
			}

		}
	}

	static void f(int var0, int var1, int var2, int var3, int var4) {
		for (int var6 = 0; var6 < 4; ++var6) {
			int var5 = 128 - (var6 << 5);
			b(var0 + var6, var1 + var3 + var6, var2, var4, var5);
			c(var0 + var2 + var6, var1 + var6, var3 + 1, var4, var5);
		}

	}

}
