import javax.sound.sampled.*;
import javax.sound.sampled.Mixer.Info;

import java.awt.*;

final class FunAudioPlayer extends FunI {

	private byte[] audioBuffer;
	private boolean soundMax = false;
	private int bufferSize;
	private AudioFormat audioFormat;
	private SourceDataLine sourceDataLine;


	final void a(Component component) {
		Info[] mixerInfoList = AudioSystem.getMixerInfo();
		if (mixerInfoList != null) {
			for (Info mixerInfo : mixerInfoList) {
				if (null != mixerInfo) {
					String var6 = mixerInfo.getName();
					if (null != var6 && var6.toLowerCase().contains("soundmax")) {
						soundMax = true;
					}
				}
			}
		}
		audioFormat = new AudioFormat((float) qc.f, 16, bi.twoChannels ? 2 : 1, true, false);
		audioBuffer = new byte[256 << (!bi.twoChannels ? 1 : 2)];
	}

	final void d() throws LineUnavailableException {
		sourceDataLine.flush();
		if (soundMax) {
			sourceDataLine.close();
			sourceDataLine = null;
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat, bufferSize << (bi.twoChannels ? 2 : 1));
			sourceDataLine = (SourceDataLine) AudioSystem.getLine(info);
			sourceDataLine.open();
			sourceDataLine.start();
		}
	}

	final void c(int bufferSize) throws LineUnavailableException {
		try {
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat, bufferSize << (!bi.twoChannels ? 1 : 2));
			sourceDataLine = (SourceDataLine) AudioSystem.getLine(info);
			sourceDataLine.open();
			sourceDataLine.start();
			this.bufferSize = bufferSize;
		} catch (LineUnavailableException var3) {
			if (~ej.a((byte) -7, bufferSize) == -2) {
				sourceDataLine = null;
				throw var3;
			} else {
				c(qa.a((byte) 125, bufferSize));
			}
		}
	}

	final int a() {
		return bufferSize - (sourceDataLine.available() >> (bi.twoChannels ? 2 : 1));
	}

	final void c() {
		int len = 256;
		if (bi.twoChannels) {
			len <<= 1;
		}

		for (int i = 0; i < len; ++i) {
			int val = d[i];
			if ((val + 8388608 & -16777216) != 0) {
				val = 8388607 ^ val >> 31;
			}

			audioBuffer[i * 2] = (byte) (val >> 8);
			audioBuffer[i * 2 + 1] = (byte) (val >> 16);
		}

		sourceDataLine.write(audioBuffer, 0, len << 1);
	}

	final void close() {
		if (sourceDataLine != null) {
			sourceDataLine.close();
			sourceDataLine = null;
		}
	}

}
