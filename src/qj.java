import java.awt.Image;

final class qj {

   static int a;
   static int b = -1;
   static int c;
   static ByteBuffer d;
   static g[] e;
   static String f = "Player";
   static ph g;
   static Image h;
   static String[] i = new String[]{"Match shapes and try to keep your bucket clear.", "Use wildcards to match multiple shapes for big points!", "Keep solid shapes under control by repeatedly popping them.", "Have you noticed the special items? Make two or more shapes at the same time to get them!", "Solid shapes will now come back with a vengeance, getting larger each time. Use special items to keep your bucket under control!", "Danger looms! Try to empty your bucket as much as possible before this next theme ends...", "Special items will no longer save you. Try to hold on and grab as many points as possible!", "Congratulations, this is the last theme! Hint: get the best highscores by choosing and perfecting a different strategy for each theme..."};


   public static void a(int var0) {
      try {
         i = null;
         g = null;
         e = null;
         f = null;
         if(var0 == 76) {
            d = null;
            h = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "qj.B(" + var0 + ')');
      }
   }

   static final nl a(boolean var0, byte var1) {
      try {
         nl var2 = new nl(5);
         int var3 = 372;
         if(ng.L) {
            var3 -= 12;
         }

         rl var4;
         label25: {
            if(nf.a((byte)-72)) {
               var4 = new rl(20, lm.a, re.g);
               var4.d = 320 - var4.f / 2;
               var4.c = var3;
               var2.a(101, var4);
               if(client.L == 0) {
                  break label25;
               }
            }

            var2.a(91, new rl(22, cj.M[0], re.g));
            var2.a(-103, new rl(22, cj.M[1], re.g));
            var2.a(var1 + -3, new rl(22, cj.M[2], re.g));
            var2.u[0].f = var2.u[1].f = var2.u[2].f = 185;
            var2.u[0].d = -(var2.u[0].f / 2) + 120;
            var2.u[1].d = 320 + -(var2.u[1].f / 2);
            var2.u[2].d = 520 - var2.u[2].f / 2;
            var2.u[0].c = var2.u[1].c = var2.u[2].c = var3;
         }

         var3 += 43;
         if(ng.L) {
            var4 = new rl(3, bn.enterMultiplayerLobby, re.g);
            var4.c = var3;
            var4.d = 320 - var4.f / 2;
            var3 += 28;
            var2.a(115, var4);
         }

         var4 = new rl(13, FunQ.u, re.g);
         var4.d = 320 + -(var4.f / 2);
         var4.c = var3;
         var2.a(-85, var4);
         var2.y = 76;
         var2.s = 272;
         var2.f = 500;
         var2.w = 70;
         if(var1 != 94) {
            a(27, (String)null);
         }

         var2.a(var0, km.a, var1 + -49);
         return var2;
      } catch (RuntimeException var5) {
         throw pl.a(var5, "qj.E(" + var0 + ',' + var1 + ')');
      }
   }

   static final void b(boolean var0, byte var1) {
      int var3 = client.L;

      try {
         byte var2;
         label50: {
            if(sh.s > 0) {
               label37: {
                  if(qh.g != null) {
                     mh.e = qh.g.a(16);
                     pm.a(11781092, 2);
                     if(var3 == 0) {
                        break label37;
                     }
                  }

                  mh.e = ok.a(lf.s, 21477, 0, 480, 640, 0);
               }

               if(mh.e != null) {
                  var2 = 2;
                  bf.a(-42, mh.e);
                  if(var3 == 0) {
                     break label50;
                  }
               }

               var2 = 3;
               if(var3 == 0) {
                  break label50;
               }
            }

            if(!nf.a((byte)-54)) {
               var2 = 1;
               if(var3 == 0) {
                  break label50;
               }
            }

            var2 = 0;
         }

         if(var1 <= 60) {
            a = -32;
         }

         if(qh.g == null && dn.b) {
            he.a((byte)-70, var0, var2);
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "qj.C(" + var0 + ',' + var1 + ')');
      }
   }

   static final boolean b(int var0) {
      try {
         int var1 = 27 % ((var0 - -20) / 63);
         return ah.hb < 20 || !fl.b((byte)78) || ~FunO.k < -1 && !pm.d(2053254);
      } catch (RuntimeException var2) {
         throw pl.a(var2, "qj.D(" + var0 + ')');
      }
   }

   static final boolean a(int var0, String var1) {
      int var4 = client.L;

      try {
         int var2 = 0;
         if(var4 == 0 && var2 >= var1.length()) {
            if(var0 != 3) {
               a(10, (String)null);
            }

            return false;
         } else {
            do {
               char var3 = var1.charAt(var2);
               if(!eh.a(var3, -91) && !sm.a((int)108, var3)) {
                  return true;
               }

               ++var2;
            } while(var2 < var1.length());

            if(var0 != 3) {
               a(10, (String)null);
            }

            return false;
         }
      } catch (RuntimeException var5) {
         throw pl.a(var5, "qj.A(" + var0 + ',' + (var1 != null?"{...}":"null") + ')');
      }
   }

}
