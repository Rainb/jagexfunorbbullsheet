import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;

final class om implements Runnable {

	RandomAccessWrapper mainCacheIdx = null;
	private static volatile long b = 0L;
	static String os;
	private FunDisplay d;
	private sd e;
	private ObjectStore f = null;
	private static String userHome;
	private static int h;
	static Method focusCycleRootMethod;
	EventQueue systemEventQueue;
	private Thread k;
	RandomAccessWrapper mainCacheDataFile = null;
	RandomAccessWrapper[] indexList;
	static String javaVendor;
	private boolean o = false;
	private FunCustomCursor p;
	private static String q;
	private ti r;
	private static String osName;
	static String t;
	static Method focusTraversalKeysMethod;
	RandomAccessWrapper randomDat = null;
	boolean w = false;
	private boolean x = false;
	private DirectAudio directAudio;
	private ObjectStore z = null;
	static Class A;
	static Class B;
	static Class C;
	static Class D;


	private ObjectStore a(int id, Object o, int var3, int var4, int var5) {
		ObjectStore objectStore = new ObjectStore();
		objectStore.c = var4;
		objectStore.g = o;
		objectStore.a = var5;
		objectStore.e = id;
		try {
			if (f != null) {
				f.d = objectStore;
				f = objectStore;
				synchronized (this) {
					notify();
				}
			} else {
				f = z = objectStore;
			}


		} catch (Throwable var10) {
			try {
				throw var10;
			} catch (Throwable throwable) {
				throwable.printStackTrace();
			}
		}
		if (var3 != -9) {
			a(1);
		}
		return objectStore;
	}

	final void close(byte var1) {
		try {
			o = true;
			synchronized (this) {
				notifyAll();
			}
		} catch (Throwable var15) {
			try {
				throw var15;
			} catch (Throwable throwable) {
				throwable.printStackTrace();
			}
		}
		if (var1 != 22) {
			osName = null;
		}
		try {
			k.join();
		} catch (InterruptedException ignored) {
		}
		if (mainCacheDataFile != null) {
			try {
				mainCacheDataFile.close(var1 ^ -1262);
			} catch (IOException ignored) {
			}
		}
		if (mainCacheIdx != null) {
			try {
				mainCacheIdx.close(-1276);
			} catch (IOException ignored) {
			}
		}
		if (indexList != null) {
			for (RandomAccessWrapper raw : indexList) {
				if (raw != null) {
					try {
						raw.close(-1276);
					} catch (IOException ignored) {
					}
				}
			}
		}
		if (randomDat != null) {
			try {
				randomDat.close(-1276);
			} catch (IOException ignored) {
			}
		}
	}

	final boolean a(int var1) {
		return w && var1 == 0 && (x ? r != null : d != null);
	}

	final ObjectStore a(int var1, int var2, int var3, int var4, int var5) {
		if (var3 < 29) {
			z = null;
		}
		return a(6, null, -9, (var4 << 16) - -var2, var5 + (var1 << 16));
	}

	final ObjectStore a(int var1, boolean var2, Runnable var3) {
		return var2 ? null : a(2, var3, -9, var1, 0);
	}

	private static RandomAccessWrapper getJagexPreferences(int mode, String suffix, boolean var2, String prefix) {
		String file;
		if (mode != 33) {
			if (~mode == -35) {
				file = "jagex_" + prefix + "_preferences" + suffix + "_wip.dat";
			} else {
				file = "jagex_" + prefix + "_preferences" + suffix + ".dat";
			}
		} else {
			file = "jagex_" + prefix + "_preferences" + suffix + "_rc.dat";
		}
		if (var2) {
			t = null;
		}
		String[] paths = new String[] { "c:/rscache/", "/rscache/", userHome, "c:/windows/", "c:/winnt/", "c:/", "/tmp/", "" };
		for (String path : paths) {
			if (path.length() <= 0 || new File(path).exists()) {
				try {
					return new RandomAccessWrapper(new File(path, file), "rw", 10000L);
				} catch (Exception ignored) {
				}
			}
		}
		return null;
	}

	public final void run() {
		while (true) {


			ObjectStore var1;
			label697:
			{
				try {
					while (!o) {
						if (z != null) {
							var1 = z;
							z = z.d;
							if (z == null) {
								f = null;
							}
							break label697;
						}

						try {
							synchronized (this) {
								wait();
							}
						} catch (InterruptedException ignored) {
						}
					}
				} catch (Throwable ignored) {
				}

				return;
			}

			try {
				int var2 = var1.e;
				if (~var2 != -2) {
					if (~var2 == -23) {
						if (~FunAB.a((byte) 8) > ~b) {
							throw new IOException();
					}

						try {
							var1.object = dk.a((String) var1.g, false, var1.c).getSocket((byte) -75);
						} catch (FunIOException var23) {
							var1.object = var23.getMessage();
							throw var23;
						}
					} else if (var2 == 2) {
						Thread var3 = new Thread((Runnable) var1.g);
						var3.setDaemon(true);
						var3.start();
						var3.setPriority(var1.c);
						var1.object = var3;
					} else if (~var2 == -5) {
						if (b > FunAB.a((byte) 103)) {
							throw new IOException();
						}

						var1.object = new DataInputStream(((URL) var1.g).openStream());
					} else {
						Object[] objects;
						if (~var2 != -9) {
							if (~var2 == -10) {
								objects = (Object[]) var1.g;
								if (w && ((Class) objects[0]).getClassLoader() == null) {
									throw new SecurityException();
								}

								var1.object = ((Class) objects[0]).getDeclaredField((String) objects[1]);
							} else if (var2 != 18) {
								if (var2 != 19) {
									if (!w) {
										throw new Exception("");
									}

									String var31;
									if (~var2 == -4) {
										if (FunAB.a((byte) -128) < b) {
											throw new IOException();
										}

										var31 = (var1.c >> 24 & 255) + "." + (var1.c >> 16 & 255) + "." + ((var1.c & '\uff4a') >> 8) + "." + (var1.c & 255);
										var1.object = InetAddress.getByName(var31).getHostName();
									} else if (var2 == 21) {
										if (FunAB.a((byte) -128) < b) {
											throw new IOException();
										}

										var1.object = InetAddress.getByName((String) var1.g).getAddress();
									} else if (~var2 != -6) {
										if (var2 != 6) {
											if (var2 != 7) {
												RandomAccessWrapper jagexPrefs;
												if (var2 == 12) {
													jagexPrefs = getJagexPreferences(h, (String) var1.g, false, q);
													var1.object = jagexPrefs;
												} else if (var2 != 13) {
													if (w && ~var2 == -15) {
														int var35 = var1.c;
														int var34 = var1.a;
														if (x) {
															e.setCursorLocation(var34, var35, (byte) -112);
														} else {
															p.mouseMove(var35, var34);
														}
													} else if (w && ~var2 == -16) {
														boolean b = ~var1.c != -1;
														Component component = (Component) var1.g;
														if (!x) {
															p.setCursor(component, b);
														} else {
															e.a((byte) 54, component, b);
														}
													} else if (!x && ~var2 == -18) {
														objects = (Object[]) var1.g;
														p.setCustomCursor((Component) objects[0], (int[]) objects[1], var1.c, var1.a, (Point) objects[2]);

													} else {
														if (var2 != 16) {
															throw new Exception("");
														}

														try {
															if (!os.startsWith("win")) {
																throw new Exception();
															}

															var31 = (String) var1.g;
															if (!var31.startsWith("http://") && !var31.startsWith("https://")) {
																throw new Exception();
															}

															String var4 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?&=,.%+-_#:/*";

															for (int var5 = 0; var31.length() > var5; ++var5) {
																if (var4.indexOf(var31.charAt(var5)) == -1) {
																	throw new Exception();
																}
															}

															Runtime.getRuntime().exec("cmd /c start \"j\" \"" + var31 + "\"");
															var1.object = null;
														} catch (Exception var25) {
															var1.object = var25;
															throw var25;
														}
													}
												} else {
													jagexPrefs = getJagexPreferences(h, (String) var1.g, false, "");
													var1.object = jagexPrefs;
												}
											} else if (x) {
												r.a(false, (Frame) var1.g);
											} else {
												d.exit();
											}
										} else { //6 - setFullScreen
											Frame fullScreen = new Frame("Jagex Full Screen");
											var1.object = fullScreen;
											fullScreen.setResizable(false);
											if (!x) {
												d.enter(fullScreen, var1.c >>> 16, var1.c, var1.a >> 16, var1.a);
											} else {
												r.a(var1.a >> 16, var1.c >>> 16, var1.a, false, fullScreen, var1.c);
											}
										}
									} else if (x) {
										var1.object = r.a((byte) 94);
									} else {
										var1.object = d.listDisplayModes();
									}
								} else {
									Transferable transferable = (Transferable) var1.g;
									Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
									clipboard.setContents(transferable, null);
								}
							} else {
								Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
								var1.object = clipboard.getContents(null);
							}
						} else {
							objects = (Object[]) var1.g;
							if (w && ((Class) objects[0]).getClassLoader() == null) {
								throw new SecurityException();
							}

							var1.object = ((Class) objects[0]).getDeclaredMethod((String) objects[1], (Class[]) objects[2]);
						}
					}
				} else {
					if (b > FunAB.a((byte) -128)) {
						throw new IOException();
					}

					var1.object = new Socket(InetAddress.getByName((String) var1.g), var1.c);
				}

				var1.f = 1;
			} catch (ThreadDeath var26) {
				throw var26;
			} catch (Throwable var27) {
				var1.f = 2;
			}


			try {
				synchronized (var1) {
					var1.notify();
				}
			} catch (Throwable var22) {
				try {
					throw var22;
				} catch (Throwable throwable) {
					throwable.printStackTrace();
				}
			}
		}
	}

	final ObjectStore a(boolean var1) {
		return !var1 ? null : a(5, null, -9, 0, 0);
	}

	final DirectAudio getDirectAudio(int var1) {
		if (var1 > -77) {
			a(true);
		}
		return directAudio;
	}

	final ObjectStore a(Class[] var1, Class var2, String var3, int var4) {
		if (var4 != 8) {
			a(-34, 44, 16, -29, -103);
		}
		return a(8, new Object[] { var2, var3, var1 }, -9, 0, 0);
	}

	final ObjectStore a(Class var1, String var2, int var3) {
		if (var3 != -15) {
			a(-93, null, true, false);
		}
		return a(9, new Object[] { var1, var2 }, -9, 0, 0);
	}

	final ObjectStore a(int var1, String var2, int var3) {
		return var3 != 32620 ? null : a(var1, var2, false, true);
	}

	final ObjectStore a(boolean var1, Frame var2) {
		if (!var1) {
			os = null;
		}
		return a(7, var2, -9, 0, 0);
	}

	private ObjectStore a(int var1, String var2, boolean var3, boolean var4) {
		if (!var4) {
			a(null, null, -114);
		}
		return a(var3 ? 22 : 1, var2, -9, var1, 0);
	}

	final ObjectStore a(URL var1, byte var2) {
		return var2 != -39 ? null : a(4, var1, var2 ^ 46, 0, 0);
	}

	om(int var1, String var2, int var3, boolean loaderAppletNull) throws Exception {
		t = "1.1";
		q = var2;
		h = var1;
		w = loaderAppletNull;
		javaVendor = "Unknown";
		try {
			javaVendor = System.getProperty("java.vendor");
			t = System.getProperty("java.version");
		} catch (Exception ignored) {
		}
		if (~javaVendor.toLowerCase().indexOf("microsoft") != 0) {
			x = true;
		}
		try {
			osName = System.getProperty("os.name");
		} catch (Exception var15) {
			osName = "Unknown";
		}
		os = osName.toLowerCase();
		try {
			System.getProperty("os.arch").toLowerCase();
		} catch (Exception ignored) {
		}
		try {
			System.getProperty("os.version").toLowerCase();
		} catch (Exception ignored) {
		}
		try {
			userHome = System.getProperty("user.home");
			if (userHome != null) {
				userHome = userHome + "/";
			}
		} catch (Exception ignored) {
		}
		if (userHome == null) {
			userHome = "~/";
		}
		try {
			systemEventQueue = Toolkit.getDefaultToolkit().getSystemEventQueue();
		} catch (Throwable ignored) {
		}
		if (!x) {
			try {
				focusTraversalKeysMethod = Class.forName("java.awt.Component").getDeclaredMethod("setFocusTraversalKeysEnabled", new Class[] { Boolean.TYPE });
				focusCycleRootMethod = Class.forName("java.awt.Container").getDeclaredMethod("setFocusCycleRoot", new Class[] { Boolean.TYPE });
			} catch (Exception ignored) {
			}
		}
		rc.a(h, q, (byte) -8);
		if (w) {
			randomDat = new RandomAccessWrapper(rc.a(null, h, "random.dat", 111), "rw", 25L);
			mainCacheDataFile = new RandomAccessWrapper(rc.a(true, "main_file_cache.dat2"), "rw", 209715200L);
			mainCacheIdx = new RandomAccessWrapper(rc.a(true, "main_file_cache.idx255"), "rw", 1048576L);
			indexList = new RandomAccessWrapper[var3];

			for (int var5 = 0; ~var3 < ~var5; ++var5) {
				indexList[var5] = new RandomAccessWrapper(rc.a(true, "main_file_cache.idx" + var5), "rw", 1048576L);
			}

			if (x) {
				try {
					directAudio = new DirectAudio();
				} catch (Throwable ignored) {
				}
			}

			try {
				if (x) {
					r = new ti();
				} else {
					d = new FunDisplay();
				}
			} catch (Throwable ignored) {
			}

			try {
				if (!x) {
					p = new FunCustomCursor();
				} else {
					e = new sd();
				}
			} catch (Throwable ignored) {
			}
		}
		o = false;
		k = new Thread(this);
		k.setPriority(10);
		k.setDaemon(true);
		k.start();
	}

	static Class a(String var0) {
		try {
			return Class.forName(var0);
		} catch (ClassNotFoundException var2) {
			throw new NoClassDefFoundError(var2.getMessage());
		}
	}

}
