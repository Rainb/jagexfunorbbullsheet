
final class eh extends mg {

   static String u = "Elapsed time";
   static String v = "Only show private chat from my friends and opponents";
   private String w = "";
   private boolean x = false;
   private FunM y;
   static String[] monthAbv = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
   static int[] A = ca.newEightIntArray((byte) 110);
   static FunImage machineGlass;
   static cl C;
   static String D = "Unfortunately there was a focus problem while setting fullscreen mode. You could try disabling any multiple monitor drivers or window enhancements, if you have any enabled.";
   static String E = "To report a player, click on the most suitable option from the Rules of Conduct. Please do not abuse this form.";
   static String F = "Sorry, you were removed from the game you were in. This can happen if you are disconnected for too long or if the server is updated.";


   final pf b(int var1, String var2) {
      try {
         if(y.b(127, var2) == tl.h) {
            return tl.h;
         } else {
            if(!var2.equals(w)) {
               hf var3 = ug.a(-1, var2);
               if(!var3.c((byte)-114)) {
                  return qf.a;
               }

				w = var2;
				x = var3.a((int)255);
            }

            if(var1 <= 122) {
               c((byte)77);
            }

            return !x ?tl.h:bh.a;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "eh.L(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   public static void f(int var0) {
      try {
         F = null;
         machineGlass = null;
         u = null;
         E = null;
         C = null;
         A = null;
         v = null;
         D = null;
         monthAbv = null;
         if(var0 > -85) {
            a(115, false);
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "eh.D(" + var0 + ')');
      }
   }

   static final FunImage[] a(int var0, int var1, int var2, int var3, int var4, int var5) {
      try {
         int[] var6 = cd.pixels;
         int var7 = cd.width;
         int var8 = cd.height;
         FunImage var9 = new FunImage(var0, -(var0 * 2) + var4);
         var9.f();
         cd.c(0, 0, var0, var4 - var0 * 2, var5, var1);
         FunImage var10 = new FunImage(var0, var0);
         if(var3 != -19038) {
            machineGlass = null;
         }

         var10.f();
         cd.d(0, 0, var0, var0, var5);
         FunImage var11 = new FunImage(16, var0);
         var11.f();
         cd.d(0, 0, 16, var0, var5);
         FunImage var12 = new FunImage(var0, var0);
         var12.f();
         cd.d(0, 0, var0, var0, var1);
         FunImage var13 = new FunImage(16, var0);
         var13.f();
         cd.d(0, 0, 16, var0, var1);
         FunImage var14 = null;
         if(~var2 < -1) {
            var14 = new FunImage(16, 16);
            var14.f();
            cd.d(0, 0, 16, 16, var2);
         }

         cd.a(var6, var7, var8);
         return new FunImage[]{var10, var11, var10, var9, var14, var9, var12, var13, var12};
      } catch (RuntimeException var15) {
         throw pl.a(var15, "eh.F(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

   final String a(int var1, String var2) {
      try {
         if(y.b(124, var2) == tl.h) {
            return y.a(var1, var2);
         } else {
            if(var1 != -1) {
               b(77, 44, 118);
            }

            return b(126, var2) == tl.h?cg.invalidEmail :aj.Ab;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "eh.B(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   eh(FunWB var1, FunWB var2) {
      super(var1);

      try {
		  y = new FunM(var1, var2);
      } catch (RuntimeException var4) {
         throw pl.a(var4, "eh.<init>(" + (var1 != null?"{...}":"null") + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   static final boolean a(char var0, int var1) {
      try {
         if(var1 != -91) {
            D = null;
         }

         return var0 >= 65 && ~var0 >= -91 || ~var0 <= -98 && ~var0 >= -123;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "eh.C(" + var0 + ',' + var1 + ')');
      }
   }

   static final boolean b(int var0, int var1, int var2) {
      try {
         if(FunPB.L == 13) {
            af.b(true);
            return true;
         } else {
            int var3 = 63 / ((-37 - var2) / 61);
            if(FunPB.L == 102) {
               kn.c.k(-118);
               return true;
            } else {
               return kn.c != null && kn.c.b(var1, var0, 1);
            }
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "eh.W(" + var0 + ',' + var1 + ',' + var2 + ')');
      }
   }

   static final void a(int var0, boolean var1) {
      int var3 = client.L;

      try {
         pn.a(0, 1, qg.d, var0, ek.v, var1, nc.d);
         int var2 = 0;
         if(var3 != 0) {
            h.h[var0 + var2] = var2++;
         }

         while(~var2 > ~ek.v) {
            h.h[var0 + var2] = var2++;
         }

         pn.a(var0, 1, oi.a, var0 - -var0, var0 + ek.v, false, hm.a);
         if(var0 < ek.v) {
            ek.v = var0;
         }
      } catch (RuntimeException var4) {
         throw pl.a(var4, "eh.AA(" + var0 + ',' + var1 + ')');
      }
   }

   static final FunImage[] a(byte var0, String var1, nf var2, String var3) {
      try {
         if(var0 != -89) {
            return null;
         } else {
            int var4 = var2.a((int)-28144, var3);
            int var5 = var2.a(var4, var1, true);
            return uj.a((byte)-95, var2, var4, var5);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "eh.E(" + var0 + ',' + (var1 != null?"{...}":"null") + ',' + (var2 != null?"{...}":"null") + ',' + (var3 != null?"{...}":"null") + ')');
      }
   }

   static final boolean c(byte var0) {
      try {
         int var1 = 81 % ((53 - var0) / 47);
         return mh.e == null? FunNodeList.f:true;
      } catch (RuntimeException var2) {
         throw pl.a(var2, "eh.BA(" + var0 + ')');
      }
   }

}
