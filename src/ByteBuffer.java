import java.awt.*;
import java.math.BigInteger;

class ByteBuffer extends FunNode {

	static String f = "NEW THEME<br>UNLOCKED!";
	static Frame g;
	static dd h = new dd(0, 2, 2, 1);
	int offset;
	byte[] buffer;
	static FunImage k;
	static String l = "The game options are not all set.";
	static String m = "Exploiting a bug";
	static od n;
	static String underTheSea = "Under the Sea";


	final void a(int var1, byte var2) {
		try {
			buffer[-2 + -var1 + offset] = (byte) (var1 >> 8);
			if (var2 < 21) {
				putJString(77, null);
			}

			buffer[-1 + offset - var1] = (byte) var1;
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.NA(" + var1 + ',' + var2 + ')');
		}
	}

	final void a(String var1, int var2) {
		try {
			int var3 = var1.indexOf((char) var2);
			if (var3 >= 0) {
				throw new IllegalArgumentException("NUL character at " + var3 + " - cannot pjstr2");
			} else {
				buffer[offset++] = 0;
				offset += hf.a(offset, var1, buffer, var1.length(), true, 0);
				buffer[offset++] = 0;
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.GA(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
		}
	}

	final int a(boolean var1) {
		try {
			if (!var1) {
				l = null;
			}

			int var2 = buffer[offset] & 255;
			return ~var2 <= -129 ? -49152 + j(1808469224) : -64 + getInt(false);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.OA(" + var1 + ')');
		}
	}

	final void putInt(int i, byte var2) {
		try {
			buffer[offset++] = (byte) (i >> 24);
			buffer[offset++] = (byte) (i >> 16);
			buffer[offset++] = (byte) (i >> 8);
			buffer[offset++] = (byte) i;
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.putInt(" + i + ')');
		}
	}

	public static void a() {
		try {
			underTheSea = null;
			l = null;
			k = null;
			g = null;
			m = null;
			f = null;
			h = null;
			n = null;
		} catch (RuntimeException var2) {
			throw pl.a(var2, "ByteBuffer.dispose()");
		}
	}

	final int a(boolean var1, int var2) {
		try {
			int var3 = fk.a(-29280, offset, var2, buffer);
			if (!var1) {
				return 55;
			} else {
				putInt(var3, (byte) -124);
				return var3;
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.IA(" + var1 + ',' + var2 + ')');
		}
	}

	final void putJString(int var1, String str) {
		try {
			int nulIndex = str.indexOf(0);
			if (nulIndex >= 0) {
				throw new IllegalArgumentException("NUL character at " + nulIndex + " - cannot pjstr");
			} else {
				offset += hf.a(offset, str, buffer, str.length(), true, 0);
				if (var1 != -1192815152) {
					b((byte) 6, -110);
				}

				buffer[offset++] = 0;
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.putJString(" + var1 + ',' + (str != null ? "{...}" : "null") + ')');
		}
	}

	final String getJString2(int requestedVer) {
		try {
			byte ver = buffer[offset++];
			if (ver != requestedVer) {
				throw new IllegalStateException("Bad version number in gjstr2");
			} else {
				int lastOffset = offset;

				while (~buffer[offset++] != -1) {
				}
				int var4 = offset - lastOffset - 1;
				return var4 == 0 ? "" : sh.a(buffer, var4, requestedVer + -81, lastOffset);
			}
		} catch (RuntimeException var5) {
			throw pl.a(var5, "ByteBuffer.getJString2(" + requestedVer + ')');
		}
	}

	final String getString(byte useless) {
		try {
			if (useless != -80) {
				return null;
			} else {
				int start = offset;

				while (buffer[offset++] != 0) {
				}

				int end = offset - (start + 1);
				return end == 0 ? "" : sh.a(buffer, end, -98, start);
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.EA(" + useless + ')');
		}
	}

	final void a(byte var1, int var2) {
		try {
			if (~var2 > -65 && ~var2 <= 63) {
				a(var2 - -64, 122);
			} else if (var2 < 16384 && var2 >= -16384) {
				b((byte) -104, var2 + '\uc000');
			} else {
				if (var1 != 110) {
					g = null;
				}

				throw new IllegalArgumentException();
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.HA(" + var1 + ',' + var2 + ')');
		}
	}

	final void b(byte var1, int var2) {
		try {
			buffer[offset++] = (byte) (var2 >> 8);
			buffer[offset++] = (byte) var2;
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.P(" + var1 + ',' + var2 + ')');
		}
	}

	final String d(int var1) {
		try {
			if (var1 > -111) {
				d(76);
			}

			if (buffer[offset] == 0) {
				++offset;
				return null;
			} else {
				return getString((byte) -80);
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.D(" + var1 + ')');
		}
	}

	final long e(int var1) {
		try {
			if (var1 != 16711680) {
				return -53L;
			} else {
				long var2 = 4294967295L & (long) getInt(255);
				long var4 = (long) getInt(255) & 4294967295L;
				return (var2 << 32) + var4;
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "ByteBuffer.JA(" + var1 + ')');
		}
	}

	final int f(int var1) {
		try {
			int var2 = buffer[offset] & 255;
			return ~var2 <= var1 ? j(1808469224) - '\u8000' : getInt(false);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.Q(" + var1 + ')');
		}
	}

	final void b(int var1, String str) {  //write username?
		int var10 = client.L;

		try {
			long hash = 0L;
			long subHash = 0L;
			if (var1 != 2098) {
				a(106, 91);
			}

			int len = str.length();
			int idx = 19;
			if (var10 == 0 && idx < 0) {
				a(hash, (byte) -109);
				a(subHash, (byte) 43);
			} else {
				do {
					hash *= 38L;
					if (~len < ~idx) {
						label77:
						{
							char c = str.charAt(idx);
							if (c < 65 || ~c < -91) {
								if (~c <= -98 && ~c >= -123) {
									hash += (long) (c + 2 - 97);
									if (var10 == 0) {
										break label77;
									}
								}

								if (c >= 48 && c <= 57) {
									hash += (long) (28 - -c + -48);
									if (var10 == 0) {
										break label77;
									}
								}

								++hash;
								if (var10 == 0) {
									break label77;
								}
							}

							hash += (long) (c + -63);
						}
					}

					if (~idx == -11) {
						subHash = hash;
						hash = 0L;
					}

					--idx;
				} while (idx >= 0);

				a(hash, (byte) -109);
				a(subHash, (byte) 43);
			}
		} catch (RuntimeException var11) {
			throw pl.a(var11, "ByteBuffer.A(" + var1 + ',' + (str != null ? "{...}" : "null") + ')');
		}
	}

	final void putLong(long var1, int var3) {
		try {
			buffer[offset++] = (byte) (int) (var1 >> 56);
			buffer[offset++] = (byte) (int) (var1 >> 48);
			buffer[offset++] = (byte) (int) (var1 >> 40);
			if (var3 != -268435456) {
				l = null;
			}

			buffer[offset++] = (byte) (int) (var1 >> 32);
			buffer[offset++] = (byte) (int) (var1 >> 24);
			buffer[offset++] = (byte) (int) (var1 >> 16);
			buffer[offset++] = (byte) (int) (var1 >> 8);
			buffer[offset++] = (byte) (int) var1;
		} catch (RuntimeException var5) {
			throw pl.a(var5, "ByteBuffer.C(" + var1 + ',' + var3 + ')');
		}
	}

	private void a(long var1, byte var3) {
		try {
			buffer[offset++] = (byte) (int) (var1 >> 48);
			buffer[offset++] = (byte) (int) (var1 >> 40);
			buffer[offset++] = (byte) (int) (var1 >> 32);
			buffer[offset++] = (byte) (int) (var1 >> 24);
			buffer[offset++] = (byte) (int) (var1 >> 16);
			buffer[offset++] = (byte) (int) (var1 >> 8);
			buffer[offset++] = (byte) (int) var1;
		} catch (RuntimeException var5) {
			throw pl.a(var5, "ByteBuffer.FA(" + var1 + ',' + var3 + ')');
		}
	}

	final int getInt(boolean var1) {
		try {
			return var1 ? -17 : buffer[offset++] & 255;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.PA(" + var1 + ')');
		}
	}

	final void b(boolean var1, int var2) {
		try {
			buffer[offset++] = (byte) (var2 >> 16);
			buffer[offset++] = (byte) (var2 >> 8);
			if (!var1) {
				a(false, 110);
			}

			buffer[offset++] = (byte) var2;
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.H(" + var1 + ',' + var2 + ')');
		}
	}

	final void a(int var1, int var2) {
		try {
			buffer[offset++] = (byte) var1;
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.J(" + var1 + ',' + var2 + ')');
		}
	}

	final int g(int var1) {
		try {
			offset += 3;
			if (var1 >= -114) {
				b(-21, null);
			}

			return ('\uff00' & buffer[-2 + offset] << 8) + ((255 & buffer[offset - 3]) << 16) + (buffer[offset + -1] & 255);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.AA(" + var1 + ')');
		}
	}

	final void c(int var1, byte var2) {
		int var4 = client.L;

		try {
			if (var4 != 0) {
				buffer[offset++] = 0;
			}

			while (~offset > ~var1) {
				buffer[offset++] = 0;
			}

			if (var2 != 0) {
				get(true);
			}
		} catch (RuntimeException var5) {
			throw pl.a(var5, "ByteBuffer.DA(" + var1 + ',' + var2 + ')');
		}
	}

	final void a(byte[] buf, int var2, int var3, int var4) {
		int var6 = client.L;

		try {
			int var5 = var3;
			if (var4 >= -71) {
				g(92);
				if (var6 == 0 && var3 >= var2 + var3) {
					return;
				}
			} else if (var3 >= var2 + var3) {
				return;
			}

			do {
				buf[var5] = buffer[offset++];
				++var5;
			} while (var5 < var2 + var3);

		} catch (RuntimeException var7) {
			throw pl.a(var7, "ByteBuffer.CA(" + (buf != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
		}
	}

	final void a(BigInteger var1, BigInteger var2, int var3) {
		try {
			int var4 = offset;
			offset = var3;
			byte[] buf = new byte[var4];
			a(buf, var4, 0, -119);
			BigInteger bigInt = new BigInteger(buf);
			BigInteger modPow = bigInt.modPow(var1, var2);
			byte[] var8 = modPow.toByteArray();
			offset = 0;
			b((byte) 110, var8.length);
			put(0, var8.length, var8, (byte) -114);
		} catch (RuntimeException var9) {
			throw pl.a(var9, "ByteBuffer.I(" + (var1 != null ? "{...}" : "null") + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
		}
	}

	final boolean h(int var1) {
		try {
			offset -= 4;
			int var2 = fk.a(-29280, offset, 0, buffer);
			if (var1 > -61) {
				k = null;
			}

			int var3 = getInt(255);
			return ~var3 == ~var2;
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.LA(" + var1 + ')');
		}
	}

	final int getInt(int var1) {
		try {
			if (var1 != 255) {
				return 105;
			} else {
				offset += 4;
				return (16711680 & buffer[offset - 3] << 16) + (-16777216 & buffer[-4 + offset] << 24) - (-((buffer[-2 + offset] & 255) << 8) + -(255 &
						buffer[-1 + offset]));
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.V(" + var1 + ')');
		}
	}

	final void a(byte var1, int var2, int[] var3, int var4) {
		int var13 = client.L;

		try {
			int var5 = offset;
			offset = var2;
			int var6 = (var4 - var2) / 8;
			if (var1 <= -105) {
				int var7 = 0;
				if (var13 == 0 && ~var7 <= ~var6) {
					offset = var5;
				} else {
					do {
						int var8 = getInt(255);
						int var9 = getInt(255);
						int var10 = -957401312;
						int var11 = -1640531527;
						int var12 = 32;
						if (var13 == 0 && ~var12-- >= -1) {
							offset -= 8;
							putInt(var8, (byte) 80);
							putInt(var9, (byte) -124);
							++var7;
						} else {
							do {
								var9 -= (var8 << 4 ^ var8 >>> 5) - -var8 ^ var3[var10 >>> 11 & 1543503875] + var10;
								var10 -= var11;
								var8 -= (var9 >>> 5 ^ var9 << 4) + var9 ^ var3[3 & var10] + var10;
							} while (~var12-- < -1);

							offset -= 8;
							putInt(var8, (byte) 80);
							putInt(var9, (byte) -124);
							++var7;
						}
					} while (~var7 > ~var6);

					offset = var5;
				}
			}
		} catch (RuntimeException var14) {
			throw pl.a(var14, "ByteBuffer.G(" + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ')');
		}
	}

	final void put(int off, int len, byte[] buf, byte useless) {
		int var6 = client.L;

		try {
			if (useless < -45) {
				int i = off;
				if (var6 != 0 || off - -len > off) {
					do {
						buffer[offset++] = buf[i];
						++i;
					} while (off - -len > i);

				}
			}
		} catch (RuntimeException var7) {
			throw pl.a(var7, "ByteBuffer.put(" + off + ',' + len + ',' + (buf != null ? "{...}" : "null") + ',' + useless + ')');
		}
	}

	final void a(int[] var1, byte var2) {
		int var11 = client.L;

		try {
			int var4 = offset / 8;
			offset = 0;
			int var5 = 0;
			if (var11 != 0 || ~var4 < ~var5) {
				do {
					int var6 = getInt(255);
					int var7 = getInt(255);
					int var8 = 0;
					int var9 = -1640531527;
					int var10 = 32;
					if (var11 == 0 && var10-- <= 0) {
						offset -= 8;
						putInt(var6, (byte) -125);
						putInt(var7, (byte) 7);
						++var5;
					} else {
						do {
							var6 += var7 + (var7 >>> 5 ^ var7 << 4) ^ var1[var8 & 3] + var8;
							var8 += var9;
							var7 += (var6 << 4 ^ var6 >>> 5) - -var6 ^ var8 + var1[(var8 & 6757) >>> 11];
						} while (var10-- > 0);

						offset -= 8;
						putInt(var6, (byte) -125);
						putInt(var7, (byte) 7);
						++var5;
					}
				} while (~var4 < ~var5);

			}
		} catch (RuntimeException var12) {
			throw pl.a(var12, "ByteBuffer.R(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
		}
	}

	final void b(int var1, int var2) {
		try {
			if (var1 >= 13) {
				if (var2 >= 0 && ~var2 > -129) {
					a(var2, 10);
				} else if (~var2 <= -1 && ~var2 > -32769) {
					b((byte) 98, '\u8000' + var2);
				} else {
					throw new IllegalArgumentException();
				}
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.B(" + var1 + ',' + var2 + ')');
		}
	}

	static void c(byte var0) {
	}

	final byte get(boolean var1) {
		try {
			return buffer[offset++];
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.get()");
		}
	}

	final int j(int var1) {
		try {
			offset += 2;
			return ((255 & buffer[offset - 2]) << 8) - -(255 & buffer[offset - 1]);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.L(" + var1 + ')');
		}
	}

	final void d(int var1, byte var2) {
		try {
			buffer[-var1 + offset - 1] = (byte) var1;
			if (var2 < 44) {
				buffer = null;
			}
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.K(" + var1 + ',' + var2 + ')');
		}
	}

	final int k(int var1) {
		int var4 = client.L;

		try {
			byte var2 = buffer[offset++];
			int var3 = 0;
			if (var4 == 0 && var2 >= 0) {
				if (var1 >= -114) {
					g(115);
				}

				return var3 | var2;
			} else {
				do {
					var3 = (var2 & 127 | var3) << 7;
					var2 = buffer[offset++];
				} while (var2 < 0);

				if (var1 >= -114) {
					g(115);
				}

				return var3 | var2;
			}
		} catch (RuntimeException var5) {
			throw pl.a(var5, "ByteBuffer.N(" + var1 + ')');
		}
	}

	static void a(jh var0, int var1, byte var2) {
		try {
			pf.d.a(94, var0);
			hj.a(var0, var1, 13250);
			int var3 = 89 / ((var2 - -55) / 62);
		} catch (RuntimeException var4) {
			throw pl.a(var4, "ByteBuffer.M(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ',' + var2 + ')');
		}
	}

	ByteBuffer(int var1) {
		try {
			offset = 0;
			buffer = hm.a(var1, (byte) 121);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.<init>(" + var1 + ')');
		}
	}

	ByteBuffer(byte[] buffer) {
		try {
			this.buffer = buffer;
			offset = 0;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "ByteBuffer.<init>(" + (buffer != null ? "{...}" : "null") + ')');
		}
	}

}
