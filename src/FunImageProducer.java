import java.awt.*;
import java.awt.image.*;

final class FunImageProducer extends jl implements ImageProducer, ImageObserver {

	static ln l = new ln();
	static int[] m;
	static String n = "Public chat is unavailable while setting up a rated game.";
	private ColorModel colorModel;
	static boolean p = true;
	private ImageConsumer imageConsumer;
	static int r = 0;
	static int s;


	private synchronized void setPixels(byte var1) {
		try {
			if (imageConsumer != null) {
				if (var1 != 121) {
					setPixels((byte) 69);
				}
				imageConsumer.setPixels(0, 0, super.width, super.height, colorModel, super.pixels, 0, super.width);
				imageConsumer.imageComplete(2);
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunImageProducer.H(" + var1 + ')');
		}
	}

	final void draw(int x, Graphics graphics, int var3, int y) {
		try {
			setPixels((byte) 121);
			graphics.drawImage(super.image, x, y, this);
			if (var3 != -16711898) {
				removeConsumer(null);
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "FunImageProducer.A(" + x + ',' + (graphics != null ? "{...}" : "null") + ',' + var3 + ',' + y + ')');
		}
	}

	static ck a(byte[] var0, g[] var1, byte var2) {
		int var9 = client.L;

		try {
			if (~var1.length != -257) {
				throw new IllegalArgumentException();
			} else {
				int[] var3 = new int[256];
				int[] var4 = new int[256];
				int[] var5 = new int[256];
				int[] var6 = new int[256];
				byte[][] var7 = new byte[256][];
				int var8 = 0;
				if (var9 == 0 && var8 >= 256) {
					if (var2 >= -16) {
						p = true;
					}

					return new ck(var0, var3, var4, var5, var6, var7);
				} else {
					do {
						var3[var8] = var1[var8].i;
						var4[var8] = var1[var8].h;
						var5[var8] = var1[var8].e;
						var6[var8] = var1[var8].g;
						var7[var8] = var1[var8].p;
						++var8;
					} while (var8 < 256);

					if (var2 >= -16) {
						p = true;
					}

					return new ck(var0, var3, var4, var5, var6, var7);
				}
			}
		} catch (RuntimeException var10) {
			throw pl.a(var10, "FunImageProducer.L(" + (var0 != null ? "{...}" : "null") + ',' + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
		}
	}

	public final synchronized void addConsumer(ImageConsumer imageConsumer) {
		try {
			this.imageConsumer = imageConsumer;
			imageConsumer.setDimensions(super.width, super.height);
			imageConsumer.setProperties(null);
			imageConsumer.setColorModel(colorModel);
			imageConsumer.setHints(14);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunImageProducer.addConsumer(" + (imageConsumer != null ? "{...}" : "null") + ')');
		}
	}

	public final void startProduction(ImageConsumer imageConsumer) {
		try {
			addConsumer(imageConsumer);
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunImageProducer.startProduction(" + (imageConsumer != null ? "{...}" : "null") + ')');
		}
	}

	public final synchronized boolean isConsumer(ImageConsumer consumer) {
		try {
			return imageConsumer == consumer;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunImageProducer.isConsumer(" + (consumer != null ? "{...}" : "null") + ')');
		}
	}

	static void a(float var0, String text, int var2, boolean var3) {
		try {
			if (FunKB.Jb == null) {
				FunKB.Jb = new uf(od.Qb, me.f);
				od.Qb.b((byte) 97, FunKB.Jb);
			}

			if (var2 != 0) {
				a('\uffa5', -40);
			}

			FunKB.Jb.a(10028, text, var0, var3);
			cd.a();
			FunO.a(true, 53);
		} catch (RuntimeException var5) {
			throw pl.a(var5, "FunImageProducer.I(" + var0 + ',' + (text != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ')');
		}
	}

	public final boolean imageUpdate(Image var1, int var2, int var3, int var4, int var5, int var6) {
		try {
			return true;
		} catch (RuntimeException var8) {
			throw pl.a(var8, "FunImageProducer.imageUpdate(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ')');
		}
	}

	public static void b(byte var0) {
		try {
			if (var0 < -86) {
				l = null;
				n = null;
				m = null;
			}
		} catch (RuntimeException var2) {
			throw pl.a(var2, "FunImageProducer.J(" + var0 + ')');
		}
	}

	final void init(Component component, int width, int height, byte var4) {
		try {
			super.pixels = new int[width * height + 1];
			super.height = height;
			super.width = width;
			colorModel = new DirectColorModel(32, 16711680, '\uff00', 255);
			super.image = component.createImage(this);
			setPixels((byte) 121);
			component.prepareImage(super.image, this);
			setPixels((byte) 121);
			component.prepareImage(super.image, this);
			if (var4 >= 42) {
				setPixels((byte) 121);
				component.prepareImage(super.image, this);
				a(true);
			}
		} catch (RuntimeException var6) {
			throw pl.a(var6, "FunImageProducer.B(" + (component != null ? "{...}" : "null") + ',' + width + ',' + height + ',' + var4 + ')');
		}
	}

	public final synchronized void removeConsumer(ImageConsumer consumer) {
		try {
			if (consumer == imageConsumer) {
				imageConsumer = null;
			}
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunImageProducer.removeConsumer(" + (consumer != null ? "{...}" : "null") + ')');
		}
	}

	static boolean a(char c, int var1) {
		try {
			if (var1 != 0) {
				p = true;
			}

			return c == 160 || ~c == -33 || ~c == -96 || c == 45;
		} catch (RuntimeException var3) {
			throw pl.a(var3, "FunImageProducer.K(" + c + ',' + var1 + ')');
		}
	}

	public final void requestTopDownLeftRightResend(ImageConsumer var1) {
	}

}
