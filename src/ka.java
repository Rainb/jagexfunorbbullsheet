import java.security.SecureRandom;
import java.util.Hashtable;

abstract class ka extends bn implements k {

   bn[] v;
   static SecureRandom w;
   static int x;
   static qk y;
   static String z = "Orb coins: ";


   final boolean a(bn var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      int var11 = client.L;

      try {
         if(this.v == null) {
            return false;
         } else {
            bn[] var8 = this.v;
            if(var6 != -16581) {
               w = null;
            }

            int var9 = 0;
            if(var11 == 0 && ~var9 <= ~var8.length) {
               return false;
            } else {
               do {
                  bn var10 = var8[var9];
                  if(var10 != null && var10.a(var1, var2, super.p + var3, var4, super.i + var5, -16581, var7)) {
                     return true;
                  }

                  ++var9;
               } while(~var9 > ~var8.length);

               return false;
            }
         }
      } catch (RuntimeException var12) {
         throw pl.a(var12, "ka.MA(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
      }
   }

   final int f(int var1) {
      int var7 = client.L;

      try {
         int var2 = 0;
         bn[] var3 = this.v;
         if(var1 != 13178) {
            z = null;
         }

         int var4 = 0;
         if(var7 == 0 && var3.length <= var4) {
            return var2;
         } else {
            do {
               bn var5 = var3[var4];
               if(var5 != null) {
                  int var6 = var5.f(13178);
                  if(~var6 < ~var2) {
                     var2 = var6;
                  }
               }

               ++var4;
            } while(var3.length > var4);

            return var2;
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "ka.QA(" + var1 + ')');
      }
   }

   static final void a(int var0, int var1, ac var2) {
      try {
         FunN var3 = bf.f;
         var3.a(var0, true);
         ++var3.offset;
         int var4 = var3.offset;
         var3.a((int)1, var1 ^ -6011);
         if(var1 != -6003) {
            a(false);
         }

         label25: {
            if(var2.i == null) {
               var3.a((int)0, (int)124);
               if(client.L == 0) {
                  break label25;
               }
            }

            var3.a(var2.i.length, (int)119);
            var3.put((int) 0, var2.i.length, var2.i, (byte) -107);
         }

         var3.a(true, var4);
         var3.offset -= 4;
         var2.f = var3.getInt(255);
         var3.d(-var4 + var3.offset, (byte)54);
      } catch (RuntimeException var5) {
         throw pl.a(var5, "ka.L(" + var0 + ',' + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   public static void a(boolean var0) {
      try {
         w = null;
         if(var0) {
            z = null;
            y = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "ka.B(" + var0 + ')');
      }
   }

   final boolean a(int var1, bn var2) {
      int var6 = client.L;

      try {
         if(var1 != -11129) {
            this.a(18, (bn)null, false);
         }

         bn[] var3 = this.v;
         int var4 = 0;
         if(var6 == 0 && ~var4 <= ~var3.length) {
            return false;
         } else {
            do {
               bn var5 = var3[var4];
               if(var5 != null && var5.a(-11129, var2)) {
                  return true;
               }

               ++var4;
            } while(~var4 > ~var3.length);

            return false;
         }
      } catch (RuntimeException var7) {
         throw pl.a(var7, "ka.PA(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   final void a(int var1, int var2, int var3, int var4, int var5, bn var6) {
      int var10 = client.L;

      try {
         if(this.v != null) {
            bn[] var7 = this.v;
            int var8 = 0;
            if(var10 == 0 && ~var7.length >= ~var8) {
               if(var2 < 70) {
                  this.a(44, 119, -78, 122, -120, (bn)null);
               }
            } else {
               do {
                  bn var9 = var7[var8];
                  if(var9 != null) {
                     var9.a(var1, 123, var3 - -super.i, super.p + var4, var5, var6);
                  }

                  ++var8;
               } while(~var7.length < ~var8);

               if(var2 < 70) {
                  this.a(44, 119, -78, 122, -120, (bn)null);
               }
            }
         }
      } catch (RuntimeException var11) {
         throw pl.a(var11, "ka.IA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + (var6 != null?"{...}":"null") + ')');
      }
   }

   final boolean a(int var1, byte var2, bn var3, char var4) {
      int var8 = client.L;

      try {
         if(this.v == null) {
            return false;
         } else {
            bn[] var5 = this.v;
            int var6 = 0;
            bn var7;
            if(var8 != 0) {
               var7 = var5[var6];
               if(var7 != null && var7.e(var2 ^ 2147483586) && var7.a(var1, (byte)61, var3, var4)) {
                  return true;
               }

               ++var6;
            }

            while(~var6 > ~var5.length) {
               var7 = var5[var6];
               if(var7 != null && var7.e(var2 ^ 2147483586) && var7.a(var1, (byte)61, var3, var4)) {
                  return true;
               }

               ++var6;
            }

            if(var2 != 61) {
               this.a(4, -12, 107, 80, 114, (bn)null);
            }

            if(~var1 == -81) {
               if(ed.e[81]) {
                  return this.a(false, var3);
               } else {
                  return this.a((byte)-113, var3);
               }
            } else {
               return false;
            }
         }
      } catch (RuntimeException var9) {
         throw pl.a(var9, "ka.E(" + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

   final StringBuffer a(int var1, Hashtable table, byte b, StringBuffer buffer) {
      try {
         if(b != 99) {
            this.a((bn)null, -60, -97, -120);
         }

         if(this.a(0, buffer, var1, table)) {
            this.b(31, buffer, var1, table);
            this.a((byte)-113, table, buffer, var1);
         }

         return buffer;
      } catch (RuntimeException var6) {
         throw pl.a(var6, "ka.GA(" + var1 + ',' + (table != null?"{...}":"null") + ',' + b + ',' + (buffer != null?"{...}":"null") + ')');
      }
   }

   private final void a(byte var1, Hashtable var2, StringBuffer var3, int var4) {
      int var10 = client.L;

      try {
         int var5 = 34 / ((var1 - -29) / 62);
         if(this.v != null) {
            bn[] var6 = this.v;
            int var7 = 0;
            if(var10 != 0 || ~var6.length < ~var7) {
               do {
                  bn var8 = var6[var7];
                  var3.append('\n');
                  int var9 = 0;
                  if(var10 != 0) {
                     var3.append(' ');
                     ++var9;
                  }

                  while(~var4 <= ~var9) {
                     var3.append(' ');
                     ++var9;
                  }

                  label38: {
                     if(var8 != null) {
                        var8.a(var4 + 1, var2, (byte)99, var3);
                        if(var10 == 0) {
                           break label38;
                        }
                     }

                     var3.append("null");
                  }

                  ++var7;
               } while(~var6.length < ~var7);

            }
         }
      } catch (RuntimeException var11) {
         throw pl.a(var11, "ka.K(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ')');
      }
   }

   private final boolean a(int var1, bn var2, boolean var3) {
      int var7 = client.L;

      try {
         if(this.v == null) {
            return false;
         } else if(var3) {
            return false;
         } else {
            int var4 = 0;
            if(var7 == 0 && ~this.v.length >= ~var4) {
               return false;
            } else {
               do {
                  bn var5 = this.v[var4];
                  if(var5 != null && (var5.e(Integer.MAX_VALUE) || var7 != 0)) {
                     var4 += var1;
                     if(var7 != 0 || this.v.length > var4) {
                        do {
                           bn var6 = this.v[var4];
                           if(var6 != null && var6.a(-11129, var2)) {
                              return true;
                           }

                           var4 += var1;
                        } while(this.v.length > var4);
                     }
                  }

                  ++var4;
               } while(~this.v.length < ~var4);

               return false;
            }
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "ka.J(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

   final void a(int var1, int var2, byte var3, int var4, int var5) {
      try {
         if(var3 > -54) {
            w = null;
         }

         super.a(var1, var2, (byte)-121, var4, var5);
         this.a(90);
      } catch (RuntimeException var7) {
         throw pl.a(var7, "ka.BA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

   void a(bn var1, int var2, int var3, int var4) {
      int var8 = client.L;

      try {
         super.a(var1, var2, -128, var4);
         if(this.v != null) {
            bn[] var5 = this.v;
            int var6 = 0;
            int var10;
            if(var8 == 0 && ~var6 <= ~var5.length) {
               var10 = -24 / ((40 - var3) / 38);
            } else {
               do {
                  bn var7 = var5[var6];
                  if(var7 != null) {
                     var7.a(var1, var2 + super.p, -9, var4 + super.i);
                  }

                  ++var6;
               } while(~var6 > ~var5.length);

               var10 = -24 / ((40 - var3) / 38);
            }
         }
      } catch (RuntimeException var9) {
         throw pl.a(var9, "ka.D(" + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   private final boolean a(boolean var1, bn var2) {
      try {
         return var1?false:this.a(1, var2, 0);
      } catch (RuntimeException var4) {
         throw pl.a(var4, "ka.G(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   ka(int var1, int var2, int var3, int var4, vg var5) {
      super(var1, var2, var3, var4, var5, (aa)null);
   }

   private final bn c(byte var1) {
      int var5 = client.L;

      try {
         if(this.v == null) {
            return null;
         } else {
            if(var1 > -95) {
               y = null;
            }

            bn[] var2 = this.v;
            int var3 = 0;
            if(var5 == 0 && ~var3 <= ~var2.length) {
               return null;
            } else {
               do {
                  bn var4 = var2[var3];
                  if(var4 != null && var4.e(Integer.MAX_VALUE)) {
                     return var4;
                  }

                  ++var3;
               } while(~var3 > ~var2.length);

               return null;
            }
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "ka.H(" + var1 + ')');
      }
   }

   abstract void a(int var1);

   final boolean e(int var1) {
      try {
         return var1 != Integer.MAX_VALUE?true:this.c((byte)-122) != null;
      } catch (RuntimeException var3) {
         throw pl.a(var3, "ka.DA(" + var1 + ')');
      }
   }

   private final boolean a(byte var1, bn var2) {
      try {
         int var3 = 18 % ((var1 - -32) / 45);
         return this.a(1, var2, false);
      } catch (RuntimeException var4) {
         throw pl.a(var4, "ka.F(" + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   final boolean a(int var1, int var2, int var3, int var4, int var5, int var6, bn var7) {
      int var11 = client.L;

      try {
         if(this.v == null) {
            return false;
         } else {
            bn[] var8 = this.v;
            if(var6 < 82) {
               this.a(53, -8, (byte)64, -83, 58);
            }

            int var9 = 0;
            if(var11 == 0 && ~var9 <= ~var8.length) {
               return false;
            } else {
               do {
                  bn var10 = var8[var9];
                  if(var10 != null && var10.e(Integer.MAX_VALUE) && var10.a(var1, var2, var3, var4, var5, 100, var7)) {
                     return true;
                  }

                  ++var9;
               } while(~var9 > ~var8.length);

               return false;
            }
         }
      } catch (RuntimeException var12) {
         throw pl.a(var12, "ka.FA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + (var7 != null?"{...}":"null") + ')');
      }
   }

   void a(int var1, int var2, int var3, int var4) {
      int var7 = client.L;

      try {
         if(var2 == var4 && super.renderer != null) {
            super.renderer.a(true, this, var3, true, var1);
         }

         if(this.v != null) {
            int var5 = this.v.length - 1;
            if(var7 != 0 || ~var5 <= -1) {
               do {
                  bn var6 = this.v[var5];
                  if(var6 != null) {
                     var6.a(var1 + super.p, 0, var3 + super.i, var4);
                  }

                  --var5;
               } while(~var5 <= -1);

            }
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "ka.AA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
      }
   }

   private final boolean a(int var1, bn var2, int var3) {
      int var7 = client.L;

      try {
         if(this.v == null) {
            return false;
         } else {
            int var4 = -1 + this.v.length;
            if(var7 == 0 && var4 < 0) {
               if(var3 != 0) {
                  a(8, 55, (ac)null);
               }

               return false;
            } else {
               do {
                  bn var5 = this.v[var4];
                  if(var5 != null && (var5.e(var3 + Integer.MAX_VALUE) || var7 != 0)) {
                     var4 -= var1;
                     if(var7 != 0 || var4 >= 0) {
                        do {
                           bn var6 = this.v[var4];
                           if(var6 != null && var6.a(-11129, var2)) {
                              return true;
                           }

                           var4 -= var1;
                        } while(var4 >= 0);
                     }
                  }

                  --var4;
               } while(var4 >= 0);

               if(var3 != 0) {
                  a(8, 55, (ac)null);
               }

               return false;
            }
         }
      } catch (RuntimeException var8) {
         throw pl.a(var8, "ka.A(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

   final String d(int var1) {
      int var6 = client.L;

      try {
         if(this.v == null) {
            return null;
         } else if(var1 != -27681) {
            return null;
         } else {
            bn[] var2 = this.v;
            int var3 = 0;
            if(var6 == 0 && ~var3 <= ~var2.length) {
               return null;
            } else {
               do {
                  bn var4 = var2[var3];
                  if(var4 != null) {
                     String var5 = var4.d(-27681);
                     if(var5 != null) {
                        return var5;
                     }
                  }

                  ++var3;
               } while(~var3 > ~var2.length);

               return null;
            }
         }
      } catch (RuntimeException var7) {
         throw pl.a(var7, "ka.KA(" + var1 + ')');
      }
   }

   final void c(boolean var1) {
      int var5 = client.L;

      try {
         if(!var1) {
            bn[] var2 = this.v;
            int var3 = 0;
            if(var5 != 0 || ~var3 > ~var2.length) {
               do {
                  bn var4 = var2[var3];
                  if(var4 != null) {
                     var4.c(false);
                  }

                  ++var3;
               } while(~var3 > ~var2.length);

            }
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "ka.NA(" + var1 + ')');
      }
   }

}
