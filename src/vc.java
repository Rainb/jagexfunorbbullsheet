import java.io.IOException;

final class vc {

   static FunHB a;
   static boolean[] b;
   static dd c = new dd(6, 0, 4, 2);
   static String createPasswordHint = "Passwords must be between 5 and 20 letters and numbers";
   static FunCB e;


   static final void a(int var0, int var1) {
      try {
         ae.f = lc.b[var1];
         if(var0 == -24531) {
            ng.G = ec.waitingForTranslation[var1];
            qi.c = ah.loadingText[var1];
         }
      } catch (RuntimeException var3) {
         throw pl.a(var3, "vc.E(" + var0 + ',' + var1 + ')');
      }
   }

   static final String a(int var0, long var1) {
      int var11 = client.L;

      try {
         if(var1 > 0L && var1 < 6582952005840035281L) {
            if(var1 % 37L == 0L) {
               return null;
            } else if(var0 != -22221) {
               return null;
            } else {
               int var3 = 0;
               long var4 = var1;
               if(var11 != 0 || var1 != 0L) {
                  do {
                     var4 /= 37L;
                     ++var3;
                  } while(var4 != 0L);
               }

               StringBuffer var6 = new StringBuffer(var3);
               if(var11 == 0 && ~var1 == -1L) {
                  var6.reverse();
                  var6.setCharAt(0, Character.toUpperCase(var6.charAt(0)));
                  return var6.toString();
               } else {
                  do {
                     long var7 = var1;
                     var1 /= 37L;
                     char var9 = AbstractByteBuffer.g[(int)(-(37L * var1) + var7)];
                     if(var9 == 95) {
                        int var10 = -1 + var6.length();
                        var6.setCharAt(var10, Character.toUpperCase(var6.charAt(var10)));
                        var9 = 160;
                     }

                     var6.append(var9);
                  } while(~var1 != -1L);

                  var6.reverse();
                  var6.setCharAt(0, Character.toUpperCase(var6.charAt(0)));
                  return var6.toString();
               }
            }
         } else {
            return null;
         }
      } catch (RuntimeException var12) {
         throw pl.a(var12, "vc.A(" + var0 + ',' + var1 + ')');
      }
   }

   public static void a(boolean var0) {
      try {
         createPasswordHint = null;
         if(var0) {
            e = null;
            a = null;
            c = null;
            b = null;
         }
      } catch (RuntimeException var2) {
         throw pl.a(var2, "vc.D(" + var0 + ')');
      }
   }

   static final void b(boolean var0) {
      try {
         ul.o = new ph(0L, (ph)null);
         if(ai.d) {
            ul.o.a(14939, ie.c);
         }

         ul.o.a(14939, FunTB.Jb);
         wm.a = new ml(FunIOException.lobbyText, ul.o);
         wc.e = new ph(0L, (ph)null);
         wc.e.a(14939, (ph)wm.a.a);
         if(!var0) {
            b(false);
         }

         wc.e.a(14939, ae.d);
         uf.o(-14402);
      } catch (RuntimeException var2) {
         throw pl.a(var2, "vc.B(" + var0 + ')');
      }
   }

   static final void a(ByteBuffer var0, boolean var1) {
      int var4 = client.L;

      try {
         if(var1) {
            byte[] var2 = new byte[24];
            if(jg.m != null) {
               int var3;
               try {
                  jg.m.setOffset(0L, (byte) 95);
                  jg.m.a(var2, false);
                  var3 = 0;
                  if(var4 != 0 || ~var3 > -25) {
                     while(var2[var3] == 0 || var4 != 0) {
                        ++var3;
                        if(~var3 <= -25) {
                           break;
                        }
                     }
                  }

                  if(var3 >= 24) {
                     throw new IOException();
                  }
               } catch (Exception var5) {
                  var3 = 0;
                  if(var4 != 0 || var3 < 24) {
                     do {
                        var2[var3] = -1;
                        ++var3;
                     } while(var3 < 24);
                  }
               }
            }

            var0.put((int) 0, 24, var2, (byte) -70);
         }
      } catch (RuntimeException var6) {
         throw pl.a(var6, "vc.C(" + (var0 != null?"{...}":"null") + ',' + var1 + ')');
      }
   }

}
