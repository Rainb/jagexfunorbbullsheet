import java.awt.*;

public final class FunDisplay {

	private GraphicsDevice graphicsDevice;
	private DisplayMode displayMode;


	public final int[] listDisplayModes() {
		DisplayMode[] var1 = graphicsDevice.getDisplayModes();
		int[] var2 = new int[var1.length << 2];
		for (int var3 = 0; ~var1.length < ~var3; ++var3) {
			var2[var3 << 2] = var1[var3].getWidth();
			var2[(var3 << 2) - -1] = var1[var3].getHeight();
			var2[(var3 << 2) - -2] = var1[var3].getBitDepth();
			var2[(var3 << 2) - -3] = var1[var3].getRefreshRate();
		}
		return var2;
	}

	public final void enter(Frame frame, int var2, int var3, int var4, int var5) {
		displayMode = graphicsDevice.getDisplayMode();
		if (displayMode == null) {
			throw new NullPointerException();
		} else {
			frame.setUndecorated(true);
			frame.enableInputMethods(false);
			setFullScreenWindow(frame, -109);
			if (~var5 == -1) {
				int refreshRate = displayMode.getRefreshRate();
				DisplayMode[] displayModes = graphicsDevice.getDisplayModes();
				boolean var8 = false;

				for (DisplayMode mode : displayModes) {
					if (mode.getWidth() == var2 && var3 == mode.getHeight() && mode.getBitDepth() == var4) {
						int var10 = mode.getRefreshRate();
						if (!var8 || ~Math.abs(-refreshRate + var10) > ~Math.abs(-refreshRate + var5)) {
							var5 = var10;
							var8 = true;
						}
					}
				}

				if (!var8) {
					var5 = refreshRate;
				}
			}

			graphicsDevice.setDisplayMode(new DisplayMode(var2, var3, var4, var5));
		}
	}

	private void setFullScreenWindow(Frame frame, int var2) {
		if (var2 <= -106) {
			graphicsDevice.setFullScreenWindow(frame);
		}
	}

	public final void exit() {
		if (displayMode != null) {
			graphicsDevice.setDisplayMode(displayMode);
			if (!graphicsDevice.getDisplayMode().equals(displayMode)) {
				throw new RuntimeException("Did not return to correct resolution!");
			}

			displayMode = null;
		}
		setFullScreenWindow(null, -126);
	}

	public FunDisplay() throws Exception {
		GraphicsEnvironment var1 = GraphicsEnvironment.getLocalGraphicsEnvironment();
		graphicsDevice = var1.getDefaultScreenDevice();
		if (!graphicsDevice.isFullScreenSupported()) {
			for (GraphicsDevice device : var1.getScreenDevices()) {
				if (null != device && device.isFullScreenSupported()) {
					graphicsDevice = device;
					return;
				}
			}

			throw new Exception();
		}
	}
}
